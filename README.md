# More information on the [VR2Normal site](https://vongoob9.gitlab.io/vr2normal).

## VR2Normal

This program allows you to convert a 3D VR video into a normal video, using a virtual camera to select the most relevant parts of the video at different times. It also allows you to create screenshots and small webp and gif animations.

The program is basically a GUI for ffmpeg using the v360 and sendcmd plugins. It generates the ffmpeg command and the sendcmd file needed to convert the video.

## Screenshot

![Cover](Screenshots/Cover.jpg "Cover screenshot")

## User Manual

[All program options are explained in the user manual](UserManual.md)

## Step by step howto

[How to re-encode virtual reality videos to normal using VR2Normal program](StepByStep.md)

## Download

The source code, the binaries for windows, linux and macOS can be downloaded from the [Releases download page](https://gitlab.com/vongooB9/vr2normal/-/releases).

## Requirements

This program needs at least version 5 of ffmpeg to work. It is not necessary to install ffmpeg on the system. If your linux distribution does not have version 5 available, you can download it from the following pages, uncompress it and then configure it in the program.

- [For linux https://johnvansickle.com/ffmpeg/](https://johnvansickle.com/ffmpeg/)
- [For windows https://www.gyan.dev/ffmpeg/builds/](https://www.gyan.dev/ffmpeg/builds/)
- [For windows and linux https://github.com/BtbN/FFmpeg-Builds/releases](https://github.com/BtbN/FFmpeg-Builds/releases)
- [For macOS https://evermeet.cx/ffmpeg/](https://evermeet.cx/ffmpeg/)

### Windows binaries

Only 64bit version is available. This is because I could not find 32bit ffmpeg binaries. All other libraries are included in the zip file.

### Debian/Ubuntu deb package

The deb package has the following dependencies ffmpeg >= 5.0.0, libqt5core5a >= 5.15.0, libqt5gui5 >= 5.15.0

### Compiling

The compilation process is the same as this other program I made. Obviously changing the repositories and names.

[Compilation process on linux](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/blob/main/Manuals/compile_linux.md)

[Compilation process on windows](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/blob/main/Manuals/compile_windows.md)



