%
% Written by vongooB9 <vongooB9@protonmail.com>
%

# How to re-encode virtual reality videos to normal using VR2Normal program

In this document I will explain the steps to use VR2Normal correctly, the basic steps as well as some important recommendations, but it is not going to be an exhaustive explanation of all the options (There is a [complete manual](UserManual.md) for that) nor will I explain how to install ffmpeg. On the [VR2Normal website](https://vongoob9.gitlab.io/vr2normal/) are the instructions to download and install the application on the different platforms.

## Configure the application

When the program opens and is not configured, the **Config tab** is displayed automatically.

![Config](Screenshots/StepByStepConfig.png "Config")

The most important thing is to configure the path to ffmpeg, on linux if ffmpeg is installed on the system the default value "ffpmeg" should work. On windows it is safest to specify the full path to "ffmpeg.exe". If ffprobe and ffplay are located in the same folder as ffmpeg they are automatically configured when ffmpeg is selected. ffplay is the video player that is included in the ffmpeg package. It is only mandatory to configure ffmpeg but ffprobe is highly recommended. The video player and image viewer, only used to open previews and screenshots, are optional. The other options have defaults that are safe to use on all types of hardware, but possibly not the best.

When configured, click the Save Config button.

### OPTIONAL: Recommended configuration

This is an example of a recommended configuration, somewhat closer to what I use.

![Config Options](Screenshots/StepByStepConfigOptions.png "Config Options")
 
There is a detailed explanation of all options in the [user manual](UserManual.md#config). The options that have the most impact on performance are cache and hardware decoding. 

## Create a new camera movement file

Go to the **Movements** tab and click on the **New** button. Select the path and the name of the new file and accept.

![New](Screenshots/StepByStepNew.png "New")

After accepting, the tab will automatically change to Input, here you have to set the source VR file and its options.

## Configure an input video file

Now you have to configure the video file to be converted, to do this go to the **Input** tab and click on the **Select** button aside of **Video File** field.

![Select Video File](Screenshots/StepByStepSelectVideoFile.png "Select Video file")

The small file is to use a version with less quality but faster previews, it can be another lower quality version of the same video or you can generate it yourself as explained in the optional steps below, it has to be compatible with the original. It is optional but highly recommended for working with the highest resolution videos 6K, 8K...

If the input file name contains the correct wildcards to indicate the format (*mkx200*, *fisheye190*, *360_sbs*, etc...), the format, mode and FOV are automatically set. If ffprobe is configured, the FPS, Video Resolution and Video Length parameters are configured automatically, if not, we have to configure them manually.

To check that the format options are correct, we can use the Camera Editor tab to preview the current settings by moving the time to a part of the video where the image is clearly visible.

![Video File](Screenshots/StepByStepVideoFile.jpg "Video file")

### OPTIONAL: Check if small file is compatible

When the small file is set up a new tab "Compare Small" appears. This tab takes screenshots, distributed over the entire duration of the video, of the two files. It presents them side by side so that you can see if they are the same, which means that they are compatible. When the small file is set up a new tab "Compare Small" appears. This tab takes screenshots, distributed over the entire duration of the video, of the two files. It presents them side by side so that you can see if they are the same, which means that they are compatible. In the [user manual](UserManual.md#compare-small) is the complete explanation of all options.

![Compare Small](Screenshots/StepByStepCompareSmall.jpg "Compare Small")

### OPTIONAL: Generate small file and search for fade to black transitions

The small file can be generated in the "Input processing" tab, at the same time scanning the video for black transitions to add the corresponding lines of camera movements.

![Input processing](Screenshots/StepByStepInputProcessing.png "Input processing")

This process, depending on the hardware, can take quite a long time, so it is recommended, if necessary, do both tasks at the same time.

All you have to do is activate the processes and set their options, which as always are all explained in the [user manual](UserManual.md#input-processing). Then press the **Start** button. A progress bar will appear at the bottom.

When the small file is finished it will be set automatically and if the option to search for black transitions is enabled, the text below the buttons will be filled with the black frames it found. Click the **Add transitions** button to add the lines corresponding to the camera movements.

![Add Transitions](Screenshots/StepByStepAddTransitions.png "Add Transitions")

## Add the first line to the list of camera movements

Now we have to start working on telling the program how to position the camera at each moment of the video. Go to the **Movements** and **Camera Editor** tabs.

Click on the **Add** button. As there is no line yet, 2 lines will be created automatically, the first one with time 00:00:00.000 and the last one with the total time of the input file. These lines are mandatory and removing them or modifying their times may cause the program to fail. All other lines are to be added in order of time between these 2 lines. Maybe if you did the optional step of searching the transitions to black, there are already lines created, but it is important to explain the importance of the first and last line.

If we are at the beginning of the video only these automatic lines are added, but if not, a line with the current camera position is also added between them.

![New Line](Screenshots/StepByStepNewLine.png "New line")

## Gradually advance the video and add lines of movements

Now we have to advance the video from the beginning and move the camera and add lines until the video is finished.
To move the time and camera we can use the buttons on the **Camera Editor** tab or use the keyboard shortcuts, I recommend using the keys, it is faster. You can see the complete list of keys, and change them if you want, in the **Shortcuts** tab and in the [complete manual](UserManual.md#camera-editor). To describe it simply, the camera is controlled with "wasd", the time with the arrow keys and adding a new line with "Insert".

I advance the video and when I see that what I want to see is out of shot, I go back to where it is still completely visible using the second by second movement, add a new line, advance a little and place the camera until it is as I want, and then I advance again faster and until the shot changes and repeat the process.

In this part is where no manual can tell you what to do, point the camera where you want to look, move it as much as necessary, and frame the image as you think is better. This is the most laborious part of the process and it depends on the creativity of each person to place the camera in the best possible way.

This is what a finished video would look like

![All Lines](Screenshots/StepByStepAllLines.jpg "All lines")

### Changing eye

We must be careful if we decide to change eyes in the middle of the video. Because changing the eye always causes an abrupt jump in the image. It is better to take advantage of the transitions to black and make the eye change at that moment. Or do it intentionally in an abrupt change.

The program marks the cell text in red when it detects that there is an unintentional abrupt change. In other words, a change of eye when the transition time is different from 0. But this is only a visual warning, it does not cause an error or affect operation.

## Correct framing

The program has lines to help you frame the image better. you can activate them in the option "Show framing lines" in Config tab. These lines show the corners of a square and horizontal and vertical lines centered on the edges. The corners of the square are for ease of use the [Rule of thirds](https://en.wikipedia.org/wiki/Rule_of_thirds) and the lines at the edges point to the center of the composition. They can also be used as a guide to align the horizon.

### FOV

The program does not have any limitations when using the FOV, but it is not recommended to use either too low or too high values. With very low values, the problem is the loss of image quality, it is like doing digital zoom on a camera or enlarging an image by rescaling it, it looks bigger but has no more detail. I do not recommend going below 90 degrees of diagonal FOV.

With very high values the problem is very different. Normal cameras use [rectilinear lenses](https://en.wikipedia.org/wiki/Rectilinear_lens). This program converts images recorded with wide-angle lenses to what they would look like with a rectilinear lens. So it has the same problems as this type of lens in reality. 

    "At particularly wide angles, however, the rectilinear perspective will cause objects to appear increasingly stretched and enlarged as they near the edge of the frame"
    
To avoid this problem, I recommend not to go above 105 degrees diagonal FOV. The program turns the FOV orange or red when it detects a value that is too high. But it is only a visual warning.

### Roll

Be careful when the VR camera is not aligned with the horizon. Because a lateral movement can also imply a rotational movement, you must also adjust the roll option to correct it.

## Previews

As the on-screen preview cannot show the transitions, to see if they look good, we have the **Clip Preview** tab. From it we can create short videos of a few seconds where we can see the camera transitions of the selected line in the **Movements** tab.

First we have to select the folder where the previews will be generated by clicking the select button next to Preview Dir. We can also change the other options but the default ones should work fine.

To generate the previews of the transitions, we have to select a line and we can directly use the **Preview button** in the **Movements** tab or the **Preview Transition** button in the **Previews** tab. To use the **Movements** tab button it is not necessary to have the **Previews** tab selected.

To generate previews of a portion of the video we have to fill in the Fragment Start and End fields and press the **Preview Fragment** button.

If the **Run previews automaticaly** option is not checked in the **Config** tab the **Preview Transition** and **Preview Fragment** buttons just generate the command, you have to press the **Run** button to start it. If we have configured a video player and checked the corresponding option, it will be played automatically after processing.

![Preview](Screenshots/StepByStepPreview.png "Preview")

You can also create a complete preview in the **Comp. Preview** tab. Just select where to save the file in **Preview File** and press the **Generate ffmpeg Command** and **Run** buttons if the automatic option is not checked.

## OPTIONAL: Image Filters

Since version 2.0 the program has simple filters to manipulate the image (such as exposure, brightness, contrast, saturation, etc...) over time. We can add filters to the lines of camera movements so that from that point it adjusts the image as we want, it also works using the transitions to avoid abrupt changes.

I recommend configuring all camera movements before adding the filters, because these can only be created from the movement lines already created.

To create a filter you have to select the line from which you want to apply it and press the **Add Filters** button. When you do it the tab will change to the **Filters** tab and you will have a new line in the list below with the time of the selected motion line. Now we can activate and configure the filters we are interested in. The preview is updated automatically when touching any filter parameter, to better appreciate the changes. At the bottom, there is the **Compare** button, it applies the filters only to half of the image, so you can compare how it looks with and without the filter.

![Filters](Screenshots/StepByStepFilters.jpg "Filters")

Keep in mind that the preview, due to performance issues, is not a accurate representation, only an approximation, the final adjustment is recommended to do it by creating **Screenshots in PNG** format or using the *Clip Preview* tab using the same codec as in the output. Because the codec can influence the color processing and differences may appear between the different codecs.

## OPTIONAL: Choosing the correct output resolution

*Since version 2.0 the program is able to automatically calculate the recommended resolution based on the resolution of the input and the average FOV used. This calculation is made by clicking the Calculate button or by generating the ffmpeg command in the "Convert" tab. But it can be configured manually*

![Calculate Resolution](Screenshots/StepByStepUnsetResolution.png "Calculate Resolution")

*That's why this step is optional, even so it is not superfluous to understand how this calculation is done. The explanation is as follows.*

The output resolution depends mainly on 2 factors, the resolution of the input video and the viewing angle of the camera we are using at any given moment. I will not take into account the actual quality of the input video because it is very subjective, but obviously it also influences. There are many VR videos that do not have the quality you would expect from their resolution.

The program has an option that shows the approximate estimated resolution of the current camera preview, it is in the Config tab "Show Estimated Resolution". It is a help to decide the most suitable resolution for the converted video. To calculate the estimated resolution it uses the aspect ratio of the preview in the Config tab. If you want to use another aspect ratio you must change the resolution of the preview before you start adjusting the camera, otherwise this preview is wrong and the image in the final result will be badly framed.

Personally, I always use standard resolutions with 16/9 aspect ratio (1920x1080, 1280x720, 960x540...). For example, if the estimated resolution is 1600x900 for most of the video, I will convert it to 1920x1080, but if it is approximately 1350x760, I will convert it to 1280x720.

Another method to check which resolution is the most appropriate is to use the screenshots tab and take several screenshots at different resolutions of the same image and then compare them visually. It is a somewhat subjective method and you have to be careful that the rescaling methods when viewing the images do not influence the result.

As a general rule and using a FOV between 90-110 these would be the recommended resolutions for each input resolution.

- **4K:** 960x540
- **5K:** Bad 1280x720
- **6K:** Good 1280x720 or bad 1920x1080
- **7K:** 1920x1080
- **8K:** Good 1920x1080

By bad I mean that even if that resolution is used, the level of detail will not be similar to a video that is actually at that resolution, but it will have some more detail than the previous resolution. And by good I mean that it will have a good level of detail at that resolution.

Also note that not all videos that claim to be, for example 6K, do not have the same resolution, some are closer to 5K. There are also videos in different formats such as fisheye and with 190 and 200 degrees of field of view. In these cases the output resolution should be lower, for example 1280x720 in 6K video.

But these rules are only valid if the video has the quality that corresponds to its resolution, there are videos that do not comply, usually claiming to be remastered. But in reality they are simply upscaled, and often have no more detail than the originals.

## Execute the conversion process

Once we are satisfied with our work moving the camera we have to go to the **Convert** tab, configure the output video options, generate the command and run it on our own or press the **Run** button.
When a process is executed with the Run button, a bar appears at the bottom with the process data, a progress bar and a cancel button.

![Processing](Screenshots/StepByStepProcessing.png "Processing")

When finished you can check the output file and see if everything is correct.

### OPTIONAL: Split the conversion process

Since version 3.0 of VR2Normal it is possible to split the conversion process into smaller independent pieces, this allows to pause and resume processing, modify the camera without having to process all over again and process different pieces on different computers to speed up the processing.

As in the normal process the first thing to do is to configure the output options, this is done in the same way in the Convert tab, but this time without pressing the Generate command and Run button. Switch to the **Split tab**. Check the Convert tab under **Split Output Options**, the other options should be fine by now.

![Processing](Screenshots/StepByStepSplitOptions.png "Split Options")

And click the **Generate Splits** button. A progress bar will appear at the bottom, depending on the input file it may take some time because it is looking for the safe split points. This will fill the table with all the time data and file names.

![Processing](Screenshots/StepByStepSplitTable.png "Split Table")

Once finished looking for all the split points, to start processing press the **Run** button, this button stays pressed until it finishes with all the pieces or until it is pressed again, when doing that stops the process, but let finish the piece currently processing. can close the program, turn off the computer or do anything else, to start again simply open again VR2Normal and press **Run** again.

![Processing](Screenshots/StepByStepSplitProgress.png "Split Progress")

When finished the **State** column of all the lines must be set to **Finished** in green, click the **Recheck State** button to update it, then click the **Join** button. It will launch a process that will join all the pieces with the audio generating the output file configured in the **Convert** tab.

![Processing](Screenshots/StepByStepSplitJoinProgress.png "Split Join")

If after finishing everything you notice an error in the camera position you can correct it and click the **Generate Splits** button again. This will generate new file names for the pieces affected by the change leaving the others as they are. Now you can press **Run** again to process only the changed pieces and **Join** to generate the output file again. This way you can correct errors without having to reprocess everything again.

# BUGS
You can report bugs or problems on the [gitlab page](https://gitlab.com/vongooB9/vr2normal/-/issues)

# COPYRIGHT
Copyright © vongooB9.

License GPLv3: GNU GPL version 3 [https://gnu.org/licenses/gpl.html](https://gnu.org/licenses/gpl.html). This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.


---
header-includes: |
  <style>
  body {max-width: 90%;}
  figcaption {display: none;}
  figure {text-align: center;}
  a, a:visited {color: #1a1aaa;}
  @media (prefers-color-scheme: dark) {
    html {background: black; color: white;}
    a, a:visited {color: #AAF;}
  }
  </style>
---
