%
% Written by vongooB9 <vongooB9@protonmail.com>
%

# VR2Normal User Manual

Installation instructions can be found on the [VR2Normal website](https://vongoob9.gitlab.io/vr2normal/) and the [Step by step howto](StepByStep.md).

## General Organization

The user interface of the application is divided into 2 large areas, each with its corresponding tabs. This is in order to be able to combine several of the tabs that need to be used together.

## Left Tabs

### Camera Editor

![Camera Editor](Screenshots/TabCameraEditor.jpg "Camera Editor")

This tab is the one that allows you to move the camera to add lines to the list of movements. It is intended to be used side by side with the [Movements](#movements) and [Input](#input) tabs. It is composed of 2 images and 2 control bars. The image at the top is a thumbnail of what the raw image we are adjusting looks like. The image below is the preview with all the options applied, it is a thumbnail of what you would see with the current camera options.

When a line is selected in the [Movements tab](#movements), changing the values updates the data for that line.

#### Time navigation bar

![Time Navigation Bar](Screenshots/TimeNavigationBar.png "Time navigation bar")

The buttons and editors bar just below the first image is in charge of moving the time of the video.

The editor in the middle shows which moment of the video we are watching in the preview images. We can edit it and go to any moment of the video. The buttons around it are shortcuts for easy movement.

- **-60s**: Set back 1 minute the time, default shortcut *Ctrl+Up*
- **-10s**: Set back 10 seconds the time, default shortcut *Up*
- **-1s**: Set back 1 second the time, default shortcut *Left*
- **-1f**: Set back the equivalent time of 1 frame, default shortcut *Ctrl+Left*
- **+1f**: Forward the time equivalent to 1 frame, default shortcut *Ctrl+Right*
- **+1s**: Forward 1 second the time, default shortcut *Right*
- **+10s**: Forward 10 seconds the time, default shortcut *Down*
- **+60s**: Forward 1 minute the time, default shortcut *Ctrl+Down*

For a better performance of the cache except for the frame by frame movement all the rest round to whole seconds and to values ending in 0. For example if we are at 00:01:05.500 the buttons would work as follows:

- ***-1s***: 00:01:05.000
- ***-10s***: 00:01:00.000
- ***-60s***: 00:01:00.000
- ***+1s***: 00:01:06.000
- ***+10s***: 00:01:10.000
- ***+60s***: 00:02:00.000

Frame by frame movement is not precise, it depends on the frames per second setting of the video.

#### Camera motion controls

![Camera Motion Controls](Screenshots/CameraMotionControls.png "Camera motion controls")

The controls at the bottom are all necessary to move the camera and add movements to the list.

- **Pitch**
    - Increasing its value moves the camera upward, default shortcut *W*
    - Reducing its value moves the camera downward, default shortcut *S*
- **Yaw**
    - Increasing its value moves the camera to the right, default shortcut *D*
    - Reducing its value moves the camera to the left, default shortcut *A*
- **Roll**
    - Increase its value rotates the camera counterclockwise, default shortcut *X*
    - Reducing Increase its value rotates the camera clockwise, default shortcut *C*
- **Fov**: Diagonal field of view of the virtual camera. It turns orange when the angle is wide and red when it exceeds what can be done with a normal camera.
    - Increasing its value reduces the camera zoom, default shortcut *Q*
    - Reducing its value increases the camera zoom, default shortcut *E*
- **Trans.**: Time in seconds of the duration of the transition from the previous camera position to this one. If 0, the camera changes instantly. It can be increased by pressing *R* and decreased by pressing *F*.
- **Text.**: Optional text to identify this camera angle. To start typing you can press *T* and then press *Enter* when finished. It can be used to create the chapters in the output file.
- **V** and **H**: Vertical and horizontal displacement. It is applied to the resulting image, not really a camera movement. It is intended to center on the screen fixed elements, such as logos or texts.
- **Right Eye.**: If checked, the image corresponding to the right eye is used, otherwise the left eye is used. If the input video is 2d, this parameter is ignored. Can be checked and unchecked by pressing *Z*

### Sendcmd File

![Sendcmd File](Screenshots/TabSendcmdFile.jpg "Sendcmd file")

In this tab you configure where the file for sendcmd will be created. This tab is normally hidden, its operation is automatic and it is not required to use the program normally. There is an option in the [Config tab](#config) to display it. If this path is blank, the file is generated in a temporary directory and deleted when the program is closed.

It shows the file path at the top, the buttons to generate, save the file and the content.

### Clip Preview

![Clip Preview](Screenshots/TabClipPreview.jpg "Clip preview")

The usefulness of this tab is to configure and generate short previews of specific moments of the video, such as moments in which there is a transition to make sure it looks good without having to wait to process a much longer video. They can be of two types, as I mentioned before those of the transitions and specifying a time range.

This tab contains general formatting options, these options are explained in [Common format options](#common-format-options). Here I will only explain the specific options of this tab. In this tab are available formats that are only used in short videos, such as *gif* and *webp*.

- **Preview FPS**: Allows you to limit the frames per second of the preview, this speeds up the process. It is necessary to look for a suitable value depending on the video or codec used. For 60fps input video I recommend 30fps, and for gif format 12fps.
- **Transitions time added**: Time added before and after when generating a transition preview. It serves to give context to the transition.
- **Fragment Start**: Start time when a preview of a video fragment is generated.
- **Fragment End**: End time when a preview of a video fragment is generated.
- **Preview Dir**: Folder where the preview files will be saved, the concrete name will be generated automatically, including the name of the original file, the mode and the time of the original it represents.
- **Preview Transition**: When pressed, it generates the corresponding ffmpeg command to generate the preview of the selected transition in [Movements tab](#movements). If the *Run previews automatically* option is checked, the command is executed immediately.
- **Preview Fragment**: When pressed, it generates the ffmpeg command using the time range specified above. If the *Run previews automatically* option is checked, the command is executed immediately.
- **Command text**: Displays the last generated command for review or execute it independently of the program. For example, it can be used on a remote server.
- **Run**: When pressed it executes the last generated command.

### Comp. Preview

![Comp. Preview](Screenshots/TabCompletePreview.jpg "Complete preview")

This tab allows you to configure, generate and execute the necessary comma to generate a complete preview of the video. Contains general formatting options, these options are explained in [Common format options](#common-format-options). Here I will only explain the specific options of this tab.

- **Preview FPS**: Allows you to limit the frames per second of the preview, this speeds up the process. It is necessary to look for a suitable value depending on the video or codec used. For 60fps input video I recommend 30fps.
- **Preview File**: Here you can cover with the file name you want to generate. It is not necessary, a name is generated automatically, in the same folder as the input file.
- **New**: Allows you to browse the file system to select where and under what name should the file generated.
- **Generate ffmpeg command**: When pressed, it generates the ffmpeg command. If the *Run previews automatically* option is checked, the command is executed immediately.
- **Command text**: Displays the last generated command for review or execute it independently of the program. For example, it can be used on a remote server.
- **Run**: When pressed it executes the last generated command.

### Convert

![Convert](Screenshots/TabConvert.jpg "Convert")

This tab configures, generates and executes the command that performs the final conversion. Contains general formatting options, these options are explained in [Common format options](#common-format-options). Here I will only explain the specific options of this tab.

- **Calculate** button: When pressed it automatically calculates the output resolution based on the resolution, the input FOV and the average FOV of the virtual camera.
- **Output Default Dir**: Default directory where the file will be generated. It is not a limitation, with the ***New button***, you can select where to save the file, it is only the default option if nothing is selected. If it is blank, it uses the directory where the original file is located.
- **Output Default suffix**: Suffix to be added to the original name to generate the new name.
- **Output file**: Here you can cover with the file name you want to generate. It is not necessary, a name is generated automatically using the above information.
- **New**: Allows you to browse the file system to select where and under what name should the file generated by the conversion be created.
- **Execute on finish**: Execute this command when the video processing is finished. You can use the variable %OUTPUT% which is replaced by the final path of the output file. This command is launched detached without waiting or checking its result.
- **Generate ffmpeg command**: When pressed, it generates the ffmpeg command. If the *Run previews automatically* option is checked, the command is executed immediately.
- **Command text**: Displays the last generated command for review or execute it independently of the program. For example, it can be used on a remote server.
- **Run**: When pressed it executes the last generated command.

### Split

This tab splits the output file into pieces so that they can be processed individually, it can use the options of the Comp. Preview and Convert tabs. This different process from the normal one has the following advantages:

- Allows to pause or cancel the process without losing all the progress, it can be resumed at another time processing only the missing pieces.
- Once the process is finished it is possible to edit the camera movements and it will only be necessary to process the pieces that were affected by the changes instead of the whole video.
- The pieces can be processed on different computers in parallel, allowing to complete the processing faster.

![Split](Screenshots/TabSplit.jpg "Split")

Tab options:

- **Split Seconds**: Approximate seconds of duration of each of the piece. It is not always accurate because the program is looking for split points that work properly.
- **Split Output Options**: You can use the format and output file options from one of the **Comp. Preview** or **Convert tabs**. If **Comp. Preview** is used, the time in seconds of each frame is automatically added to the upper right corner of the video. So you can check the continuity at the split points. This way it is possible to know if there are missing or extra frames.
- **Split Files Tempral Dir**: Folder to store the processed pieces. If not configured, the VR2NormalSplitDir folder is created in the same location as the input file.
- **Generate Splits**: Clicking this button generates the split list, with all the necessary data. This process can take quite some time, a progress bar will be displayed at the bottom of the tab. This list is already tested and unlike previous versions, all the lines it generates produce precise cuts without loss or duplicated frames. Because of this the length time of the pieces is not accurate and some split lines may be skipped producing pieces twice or three times as long.
- **Split List**: List of all splits with their corresponding file name and status. It has the following columns:
	+ *State*: The status of this piece, updated by pressing the Recheck State button or automatically during processing, is normally blank when generating the list or opening a movement file until this button is pressed, it may contain the following values:
		+ Empty: Indicates that its status is unknown or does not exist yet.
		+ Finished in green: To indicate that the piece is processed.
		+ Procesing in blue: Indicates that this piece is being processed on another computer or that due to some unexpected shutdown the temporary file was left incomplete.
		+ Percentage in blue: Indicates that this piece is currently being processed on this computer and reports the progress.
		+ Error in red: Appears when there is a problem generating that piece, see the Errors tab for more information.
	+ *Time*: Time in HH:MM:SS to make the split.
	+ *Before*: Time in seconds where the previous piece will end.
	+ *After*: Time in seconds where this piece will start.
	+ *Adj. Before*: The Before column adjusted to a tested value that produces an accurate split. This is the value that will be passed to ffmpeg as the end of the previous piece.
	+ *Adj. After*: The After column adjusted to a tested value that produces an accurate split. This is the value that will be passed to ffmpeg as the start of this piece.
	+ *File*: The file name of this piece. The name has the following structure, number of the piece followed by \_, a generated Hash with all the options that affect this particular piece and ending in .mkv.
- **Recheck State**: Pressing it updates the State column in the Split List, normally it is not necessary to press it, it updates automatically when needed.
- **Remove Tmp Files**: The processing of the pieces is done on a file with the same name as the piece but ending in .tmp. Once the process is finished this file is renamed to the definitive name, due to unexpected errors (such as power outages, network disconnections, etc.) there may be incomplete files that the program detects as if the piece was being processed on another computer. Pressing this button deletes those files, but you have to make sure that they are really erroneous and that they are not being processed on another computer.
- **Save Ffmpeg Commands**: Generate a list of all ffmpeg commands to process all the pieces and put them all together, in a file called FfmpegCommad.txt in the **Split Files Tempral Dir** folder with all the other necessary files like cmd, etc. It can be used to create scripts.
- **Process Order Select**: Order of processing the pieces:
	+ *Process in order*: 1, 2, 3 ... 50
	+ *Process in inverse order*: 50, 49, 48 ... 1
	+ *Process in random order*: Randomly selects the next piece to process.
- **Run**: Clicking this button stays pressed and starts processing the pieces one by one. To pause the process you can click again to release the button and the process will end when finished processing the current piece.
- **Run Selected**: Start processing the selected line, as long as no other process is active at the time of pressing it. This button does not activate the processing like Run, it does not continue with another piece when finished.
- **Join**: Once all the pieces have been processed, pressing this button joins them all together to produce the output file configured in the corresponding tab.
- **Progress**: At the bottom of the tab when a process is running, an informative text, a progress bar and a cancel button appear.

#### Parallel processing on multiple computers

To make processing on several computers possible, a certain requirements should be met. The first and most important is that the computers must have access to the files in the same path, both input and output. The recommendation for this is to put all files in the same shared folder, the original, the **vr2n file** and the **Split Files Tempral Dir** folder. Mounted in the same way on all computers. Once the camera movements and the output configuration are finished, on the main computer you have to generate the split list and make sure to save the vr2n file so that the other computers can see the same pieces. Now launch the processing on the secondary computers by opening the vr2n file in the same way as on the main computer.

It is important that all computers use the same version of VR2Normal and Ffmpeg as well do not change input and output options, nor generate the split list again on the secondary computers, because this could make the pieces incompatible with each other.

In addition, it is not recommended to use any hardware encoding/decoding because different computers may generate different quality or even incompatible pieces.

Due to the need to use the same path and the same version of Ffmpeg on all computers it is not possible to mix computers from different platforms.

### Input Processing

![InputProcessiong](Screenshots/TabInputProcessing.jpg "Input Processing")

This tab processes the input file to generate the camera movement lines that correspond to the fade-to-black transitions, and also the small file at the same time. The two are independent and can be switched on and off independently. But as it is a time consuming process it is better to do both at the same time.

- **HW Decode**: As in other parts of the application, activate the selected hardware decoding. Depending on CPU and GPU speed it is not always faster. Usually it is incompatible with hardware encoding.
- **Detect fade to black transitions**:Activates the filter that detects black frames in the input file. When enabled, a list of these frames is displayed at the bottom of the this tab. Uses [blackframes filter](https://ffmpeg.org/ffmpeg-filters.html#blackframe). If the small file is already configured, the search is performed with it to speed up the process.
	+ *Amount*: Minimum percentage of pixels in black that the frame must have to be displayed in the list.
	+ *Threshold*: The threshold below which a pixel value is considered black.
	+ *Exclude first*: Time in seconds at the beginning that is not taken into account for adding lines of camera movements. Avoid adding lines at the beginning of the videos for logos and credits.
	+ *Exclude last*: The same as the previous option but for the end.
- **Generate small file**: When activated it generates the small file with the following options. These are almost the same options as [Common format options](#common-format-options). If the small file is already configured, this option block is disabled.
	+ *Output video format*: It is the same as the [Common format options](#common-format-options). But for the small file we are interested in using a codec that is fast to decode, image quality is not important, and also fast to encode. I recommend H264 and hardware encoding if available.
	+ *Output audio format*: If the small file is only to be used in this program I recommend to deactivate the audio selecting None.
	+ *Video resolution*: These options work a little differently from the common ones. Here you can use the value -1 and -2 in one of the dimensions to indicate that it is automatically calculated to respect the aspect ratio. -2 also ensures that the calculated value is even, some codecs give error when setting odd resolutions.
	+ *Use HW Scale*: This option is not in the common ones and allows to use hardware acceleration to rescale the image. Because of the GPU workflow, when combined with *Detect fade to black transitions* it is likely to be slower.
	+ *Output File*: Small file to generate. For compatibility it is recommended to use the same container as the source file. If empty, automatically assigns a file name in the same folder as the original file.
- **Start**: Pressing this button starts the process with the above options.
- **Add Transitions**: After finishing the process click this button to add the detected black transitions to the list of camera movements. If the list is empty, this button does nothing.
- **List of black frames**: Displays the complete list of detected black frames. It has the following format:

```
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:1679 pblack:99 pts:1680679 t:28.011317 type:P last_keyframe:1650
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:1680 pblack:99 pts:1681680 t:28.028000 type:I last_keyframe:1680
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:1681 pblack:99 pts:1682681 t:28.044683 type:P last_keyframe:1680
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:1682 pblack:99 pts:1683682 t:28.061367 type:P last_keyframe:1680
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:1683 pblack:99 pts:1684683 t:28.078050 type:P last_keyframe:1680
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:1684 pblack:99 pts:1685684 t:28.094733 type:P last_keyframe:1680
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:1685 pblack:99 pts:1686685 t:28.111417 type:P last_keyframe:1680
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:1686 pblack:98 pts:1687686 t:28.128100 type:P last_keyframe:1680
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:12982 pblack:98 pts:12994982 t:216.583033 type:P last_keyframe:12960
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:12983 pblack:99 pts:12995983 t:216.599717 type:P last_keyframe:12960
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:12984 pblack:99 pts:12996984 t:216.616400 type:P last_keyframe:12960
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:12985 pblack:99 pts:12997985 t:216.633083 type:P last_keyframe:12960
[Parsed_blackframe_1 @ 0x5640e009a7c0] frame:12986 pblack:98 pts:12998986 t:216.649767 type:P last_keyframe:12960
```

## Right Tabs

### Movements

![Movements](Screenshots/TabMovements.jpg "Movements")

This tab is the main part of the application together with the [Camera Editor tab](#camera-editor). In it we introduce the necessary camera movements needed to convert the video.

- **File**: This is the file that is currently being edited.
- **New**: Open the file browser to specify where to save the new movement file.
- **Open**: Open the file browser to select the motion file to be opened.
- **Add**: Add a new line, after the current one, to the motion list, with the current time data and camera settings in the [Camera Editor tab](#camera-editor).
- **Add Tran. 0**: Same as the previous button, adds a new line but with the transition time set to 0. It is used to directly create lines with instantaneous camera movements.
- **Add Prev.**: Same as *Add*, but the new line has the camera settings of the previous line to the selected one.
- **Remove**: Deletes the selected line
- **Save**: Save the file

When a line is selected in the list, its data is automatically loaded into the [Camera Editor tab](#camera-editor), updating the preview as well.

- **Update Time**: When pressed it updates the time of the selected line with the one currently in the [Camera Editor tab](#camera-editor).
- **Sort**: When pressing sort the list using the time field, lines with time shorter than the previous line are not allowed. this allows to fix that problem.
- **Unselect**: Deselect the line. Allows you to use the [Camera Editor tab](#camera-editor) freely without it updating the list data.
- **Preview**: When pressed, it generates a preview of the selected line transition. Use the settings specified in the [Clip Preview tab](#Clip-Preview) tab.
- **Pause Ondemand Cache**: It's a checkable button, which when pressed pauses the on-demand cache generation.
- **Add Filters**: This button adds a new line in the [filters tab](#filters).
- **Update Tran.**: Updates the transition time using the current position as the end and the line as the start. Automatically calculates the time.

Incorrect values are shown in red, to indicate incorrect lines that will be omitted, with the exception of the FOV and Eye which can be red to warning visually some problems with the value. more details in [Incorrect Lines](#incorrect-lines).

### Filters

![Filters](Screenshots/TabFilters.jpg "Filters")

This tab allows you to apply various image filters at different moments of the video. At the top there is a series of blocks with a checkbox, which globally activates that filter for the whole video. When a filter is activated within the block all its options appear. If a filter is disabled it will not work even if any of the lines contain its options, they are simply ignored.

This tab is intended to be displayed together with the camera editor, in order to preview the changes made to the filters. When clicking on the lines the preview moves to the corresponding video position applying the filters configured on that line. Changing the filter options updates both the preview and the current selected line.

***Filters currently supported***:

- **Exposure**: Adjusts the exposure of the image making it brighter or darker. [exposure ffmpeg filter](https://ffmpeg.org/ffmpeg-filters.html#exposure).
- **Color Temperature**: Changes the color temperature of the image. [colortemperature ffmpeg filter](https://ffmpeg.org/ffmpeg-filters.html#colortemperature).
- **EQ**: Changes the main characteristics of the image, brightness, contrast, saturation, gamma. [eq ffmpeg filter](https://ffmpeg.org/ffmpeg-filters.html#eq).
- **Color Balance**: Modify intensity of primary colors. [colorbalance ffmpeg filter](https://ffmpeg.org/ffmpeg-filters.html#colorbalance).

***Buttons***:

- **Defaults**: This button appears below the options of each filter and changes its options to default values, which is the same as disabling the filter for the current time, but keeping it globally active for use at other times.
- **Compare**: It is a switch that when pressed shows in the preview the filters applied to only half of the image, when not pressed the entire preview has them applied.
- **Remove**: Remove the selected line of filters.
- **Save**: Save the entire movement file.


### Screenshots

![Screenshots](Screenshots/TabScreenshots.jpg "Screenshots")

This tab allows you to take screenshots directly from the input file, to check the quality of the result and to adjust the resolution of the final process. It can also be used if simply want to get screenshots with the distortion corrected. It is designed to work together with the [Camera Editor tab](#camera-editor) because the captures are always taken with the current parameters.

- **Out. Dir**: Folder where the files of the screenshots are saved.
- **VR filter Resolution**: Resolution at which the distortion correction filter will render the image. This option is required.
- **Final Resolution**: Final resolution of the image, the output of the RV filter is rescaled to this resolution, if one of the parameters is 0 it is automatically calculated to respect the aspect ratio, if both are 0, this rescaling is disabled.
- **Format**: Format of the resulting screenshots, For quality comparison ***png*** or ***bmp*** is recommended.
- **Quality**: Image quality for lossy formats, each format has its own scale of values. For jpg values between 2 and 5 are recommended, with 2 being the highest quality. For webp acts as a percentage, 100 is the best quality, a value of 80 is recommended. In png it does not affect the image quality just the compression, it accepts values from 0 to 9, 0 is no compression and 9 is the maximum.
- **Disable filters**: When checked, disables filters, if present, when generating the screenshot. It has an intermediate state that only applies filters to half of the image. Depending on the state, it changes the suffix of the screenshot name, to avoid overwriting the file and to be able to compare the screenshots.
- **Open automatically when finished**: Automatically opens screenshots using the image viewer specified in the Config tab.
- **Take screenshot**: Pressing this button performs the screen capture using the parameters selected in the [Camera Editor tab](#camera-editor) and the options configured above.
- **Take screenshot at 720p**: Perform the capture the same as the previous button but using 1280x720 as the resolution of the VR filter.
- **Take screenshot at 1080p**: Perform the capture the same as the previous button but using 1920x1080 as the resolution of the VR filter.
- **Take screenshot at 720p rescaled to 1080p**: Perform the capture the same as the previous button but using 1280x720 as the resolution of the VR filter and 1920x1080 as the final resolution.

Screenshots take a short time to process, so a message will appear at the bottom indicating when they are being worked on and when they are finished. While they are in process the buttons are disabled.

### Input

![Input](Screenshots/TabInput.jpg "Input")

This tab contains all options related to the input video file. All these options are stored in the camera movement file. More information about some of these options can be found in the [ffmpeg filter manual](https://ffmpeg.org/ffmpeg-filters.html#v360).

- **Video File**: Location of the video file to be converted. When you specify a video, the program will automatically search in the name for keywords to try to set the format correctly. These keywords are among others *mkx200*, *fisheye190*, *360_sbs*.
- **Small Video File**: Here you can set a lower quality version of the input video, the previews will use this smaller video to take less time. Make sure that the two versions are compatible. You can also re-encode the video to a lower resolution, for example an 8k video is very slow, you can re-encode it to 1080p, which is enough and very fast to preview, to place the camera. When it is properly configured, the [Compare Small tab](#compare-small) appears. It can be generated using the [Input Processing tab](#input-processing)
- **Format**: Geometric algorithm using the video to store a wide-angle image on a flat image.
    - *hequirect*: Half equirectangular projection. The most common for 180-degree videos. It can be recognized because it fills the corners of the image and at the top and bottom edges the image is very stretched to the sides.
    - *equirect*: Equirectangular projection. The most common for 360-degree videos. The same as the previous format, but for 360-degree videos.
    - *fisheye*: Fisheye projection. It is the second most common format and is how cameras actually record. It is recognized because the image is in the shape of a circle. Examples: [Wikipedia Fisheye lens](https://en.wikipedia.org/wiki/Fisheye_lens)
    - *sg*: Stereographic format. Similar to fisheye, never used in practice.
    - *equisolid*: Equisolid format. Never used in practice.
    - *pannini*: Pannini projection. Never used in practice.
- **Mode**: Input video stereo format.
    - **sbs**: Side by side. The two images in the video are placed side by side.
    - **tb**: Top bottom. The two images in the video are placed one on top of the other.
    - **2d**: 2D mono. The video is not 3D.
- **Output Mode**: Output stero format. When a 3D mode is selected, the preview shows the image of the 2 eyes and ignores the eye selection in the camera. Supports the same modes as the previous option.
- **File FPS**: If the ffprobe option is configured, it is automatically read from the video file. It is only used for frame-by-frame movement through the video.
- **Vertical FOV**: Vertical field of view. Normally has a value of 180.
- **Horizontal FOV**: Horizontal field of view. The most common values are 180, 200 and 360.
- **Input Padding Res.**: This option is used to make the cropped fisheye format work correctly. Here you have to put the resolution that the video would have if it were not cropped. These options are incompatible with the small file and "Prev. Scale", they are automatically disabled when used.
- **Input Padding Pos.**: Related to the previous one, here you have to set the coordinates of the upper left corner, where to place the image of the video in the desired size. The objective is to perfectly center and fit the fisheye circle in the enlarged image.
- **Video Resolution**: Input video resolution, used to calculate the estimated resolution of each shot. If ffprobe is configured it is automatically covered.
- **Pixel Format**: It is the pixel format of the input video, for information only, automatically read by ffprobe. Used as information to correctly set the output pixel format.
- **Video Length**: Video duration, it is used to not allow to exceed it in the previews, it can give ffmpeg errors if it is exceeded. It is read automatically if the ffprobe option is covered.
- **Movement**: Allows to set the camera movement during transitions time. Only the Intermediate option uses the percentage.
    - *Linear*: The movement is distributed linearly with respect to the duration of the transition. This is how the application has been working until version 1.1.
    - *Smooth*: The camera starts moving slowly and speeds up until the middle of the transition, then slows down progressively until it stops. Since version 1.2 this is the default option.
    - *Intermediate*: Intermediate version to the previous ones, uses the specified percentage to get closer to the behavior of one than the other, 0% is equal to Linear, 100% is equal to Smooth.
- **Custom v360**: Allows to customize the fixed parameters of the v360 filter, and to add parameters that are not included in the program. It can be used to configure the input and output formats by overwriting the previous options. When blank this option acts as if it had the following value *<Format>:flat:in_stereo=2d:out_stereo=2d*, *<Format>* is the value set in the **Format** field. For more information see the [v360 filter documentation](https://ffmpeg.org/ffmpeg-filters.html#v360). To see the changes, click on the **Apply** button.
- **Custom Output Filters**: Allows to add additional filters before the v360 filter. It can be any of the [ffmpeg video filters](https://ffmpeg.org/ffmpeg-filters.html#Video-Filters), more than one can be added separated by ",". To see the changes, click on the **Apply** button.
- **Custom Output Filters**: Allows to add additional filters after the v360 filter. Works the same as the previous one.
- **Title**: Title of the video, it will be added to the tags of the output file. It is optional.
- **Studio**, **Actors**, **Release Date:**: Same as Title when they are filled, they are added to the tags of the output file.
- **Generate Chapters**: When checked, using the texts entered in [Moves](#movements), it generates the chapters in the [Complete preview](#comp.-preview) and [Convert](#convert) output files.
- **Apply**: If the Camera Editor tab is visible, updates the previews with the current options.
- **Save**: Saves the current options in the file.
- **Gen. Minutes Cache**: Generates the cache for all minutes, it is a background process, it does not interfere with normal operation, once generated it allows to move with the -60s and +60s buttons much faster. Generates one frame for every minute of video
- **Gen. 10 Seconds Cache**: Same as above but every 10s, so that moving with -10s and +10s is much faster, it generates 6 frames for every minute of video.

This tab is intended to be used in conjunction with the [Camera Editor tab](#camera-editor) to quickly identify the correct settings in the preview.

### Compare Small

![CompareSmall](Screenshots/TabCompareSmall.jpg "Compare small")

This tab allows you to generate small previews at regular intervals of the 2 input video files. In order to check if they are compatible or not, to be compatible the images of the 2 files must match.

It is only visible when the 2 input files are correctly configured. You can configure how many images to generate and their size. To generate them just click on the "Generate Previews" button.

The images not yet generated are shown as noise. They use the cache so some images may be loaded instantly. 

Even if we re-encode the small video ourselves, these images may vary by one frame. This is normal and does not mean that the file is incompatible. 

### Config

![Config](Screenshots/TabConfig.jpg "Config")

This tab contains the main configuration of the application, it is the first thing to cover when using it for the first time.

- **Ffmpeg Binary**: Location of the ffmpeg executable. This is the most important configuration option if it is not covered the program does not work.
- **Ffprobe Binary**: Location of the ffprobe executable. It is optional, but without it some parameters of the input video are not automatically covered, such as the total duration.
- **Video Player**: Location of the video player to play the generated previews. It is optional.
- **Image Viewer**: Image viewer to be used when the option to open screenshots automatically is activated. You can use ffplay as well.
- **Prev. Resolution**: Resolution at which the camera preview image will be rendered. It is important to use the same aspect ratio here as in the final process, otherwise the preview will not be able to properly framing the image.
- **Prev. Interpol.**: Interpolation algorithm used by the Camera Editor tab to generate the preview image. I recommend *nearest* or *linear*, because in this case speed is more important than image quality.
- **Prev. Scale**: If enabled, it reduces the raw images in the previews. Can make camera movements in high resolution videos smoother. Also reduces the size they occupy in the cache. You can select the height or width of the images, for example "Auto x 1080" reduces the images to 1080 height and the corresponding width to maintain the aspect ratio.
- **Prev. HW Decode**: Allows enabling hardware decoding for preview and cache generation. ***Experimental. May be slower than software decoding.*** The available options are explained in [Common format options](#common-format-options).
- **Def. Tran. Val.**: Default value of transition time when adding a new line.
- **Fov Limits**: Maximum and minimum value for the FOV of the virtual camera.
- **Cache Format**: Format used to store images in RAM and caches.It works even with the caches disabled because at least one image must be stored in RAM.
    -*BMP*: Uncompressed format, it's faster, but the images require a lot of space. Only recommended with caches disabled. One 7K image occupies approximately 75MB.
    -*PNG*: Lossless compression, it is the intermediate option, it occupies quite a lot, but much less than BMP.
    -*JPG*: High compression, the slowest, but images take up only a little space. It is the default option, because although it may be slow it is safer for everyone.
- **RAM Cache**: Maximum size in megabytes of the frame cache in RAM. 0 To disable. Allows to return with much more speed to the frames that we have already seen before. When full, the oldest frames are automatically deleted.
    - *Clear*: Delete the contents of the cache.
    - *RAM Usage*: In MB, RAM memory currently in use by the cache.
- **Disk Cache Dir**: Folder where the disk cache files will be stored. This cache is persistent even if the program is closed and the folder is shared by all the files you are working on. ***WARNING: It must be an exclusive folder for VR2Normal, because it will delete the files inside when the maximum size is exceeded***.
- **Cache Process**: Number of parallel processes that will be launched to generate the cache. In some cases it can make generating the cache faster, especially if this option is combined with hardware decoding. It does not necessarily have to match the number of processor cores, because ffmpeg itself uses more than one. This option is more useful to avoid waiting times than to take advantage of multiple cores, ffmpeg does well in that aspect.
- **Preload Seconds**: Number of seconds for which to generate the cache automatically from the current time of the video. For example if it is at 5 and we are at minute 1, it will generate the cache for 1:01, 1:02, 1:03, 1:04 and 1:05. Set to 0 to deactivate.
- **Disk Cache**: Maximum size in megabytes of the frame cache in disk. 0 To disable. Allows to pregenerate the cache to move forward much faster. When it fills up, the oldest frames are erased
    - *Clear*: Delete the contents of the cache.
    - *RAM Usage*: In MB, Disk space currently in use by the cache.
- **Cache Priority**: Priority of the ffmpeg process to generate the cache. You can only reduce the priority because in many operating systems increasing the priority is restricted to users with certain privileges.
	- *Default*: No change is applied to the process priority.
	- *Low*: In Windows it is equivalent to the priority below normal. In Linux is equal to priority 10.
	- *Idle*: In Windows it is equivalent to the priority low. In Linux is equal to priority 19.
- **Process Priority**: Priority of the ffmpeg process for [Clip Preview](#clip-preview), [Comp. Preview](#comp.-preview), and [Convert](#convert) tabs. The priorities are the same as described in the previous option.
- **Pitch, Yaw Steps**: This is the value that pitch and yaw are increased or decreased each time the corresponding key is pressed. Adjust the amount of camera movement at each step and define the minimum movement without manually changing the number.
- **Roll Steps**: The same as the previous option but applied to Roll.
- **Fov Steps**: The same as the previous option but applied to Fov.
- **Trans. Steps**: The same as the previous option but applied to Trans.
- **Error Color**: Color used by the program to indicate wrong lines to be ignored and error messages. It can be an exadecimal code #RRGGBB or a name from the list [SVG color keyword names](https://www.w3.org/TR/SVG11/types.html#ColorKeywords). Clicking the Select button opens a dialog that displays a color selection dialog.
- **Low Warning Color**: Color used by the program to show minor warnings. Same format as *Error Color*.
- **High Warning Color**: Color used by the program to show more important warnings. Same format as *Error Color*.
- **Add VR2Normal file as attach to MKV**: Adds a reduced version (without full paths and current position) of the [current VR2Normal file](#movements) to the MKV file generated by the [Convert tab](#convert).
- **Show Tray Icon**: Displays the icon in the system tray. When it is checked and in the middle of a process, closing the application hides it, but it is still active in the tray. Clicking on the icon hides and shows the window.
- **Show Progress on Taskbar**: Displays the progress in the taskbar, in the same way as when copying a file. Each operating system displays this progress differently. Qt6 removed the code needed to use it in windows, for now until an alternative is found this option does not work with Qt6 in windows.

![TaskbarKDE](Screenshots/TaskbarProgressKDE.jpg "Taskbar Progress KDE")

![TaskbarUbuntu](Screenshots/TaskbarProgressUbuntu.jpg "Taskbar Progress Ubuntu")

![TaskbarWin11](Screenshots/TaskbarProgressW11.jpg "Taskbar Progress Windows 11")

- **Show framing lines**: Displays auxiliary lines to correctly frame the output image. They are displayed in the [Camera Editor tab](#camera-editor) in the preview image.

![Framing Lines](Screenshots/FramingLines.jpg "Framing lines")

- **Show Estimated Resolution**: Displays the estimated resolution, the horizontal and vertical field of view of the current preview. This resolution is an aid in selecting a suitable resolution for final processing. It is calculated based on the resolution of the original video and the field of view of both the original video and the current shot. It does not take into account the lens distortion correction so it is not accurate, but it is a good help to decide if the conversion is worth doing at a certain resolution. If the original video image does not respect the aspect ratio and is stretched in any way this calculation may also have a different aspect ratio than desired, for example in the 360 up-down videos.

It will be displayed in the [Camera Editor tab](#camera-editor) next to the preview image.

![Estimated Size](Screenshots/EstimatedSize.jpg "Estimated Size")

- **Limit camera movement speed**: Limit camera movement using the keys to the maximum speed the camera can display.
- **Autosave movement file**: If checked, automatically saves the file with camera movements every minute and when closing the application.
- **Autosave configuration**: If checked, automatically saves the configuration when closing the application.
- **Save window settings in config**: If checked, saves the current state of the window in the configuration file, size, position, active tabs...
- **Run previews automatically**: If checked, the commands to generate previews are automatically executed. It will be explained in more detail where it is used.
- **Play previews automatically**: If checked, the previews are played automatically. You need the *Video Player* option to be set.
- **Show Sendcmd File Tab**: Displays the [Sendcmd File tab](#sendcmd-file), where it is possible to configure where this file is saved, by default it is generated in a temporary directory that is deleted when the program is closed.
- **Debug***: When checked, it displays the Debug tab, in this tab you can see a log of various actions and processing times.
- **Save Config**: If pressed, saves the configuration now.

The Select buttons when pressed allow you to navigate through the file system to find the corresponding file.

### Shortcuts

![Shortcuts](Screenshots/TabShortcuts.jpg "Shortcuts")

In this tab you can configure all the keyboard shortcuts. To do it, click on the option and then press the desired key or key combinations and wait a couple of seconds.

### Debug

![Debug](Screenshots/TabDebug.jpg "Debug")

This tab shows when visible displays a log of various actions and the time it takes for the program to complete them. The following messages may appear.

- **Moving to time 0:00:00.000**: Indicates that the current time has been changed.
- **Raw - Ram Cache read - Total: 10**: The raw video preview was updated, the RAM cache was used and it took 10ms to complete.
- **Raw - Disk Cache read - Total: 20**: The raw video preview was updated, the disk cache was used and took 20ms to read.
- **Raw - Start: 8 Read: 244 Preview: 7 Total: 259**: The raw video preview was updated, The frame was generated directly from the input file. All values are in miliseconds.
    - *Start:* Time to launch ffmpeg.
    - *Read:* Time taken by ffmpeg to send the frame.
    - *Preview:* Time the program needs to update the preview.
    - *Total:* The sum of all of the above.
- **Filter - Start: 6 Send: 0 Read: 169 Preview: 2 Total: 177**:
    - *Start:* Time to launch ffmpeg.
    - *Send:* Time that the program took to send to ffmpeg the frame to process.
    - *Read:* Time taken by ffmpeg to send the result.
    - *Preview:* Time the program needs to update the preview, including the time to paint the framing lines.
    - *Total:* The sum of all of the above.
- **Cache Generating time: 0:01:00.000**: Indicates that the disk cache for this frame will be generated.
- **Cache - Queue: 0 Start: 3 Procecss: 239 Total: 242**:
    - *Queue:* The time it took for the program to select the next frame to be generated.
    - *Start:* Time to launch ffmpeg.
    - *Process:* The time it took for ffmpeg to take the screenshot and save the file in the cache folder.
    - *Total:* The sum of all of the above.

## Common format options

![Format Options](Screenshots/FormatOptions.jpg "Format Options")

These options are the same in several tabs, so I am only going to explain them once.

- **HW Decode**: Allows enabling hardware decoding. ***Experimental. Depends on hardware, drivers and ffmpeg compilation. May cause errors if combined with hardware encoding. It may not be faster than software decoding.***
	- ***None***: Hardware decoding disabled. Default.
	- ***VAAPI (linux)***: For intel graphics cards and vaapi compatible.
	- ***CUDA***: For Nvidia graphics cards.
	- ***DXVA2 (windows)***: Uses DirectX 9, most graphics cards on windows.  ***Untested***.
	- ***D3D11 (windows)***: Uses DirectX 11. ***Untested***.
- **Output video format**: There are 3 fields that define the video codec and the quality.
    - *Codec*
        - ***x265***: Newer and more advanced but slow codec.
        - ***x265 lossless***: Lossless configuratión for x265. Produces huge size outputs, only suitable to use as intermediate for other conversions.
        - ***x264***: The most widely used codec today.
        - ***x264 lossless***: Lossless configuratión for x264. Produces huge size outputs, only suitable to use as intermediate for other conversions. Incompatible with some players.
        - ***Xvid***: Older codec, but very fast, the quality is not good, but it is adequate for previews.
        - ***SVT-AV1***: New Format, successor of VP9.
        - ***VP9***: VP9 is an open and royalty-free video coding format developed by Google.
        - ***VP9 lossless***: Lossless configuratión for VP9. Produces huge size outputs, only suitable to use as intermediate for other conversions.
        - ***FFV1***: FF Video 1 is a lossless intra-frame video coding format. Produces huge size outputs, only suitable to use as intermediate for other conversions.
        - ***NVENC H265***: H265 codec using NVidia graphics cards, to use it you need a card that supports this particular codec and a version of Ffmpeg compiled to support it. Faster than x265 but less efficient with file size, with the same quality it produces larger files, with the same file size the quality is lower, I only recommend it for previews.
        - ***NVENC H264***: The same as the previous option but for the H264 codec.
        - ***VAAPI H265 (linux)***: H265 codec using Intel graphics cards or VAAPI compatible on linux, Only recommended for previews.
        - ***VAAPI H264 (linux)***: The same as the previous option but for the H264 codec.
        - ***Intel QSV H265***: H265 codec using Intel graphics cards. Only recommended for previews.
        - ***Intel QSV H264***: The same as the previous option but for the H264 codec.
        - ***gif***: Only available for short videos.
        - ***webp***: Only available for short videos.
    - *Preset* It is only used by the x265 and x264 codecs. It allows you to adjust the quality and compression speed, the slower the better the quality. *veryfast* is recommended for previews and *medium*, *slow* or *slower* for final re-encoding. NVENC doesn't supports all the options, only *fast*, *medium* and *slow*.
    - *Quality* This is a numerical value that represents the image quality we want to achieve. Each codec has its own scale and the values of one have nothing to do with the others. The smaller the number, the higher the quality we get.
        - For x265, x264 and NVENC, values between 18 and 24 are recommended.
        - For Xvid, values between 3 and 6 are recommended.
        - The gif format does not use this option
        - For webp, values between 50 and 100 are recommended.
- **Output audio format**
    - *Codec*
        - ***Copy***: Copies the input audio without modifying it, in this case the bitrate is ignored. Recommended
        - ***AAC***: Most used audio format today.
        - ***MP3***: Older format but very compatible, usually used with the *Xvid* video format.
        - ***Opus***:
        - ***Vorbis***:
        - ***None***: Generates an output file without audio.
    - *Bitrate* Number in kbps. It depends on the codec used, and the number of channels. Recommended value between 128 and 256.
- **Custom format**: This field allows you to customize the ffmpeg command. It is empty by default. The following variables are available for composing the command.
    - ***%FFMPEG%***: Contains the value for *Ffmpeg Binary* in the [Config tab](#config).
    - ***%FFMPEG_GUI_OPTIONS%***: Parameters required to display the progress bar.
    - ***%HWDECODE%***: Parameters that activate the selected hardware decoding.
    - ***%START_TIME%***: When we do a preview this tells ffmpeg where to start. it is only necessary in previews, when not used its value is empty.
    - ***%INPUT%***: Input file parameter.
    - ***%TO_TIME%***: When we are in a short preview it tells ffmpeg when to end. When not in use its value is empty.
    - ***%MAP%***: Selection of the tracks to process, Select only the first video track and if configured all audio tracks.
    - ***%SIMPLE_FILTER%***: Parameter with complete filter.
    - ***%RAW_FILTER%***: Raw value of the filter without ffmpeg parameter, can be used to add more filters before or after or use complex filters.
    - ***%FORMAT%***: Complete output format with video, audio and metadata options.
    - ***%VIDEO_FORMAT%***: Video only options.
    - ***%AUDIO_FORMAT%***: Audio only options.
    - ***%VIDEO_QUALITY%***: Value selected in the configuration form
    - ***%VIDEO_PRESET%***: Value selected in the configuration form
    - ***%VIDEO_CODEC%***: Value selected in the configuration form
    - ***%AUDIO_QUALITY%***: Value selected in the configuration form
    - ***%AUDIO_CODEC%***: Value selected in the configuration form
    - ***%METADATA%***: Metadata-related options, for now only the *Force Mono Metadata* in [Input tab](#input)
    - ***%OUTPUT%***: Output file configured in the form.
    - ***%OUT_FILE_NAME%***: Output file without the extension, it allows to add the extension we need.
    - **Example default value**: `%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %START_TIME% %INPUT% %TO_TIME% %MAP% %SIMPLE_FILTER% %FORMAT% %OUTPUT%`
    - **Example gif format value**: `%FFMPEG% %FFMPEG_GUI_OPTIONS% %START_TIME% %INPUT% %TO_TIME% -map 0:v:0 -filter_complex "[0:v] %RAW_FILTER%,split [a][b]; [a] palettegen=stats_mode=single [p]; [b][p] paletteuse=new=1" %OUTPUT%`
    - **Example NVENC H265**: `%FFMPEG% %FFMPEG_GUI_OPTIONS% -hwaccel_output_format cuda %START_TIME% %INPUT% %TO_TIME% %MAP% %SIMPLE_FILTER% -c:v hevc_nvenc -preset %VIDEO_PRESET% -cq %VIDEO_QUALITY% %AUDIO_FORMAT% %METADATA% %OUTPUT%`
    - **Example NVENC H264**: `%FFMPEG% %FFMPEG_GUI_OPTIONS% -hwaccel_output_format cuda %START_TIME% %INPUT% %TO_TIME% %MAP% %SIMPLE_FILTER% -c:v h264_nvenc -preset %VIDEO_PRESET% -cq %VIDEO_QUALITY% -profile:v high %AUDIO_FORMAT% %METADATA% %OUTPUT%`
    - **Example linux optimus nvidia hardware encoding**: `/usr/bin/optirun %FFMPEG% %FFMPEG_GUI_OPTIONS% -hwaccel_output_format cuda %START_TIME% %INPUT% %TO_TIME% %MAP% %SIMPLE_FILTER% -c:v h264_nvenc -preset %VIDEO_PRESET% -cq %VIDEO_QUALITY% -profile:v high %AUDIO_FORMAT% %METADATA% %OUTPUT%`
- **Video resolution**: Output image related options.
    - ***Width***: Number of pixels of width of the image we want to generate
    - ***Height***: Number of pixels of height of the image we want to generate
    - ***Interpolation***: Interpolation algorithm to be used to generate the image, for previews I recommend *nearest* or *linear*, for final re-encoding *lanzcos*.
    - ***Pixel format***: Pixel format to be used in the output, by default **Auto**. If a format is specified a conversion is added before the output to force it. Normally this is only necessary if you use some filters that modify it, for example *exposure* and *eq*. It is automatically set when filters are used, so that they do not alter the pixel format.

## Processing

When a command is being processed, a bar appears at the bottom with the process data, a progress bar and a cancel button. Displays a text with the following information.

- **Real Time**: Actual processing time.
- **Video Time**: Position in the video currently processing.
- **Speed**: Current processing speed in X. 
- **Size**: Size occupied by the processed video portion.
- **Bitrate**: Average bit rate of the processed video.
- **ETR**: Estimated remaining time. Estimated actual time remaining to completion.
- **Estimated Size**: Estimated size of the completed video.

![Progress](Screenshots/Progress.jpg "Progress")

When the process is finished, the progress bar and the button disappear, displaying a message indicating whether the process was successful or not. If successfully completed, displays the total processing time.

## Errors

### Processing

If there is an error, an additional tab *Error Log* may appear, with the command and the error it returned.

![Error Log](Screenshots/TabErrorLog.jpg "Error Log")

The **Clear Log** button clears the message and hides the tab.

### Incorrect Lines

When there are incorrect lines in the [Movements tab](#movements), they are marked in red as follows. This color can be changed in the *Error Color* option in the [Config tab](#config). But I am going to refer to it as red because that is its default value.

![Error Lines](Screenshots/ErrorLines.png "Error lines")

The background of the line number changes to red and shows an E, the text in the cell with the incorrect value changes to red. These lines are treated as if they do not exist.

The most common errors in these lines are those related to time overlapping. For example in first red line of the image you can see that the transition is too long and overlaps with the beginning of the next line. The second red line is not sorted because the time is shorter than the previous line, it can be fixed by pressing the sort button, this does not mean that the line marked in red is the wrong one, it is only the first line in which the problem is detected.

When the preview and convert options are used while there are incorrect lines the following warning appears at the top of the application.

![Lines Warning](Screenshots/LinesWarning.png "Lines warning")

This warning will only disappear when the lines are fixed and when the sendcmd file is generated again.

Some values may be in red to indicate some kind of problem with them. These colors can be configured in the *Warning Color* options in the Config tab. But in this case it is only a visual indication of a problem, the line is still valid. The columns that may contain these values in red are:

- **FOV Column**: It can be orange if the value is a little high or red if it is a lot. I consider it to be high, when normal cameras do not usually have them and a little distortion is noticeable. and very high when this distortion is very evident.
- **Eye Column**: It is impossible to make a transition by slowly moving the camera and changing the eye at the same time without noticing a jump in the image. This is why I think that changing the eye when the value of the transition column is different from zero is an error and will be shown in red.

# BUGS
You can report bugs or problems on the [gitlab page](https://gitlab.com/vongooB9/vr2normal/-/issues)

# COPYRIGHT
Copyright © vongooB9.

License GPLv3: GNU GPL version 3 [https://gnu.org/licenses/gpl.html](https://gnu.org/licenses/gpl.html). This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.


---
header-includes: |
  <style>
  body {max-width: 90%;}
  figure {text-align: center;}
  a, a:visited {color: #1a1aaa;}
  @media (prefers-color-scheme: dark) {
    html {background: black; color: white;}
    a, a:visited {color: #AAF;}
  }
  </style>
---
