VR2NORMAL_VERSION = 3.3

QT       += core gui

win32 {
    lessThan(QT_MAJOR_VERSION, 6): QT += winextras
}

unix:!android!mac{
    QT += dbus
}

QT += widgets

CONFIG += c++17

DEFINES += VR2NORMAL_VERSION=\\\"$$VR2NORMAL_VERSION\\\"

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
	common.cpp \
	diskcache.cpp \
	filters.cpp \
	generatesplits.cpp \
	main.cpp \
	mainwindow.cpp \
	processpriority.cpp \
	qtablewidgetundo.cpp \
        taskbarprogress.cpp

HEADERS += \
	common.h \
	diskcache.h \
	filters.h \
	generatesplits.h \
	mainwindow.h \
	processpriority.h \
	qtablewidgetundo.h \
        taskbarprogress.h

FORMS += \
	mainwindow.ui

RESOURCES = icon.qrc

win32 {
    RC_ICONS = VR2Normal.ico
}

macx {
    ICON = VR2Normal.icns
}

unix:!android!mac{
	!defined(USR_DIR, var) { USR_DIR = /usr/local }
	!defined(ICONS_DIR) { ICONS_DIR = /share/icons }
	!defined(DESKTOP_DIR) { DESKTOP_DIR = /share/applications }
	!defined(MIME_DIR) { MIME_DIR = /share/mime/packages }
	BIN_DIR = $$USR_DIR/bin

	target.path = $$BIN_DIR
	INSTALLS += target

	DESKTOP_FILE = $${TARGET}.desktop
	DESKTOP_CONTENT += "[Desktop Entry]"
        #DESKTOP_CONTENT += "Version=$$VR2NORMAL_VERSION"
	DESKTOP_CONTENT += "Name=VR2Normal"
	DESKTOP_CONTENT += "Comment=Converts VR video files to normal"
        DESKTOP_CONTENT += "Type=Application"
        #DESKTOP_CONTENT += "Exec=$${BIN_DIR}/$${TARGET}"
        DESKTOP_CONTENT += "Exec=$${TARGET}"
	DESKTOP_CONTENT += "Terminal=false"
	DESKTOP_CONTENT += "Categories=AudioVideo"
        DESKTOP_CONTENT += "Icon=VR2Normal"
	DESKTOP_CONTENT += "MimeType=text/vr2n;"
	write_file($$DESKTOP_FILE, DESKTOP_CONTENT)
	desktopinstall.CONFIG = no_check_exist
	desktopinstall.path = $${USR_DIR}$${DESKTOP_DIR}
	desktopinstall.files = $$DESKTOP_FILE
	INSTALLS += desktopinstall

	iconinstall.CONFIG = no_check_exist
	iconinstall.path = $${USR_DIR}$${ICONS_DIR}
	iconinstall.files += VR2Normal.svg
	iconinstall.files += VR2NormalFile.svg
	INSTALLS += iconinstall

	mimeinstall.CONFIG = no_check_exist
	mimeinstall.path = $${USR_DIR}$${MIME_DIR}
	mimeinstall.files += vr2normal.xml
	INSTALLS += mimeinstall

}

