#include "common.h"
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTextStream>
#include <QColorDialog>

void WriteFile(QString Filename, QString Content) {
	QFile file(Filename);
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file);
	out << Content;
	file.close();
}

void AppendFile(QString Filename, QString Content) {
	QFile file(Filename);
	file.open(QIODevice::Append);
	QTextStream out(&file);
	out << Content;
	file.close();
}

QString ReadFile(QString Filename) {
	QFile file(Filename);
	if (!file.open(QFile::ReadOnly | QFile::Text)) return "";
	QTextStream in(&file);
	return in.readAll();
}

QString ScapeFFFilters(QString in) {
	QString Output = QDir::toNativeSeparators(in);
	Output.replace("\\", "\\\\");
	Output.replace(":", "\\:");
	Output.replace("%", "\\%");
	Output.replace(",", "\\,");
	Output.replace("'", "'\\\\\\''");
	Output.replace("[", "\\[");
	Output.replace("]", "\\]");
	return Output;
}

QString v360Filters(
			QString V360Format,
			QString V360Custom,
			QString V360Mode,
			QString OutMode,
			double V360IVFOV,
			double V360IHFOV,
			double V360DFOV,
			double V360Pitch,
			double V360Yaw,
			double V360Roll,
			double V360VOffset,
			double V360HOffset,
			bool V360RightEye,
			int V360W,
			int V360H,
			QString V360Interp,
			bool V360ResetRot,
			int PadW,
			int PadH,
			int PadX,
			int PadY
) {

	QString Output;

	QStringList v360_params;

	if (V360Custom == "") {
		v360_params.append(V360Format);
		v360_params.append("flat");
		if (OutMode == "2d")
			v360_params.append("in_stereo=2d");
		else
			v360_params.append("in_stereo=" + V360Mode);
		v360_params.append("out_stereo=" + OutMode);
	} else {
		v360_params.append(V360Custom);
	}

	if (V360IVFOV > 0)
		v360_params.append("iv_fov=" + QString::number(V360IVFOV));
	if (V360IHFOV > 0)
		v360_params.append("ih_fov=" + QString::number(V360IHFOV));

	v360_params.append("d_fov=" + QString::number(V360DFOV));
	v360_params.append("pitch=" + QString::number(V360Pitch));
	v360_params.append("yaw=" + QString::number(V360Yaw));
	v360_params.append("roll=" + QString::number(V360Roll));
	if (V360VOffset != 0)
		v360_params.append("v_offset=" + QString::number(V360VOffset));
	if (V360HOffset != 0)
		v360_params.append("h_offset=" + QString::number(V360HOffset));
	v360_params.append("w=" + QString::number(V360W));
	v360_params.append("h=" + QString::number(V360H));
	v360_params.append("interp=" + V360Interp);
	if (V360ResetRot)
		v360_params.append("reset_rot=1");

	QString Crop = "";

	if (OutMode == "2d") {
		if (V360Mode == "sbs") {
			if (V360RightEye) {
				Crop = "crop=w=iw/2:h=ih:x=iw/2:y=0,";
			} else {
				Crop = "crop=w=iw/2:h=ih:x=0:y=0,";
			}
		} else if (V360Mode == "tb") {
			if (V360RightEye) {
				Crop = "crop=w=iw:h=ih/2:x=0:y=ih/2,";
			} else {
				Crop = "crop=w=iw:h=ih/2:x=0:y=0,";
			}
		}
	}

	QString Pad = "";
	if (PadW != 0 && PadH != 0) {
		Pad = "pad=w=" + QString::number(PadW) +
			  ":h=" + QString::number(PadH) +
			  ":x=" + QString::number(PadX) +
			  ":y=" + QString::number(PadY) +
			  ":color=black,";
	}

	Output = Crop + Pad + "v360=" + v360_params.join(":");
	return Output;
}

QString ComposeFfmpegCommand(QString Ffmpeg,
							 QString Input,
							 QString Output,
							 QString Template,
							 QString VideoCodec,
							 int VideoQuality,
							 QString VideoPreset,
							 QString AudioCodec,
							 int AudioQuality,
							 QString Start,
							 QString End,
							 double FPS,
							 double OriginalFPS,
							 QString V360Format,
							 QString V360Custom,
							 QString V360Mode,
							 QString OutMode,
							 double V360IVFOV,
							 double V360IHFOV,
							 double V360DFOV,
							 double V360Pitch,
							 double V360Yaw,
							 double V360Roll,
							 double V360VOffset,
							 double V360HOffset,
							 bool V360RightEye,
							 int V360W,
							 int V360H,
							 QString V360Interp,
							 bool V360ResetRot,
							 int PadW,
							 int PadH,
							 int PadX,
							 int PadY,
							 QString SendCMDFile,
							 QString Filters,
							 QString PixelFormat,
							 QString FileAttach,
							 QString MetadataTitle,
							 QString MetadataStudio,
							 QString MetadataActors,
							 QString MetadataReleaseDate,
							 QString HWDec,
							 QString ChaptersFile,
							 QString CustomFilters,
							 QString CustomInputFilters,
							 bool FilterPTS,
							 QString OutFormat,
							 int KeyInt) {

	QString Fps;
	double DestFps = OriginalFPS;
	if (FPS > 0 && OriginalFPS != FPS) {
		Fps = "fps=fps=" + QString::number(FPS);
		DestFps = FPS;
	}
	int BFrames = (DestFps < 40) ? 6 : 7;

	QString v360 = v360Filters(
							   V360Format,
							   V360Custom,
							   V360Mode,
							   OutMode,
							   V360IVFOV,
							   V360IHFOV,
							   V360DFOV,
							   V360Pitch,
							   V360Yaw,
							   V360Roll,
							   V360VOffset,
							   V360HOffset,
							   V360RightEye,
							   V360W,
							   V360H,
							   V360Interp,
							   V360ResetRot,
							   PadW,
							   PadH,
							   PadX,
							   PadY
				   );

	QFileInfo OutFileInfo(Output);
	if (OutFormat == "") OutFormat = OutFileInfo.suffix().toLower();

	QString FfmpegGuiOptions = "-hide_banner -loglevel error -stats -y";

	QString RawFilters = "";
	if (CustomInputFilters != "") RawFilters = CustomInputFilters + ",";
	if (Fps != "") RawFilters += Fps + ",";
	RawFilters += "sendcmd=f='" + ScapeFFFilters(SendCMDFile) + "'," + v360;
	if (Filters != "") RawFilters += "," + Filters;
	if (PixelFormat != "" && PixelFormat != "Auto") RawFilters += ",format=" + PixelFormat;
	if (CustomFilters != "") RawFilters += "," + CustomFilters;
	if (Start != "" && FilterPTS) RawFilters += ",setpts=PTS-STARTPTS";

	QString SimpleFilter = "-vf \"" + RawFilters + "\"";
	if (Start != "")
		Start = "-ss " + Start;
	QString InputParam = "-i \"" + Input + "\"";

	if (ChaptersFile != "") {
		InputParam += " -i \"" + ChaptersFile + "\"";
	}

	QString To = "";
	if (Start != "")
		To += "-copyts";
	if (End != "")
		To += " -to " + End;

	QString Map = "-map 0:v:0";
	if (AudioCodec != "None") Map += " -map 0:a";

	QString HWDecode = "";
	if (HWDec == "CUDA") {
		HWDecode = "-hwaccel cuda";
	} else if (HWDec == "VAAPI (linux)") {
		HWDecode = "-hwaccel vaapi";
		HWDecode += " -vaapi_device " + GetVAAPIDevice();
	} else if (HWDec == "DXVA2 (windows)") {
		HWDecode = "-hwaccel dxva2";
	} else if (HWDec == "D3D11 (windows)") {
		HWDecode = "-hwaccel d3d11va";
	}

	QString FormatVideo = "";
	QString InitHWEncode = "";
	if (KeyInt == 0)
		KeyInt = qRound(DestFps) * 10;

	if (VideoCodec == "x265") {
		if (VideoPreset == "ultrafast") VideoPreset = "superfast";
		FormatVideo = "-c:v libx265 -preset " + VideoPreset +
					  " -crf " + QString::number(VideoQuality) +
					  " -x265-params keyint=" + QString::number(KeyInt) + ":bframes=" + QString::number(BFrames);
		if (DestFps < 50) {
			FormatVideo += ":gop-lookahead=12";
		} else {
			FormatVideo += ":gop-lookahead=11";
		}
	} else if (VideoCodec == "x265 lossless") {
		if (VideoPreset == "ultrafast") VideoPreset = "superfast";
		FormatVideo = "-c:v libx265 -preset " + VideoPreset + " -x265-params lossless=1";
	} else if (VideoCodec == "x264") {
		FormatVideo = "-c:v libx264 -preset " + VideoPreset +
					  " -crf " + QString::number(VideoQuality) +
					  " -tune film" +
					  " -x264-params keyint=" + QString::number(KeyInt) + ":bframes=" + QString::number(BFrames);
	} else if (VideoCodec == "x264 lossless") {
		FormatVideo = "-c:v libx264 -preset " + VideoPreset + " -qp 0";
	} else if (VideoCodec == "SVT-AV1") {
		VideoPreset = AV1Presets(VideoPreset);
		FormatVideo = "-c:v libsvtav1 -crf " + QString::number(VideoQuality);
		if (VideoPreset != "") FormatVideo += " -preset " + VideoPreset;
		FormatVideo += " -svtav1-params enable-overlays=1:enable-tf=1:enable-variance-boost=1:tune=0:keyint=" +
					   QString::number(KeyInt);
	} else if (VideoCodec == "VP9") {
		FormatVideo = "-c:v libvpx-vp9 -crf " + QString::number(VideoQuality) + " -b:v 0 -row-mt 1";
	} else if (VideoCodec == "VP9 lossless") {
		FormatVideo = "-c:v libvpx-vp9 -lossless 1 -row-mt 1";
	} else if (VideoCodec == "FFV1") {
		FormatVideo = "-vcodec ffv1 -slicecrc 0";
	} else if (VideoCodec == "Xvid") {
		FormatVideo = "-c:v libxvid -qscale:v " + QString::number(VideoQuality);
	} else if (VideoCodec == "webp") {
		FormatVideo = "-vcodec libwebp -lossless 0 -compression_level 6 -q:v " + QString::number(VideoQuality);
	} else if (VideoCodec == "VAAPI H264 (linux)") {
		InitHWEncode = "-vaapi_device " + GetVAAPIDevice();
		FormatVideo = "-c:v h264_vaapi -qp " + QString::number(VideoQuality);
	} else if (VideoCodec == "VAAPI H265 (linux)") {
		InitHWEncode = "-vaapi_device " + GetVAAPIDevice();
		FormatVideo = "-c:v hevc_vaapi -qp " + QString::number(VideoQuality);
	} else if (VideoCodec == "NVENC H264") {
		InitHWEncode = "-hwaccel_output_format cuda";
		FormatVideo = "-c:v h264_nvenc -preset " + VideoPreset +
					  " -cq " + QString::number(VideoQuality) + "-profile:v high";
	} else if (VideoCodec == "NVENC H265") {
		InitHWEncode = "-hwaccel_output_format cuda";
		FormatVideo = "-c:v hevc_nvenc -preset " + VideoPreset +
					  " -cq " + QString::number(VideoQuality);
	} else if (VideoCodec == "Intel QSV H264") {
		FormatVideo = "-c:v h264_qsv -global_quality " + QString::number(VideoQuality) + " -preset " + VideoPreset;
	} else if (VideoCodec == "Intel QSV H265") {
		FormatVideo = "-c:v hevc_qsv -global_quality " + QString::number(VideoQuality) + " -preset " + VideoPreset;
	}

	QString Metadata = "-map_metadata:g -1 -map_metadata:s:v -1";
	if (AudioCodec != "None") Metadata += " -map_metadata:s:a 0:s:a";

	//TODO: Remove others tags
	//		Metadata += " -fflags +bitexact -flags:v +bitexact -flags:a +bitexact";

	if (MetadataTitle != "")
		Metadata += " -metadata \"TITLE=" + MetadataTitle + "\"";

	if (MetadataStudio != "")
		Metadata += " -metadata \"PRODUCTION_STUDIO=" + MetadataStudio + "\"";

	if (MetadataActors != "")
		Metadata += " -metadata \"ACTOR=" + MetadataActors + "\"";

	if (MetadataReleaseDate != "" && MetadataReleaseDate != "2000-01-01")
		Metadata += " -metadata \"DATE_RELEASED=" + MetadataReleaseDate + "\"";


	if (OutFormat == "mkv" || OutFormat == "webm") {
		//		Metadata += " -write_crc32 false";
		QString Comment = "Made with VR2Normal " + QString(VR2NORMAL_VERSION);
		Metadata += " -metadata \"COMMENT=" + Comment + "\"";
		Metadata += " -metadata:s:v \"COMMENT=" + Comment + "\"";
		Metadata += " -metadata \"URL=https://gitlab.com/vongooB9/vr2normal\"";
		Metadata += " -metadata:s:v \"URL=https://gitlab.com/vongooB9/vr2normal\"";
		Metadata +=	" -metadata:s \"VENDOR_ID=\" -metadata:s \"HANDLER_NAME=\"";
	} else {
		Metadata += " -metadata \"COMMENT=Made with VR2Normal (https://gitlab.com/vongooB9/vr2normal)\"";
	}

	if (FileAttach != "" && OutFormat == "mkv") {
		Metadata += " -attach \"" + FileAttach + "\"" +
					" -metadata:s:t:0 mimetype=text/plain -metadata:s:t:0 \"filename=VR2Normal file.vr2n\"";
	}

	if (ChaptersFile != "")
		Metadata += " -map_chapters 1";

	if (OutFormat == "mp4")
		Metadata += " -movflags faststart";

	if (OutFormat == "ts" || OutFormat == "tmp") {
		Metadata = "";
		FormatVideo += " -f mpegts";
	}

	if (OutFormat == "mkv.tmp") {
		Metadata = "";
		FormatVideo += " -f matroska";
	}

	QString FormatAudio;
	if (AudioCodec == "Copy") {
		FormatAudio = "-c:a copy";
	} else if (AudioCodec == "MP3") {
		FormatAudio = "-c:a libmp3lame -b:a " + QString::number(AudioQuality) + "K";
	} else if (AudioCodec == "AAC") {
		FormatAudio = "-c:a aac -b:a " + QString::number(AudioQuality) + "K";
	} else if (AudioCodec == "Vorbis") {
		FormatAudio = "-c:a libvorbis -b:a " + QString::number(AudioQuality) + "K";
	} else if (AudioCodec == "Opus") {
		FormatAudio = "-c:a libopus -b:a " + QString::number(AudioQuality) + "K";
	}
	if (Start != "" && AudioCodec != "None" && AudioCodec != "Copy" && FilterPTS)
		FormatAudio += " -af asetpts=PTS-STARTPTS";

	QString Format = FormatVideo + " " + FormatAudio;
	if (Metadata != "") Format += " " + Metadata;

	QString Result = Template;

	if (Ffmpeg.contains(" ")) Ffmpeg = "\"" + Ffmpeg + "\"";

	if (Result.contains("%FFMPEG%")) Result.replace("%FFMPEG%", Ffmpeg);
	if (Result.contains("%FFMPEG_GUI_OPTIONS%")) Result.replace("%FFMPEG_GUI_OPTIONS%", FfmpegGuiOptions);
	if (Result.contains("%HWDECODE%")) Result.replace("%HWDECODE%", HWDecode);
	if (Result.contains("%INITHWENCODE%")) Result.replace("%INITHWENCODE%", InitHWEncode);
	if (Result.contains("%START_TIME%")) Result.replace("%START_TIME%", Start);
	if (Result.contains("%INPUT%")) Result.replace("%INPUT%", InputParam);
	if (Result.contains("%TO_TIME%")) Result.replace("%TO_TIME%", To);
	if (Result.contains("%MAP%")) Result.replace("%MAP%", Map);
	if (Result.contains("%SIMPLE_FILTER%")) Result.replace("%SIMPLE_FILTER%", SimpleFilter);
	if (Result.contains("%RAW_FILTER%")) Result.replace("%RAW_FILTER%", RawFilters);
	if (Result.contains("%FORMAT%")) Result.replace("%FORMAT%", Format);
	if (Result.contains("%VIDEO_FORMAT%")) Result.replace("%VIDEO_FORMAT%", FormatVideo);
	if (Result.contains("%AUDIO_FORMAT%")) Result.replace("%AUDIO_FORMAT%", FormatAudio);
	if (Result.contains("%VIDEO_QUALITY%")) Result.replace("%VIDEO_QUALITY%", QString::number(VideoQuality));
	if (Result.contains("%VIDEO_PRESET%")) Result.replace("%VIDEO_PRESET%", VideoPreset);
	if (Result.contains("%VIDEO_CODEC%")) Result.replace("%VIDEO_CODEC%", VideoCodec);
	if (Result.contains("%AUDIO_QUALITY%")) Result.replace("%AUDIO_QUALITY%", QString::number(AudioQuality));
	if (Result.contains("%AUDIO_CODEC%")) Result.replace("%AUDIO_CODEC%", AudioCodec);
	if (Result.contains("%METADATA%")) Result.replace("%METADATA%", Metadata);
	if (Result.contains("%OUTPUT%")) Result.replace("%OUTPUT%", "\"" + QDir::toNativeSeparators(Output) + "\"");
	if (Result.contains("%OUT_FILE_NAME%")) Result.replace("%OUT_FILE_NAME%",
				QDir::toNativeSeparators(OutFileInfo.path() + QDir::separator() + OutFileInfo.baseName()));

	return Result;
}

QString GetVAAPIDevice() {
	if (QFile::exists("/dev/dri/renderD128")) return "/dev/dri/renderD128";
	if (QFile::exists("/dev/dri/renderD129")) return "/dev/dri/renderD129";
	if (QFile::exists("/dev/dri/renderD130")) return "/dev/dri/renderD130";
	if (QFile::exists("/dev/dri/renderD131")) return "/dev/dri/renderD131";
	return "";
}

QString GetTemplate(QString Format) {
	if (Format == "gif") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %START_TIME% %INPUT% %TO_TIME% -map 0:v:0 -filter_complex \"[0:v] %RAW_FILTER%,split [a][b]; [a] palettegen=stats_mode=single [p]; [b][p] paletteuse=new=1\" %OUTPUT%";
	}
	if (Format == "NVENC H264") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %INITHWENCODE% %START_TIME% %INPUT% %TO_TIME% %MAP% %SIMPLE_FILTER% %FORMAT% %OUTPUT%";
	}
	if (Format == "NVENC H265") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %INITHWENCODE% %START_TIME% %INPUT% %TO_TIME% %MAP% %SIMPLE_FILTER% %FORMAT% %OUTPUT%";
	}
	if (Format == "VAAPI H264 (linux)") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %INITHWENCODE% %START_TIME% %INPUT% %TO_TIME% %MAP% -vf \"%RAW_FILTER%,format=nv12,hwupload\" %FORMAT% %OUTPUT%";
	}
	if (Format == "VAAPI H265 (linux)") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %INITHWENCODE% %START_TIME% %INPUT% %TO_TIME% %MAP% -vf \"%RAW_FILTER%,format=nv12,hwupload\" %FORMAT% %OUTPUT%";
	}
	if (Format == "Intel QSV H264") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %START_TIME% %INPUT% %TO_TIME% %MAP% -vf \"%RAW_FILTER%,hwupload=extra_hw_frames=64,format=qsv\" %FORMAT% %OUTPUT%";
	}
	if (Format == "Intel QSV H265") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %START_TIME% %INPUT% %TO_TIME% %MAP% -vf \"%RAW_FILTER%,hwupload=extra_hw_frames=64,format=qsv\" %FORMAT% %OUTPUT%";
	}

	return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %HWDECODE% %START_TIME% %INPUT% %TO_TIME% %MAP% %SIMPLE_FILTER% %FORMAT% %OUTPUT%";
}

QString GetExtension(QString Format) {
	QString Result = ".mkv";
	if (Format == "gif") Result = ".gif";
	if (Format == "webp") Result = ".webp";
	return Result;
}

QString AV1Presets(QString Input) {
	if (Input == "ultrafast") return "13";
	if (Input == "superfast") return "11";
	if (Input == "veryfast") return "10";
	if (Input == "faster") return "9";
	if (Input == "fast") return "8";
	if (Input == "medium") return "6";
	if (Input == "slow") return "5";
	if (Input == "slower") return "4";
	if (Input == "veryslow") return "2";
	if (Input == "placebo") return "0";
	return "";
}

QRegularExpression FfmpegProgress(
			"^frame=[[:space:]]*([0-9.]+)[[:space:]]+fps=[[:space:]]*([0-9.]+)[[:space:]]+q=[[:space:]]*([0-9.-]+)[[:space:]]*[[:alpha:]]+=[[:space:]]*([[:graph:]]+)[[:space:]]+time=([0-9:.]+)[[:space:]]+bitrate=[[:space:]]*([0-9.]*)[[:graph:]]+[[:space:]]+(?:dup=[0-9.]+[[:space:]]+drop=[0-9.]+[[:space:]]+|)speed=[[:space:]]*([0-9.]+)x[[:space:]]*$");

QRegularExpression FfmpegShortProgress(
			"^[[:alpha:]]+=[[:space:]]*([[:graph:]]+)[[:space:]]+time=([0-9:.]+)[[:space:]]+bitrate=[[:space:]]*([0-9.]*)[[:graph:]]+[[:space:]]+(?:dup=[0-9.]+[[:space:]]+drop=[0-9.]+[[:space:]]+|)speed=[[:space:]]*([0-9.]+)x[[:space:]]*$");

QRegularExpression BlackFrames(
			"^\\[Parsed_blackframe_[[:digit:]]+[[:blank:]]+\\@[[:blank:]]+[[:xdigit:]x]+\\][[:blank:]]+frame:([[:digit:]]+)[[:blank:]]+pblack:([[:digit:]]+)[[:blank:]]+pts:([[:digit:]]+)[[:blank:]]+t:([0-9.]+)[[:blank:]]+type:([[:alpha:]]+)[[:blank:]]+last_keyframe:([[:digit:]]+)$",
			QRegularExpression::MultilineOption);

void UpdateProgress(QProcess *Process, ffmpeg_progress_t *Output, QString *Buffer, QString *ErrorBuffer,
					bool SearchBlackFrames) {
	QString Lines = Process->readAllStandardError();
	Lines.replace("\r", "\n");
	Buffer->append(Lines);

	QStringList BufferLines = Buffer->split("\n", Qt::KeepEmptyParts);
	if (BufferLines.last() != "") {
		Buffer->clear();
		Buffer->append(BufferLines.last());
		BufferLines.removeLast();
	} else {
		Buffer->clear();
	}

	for (int i = 0; i < BufferLines.count(); i++) {
		QString input = BufferLines.at(i);
		if (input == "") continue;

		QString FPS;
		QString RawSize;
		QString CT;
		QString BitRate;
		QString Speed;
		bool ProgressMatch = false;

		QRegularExpressionMatch Match = FfmpegProgress.match(input);
		if (Match.hasMatch()) {
			FPS = Match.captured(2);
			RawSize = Match.captured(4);
			CT = Match.captured(5);
			BitRate = Match.captured(6);
			Speed = Match.captured(7);
			ProgressMatch = true;
		}

		QRegularExpressionMatch ShortMatch = FfmpegShortProgress.match(input);
		if (ShortMatch.hasMatch()) {
			FPS = "0";
			RawSize = ShortMatch.captured(1);
			CT = ShortMatch.captured(2);
			BitRate = ShortMatch.captured(3);
			Speed = ShortMatch.captured(4);
			ProgressMatch = true;
		}

		if (ProgressMatch) {

			Output->CurrentTime = QTime::fromString(CT.trimmed() + "0", "HH:mm:ss.zzz");
			Output->FPS = FPS.toDouble();

			Output->Size = 0;
			Output->Bitrate = 0;
			if (RawSize != "N/A") {
				if (RawSize.endsWith("iB", Qt::CaseInsensitive))
					RawSize.chop(3);
				else
					RawSize.chop(2);
				Output->Size = RawSize.toInt() * 1024;
				Output->Bitrate = BitRate.toDouble();
			}
			Output->Speed = Speed.toDouble();
			Output->EstimatedSize = (Output->TotalTime.msecsSinceStartOfDay() * Output->Bitrate) / 8;

			QTime RemainingLength = QTime::fromMSecsSinceStartOfDay(
												Output->TotalTime.msecsSinceStartOfDay() - Output->CurrentTime.msecsSinceStartOfDay());
			int RemainingLengthMS = RemainingLength.msecsSinceStartOfDay();
			int ETRMS = 0;
			if (Output->Speed > 0) ETRMS = RemainingLengthMS / Output->Speed;
			int Days = ETRMS / 86400000;
			if (Days > 0)
				Output->ETR = QString::number(Days) + " days " + QTime::fromMSecsSinceStartOfDay(ETRMS % 86400000).toString("H:mm:ss");
			else
				Output->ETR = QTime::fromMSecsSinceStartOfDay(ETRMS).toString("H:mm:ss");

			continue;
		}

		if (SearchBlackFrames) {
			Match = BlackFrames.match(input);
			if (Match.hasMatch()) {
				Output->BlackFrames.append(input.trimmed() + "\n");
				continue;
			}
		}

		ErrorBuffer->append(input + "\n");
	}
}

QString ColorChannel2Hex(uint8_t Channel) {
	QString Result = QString::number(Channel, 16);
	if (Result.length() == 1) Result = "0" + Result;
	return Result;
}

QString SelectColor(QString Text, QString CurrentColor) {
	QColor Color = QColorDialog::getColor(CurrentColor, nullptr, Text);
	if (!Color.isValid()) return CurrentColor;
	int r, g, b, a;
	Color.getRgb(&r, &g, &b, &a);
	QString ColorStr = ColorChannel2Hex(r) + ColorChannel2Hex(g) + ColorChannel2Hex(b);
	return ColorStr;
}
