#ifndef COMMON_H
#define COMMON_H
#include <QString>
#include <QTime>
#include <QDateTime>
#include <QProcess>
#include <QRegularExpression>

void WriteFile(QString Filename, QString Content);
void AppendFile(QString Filename, QString Content);
QString ReadFile(QString Filename);
QString ScapeFFFilters(QString in);

QString v360Filters(QString V360Format, QString V360Custom, QString V360Mode, QString OutMode, double V360IVFOV,
					double V360IHFOV,
					double V360DFOV, double V360Pitch, double V360Yaw, double V360Roll, double V360VOffset, double V360HOffset,
					bool V360RightEye, int V360W, int V360H, QString V360Interp, bool V360ResetRot, int PadW, int PadH, int PadX, int PadY);

QString ComposeFfmpegCommand(QString Ffmpeg, QString Input, QString Output, QString Template, QString VideoCodec,
							 int VideoQuality, QString VideoPreset, QString AudioCodec, int AudioQuality, QString Start, QString End, double FPS,
							 double OriginalFPS, QString V360Format, QString V360Custom, QString V360Mode, QString OutMode, double V360IVFOV,
							 double V360IHFOV, double V360DFOV, double V360Pitch, double V360Yaw, double V360Roll, double V360VOffset,
							 double V360HOffset, bool V360RightEye, int V360W, int V360H, QString V360Interp, bool V360ResetRot, int PadW, int PadH,
							 int PadX, int PadY, QString SendCMDDFile, QString Filters, QString PixelFormat, QString FileAttach = "",
							 QString MetadataTitle = "", QString MetadataStudio = "", QString MetadataActors = "", QString MetadataReleaseDate = "",
							 QString HWDec = "", QString ChaptersFile = "", QString CustomFilters = "", QString CustomInputFilters = "",
							 bool FilterPTS = false, QString OutFormat = "", int KeyInt = 0);

QString AV1Presets(QString Input);

QString GetVAAPIDevice();

QString GetTemplate(QString Format);

QString GetExtension(QString Format);

struct ffmpeg_progress_t {
	QTime CurrentTime;
	QTime TotalTime;
	QString ETR;
	double FPS;
	long long Size;
	long long EstimatedSize;
	double Bitrate;
	double Speed;
	QString BlackFrames;
	QDateTime ExecutionStartTime;
};

extern QRegularExpression FfmpegProgress;
extern QRegularExpression BlackFrames;

void UpdateProgress(QProcess *Process, ffmpeg_progress_t *Output, QString *Buffer, QString *ErrorBuffer,
					bool SearchBlackFrames = false);

QString ColorChannel2Hex(uint8_t Channel);
QString SelectColor(QString Text, QString CurrentColor);

#endif // COMMON_H
