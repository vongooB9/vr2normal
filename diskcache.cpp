#include "diskcache.h"
#include <QDir>
#include <QFileInfo>
#include <QFileInfoList>
#include <QDir>
#include <QDirIterator>

DiskCacheWorker::DiskCacheWorker(QObject *parent)
	: QObject{parent}, MaxSize{0} {

}

void DiskCacheWorker::CleanDiskCache(qint64 size) {
	if (MaxSize == 0) return;
	QDir Dir(CacheDir);
	QFileInfoList List = Dir.entryInfoList(QDir::Files | QDir::NoDot | QDir::NoDotDot, QDir::Time | QDir::Reversed);
	qint64 DeletedSize = 0;
	for (int i = 0; i < List.count(); ++i) {
		if (size > 0 && DeletedSize >= size) return;
		QFileInfo file = List.value(i);
		DeletedSize += file.size();
		QFile::remove(file.filePath());
	}
}

void DiskCacheWorker::EmptyDiskCache() {
	CleanDiskCache(0);
	emit CurrentCacheSize(0);
	emit CurrentCacheSizeString("0");
}

void DiskCacheWorker::SetMaxSize(int size) {
	MaxSize = size * 1024 * 1024;
}

void DiskCacheWorker::SetCacheDir(QString Dir) {
	CacheDir = Dir;
}

void DiskCacheWorker::CheckCache() {
	if (MaxSize == 0) return;
	qint64 total = 0;
	QDirIterator it(CacheDir);
	while (it.hasNext()) {
		it.next();
		total += it.fileInfo().size();
	}
	if (total > MaxSize) {
		CleanDiskCache((total - MaxSize) + (MaxSize * 0.05));
	} else {
		emit CurrentCacheSize(total);
		emit CurrentCacheSizeString(QString::number((double)total / 1024 / 1024, 'f', 2));
	}
}

