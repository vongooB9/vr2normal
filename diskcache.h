#ifndef DISKCACHE_H
#define DISKCACHE_H

#include <QObject>

class DiskCacheWorker : public QObject {
		Q_OBJECT
	public:
		explicit DiskCacheWorker(QObject *parent = nullptr);

	private:
		qint64 MaxSize;
		QString CacheDir;

	signals:
		void CurrentCacheSize(qint64 Size);
		void CurrentCacheSizeString(QString Size);

	public slots:
		void CleanDiskCache(qint64 size);
		void EmptyDiskCache();
		void SetMaxSize(int size);
		void SetCacheDir(QString Dir);
		void CheckCache();
};

#endif // DISKCACHE_H
