#include "filters.h"
#include <QStringList>

exposure_t::exposure_t(QString String) : exposure_t() {
	this->fromString(String);
}

QString exposure_t::toString() {
	QStringList Options;
	if (this->exposure != DEFAULT_EXPOSURE) Options.append("exposure=" + QString::number(this->exposure, 'f', 2));
	if (this->black != DEFAULT_BLACK) Options.append("black=" + QString::number(this->black, 'f', 2));
	QString Result = "exposure";
	if (Options.count() > 0) Result += "=" + Options.join(":");
	return Result;
}

void exposure_t::fromString(QString String) {
	bool ok;
	this->Valid = false;
	this->exposure = DEFAULT_EXPOSURE;
	this->black = DEFAULT_BLACK;
	QStringList FiltersList = String.split(",");
	for (int i = 0; i < FiltersList.count(); ++i) {
		QString FilterString = FiltersList.at(i);
		if (FilterString.section("=", 0, 0) != "exposure") continue;
		this->Valid = true;
		QString OptionsString = FilterString.section("=", 1);
		if (OptionsString == "") continue;
		QStringList OptionsList = OptionsString.split(":");
		for (int i2 = 0; i2 < OptionsList.count(); ++i2) {
			QString Option = OptionsList.at(i2);
			QString OptionName = Option.section("=", 0, 0);
			if (OptionName == "exposure") {
				this->exposure = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->exposure = DEFAULT_EXPOSURE;
			}
			if (OptionName == "black") {
				this->black = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->black = DEFAULT_BLACK;
			}
		}
	}
}

colortemperature_t::colortemperature_t(QString String) : colortemperature_t() {
	this->fromString(String);
}

QString colortemperature_t::toString() {
	QStringList Options;
	if (this->temperature != DEFAULT_TEMPERATURE)
		Options.append("temperature=" + QString::number(this->temperature));
	if (this->mix != DEFAULT_MIX) Options.append("mix=" + QString::number(this->mix, 'f', 2));
	if (this->pl != DEFAULT_PL) Options.append("pl=" + QString::number(this->pl, 'f', 2));
	QString Result = "colortemperature";
	if (Options.count() > 0) Result += "=" + Options.join(":");
	return Result;
}

void colortemperature_t::fromString(QString String) {
	bool ok;
	this->Valid = false;
	this->temperature = DEFAULT_TEMPERATURE;
	this->mix = DEFAULT_MIX;
	this->pl = DEFAULT_PL;
	QStringList FiltersList = String.split(",");
	for (int i = 0; i < FiltersList.count(); ++i) {
		QString FilterString = FiltersList.at(i);
		if (FilterString.section("=", 0, 0) != "colortemperature") continue;
		this->Valid = true;
		QString OptionsString = FilterString.section("=", 1);
		if (OptionsString == "") continue;
		QStringList OptionsList = OptionsString.split(":");
		for (int i2 = 0; i2 < OptionsList.count(); ++i2) {
			QString Option = OptionsList.at(i2);
			QString OptionName = Option.section("=", 0, 0);
			if (OptionName == "temperature") {
				this->temperature = Option.section("=", 1).toInt(&ok);
				if (!ok) this->temperature = DEFAULT_TEMPERATURE;
			}
			if (OptionName == "mix") {
				this->mix = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->mix = DEFAULT_MIX;
			}
			if (OptionName == "pl") {
				this->pl = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->pl = DEFAULT_PL;
			}
		}
	}
}

eq_t::eq_t(QString String) : eq_t() {
	this->fromString(String);
}

QString eq_t::toString() {
	QStringList Options;
	if (this->contrast != DEFAULT_CONTRAST) Options.append("contrast=" + QString::number(this->contrast, 'f', 2));
	if (this->brightness != DEFAULT_BRIGHTNESS) Options.append("brightness=" + QString::number(this->brightness, 'f', 2));
	if (this->saturation != DEFAULT_SATURATION) Options.append("saturation=" + QString::number(this->saturation, 'f', 2));
	if (this->gamma != DEFAULT_GAMMA) Options.append("gamma=" + QString::number(this->gamma, 'f', 2));
	if (this->gamma_r != DEFAULT_GAMMA_R) Options.append("gamma_r=" + QString::number(this->gamma_r, 'f', 2));
	if (this->gamma_g != DEFAULT_GAMMA_G) Options.append("gamma_g=" + QString::number(this->gamma_g, 'f', 2));
	if (this->gamma_b != DEFAULT_GAMMA_B) Options.append("gamma_b=" + QString::number(this->gamma_b, 'f', 2));
	if (this->gamma_weight != DEFAULT_GAMMA_WEIGHT)
		Options.append("gamma_weight=" + QString::number(this->gamma_weight, 'f', 2));
	QString Result = "eq";
	if (Options.count() > 0) Result += "=" + Options.join(":");
	return Result;
}

void eq_t::fromString(QString String) {
	bool ok;
	this->Valid = false;
	this->contrast = DEFAULT_CONTRAST;
	this->brightness = DEFAULT_BRIGHTNESS;
	this->saturation = DEFAULT_SATURATION;
	this->gamma = DEFAULT_GAMMA;
	this->gamma_r = DEFAULT_GAMMA_R;
	this->gamma_g = DEFAULT_GAMMA_G;
	this->gamma_b = DEFAULT_GAMMA_B;
	this->gamma_weight = DEFAULT_GAMMA_WEIGHT;
	QStringList FiltersList = String.split(",");
	for (int i = 0; i < FiltersList.count(); ++i) {
		QString FilterString = FiltersList.at(i);
		if (FilterString.section("=", 0, 0) != "eq") continue;
		this->Valid = true;
		QString OptionsString = FilterString.section("=", 1);
		if (OptionsString == "") continue;
		QStringList OptionsList = OptionsString.split(":");
		for (int i2 = 0; i2 < OptionsList.count(); ++i2) {
			QString Option = OptionsList.at(i2);
			QString OptionName = Option.section("=", 0, 0);
			if (OptionName == "contrast") {
				this->contrast = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->contrast = DEFAULT_CONTRAST;
			}
			if (OptionName == "brightness") {
				this->brightness = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->brightness = DEFAULT_BRIGHTNESS;
			}
			if (OptionName == "saturation") {
				this->saturation = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->saturation = DEFAULT_SATURATION;
			}
			if (OptionName == "gamma") {
				this->gamma = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->gamma = DEFAULT_GAMMA;
			}
			if (OptionName == "gamma_r") {
				this->gamma_r = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->gamma_r = DEFAULT_GAMMA_R;
			}
			if (OptionName == "gamma_g") {
				this->gamma_g = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->gamma_g = DEFAULT_GAMMA_G;
			}
			if (OptionName == "gamma_b") {
				this->gamma_b = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->gamma_b = DEFAULT_GAMMA_B;
			}
			if (OptionName == "gamma_weight") {
				this->gamma_weight = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->gamma_weight = DEFAULT_GAMMA_WEIGHT;
			}
		}
	}
}

colorbalance_t::colorbalance_t(QString String) {
	this->fromString(String);
}

QString colorbalance_t::toString() {
	QStringList Options;
	if (this->rs != DEFAULT_CB_VALUE) Options.append("rs=" + QString::number(this->rs, 'f', 2));
	if (this->gs != DEFAULT_CB_VALUE) Options.append("gs=" + QString::number(this->gs, 'f', 2));
	if (this->bs != DEFAULT_CB_VALUE) Options.append("bs=" + QString::number(this->bs, 'f', 2));
	if (this->rm != DEFAULT_CB_VALUE) Options.append("rm=" + QString::number(this->rm, 'f', 2));
	if (this->gm != DEFAULT_CB_VALUE) Options.append("gm=" + QString::number(this->gm, 'f', 2));
	if (this->bm != DEFAULT_CB_VALUE) Options.append("bm=" + QString::number(this->bm, 'f', 2));
	if (this->rh != DEFAULT_CB_VALUE) Options.append("rh=" + QString::number(this->rh, 'f', 2));
	if (this->gh != DEFAULT_CB_VALUE) Options.append("gh=" + QString::number(this->gh, 'f', 2));
	if (this->bh != DEFAULT_CB_VALUE) Options.append("bh=" + QString::number(this->bh, 'f', 2));
	if (pl != DEFAULT_CB_PL) Options.append("pl=" + QString::number(pl));
	QString Result = "colorbalance";
	if (Options.count() > 0) Result += "=" + Options.join(":");
	return Result;
}

void colorbalance_t::fromString(QString String) {
	bool ok;
	this->Valid = false;
	this->rs = DEFAULT_CB_VALUE;
	this->gs = DEFAULT_CB_VALUE;
	this->bs = DEFAULT_CB_VALUE;
	this->rm = DEFAULT_CB_VALUE;
	this->gm = DEFAULT_CB_VALUE;
	this->bm = DEFAULT_CB_VALUE;
	this->rh = DEFAULT_CB_VALUE;
	this->gh = DEFAULT_CB_VALUE;
	this->bh = DEFAULT_CB_VALUE;
	this->pl = false;
	QStringList FiltersList = String.split(",");
	for (int i = 0; i < FiltersList.count(); ++i) {
		QString FilterString = FiltersList.at(i);
		if (FilterString.section("=", 0, 0) != "colorbalance") continue;
		this->Valid = true;
		QString OptionsString = FilterString.section("=", 1);
		if (OptionsString == "") continue;
		QStringList OptionsList = OptionsString.split(":");
		for (int i2 = 0; i2 < OptionsList.count(); ++i2) {
			QString Option = OptionsList.at(i2);
			QString OptionName = Option.section("=", 0, 0);
			if (OptionName == "rs") {
				this->rs = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->rs = DEFAULT_CB_VALUE;
			}
			if (OptionName == "gs") {
				this->gs = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->gs = DEFAULT_CB_VALUE;
			}
			if (OptionName == "bs") {
				this->bs = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->bs = DEFAULT_CB_VALUE;
			}
			if (OptionName == "rm") {
				this->rm = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->rm = DEFAULT_CB_VALUE;
			}
			if (OptionName == "gm") {
				this->gm = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->gm = DEFAULT_CB_VALUE;
			}
			if (OptionName == "bm") {
				this->bm = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->bm = DEFAULT_CB_VALUE;
			}
			if (OptionName == "rh") {
				this->rh = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->rh = DEFAULT_CB_VALUE;
			}
			if (OptionName == "gh") {
				this->gh = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->gh = DEFAULT_CB_VALUE;
			}
			if (OptionName == "bh") {
				this->bh = Option.section("=", 1).toDouble(&ok);
				if (!ok) this->bh = DEFAULT_CB_VALUE;
			}
			if (OptionName == "pl") {
				this->pl = Option.section("=", 1).toInt(&ok) == 1;
				if (!ok) this->pl = DEFAULT_CB_PL;
			}
		}
	}
}
