#ifndef FILTERS_H
#define FILTERS_H

#include <QString>

#define DEFAULT_EXPOSURE 0
#define DEFAULT_BLACK 0

struct exposure_t {
	bool Valid;
	double exposure;
	double black;
	exposure_t():
		Valid(false),
		exposure(DEFAULT_EXPOSURE),
		black(DEFAULT_BLACK) {};
	exposure_t(QString String);
	QString toString();
	void fromString(QString String);
};

#define DEFAULT_TEMPERATURE 6500
#define DEFAULT_MIX 1
#define DEFAULT_PL 0

struct colortemperature_t {
	bool Valid;
	int temperature;
	double mix;
	double pl;
	colortemperature_t():
		Valid(false),
		temperature(DEFAULT_TEMPERATURE),
		mix(DEFAULT_MIX),
		pl(DEFAULT_PL) {};
	colortemperature_t(QString String);
	QString toString();
	void fromString(QString String);
};

#define DEFAULT_CONTRAST 1
#define DEFAULT_BRIGHTNESS 0
#define DEFAULT_SATURATION 1
#define DEFAULT_GAMMA 1
#define DEFAULT_GAMMA_R 1
#define DEFAULT_GAMMA_G 1
#define DEFAULT_GAMMA_B 1
#define DEFAULT_GAMMA_WEIGHT 1

struct eq_t {
	bool Valid;
	double contrast;
	double brightness;
	double saturation;
	double gamma;
	double gamma_r;
	double gamma_g;
	double gamma_b;
	double gamma_weight;
	eq_t():
		Valid(false),
		contrast(DEFAULT_CONTRAST),
		brightness(DEFAULT_BRIGHTNESS),
		saturation(DEFAULT_SATURATION),
		gamma(DEFAULT_GAMMA),
		gamma_r(DEFAULT_GAMMA_R),
		gamma_g(DEFAULT_GAMMA_G),
		gamma_b(DEFAULT_GAMMA_B),
		gamma_weight(DEFAULT_GAMMA_WEIGHT) {};
	eq_t(QString String);
	QString toString();
	void fromString(QString String);
};

#define DEFAULT_CB_VALUE 0
#define DEFAULT_CB_PL false
struct colorbalance_t {
	bool Valid;
	double rs;
	double gs;
	double bs;
	double rm;
	double gm;
	double bm;
	double rh;
	double gh;
	double bh;
	bool pl;
	colorbalance_t():
		Valid(false),
		rs(DEFAULT_CB_VALUE),
		gs(DEFAULT_CB_VALUE),
		bs(DEFAULT_CB_VALUE),
		rm(DEFAULT_CB_VALUE),
		gm(DEFAULT_CB_VALUE),
		bm(DEFAULT_CB_VALUE),
		rh(DEFAULT_CB_VALUE),
		gh(DEFAULT_CB_VALUE),
		bh(DEFAULT_CB_VALUE),
		pl(DEFAULT_CB_PL) {};
	colorbalance_t(QString String);
	QString toString();
	void fromString(QString String);
};

#endif // FILTERS_H
