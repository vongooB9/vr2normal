#include "generatesplits.h"
#include "common.h"
#include <QFile>
#include <QDir>
#include <QCryptographicHash>
#include <QApplication>

GenerateSplitsThread::GenerateSplitsThread(QObject *parent)
	: QThread{parent} {
	Filters = "scale=854:480,drawtext=";
	#ifdef Q_OS_WIN
	Filters += "fontfile='" + ScapeFFFilters(QApplication::applicationDirPath() + "/Fonts/DejaVuSansMono.ttf") + "':";
	#endif
	Filters += "text='%{e\\:print(t)}':"
			   "fontsize=18:"
			   "fontcolor=0xFFFFFFFF:"
			   "box=1:"
			   "boxborderw=4:"
			   "boxcolor=0x000000FF:"
			   "borderw=0:"
			   "bordercolor=000000:"
			   "shadowcolor=000000:"
			   "shadowx=0:"
			   "shadowy=0:"
			   "x=w-tw-6:"
			   "y=6";
}

void GenerateSplitsThread::run() {
	Canceled = false;
	for (int i = 0; i < NumberOfChunks; ++i) {
		if (Canceled) break;

		int ms = i * SplitSeconds * 1000;
		if (ms == 0) {
			emit NewSplit(QTime::fromMSecsSinceStartOfDay(0), "", "", "", "");
			continue;
		}

		if (ms > VideoLength) break;

		if (GenNext(ms)) continue;
		if (Canceled) break;

		if (GenNext(ms + 1000)) continue;
		if (Canceled) break;

		if (GenNext(ms + 2000)) continue;
		if (Canceled) break;

		if (GenNext(ms + 3000)) continue;
		if (Canceled) break;

		if (GenNext(ms + 5000)) continue;
		if (Canceled) break;

		emit UpdateText("Getting accurate frame times");
		QString Before, After;
		GetFrameAcurateCut(ms, Before, After);
		if (Canceled) break;

		if (GenNext(ms, Before, After, "0", "0")) continue;
		if (Canceled) break;

		if (GenNext(ms, Before, After, "+1/3", "-1/3")) continue;
		if (Canceled) break;

		if (GenNext(ms, Before, After, "+1/2", "0")) continue;
		if (Canceled) break;

		if (GenNext(ms, Before, After, "0", "-1/2")) continue;
		if (Canceled) break;
	}
}

bool GenerateSplitsThread::GenNext(int ms) {
	if (ms > VideoLength) return true;
	QString mss = QString::number((double)ms / 1000);
	emit UpdateText("Testing " + mss);
	if (Test(mss, mss)) {
		emit NewSplit(QTime::fromMSecsSinceStartOfDay(ms), mss, mss, mss, mss);
		return true;
	}
	return false;
}

bool GenerateSplitsThread::GenNext(int ms, QString B, QString A, QString BA, QString AA) {
	QString BeforeAdjusted = B;
	QString AfterAdjusted = A;
	if (BA != "0")
		BeforeAdjusted = QString::number(B.toDouble() + AdjustTime(BA, FrameTime), 'f', 6);
	if (AA != "0")
		AfterAdjusted = QString::number(A.toDouble() + AdjustTime(AA, FrameTime), 'f', 6);

	emit UpdateText("Testing " + BeforeAdjusted + " - " + AfterAdjusted);
	if (Test(BeforeAdjusted, AfterAdjusted)) {
		emit NewSplit(QTime::fromMSecsSinceStartOfDay(ms), B, A, BeforeAdjusted, AfterAdjusted);
		return true;
	}
	return false;
}

int GenerateSplitsThread::SetOptions(QString ffmpegbin, QString ffprobebin, QString cachedir, QString input,
									 QString inputhash,
									 int videolength, double fps,
									 int splitseconds, QString tmpdirectory, bool enabledebug, QString errorcolor) {
	Ffmpeg = ffmpegbin;
	Ffprobe = ffprobebin;
	CacheDir = cachedir;
	InputFile = input;
	InputHash = inputhash;
	VideoLength = videolength;
	Fps = fps;
	FrameTime = (1 / fps);
	SplitSeconds = splitseconds;
	TmpDir = tmpdirectory;
	DebugEnabled = enabledebug;
	NumberOfChunks = (VideoLength / 1000 / SplitSeconds);
	if ((videolength / 1000) % splitseconds > 10) NumberOfChunks++;
	ErrorColor = errorcolor;
	return NumberOfChunks;
}

void GenerateSplitsThread::Cancel() {
	Canceled = true;
}

bool GenerateSplitsThread::Test(QString Before, QString After) {
	if (DebugEnabled) emit Debug("Testing: " + Before + " - " + After);

	double BeforeStart = Before.toDouble() - (FrameTime * 3);

	QString CacheFile = "";
	if (CacheDir != "") {
		CacheFile = CacheDir + QDir::separator() + InputHash + "-SplitTest-" + Before + "-" + After + ".txt";
		CacheFile = QDir::toNativeSeparators(CacheFile);

		if (QFile::exists(CacheFile)) {
			if (DebugEnabled) emit Debug("Test Cache Read: " + CacheFile);
			QString Result = ReadFile(CacheFile);
			if (Result.toInt())
				return true;
			else
				return false;
		}
	}

	QString Command;
	QString V360;

	Command = Ffmpeg + " -hide_banner -loglevel info -nostats -y"
			  " -ss " + QString::number(BeforeStart) +
			  " -i \"" + InputFile + "\""
			  " -copyts -to " + Before +
			  " -map 0:v:0 -vf \"" + Filters + "\""
			  " -f image2 \"" + QDir::toNativeSeparators(TmpDir + "/TestBefore_" + Before + "_%03d.jpg") + "\"";

	QStringList CurParams = QProcess::splitCommand(Command);
	QString Program = CurParams.takeFirst();
	QProcess process;
	process.start(Program, CurParams);
	while (!process.waitForFinished(500)) {
		if (Canceled) {
			process.kill();
			return false;
		}
	}
	if (DebugEnabled) emit Debug(Command);
	if (process.exitCode() != 0 ) {
		QStringList ErrorMsg;
		ErrorMsg.append("<p><b>Error in command: " + Command + "</b></p>");
		QStringList ErrorLines = QString(process.readAllStandardError()).split("\n");
		for (int i = 0; i < ErrorLines.count(); ++i)
			ErrorMsg.append("<p style=color:" + ErrorColor + ";>" + ErrorLines.at(i) + "</p>");
		emit Error(ErrorMsg);
		return false;
	}
	QString Output = process.readAllStandardError();
	Output.replace("\r", "\n");

	QRegularExpression Time("^[0-9.]+$");
	Time.setPatternOptions(QRegularExpression::MultilineOption);
	QRegularExpressionMatchIterator MatchIterator = Time.globalMatch(Output);
	QStringList OutputTimes;
	while (MatchIterator.hasNext()) {
		QRegularExpressionMatch Match = MatchIterator.next();
		OutputTimes.append(Match.captured(0));
	}

	if (OutputTimes.isEmpty()) {
		QStringList ErrorMsg;
		ErrorMsg.append("<p><b>Error in command: " + Command + "</b></p>");
		ErrorMsg.append("<p style=color:" + ErrorColor + ";>Error reading times</p>");
		emit Error(ErrorMsg);
		return false;
	}

	QDir Tmp(TmpDir);
	QFileInfoList FileList = Tmp.entryInfoList(QStringList("TestBefore_" + Before + "_*.jpg"));
	QString LastTime = "";
	while (!FileList.isEmpty()) {
		QFileInfo info = FileList.takeFirst();
		QFile::remove(info.filePath());
		if (OutputTimes.isEmpty()) continue;
		QString FileName = info.fileName();
		QString CurTime = OutputTimes.takeFirst();
		// if (DebugEnabled) DebugList.append(FileName + " -> " + CurTime);
		LastTime = CurTime;
	}
	// if (DebugEnabled) DebugList.append("LastTime: " + LastTime);

	QString AfterFile = QDir::toNativeSeparators(TmpDir + "/TestAfter_" + After + ".jpg");
	Command = Ffmpeg + " -hide_banner -loglevel info -nostats -y"
			  " -ss " + After +
			  " -i \"" + InputFile + "\""
			  " -copyts"
			  " -map 0:v:0 -vf \"" + Filters + "\""
			  " -frames:v 1 -f image2 \"" + AfterFile + "\"";

	CurParams = QProcess::splitCommand(Command);
	Program = CurParams.takeFirst();
	process.start(Program, CurParams);
	while (!process.waitForFinished(500)) {
		if (Canceled) {
			process.kill();
			return false;
		}
	}
	if (DebugEnabled) emit Debug(Command);
	if (process.exitCode() != 0 ) {
		QStringList ErrorMsg;
		ErrorMsg.append("<p><b>Error in command: " + Command + "</b></p>");
		QStringList ErrorLines = QString(process.readAllStandardError()).split("\n");
		for (int i = 0; i < ErrorLines.count(); ++i)
			ErrorMsg.append("<p style=color:" + ErrorColor + ";>" + ErrorLines.at(i) + "</p>");
		emit Error(ErrorMsg);
		return false;
	}
	Output = process.readAllStandardError();
	Output.replace("\r", "\n");

	Time.setPatternOptions(QRegularExpression::MultilineOption);
	MatchIterator = Time.globalMatch(Output);
	OutputTimes.clear();
	while (MatchIterator.hasNext()) {
		QRegularExpressionMatch Match = MatchIterator.next();
		OutputTimes.append(Match.captured(0));
	}

	if (OutputTimes.isEmpty()) {
		QStringList ErrorMsg;
		ErrorMsg.append("<p><b>Error in command: " + Command + "</b></p>");
		ErrorMsg.append("<p style=color:" + ErrorColor + ";>Error reading times</p>");
		emit Error(ErrorMsg);
		return false;
	}

	QString FirstTime = OutputTimes.takeFirst();
	//if (DebugEnabled) emit Debug("FirstTime: " + FirstTime);

	double Diff = FirstTime.toDouble() - LastTime.toDouble();
	if (DebugEnabled) emit Debug("Diff: " + QString::number(Diff, 'f', 6));

	QFile::remove(AfterFile);

	bool result = (Diff < FrameTime * 1.1 && Diff > FrameTime * 0.9);

	if (CacheFile != "") WriteFile(CacheFile, QString::number(result));

	return result;
}

void GenerateSplitsThread::GetFrameAcurateCut(int ms, QString &Before, QString &After) {
	Before = "";
	After = "";
	int seconds = ms / 1000;
	QString CurrentSearch = QString::number(seconds) + "%+12";
	QString CurrentCacheName = "";
	if (CacheDir != "") CurrentCacheName = CacheDir + QDir::separator() + InputHash + "-SearchFrames-" + CurrentSearch +
											   ".txt";
	QString FrameTimesData;

	if (CurrentCacheName != "" && QFile::exists(CurrentCacheName)) {
		FrameTimesData = ReadFile(CurrentCacheName);
	}  else {
		QStringList CurParams = {InputFile,
								 "-read_intervals",
								 CurrentSearch,
								 "-select_streams",
								 "v:0",
								 "-show_entries",
								 "frame=pts_time",
								 "-of",
								 "default=nk=1:nw=1"
								};

		QProcess process;
		process.start(Ffprobe, CurParams);
		while (!process.waitForFinished(500)) {
			if (Canceled) {
				process.kill();
				return;
			}
		}
		if (DebugEnabled) emit Debug(Ffprobe + " " + CurParams.join(""));
		if (process.exitCode() != 0 ) {
			QStringList ErrorMsg;
			ErrorMsg.append("<p><b>Error in command: " + Ffprobe + " " + CurParams.join("") + "</b></p>");
			QStringList ErrorLines = QString(process.readAllStandardError()).split("\n");
			for (int i = 0; i < ErrorLines.count(); ++i)
				ErrorMsg.append("<p style=color:" + ErrorColor + ";>" + ErrorLines.at(i) + "</p>");
			emit Error(ErrorMsg);
			return;
		}
		FrameTimesData = process.readAllStandardOutput();
		FrameTimesData.replace("\r", "\n");

		if (CurrentCacheName != "") WriteFile(CurrentCacheName, FrameTimesData);
	}

	QStringList FrameList = FrameTimesData.split('\n');

	double SearchTime = (double)ms / 1000;
	QString Frame;
	QString AntFrame;
	bool ok;
	for (int i = 0; i < FrameList.count(); ++i) {
		Frame = FrameList.at(i);
		double CurFrame = Frame.toDouble(&ok);
		if (!ok) continue;
		if (CurFrame > SearchTime && i > 0) {
			Before = AntFrame;
			After = Frame;
			if (DebugEnabled) emit Debug("Acurate cut found: " + Before + " - " + After);
			break;
		}
		AntFrame = Frame;
	}
}

double GenerateSplitsThread::AdjustTime(QString Text, double In) {
	if (Text == "+1/2") return In / 2L;
	if (Text == "+1/3") return In / 3L;
	if (Text == "0") return 0;
	if (Text == "-1/3") return -In / 3L;
	if (Text == "-1/2") return -In / 2L;
	return Text.toDouble();
}

bool GenerateSplitsThread::WasCanceled() {
	return Canceled;
}
