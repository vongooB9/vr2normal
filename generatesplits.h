#ifndef GENERATESPLITS_H
#define GENERATESPLITS_H

#include <QObject>
#include <QThread>
#include <QTime>

class GenerateSplitsThread : public QThread {
		Q_OBJECT
	public:
		explicit GenerateSplitsThread(QObject *parent = nullptr);
		void run() override;
		bool GenNext(int ms);
		bool GenNext(int ms, QString B, QString A, QString BA, QString AA);
		int SetOptions(QString ffmpegbin,
					   QString ffprobebin,
					   QString cachedir,
					   QString input,
					   QString inputhash,
					   int videolength,
					   double fps,
					   int splitseconds,
					   QString tmpdirectory,
					   bool enabledebug,
					   QString errorcolor);
		bool Test(QString Before, QString After);
		void GetFrameAcurateCut(int ms, QString &BeforeAdjust, QString &After);
		double AdjustTime(QString Text, double In);
		bool WasCanceled();
	private:
		bool Canceled;
		QString Ffmpeg;
		QString Ffprobe;
		QString CacheDir;
		QString InputFile;
		QString InputHash;
		int VideoLength;
		double Fps;
		double FrameTime;
		int SplitSeconds;
		QString TmpDir;
		bool DebugEnabled;
		QString ErrorColor;
		int NumberOfChunks;
		QString Filters;

	public slots:
		void Cancel();

	signals:
		void NewSplit(QTime Time, QString Before, QString After, QString BeforeAdj, QString AfterAdj);
		void UpdateText(QString Text);
		void Debug(QString DebugMsg);
		void Error(QStringList ErrorMsg);

};

#endif // GENERATESPLITS_H
