#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[]) {
	#if QT_VERSION >= QT_VERSION_CHECK(6, 4, 0) && defined (Q_OS_WIN)
	QApplication::setStyle("fusion");
	#endif
	QApplication a(argc, argv);

	QString File = "";
	for (int i = 1; i < argc; i++) {
		QString param(argv[i]);
		if (QFile::exists(param)) {
			File = param;
			break;
		}
	}

	MainWindow w(nullptr, (File == ""));
	if (File != "") w.OpenFile(File);
	w.show();
	return a.exec();
}
