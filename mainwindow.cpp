﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>

#include <QStringList>
#include <QPixmap>
#include <QFileDialog>
#include <QTextStream>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonArray>
#include <QMenu>
#include <QPushButton>
#include <QRadioButton>
#include <QUndoCommand>
#include <QUndoGroup>
#include <QByteArray>
#include "qtablewidgetundo.h"
#include <QCryptographicHash>
#include <QDirIterator>
#include <QPainter>
#include <QTimer>
#include <QScrollArea>
#include <QGridLayout>
#include <QLabel>
#include "processpriority.h"
#include "taskbarprogress.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QDoubleSpinBox>
#include "diskcache.h"
#include "common.h"
#include <QRandomGenerator>

#ifdef Q_OS_LINUX
#else
	#include <QStandardPaths>
#endif

#if QT_VERSION >= QT_VERSION_CHECK(6, 5, 0)
	#include <QTimeZone>
#endif

#define LEFT_TAB_CAMERA 0
#define LEFT_TAB_SENDCMD 1
#define LEFT_TAB_CLIP_PREVIEW 2
#define LEFT_TAB_COMPLETE_PREVIEW 3
#define LEFT_TAB_CONVERT 4
#define LEFT_TAB_SPLIT 5
#define LEFT_TAB_INPUT_PROCESS 6
#define LEFT_TAB_ERROR_LOG 7

#define RIGHT_TAB_MOVEMENTS 0
#define RIGHT_TAB_FILTERS 1
#define RIGHT_TAB_SCREENSHOTS 2
#define RIGHT_TAB_COMPARESMALL 3
#define RIGHT_TAB_INPUT 4
#define RIGHT_TAB_CONFIG 5
#define RIGHT_TAB_SHORTCUTS 6
#define RIGHT_TAB_DEBUG 7

#define TIME_FORMAT "H:mm:ss.zzz"

#define SPLIT_STATE_COLUMN 0
#define SPLIT_TIME_COLUMN 1
#define SPLIT_BEFORE_COLUMN 2
#define SPLIT_AFTER_COLUMN 3
#define SPLIT_ADJUST_BEFORE_COLUMN 4
#define SPLIT_ADJUST_AFTER_COLUMN 5
#define SPLIT_FILE_COLUMN 6

tableline_t DefaultTableLine;

MainWindow::MainWindow(QWidget *parent, bool Autoload)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow) {
	ui->setupUi(this);

	ui->RawTime->setDisplayFormat(TIME_FORMAT);

	WindowTitleVersion = "VR2Normal " + QString(VR2NORMAL_VERSION);
	setWindowTitle(WindowTitleVersion);

	ui->ErrorLines->setVisible(false);
	ui->RunState->setVisible(false);
	ui->ProgressArea->setVisible(false);
	ui->EstimatedBlock->setVisible(false);
	ui->FiltersWarning->setVisible(false);
	ui->FilterExposure->setChecked(false);
	ui->FilterColorTemperature->setChecked(false);
	ui->FilterEQ->setChecked(false);
	ui->FilterColorBalance->setChecked(false);
	ui->SplitCurrentWidget->setVisible(false);

	Tray = new QSystemTrayIcon(this);
	Tray->setIcon(QIcon(":/VR2Normal.ico"));
	Tray->setToolTip(WindowTitleVersion);

	Task = new TaskbarProgress(this);
	Task->EndProgress();

	GenerateSplits = new GenerateSplitsThread(this);

	DisableRunButtons();

	Loading = true;

	TableUndo = new QUndoStack(this);

	DiskCacheWorker *DiskCacheObject = new DiskCacheWorker;
	DiskCacheObject->moveToThread(&DiskCacheThread);
	QObject::connect(DiskCacheObject, &DiskCacheWorker::CurrentCacheSizeString, ui->CurrentDiskUsage, &QLabel::setText);
	QObject::connect(ui->DiskCacheDir, &QLineEdit::textChanged, DiskCacheObject, &DiskCacheWorker::SetCacheDir);
	QObject::connect(ui->DiskCache, QOverload<int>::of(&QSpinBox::valueChanged),
					 DiskCacheObject, &DiskCacheWorker::SetMaxSize);
	QObject::connect(ui->ClearDiskCache, &QPushButton::clicked, DiskCacheObject, &DiskCacheWorker::EmptyDiskCache);
	DiskCacheThread.start();

	#ifdef Q_OS_WIN
	Settings = new QSettings(QCoreApplication::applicationDirPath() + QDir::separator() + "VR2Normal.ini",
							 QSettings::IniFormat);

	#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
	ui->TaskbarProgress->setText(ui->TaskbarProgress->text() + " (Not working)");
	#endif
	#else
	QString Name = QCoreApplication::applicationName();
	if (Name == "AppRun.wrapped") Name = "VR2Normal";
	Settings = new QSettings(Name, "config");
	#endif

	#ifdef Q_OS_MAC
	ui->TaskbarProgress->setText(ui->TaskbarProgress->text() + " (Not working)");
	#endif

	#ifdef Q_OS_LINUX
	ui->FfmpegBin->setText(Settings->value("ffmpegbin", "ffmpeg").toString());
	ui->FfprobeBin->setText(Settings->value("ffprobebin", "ffprobe").toString());
	ui->VideoPlayer->setText(Settings->value("VideoPlayer", "ffplay").toString());
	ui->ImageViewer->setText(Settings->value("ImageViewer", "ffplay").toString());
	#else
	if (QStandardPaths::findExecutable("ffmpeg") != "")
		ui->FfmpegBin->setText(Settings->value("ffmpegbin", "ffmpeg").toString());
	else
		ui->FfmpegBin->setText(Settings->value("ffmpegbin", "").toString());

	if (QStandardPaths::findExecutable("ffprobe") != "")
		ui->FfprobeBin->setText(Settings->value("ffprobebin", "ffprobe").toString());
	else
		ui->FfprobeBin->setText(Settings->value("ffprobebin", "").toString());

	if (QStandardPaths::findExecutable("ffplay") != "") {
		ui->VideoPlayer->setText(Settings->value("VideoPlayer", "ffplay").toString());
		ui->ImageViewer->setText(Settings->value("ImageViewer", "ffplay").toString());
	} else {
		ui->VideoPlayer->setText(Settings->value("VideoPlayer", "").toString());
		ui->ImageViewer->setText(Settings->value("ImageViewer", "").toString());
	}
	#endif
	ui->AutoRun->setChecked(Settings->value("AutoRun", "0").toBool());
	ui->AutoPlay->setChecked(Settings->value("AutoPlay", "0").toBool());
	ui->AutoSaveConfig->setChecked(Settings->value("AutoSaveConfig", "0").toBool());
	ui->AutoSaveMovement->setChecked(Settings->value("AutoSaveMovement", "0").toBool());
	ui->SaveWindowSettings->setChecked(Settings->value("SaveWindowSettings", "0").toBool());
	ui->GUIPreviewW->setValue(Settings->value("GUIPreviewW", "640").toInt());
	ui->GUIPreviewH->setValue(Settings->value("GUIPreviewH", "360").toInt());
	ui->GUIPreviewInterpolation->setCurrentText(Settings->value("GUIPreviewInterpolation", "nearest").toString());
	ui->GUIPreviewScale->setCurrentText(Settings->value("GUIPreviewScale", "None").toString());
	ui->DefaultTranValue->setValue(Settings->value("DefaultTranValue", "1").toDouble());
	ui->RAMCache->setValue(Settings->value("RAMCache", "0").toInt());
	ui->DiskCache->setValue(Settings->value("DiskCache", "0").toInt());
	ui->DiskCacheDir->setText(Settings->value("DiskCacheDir", QDir::tempPath() + "/VR2NormalCache").toString());
	ui->CacheProcess->setValue(Settings->value("CacheProcess", "1").toInt());
	ui->PreloadCacheSeoconds->setValue(Settings->value("PreloadCacheSeoconds", "0").toInt());
	ui->CacheFormat->setCurrentText(Settings->value("CacheFormat", "JPG").toString());
	ui->CachePriority->setCurrentText(Settings->value("CachePriority", "Default").toString());
	ui->Priority->setCurrentText(Settings->value("Priority", "Default").toString());
	ui->ErrorColor->setText(Settings->value("ErrorColor", "red").toString());
	ui->LowWarningColor->setText(Settings->value("LowWarningColor", "orange").toString());
	ui->HighWarningColor->setText(Settings->value("HighWarningColor", "red").toString());
	ui->IncludeFile->setChecked(Settings->value("IncludeFile", "0").toBool());
	ui->ShowTrayIcon->setChecked(Settings->value("ShowTrayIcon", "0").toBool());
	ui->TaskbarProgress->setChecked(Settings->value("TaskbarProgress", "0").toBool());
	ui->ShowFramingLines->setChecked(Settings->value("ShowFramingLines", "0").toBool());
	ui->ShowEstimatedResolution->setChecked(Settings->value("ShowEstimatedResolution", "1").toBool());
	ui->LimitMovement->setChecked(Settings->value("LimitMovement", "0").toBool());
	ui->ShowSendcmdTab->setChecked(Settings->value("ShowSendcmdTab", "0").toBool());
	ui->EnableDebug->setChecked(Settings->value("EnableDebug", "0").toBool());
	ui->HWDec->setCurrentText(Settings->value("HWDec", "None").toString());
	ui->MinFov->setValue(Settings->value("MinFOV", "90").toInt());
	ui->MaxFov->setValue(Settings->value("MaxFOV", "105").toInt());
	ui->PYSteps->setValue(Settings->value("PitchYawSteps", "1").toDouble());
	ui->RollSteps->setValue(Settings->value("RollSteps", "1").toDouble());
	ui->FOVSteps->setValue(Settings->value("FOVSteps", "1").toDouble());
	ui->TransSteps->setValue(Settings->value("TransSteps", "1").toDouble());

	ui->PreviewVideoFormat->setCurrentText(Settings->value("PreviewVideoFormat", "Xvid").toString());
	ui->PreviewPreset->setCurrentText(Settings->value("PreviewPreset", "veryfast").toString());
	ui->PreviewCRF->setValue(Settings->value("PreviewCRF", "28").toInt());
	ui->PreviewAudioFormat->setCurrentText(Settings->value("PreviewAudioFormat", "None").toString());
	ui->PreviewAudioBitrate->setValue(Settings->value("PreviewAudioBitrate", "128").toInt());
	ui->PreviewCustomFormat->setText(Settings->value("PreviewCustomFormat", "").toString());
	ui->PreviewW->setValue(Settings->value("PreviewW", "852").toInt());
	ui->PreviewH->setValue(Settings->value("PreviewH", "480").toInt());
	ui->PreviewFPS->setValue(Settings->value("PreviewFPS", "30").toDouble());
	ui->PreviewInterpolation->setCurrentText(Settings->value("PreviewInterpolation", "linear").toString());
	ui->PreviewPXFormat->setCurrentText(Settings->value("PreviewPXFormat", "Auto").toString());
	ui->PreviewDir->setText(Settings->value("PreviewDir", "").toString());
	ui->PreviewTimeBefore->setValue(Settings->value("PreviewTimeBefore", "2000").toInt());
	ui->PreviewTimeAfter->setValue(Settings->value("PreviewTimeAfter", "2000").toInt());
	ui->PreviewHWDec->setCurrentText(Settings->value("PreviewHWDec", "None").toString());

	ui->CPreviewVideoFormat->setCurrentText(Settings->value("CPreviewVideoFormat", "Xvid").toString());
	ui->CPreviewPreset->setCurrentText(Settings->value("CPreviewPreset", "veryfast").toString());
	ui->CPreviewCRF->setValue(Settings->value("CPreviewCRF", "28").toInt());
	ui->CPreviewAudioFormat->setCurrentText(Settings->value("CPreviewAudioFormat", "None").toString());
	ui->CPreviewAudioBitrate->setValue(Settings->value("CPreviewAudioBitrate", "128").toInt());
	ui->CPreviewCustomFormat->setText(Settings->value("CPreviewCustomFormat", "").toString());
	ui->CPreviewW->setValue(Settings->value("CPreviewW", "852").toInt());
	ui->CPreviewH->setValue(Settings->value("CPreviewH", "480").toInt());
	ui->CPreviewFPS->setValue(Settings->value("CPreviewFPS", "30").toDouble());
	ui->CPreviewInterpolation->setCurrentText(Settings->value("CPreviewInterpolation", "linear").toString());
	ui->CPreviewPXFormat->setCurrentText(Settings->value("CPreviewPXFormat", "Auto").toString());
	ui->CPreviewHWDec->setCurrentText(Settings->value("CPreviewHWDec", "None").toString());

	ui->OutVideoFormat->setCurrentText(Settings->value("OutVideoFormat", "x265").toString());
	ui->OutPreset->setCurrentText(Settings->value("OutPreset", "medium").toString());
	ui->OutCRF->setValue(Settings->value("OutCRF", "24").toInt());
	ui->OutAudioFormat->setCurrentText(Settings->value("OutAudioFormat", "Copy").toString());
	ui->OutAudioBitrate->setValue(Settings->value("OutAudioBitrate", "128").toInt());
	ui->OutCustomFormat->setText(Settings->value("OutCustomFormat", "").toString());
	ui->OutInterpolation->setCurrentText(Settings->value("OutInterpolation", "lanczos").toString());
	ui->OutDefaultDir->setText(Settings->value("OutDefaultDir", "").toString());
	ui->OutSuffix->setText(Settings->value("OutSuffix", "_VR2Normal").toString());
	ui->OutHWDec->setCurrentText(Settings->value("OutHWDec", "None").toString());
	ui->ExecuteOnFinish->setText(Settings->value("ExecuteOnFinish", "").toString());

	ui->ScreenshotsDir->setText(Settings->value("ScreenshotsDir", "").toString());
	ui->ScreenshotsV360W->setValue(Settings->value("ScreenshotsV360W", "1920").toInt());
	ui->ScreenshotsV360H->setValue(Settings->value("ScreenshotsV360H", "1080").toInt());
	ui->ScreenshotsW->setValue(Settings->value("ScreenshotsW", "0").toInt());
	ui->ScreenshotsH->setValue(Settings->value("ScreenshotsH", "0").toInt());
	ui->ScreenshotsFormat->setCurrentText(Settings->value("ScreenshotsFormat", "png").toString());
	ui->ScreenshotsQuality->setValue(Settings->value("ScreenshotsQuality", "3").toInt());
	ui->ScreenshotAutoOpen->setChecked(Settings->value("ScreenshotAutoOpen", "0").toBool());

	if (Settings->value("ScreenshotDisableFilters", "0").toInt() == 0)
		ui->ScreenshotDisableFilters->setCheckState(Qt::Unchecked);
	if (Settings->value("ScreenshotDisableFilters", "0").toInt() == 1)
		ui->ScreenshotDisableFilters->setCheckState(Qt::PartiallyChecked);
	if (Settings->value("ScreenshotDisableFilters", "0").toInt() == 2)
		ui->ScreenshotDisableFilters->setCheckState(Qt::Checked);

	ui->IPHWDec->setCurrentText(Settings->value("IPHWDec", "None").toString());
	ui->IPDetectBlack->setChecked(Settings->value("IPDetectBlack", "1").toBool());
	ui->IPBlackAmount->setValue(Settings->value("IPBlackAmount", "90").toInt());
	ui->IPBlackThreshold->setValue(Settings->value("IPBlackThreshold", "24").toInt());

	ui->IPVideoFormat->setCurrentText(Settings->value("IPVideoFormat", "x264").toString());
	ui->IPPreset->setCurrentText(Settings->value("IPPreset", "fast").toString());
	ui->IPCRF->setValue(Settings->value("IPCRF", "28").toInt());
	ui->IPAudioFormat->setCurrentText(Settings->value("IPAudioFormat", "Copy").toString());
	ui->IPAudioBitrate->setValue(Settings->value("IPAudioBitrate", "128").toInt());
	ui->IPW->setValue(Settings->value("IPW", "-2").toInt());
	ui->IPH->setValue(Settings->value("IPH", "1080").toInt());
	ui->IPHWScale->setChecked(Settings->value("IPHWScale", "0").toBool());

	if (ui->DiskCacheDir->text() == "") { ui->DiskCache->setValue(0); }

	LoadShortcuts();

	Settings->beginGroup("MainWindow");

	if (Settings->contains("size"))
		resize(Settings->value("size", QSize(400, 400)).toSize());
	if (Settings->contains("pos"))
		move(Settings->value("pos", QPoint(200, 200)).toPoint());

	if (Settings->value("fullscreen", "0").toBool())
		showFullScreen();
	if (Settings->value("Maximized", "0").toBool())
		showMaximized();

	ui->LeftTabs->setCurrentIndex(Settings->value("LeftTabs", "0").toInt());
	ui->RightTabs->setCurrentIndex(Settings->value("RightTabs", "0").toInt());
	ui->Table->setColumnWidth(TIME_COLUMN, Settings->value("TimeRowSize", "100").toInt());
	ui->Table->setColumnWidth(TRANSITION_COLUMN, Settings->value("TransitionRowSize", "60").toInt());
	ui->Table->setColumnWidth(PITCH_COLUMN, Settings->value("PitchRowSize", "60").toInt());
	ui->Table->setColumnWidth(YAW_COLUMN, Settings->value("YawRowSize", "60").toInt());
	ui->Table->setColumnWidth(ROLL_COLUMN, Settings->value("RollRowSize", "60").toInt());
	ui->Table->setColumnWidth(FOV_COLUMN, Settings->value("FovRowSize", "60").toInt());
	ui->Table->setColumnWidth(EYE_COLUMN, Settings->value("EyeRowSize", "60").toInt());
	ui->Table->setColumnWidth(V_OFFSET_COLUMN, Settings->value("VOffsetSize", "60").toInt());
	ui->Table->setColumnWidth(H_OFFSET_COLUMN, Settings->value("HOffsetSize", "60").toInt());
	ui->Table->setColumnWidth(TEXT_COLUMN, Settings->value("TextRowSize", "200").toInt());
	ui->PreviewSpliter->restoreState(Settings->value("PreviewSpliter", "").toByteArray());
	ui->TabsSpliter->restoreState(Settings->value("TabsSpliter", "").toByteArray());
	ui->FiltersSplitter->restoreState(Settings->value("FiltersSplitter", "").toByteArray());
	ui->FiltersList->setColumnWidth(FILTERS_TIME_COLUMN, Settings->value("FiltersTimeRowSize", "100").toInt());
	ui->FiltersList->setColumnWidth(FILTERS_FILTER_COLUMN, Settings->value("FiltersRowSize", "600").toInt());
	ui->SplitTable->setColumnWidth(SPLIT_STATE_COLUMN, Settings->value("SplitStateSize", "60").toInt());
	ui->SplitTable->setColumnWidth(SPLIT_TIME_COLUMN, Settings->value("SplitTimeSize", "60").toInt());
	ui->SplitTable->setColumnWidth(SPLIT_BEFORE_COLUMN, Settings->value("SplitBeforeSize", "60").toInt());
	ui->SplitTable->setColumnWidth(SPLIT_AFTER_COLUMN, Settings->value("SplitAfterSize", "60").toInt());
	ui->SplitTable->setColumnWidth(SPLIT_ADJUST_BEFORE_COLUMN, Settings->value("SplitAdjustedBeforeSize", "60").toInt());
	ui->SplitTable->setColumnWidth(SPLIT_ADJUST_AFTER_COLUMN, Settings->value("SplitAdjustedAfterSize", "60").toInt());
	ui->SplitTable->setColumnWidth(SPLIT_FILE_COLUMN, Settings->value("SplitFileSize", "600").toInt());
	Settings->endGroup();

	QObject::connect(ui->RawTime, &QTimeEdit::userTimeChanged, this, &MainWindow::ReloadRawImage);

	QObject::connect(&FfmpegRaw, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::GenerateRawImageTerminate);
	QObject::connect(&FfmpegRaw, &QProcess::readyReadStandardOutput, this, &MainWindow::ReadRawData);

	QObject::connect(&FfmpegFilter,  QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::GenerateFilterImageTerminate);
	QObject::connect(&FfmpegFilter, &QProcess::started, this, &MainWindow::StartFilterProcess);
	QObject::connect(&FfmpegFilter, &QProcess::readyReadStandardOutput, this, &MainWindow::ReadFilterData);

	QObject::connect(&Ffprobe, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::FfprobeTerminate);

	for (int i = 0; i < ui->CacheProcess->value(); ++i) {
		QProcess *Process = new QProcess(this);
		CacheProcess.insert(Process, {false, 0, 0, 0, 0, new QElapsedTimer()});
		QObject::connect(Process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
						 this, &MainWindow::DiskCacheProcessTerminate);
	}

	QObject::connect(&ScreenshotProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::ScreenshotProcessTerminate);

	QObject::connect(ui->Pitch, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Yaw, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Roll, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Fov, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->VOffset, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->HOffset, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	QObject::connect(ui->RightEye, &QCheckBox::checkStateChanged, this, &MainWindow::ReloadFilterImageForced);
	#else
	QObject::connect(ui->RightEye, &QCheckBox::stateChanged, this, &MainWindow::ReloadFilterImageForced);
	#endif

	QObject::connect(PitchUP, &QShortcut::activated, this, &MainWindow::PitchUPEvent);
	QObject::connect(PitchDown, &QShortcut::activated, this, &MainWindow::PitchDownEvent);
	QObject::connect(YawUP, &QShortcut::activated, this, &MainWindow::YawUPEvent);
	QObject::connect(YawDown, &QShortcut::activated, this, &MainWindow::YawDownEvent);
	QObject::connect(RollUP, &QShortcut::activated, this, &MainWindow::RollUPEvent);
	QObject::connect(RollDown, &QShortcut::activated, this, &MainWindow::RollDownEvent);
	QObject::connect(FovUP, &QShortcut::activated, this, &MainWindow::FovUPEvent);
	QObject::connect(FovDown, &QShortcut::activated, this, &MainWindow::FovDownEvent);
	QObject::connect(FocusText, &QShortcut::activated, this, &MainWindow::EditTextFocus);
	QObject::connect(TranUP, &QShortcut::activated, ui->Trans, &QDoubleSpinBox::stepUp);
	QObject::connect(TranDown, &QShortcut::activated, ui->Trans, &QDoubleSpinBox::stepDown);
	QObject::connect(VUP, &QShortcut::activated, this, &MainWindow::VUPEvent);
	QObject::connect(VDown, &QShortcut::activated, this, &MainWindow::VDownEvent);
	QObject::connect(HUP, &QShortcut::activated, this, &MainWindow::HUPEvent);
	QObject::connect(HDown, &QShortcut::activated, this, &MainWindow::HDownEvent);

	QObject::connect(RowUp, &QShortcut::activated, this, &MainWindow::TableUpSelect);
	QObject::connect(RowDown, &QShortcut::activated, this, &MainWindow::TableDownSelect);

	QObject::connect(Undo, &QShortcut::activated, this, &MainWindow::UndoSlot);
	QObject::connect(Redo, &QShortcut::activated, this, &MainWindow::RedoSlot);

	QObject::connect(Unselect, &QShortcut::activated, this, &MainWindow::UnselectSlot);

	QObject::connect(ui->SaveConfigKS, &QPushButton::clicked, this, &MainWindow::SaveShortcuts);
	QObject::connect(ui->ApplyKS, &QPushButton::clicked, this, &MainWindow::ApplyShortcuts);

	QObject::connect(ui->Save, &QPushButton::clicked, this, &MainWindow::SaveDefaultMovementFile);
	QObject::connect(ui->FilterSave, &QPushButton::clicked, this, &MainWindow::SaveDefaultMovementFile);
	QObject::connect(ui->SaveInputTab, &QPushButton::clicked, this, &MainWindow::SaveDefaultMovementFile);

	QObject::connect(&Execution, &QProcess::readyReadStandardError, this, &MainWindow::ReadProgress);
	QObject::connect(&Execution,  QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::ProcessTerminate);

	QObject::connect(ui->CPreviewRun, &QPushButton::clicked, this, &MainWindow::ExecuteProcess);
	QObject::connect(ui->ExecutePreviewCommand, &QPushButton::clicked, this, &MainWindow::ExecuteProcess);
	QObject::connect(ui->ExecuteOutCommand, &QPushButton::clicked, this, &MainWindow::ExecuteProcess);

	QObject::connect(&OndemandCacheTimer, &QTimer::timeout, this, &MainWindow::DiskCacheStart);
	QObject::connect(this, &MainWindow::UpdateDiskCacheAndRemove, DiskCacheObject, &DiskCacheWorker::CheckCache);
	QObject::connect(&Autosave, &QTimer::timeout, this, &MainWindow::AutosaveTimeout);
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	if (ui->AutoSaveMovement->isChecked()) on_AutoSaveMovement_checkStateChanged(Qt::Checked);
	#else
	if (ui->AutoSaveMovement->isChecked()) on_AutoSaveMovement_stateChanged(1);
	#endif

	QObject::connect(Tray, &QSystemTrayIcon::activated, this, &MainWindow::TrayClick);

	QObject::connect(&CompareSmallProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::CompareProcessTerminate);
	QObject::connect(&CompareSmallProcess, &QProcess::readyReadStandardOutput, this, &MainWindow::ReadCompareData);

	QObject::connect(ui->Exposure, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->ExposureBlack, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->Temperature, &QSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->Mix, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->Pl, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->Contrast, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->Brightness, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->Saturation, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->Gamma, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->GammaRed, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->GammaGreen, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->GammaBlue, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->GammaWeight, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->rs, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->gs, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->bs, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->rm, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->gm, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->bm, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->rh, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->gh, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);
	QObject::connect(ui->bh, &QDoubleSpinBox::textChanged, this, &MainWindow::UpdateFiltersCell);

	QObject::connect(ui->Exposure, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->ExposureBlack, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Temperature, &QSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Mix, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Pl, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Contrast, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Brightness, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Saturation, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->Gamma, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->GammaRed, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->GammaGreen, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->GammaBlue, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->GammaWeight, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->rs, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->gs, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->bs, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->rm, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->gm, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->bm, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->rh, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->gh, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->bh, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImageForced);
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	QObject::connect(ui->pl, &QCheckBox::checkStateChanged, this, &MainWindow::ReloadFilterImageForced);
	#else
	QObject::connect(ui->pl, &QCheckBox::stateChanged, this, &MainWindow::ReloadFilterImageForced);
	#endif
	QObject::connect(ui->FilterExposure, &QGroupBox::toggled, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->FilterColorTemperature, &QGroupBox::toggled, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->FilterEQ, &QGroupBox::toggled, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->FilterColorBalance, &QGroupBox::toggled, this, &MainWindow::ReloadFilterImageForced);
	QObject::connect(ui->FilterCompare, &QPushButton::toggled, this, &MainWindow::ReloadFilterImageForced);

	QObject::connect(ui->InputPadW, &QSpinBox::textChanged, this, &MainWindow::ChangeInputPad);
	QObject::connect(ui->InputPadH, &QSpinBox::textChanged, this, &MainWindow::ChangeInputPad);
	QObject::connect(ui->InputPadX, &QSpinBox::textChanged, this, &MainWindow::ChangeInputPad);
	QObject::connect(ui->InputPadY, &QSpinBox::textChanged, this, &MainWindow::ChangeInputPad);

	QObject::connect(ui->OutResCalc, &QPushButton::clicked, this, &MainWindow::CalculateOutputResolution);

	QObject::connect(ui->InputFile, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->SmallInputFile, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->formatComboBox, &QComboBox::currentTextChanged, this, &MainWindow::Changes);
	QObject::connect(ui->modeComboBox, &QComboBox::currentTextChanged, this, &MainWindow::Changes);
	QObject::connect(ui->FPSSpinBox, &QDoubleSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->VFovSpinBox, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->HFovSpinBox, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->InputPadH, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->InputPadW, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->InputPadX, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->InputPadY, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->InputResolutionH, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->InputResolutionW, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->VideoLength, &QTimeEdit::timeChanged, this, &MainWindow::Changes);
	QObject::connect(ui->InputFile, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->MovementAlg, &QComboBox::currentTextChanged, this, &MainWindow::Changes);
	QObject::connect(ui->IntermediatePercent, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->CustomV360, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->CustomInputFilters, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->CustomFilters, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->MetadataTitle, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->MetadataStudio, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->MetadataActors, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->MetadataReleaseDate, &QDateEdit::dateChanged, this, &MainWindow::Changes);
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	QObject::connect(ui->GenerateChapters, &QCheckBox::checkStateChanged, this, &MainWindow::Changes);
	#else
	QObject::connect(ui->GenerateChapters, &QCheckBox::stateChanged, this, &MainWindow::Changes);
	#endif
	QObject::connect(ui->OutW, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->OutH, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->OutPXFormat, &QComboBox::currentTextChanged, this, &MainWindow::Changes);
	QObject::connect(ui->CMDFile, &QLineEdit::textChanged, this, &MainWindow::Changes);

	QObject::connect(ui->SplitSeconds, &QSpinBox::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->SplitConvert, &QRadioButton::toggled, this, &MainWindow::Changes);
	QObject::connect(ui->SplitTMPDir, &QLineEdit::textChanged, this, &MainWindow::Changes);
	QObject::connect(ui->SplitRecheckState, &QPushButton::clicked, this, &MainWindow::CheckSplitState);
	QObject::connect(&ProcessChunks, &QProcess::readyReadStandardError, this, &MainWindow::ProcessChunksData);
	QObject::connect(&ProcessChunks,  QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::ProcessChunksTerminate);
	QObject::connect(&SplitTimer, &QTimer::timeout, this, &MainWindow::SplitTimerTimeout);

	QObject::connect(GenerateSplits, &GenerateSplitsThread::NewSplit, this, &MainWindow::NewSplit);
	QObject::connect(GenerateSplits, &GenerateSplitsThread::UpdateText, this, &MainWindow::UpdateSplitText);
	QObject::connect(GenerateSplits, &GenerateSplitsThread::Debug, this, &MainWindow::NewSplitDebug);
	QObject::connect(GenerateSplits, &GenerateSplitsThread::Error, this, &MainWindow::NewSplitError);
	QObject::connect(GenerateSplits, &GenerateSplitsThread::finished, this, &MainWindow::NewSplitFinish);

	ui->LeftTabs->setTabVisible(LEFT_TAB_ERROR_LOG, false);
	ui->LeftTabs->setTabVisible(LEFT_TAB_SENDCMD, ui->ShowSendcmdTab->isChecked());

	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	on_EnableDebug_checkStateChanged(ui->EnableDebug->checkState());
	#else
	on_EnableDebug_stateChanged(ui->EnableDebug->checkState());
	#endif

	BlockRefresh = false;

	ScreenshotFilename = "";
	CurrentSplitRow = -1;
	FfmpegRawCanceled = false;
	FfmpegFilterCanceled = false;
	SplitErrors = false;

	if (Autoload && Settings->value("MovementFile", "").toString() != "") {
		if (QFile::exists(Settings->value("MovementFile", "").toString())) {
			ui->MovementFile->setText(Settings->value("MovementFile", "").toString());
			LoadMovenmentFile();
		}
	}
	RefreshPreviews();

	if (ui->FfmpegBin->text() == "") ui->RightTabs->setCurrentIndex(RIGHT_TAB_CONFIG);

	Loading = false;

	//TODO: Delete tis alter check that shorcuts load correctly o qt6 linux
	#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0) && defined (Q_OS_LINUX)
	QTimer::singleShot(5000, this, &MainWindow::ApplyShortcuts);
	#endif
}

MainWindow::~MainWindow() {
	if (ui->AutoSaveMovement->isChecked()) SaveMovementFile();
	if (ui->AutoSaveConfig->isChecked()) on_SaveConfig_clicked();
	DiskCacheThread.quit();
	DiskCacheThread.wait();
	delete ui;
}

void MainWindow::OpenFile(QString File) {
	ui->MovementFile->setText(File);
	LoadMovenmentFile();
}

void MainWindow::closeEvent(QCloseEvent *event) {
	if (Execution.state() != QProcess::NotRunning ||
		ProcessChunks.state() != QProcess::NotRunning) {
		if (ui->ShowTrayIcon->isChecked()) {
			this->hide();
			event->ignore();
			return;
		}

		QMessageBox::StandardButton Result = QMessageBox::question(this, "Close Confirmation",
											 "There are processes in progress, if you close them they will be cancelled.\nDo you want to close the application anyway?",
											 QMessageBox::Yes | QMessageBox::No);

		if (Result == QMessageBox::No) {
			event->ignore();
			return;
		}

		if (ProcessChunks.state() != QProcess::NotRunning) {
			ExecutionCanceled = true;
			ProcessChunks.close();
		}

	}
	Tray->hide();
}

void MainWindow::LoadMovenmentFile() {
	if (!QFile::exists(ui->MovementFile->text())) return;

	QString Data = ReadFile(ui->MovementFile->text());
	QStringList DataLines = Data.split("\n");

	static QRegularExpression
	TableLine("^([0-9.:]+)[[:space:]]+([0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([0-9.]+)[[:space:]]+([01]+)[[:space:]]*([^\\t]*)$");
	static QRegularExpression
	OffsetLine("^([0-9.:]+)[[:space:]]+([0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([0-9.]+)[[:space:]]+([01]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]*([^\\t]*)$");

	static QRegularExpression InputOptions("^#(\\S*): (.*)$");

	static QRegularExpression FilterLine("^FilterList: ([0-9.:]+)[[:space:]]*([^\\t]*)$");

	static QRegularExpression
	SplitLine("^SplitList:[[:space:]]+([0-9.:]+)[[:space:]]+([0-9.]*)[[:space:]]+([0-9.]*)[[:space:]]+([0-9.]*)[[:space:]]+([0-9.]*)[[:space:]]+([^\\t]*)$");

	TableUndo->clear();
	CleanTable(ui->Table);
	CleanTable(ui->FiltersList);
	CleanTable(ui->SplitTable);

	BlockRefresh = true;
	int CurrentSelectedRow = -1;

	SetDefaultInputOptions();

	QString Version = "";

	for (int i = 0; i < DataLines.count(); ++i) {
		QString Line = DataLines.at(i);
		if (Line == "") continue;

		QRegularExpressionMatch Match = OffsetLine.match(Line);
		if (Match.hasMatch()) {
			QString Time = Match.captured(1);
			QString Transition = Match.captured(2);
			QString Pitch = Match.captured(3);
			QString Yaw = Match.captured(4);
			QString Roll = Match.captured(5);
			QString Fov = Match.captured(6);
			QString Eye = Match.captured(7);
			QString V = Match.captured(8);
			QString H = Match.captured(9);
			QString Text = Match.captured(10);

			int newrow = ui->Table->rowCount();

			ui->Table->blockSignals(true);
			ui->Table->insertRow(newrow);

			ui->Table->setItem(newrow, TIME_COLUMN, new QTableWidgetItem(Time));
			ui->Table->setItem(newrow, TRANSITION_COLUMN, new QTableWidgetItem(Transition));
			ui->Table->setItem(newrow, PITCH_COLUMN, new QTableWidgetItem(Pitch));
			ui->Table->setItem(newrow, YAW_COLUMN, new QTableWidgetItem(Yaw));
			ui->Table->setItem(newrow, ROLL_COLUMN, new QTableWidgetItem(Roll));
			ui->Table->setItem(newrow, FOV_COLUMN, new QTableWidgetItem(Fov));
			ui->Table->setItem(newrow, EYE_COLUMN, new QTableWidgetItem(Eye));
			ui->Table->setItem(newrow, V_OFFSET_COLUMN, new QTableWidgetItem(V));
			ui->Table->setItem(newrow, H_OFFSET_COLUMN, new QTableWidgetItem(H));
			ui->Table->blockSignals(false);
			ui->Table->setItem(newrow, TEXT_COLUMN, new QTableWidgetItem(Text));

			ReadRow(newrow);
			continue;
		}

		Match = TableLine.match(Line);
		if (Match.hasMatch()) {
			QString Time = Match.captured(1);
			QString Transition = Match.captured(2);
			QString Pitch = Match.captured(3);
			QString Yaw = Match.captured(4);
			QString Roll = Match.captured(5);
			QString Fov = Match.captured(6);
			QString Eye = Match.captured(7);
			QString Text = Match.captured(8);

			int newrow = ui->Table->rowCount();

			ui->Table->blockSignals(true);
			ui->Table->insertRow(newrow);

			ui->Table->setItem(newrow, TIME_COLUMN, new QTableWidgetItem(Time));
			ui->Table->setItem(newrow, TRANSITION_COLUMN, new QTableWidgetItem(Transition));
			ui->Table->setItem(newrow, PITCH_COLUMN, new QTableWidgetItem(Pitch));
			ui->Table->setItem(newrow, YAW_COLUMN, new QTableWidgetItem(Yaw));
			ui->Table->setItem(newrow, ROLL_COLUMN, new QTableWidgetItem(Roll));
			ui->Table->setItem(newrow, FOV_COLUMN, new QTableWidgetItem(Fov));
			ui->Table->setItem(newrow, EYE_COLUMN, new QTableWidgetItem(Eye));
			ui->Table->setItem(newrow, V_OFFSET_COLUMN, new QTableWidgetItem("0"));
			ui->Table->setItem(newrow, H_OFFSET_COLUMN, new QTableWidgetItem("0"));
			ui->Table->blockSignals(false);
			ui->Table->setItem(newrow, TEXT_COLUMN, new QTableWidgetItem(Text));

			ReadRow(newrow);
			continue;
		}

		Match = InputOptions.match(Line);
		if (Match.hasMatch()) {
			if (Match.captured(1) == "Version") Version = Match.captured(2);
			else if (Match.captured(1) == "File") ui->InputFile->setText(Match.captured(2));
			else if (Match.captured(1) == "SmallFile") ui->SmallInputFile->setText(Match.captured(2));
			else if (Match.captured(1) == "Format") ui->formatComboBox->setCurrentText(Match.captured(2));
			else if (Match.captured(1) == "Mode") ui->modeComboBox->setCurrentText(Match.captured(2));
			else if (Match.captured(1) == "OutputMode") ui->OutputMode->setCurrentText(Match.captured(2));
			else if (Match.captured(1) == "HFOV") ui->HFovSpinBox->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "VFOV") ui->VFovSpinBox->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "FPS") ui->FPSSpinBox->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "CMDFile") ui->CMDFile->setText(Match.captured(2));
			else if (Match.captured(1) == "InputResolutionW") ui->InputResolutionW->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "InputResolutionH") ui->InputResolutionH->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "InputPixelFormat") ui->InputPixelFormat->setText(Match.captured(2));

			else if (Match.captured(1) == "VideoLength")
				ui->VideoLength->setTime(QTime::fromString(Match.captured(2), TIME_FORMAT));

			else if (Match.captured(1) == "InputPadW") ui->InputPadW->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "InputPadH") ui->InputPadH->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "InputPadX") ui->InputPadX->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "InputPadY") ui->InputPadY->setValue(Match.captured(2).toInt());

			else if (Match.captured(1) == "OutputFile") ui->OutFile->setText(Match.captured(2));
			else if (Match.captured(1) == "OutputResolutionW") ui->OutW->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "OutputResolutionH") ui->OutH->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "OutputPixelFormat") ui->OutPXFormat->setCurrentText(Match.captured(2));

			else if (Match.captured(1) == "CurrentTime")
				ui->RawTime->setTime(QTime::fromString(Match.captured(2), TIME_FORMAT));
			else if (Match.captured(1) == "CurrentPitch") ui->Pitch->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "CurrentYaw") ui->Yaw->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "CurrentRoll") ui->Roll->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "CurrentFOV") ui->Fov->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "CurrentEye") ui->RightEye->setChecked(Match.captured(2).toInt() > 0);
			else if (Match.captured(1) == "MovementAlg") ui->MovementAlg->setCurrentText(Match.captured(2));
			else if (Match.captured(1) == "IntermediatePercent") ui->IntermediatePercent->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "CustomV360") ui->CustomV360->setText(Match.captured(2));
			else if (Match.captured(1) == "CustomInputFilters") ui->CustomInputFilters->setText(Match.captured(2));
			else if (Match.captured(1) == "CustomFilters") ui->CustomFilters->setText(Match.captured(2));

			else if (Match.captured(1) == "FilterExposure") ui->FilterExposure->setChecked(Match.captured(2).toInt() > 0);
			else if (Match.captured(1) == "FilterColorTemperature")
				ui->FilterColorTemperature->setChecked(Match.captured(2).toInt() > 0);
			else if (Match.captured(1) == "FilterEQ") ui->FilterEQ->setChecked(Match.captured(2).toInt() > 0);
			else if (Match.captured(1) == "FilterColorBalance") ui->FilterColorBalance->setChecked(Match.captured(2).toInt() > 0);

			else if (Match.captured(1) == "MetadataTitle") ui->MetadataTitle->setText(Match.captured(2));
			else if (Match.captured(1) == "MetadataStudio") ui->MetadataStudio->setText(Match.captured(2));
			else if (Match.captured(1) == "MetadataActors") ui->MetadataActors->setText(Match.captured(2));
			else if (Match.captured(1) == "MetadataReleaseDate")
				ui->MetadataReleaseDate->setDate(QDate::fromString(Match.captured(2), ui->MetadataReleaseDate->displayFormat()));
			//			else if (Match.captured(1) == "MetadataSynopsis") ui->MetadataSynopsis->setText(Match.captured(2));
			else if (Match.captured(1) == "CurrentVOffset") ui->VOffset->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "CurrentHOffset") ui->HOffset->setValue(Match.captured(2).toDouble());
			else if (Match.captured(1) == "CurrentSelected") CurrentSelectedRow = Match.captured(2).toInt();
			else if (Match.captured(1) == "PausedOndemandCache") ui->PauseOndemand->setChecked(Match.captured(2).toInt() > 0);
			else if (Match.captured(1) == "GenerateChapters") ui->GenerateChapters->setChecked(Match.captured(2).toInt() > 0);
			else if (Match.captured(1) == "FilterCompare") ui->FilterCompare->setChecked(Match.captured(2).toInt() > 0);
			else if (Match.captured(1) == "CurrentFilters") LoadFiltersFields(Match.captured(2));

			else if (Match.captured(1) == "SplitSeconds") ui->SplitSeconds->setValue(Match.captured(2).toInt());
			else if (Match.captured(1) == "SplitConvert") {
				if (Match.captured(2).toInt() > 0)
					ui->SplitConvert->setChecked(true);
				else
					ui->SplitPreview->setChecked(true);
			} else if (Match.captured(1) == "SplitTMPDir") ui->SplitTMPDir->setText(Match.captured(2));
			else if (Match.captured(1) == "SplitFormat") SplitFormat = Match.captured(2);
			else if (Match.captured(1) == "SplitOrder") ui->SplitOrder->setCurrentText(Match.captured(2));
			continue;
		}

		Match = FilterLine.match(Line);
		if (Match.hasMatch()) {
			QString Time = Match.captured(1);
			QString Filters = Match.captured(2);

			int newrow = ui->FiltersList->rowCount();
			ui->FiltersList->blockSignals(true);
			ui->FiltersList->insertRow(newrow);
			ui->FiltersList->setItem(newrow, FILTERS_TIME_COLUMN, new QTableWidgetItem(Time));
			ui->FiltersList->setItem(newrow, FILTERS_FILTER_COLUMN, new QTableWidgetItem(Filters));
			ui->FiltersList->blockSignals(false);
			continue;
		}

		Match = SplitLine.match(Line);
		if (Match.hasMatch()) {
			QString Time = Match.captured(1);
			QString Before = Match.captured(2);
			QString After = Match.captured(3);
			QString AdjustedBefore = Match.captured(4);
			QString AdjustedAfter = Match.captured(5);
			QString File = Match.captured(6);

			int newrow = ui->SplitTable->rowCount();
			ui->SplitTable->blockSignals(true);
			ui->SplitTable->insertRow(newrow);
			ui->SplitTable->setItem(newrow, SPLIT_STATE_COLUMN, new QTableWidgetItem(""));
			ui->SplitTable->setItem(newrow, SPLIT_TIME_COLUMN, new QTableWidgetItem(Time));
			ui->SplitTable->setItem(newrow, SPLIT_BEFORE_COLUMN, new QTableWidgetItem(Before));
			ui->SplitTable->setItem(newrow, SPLIT_AFTER_COLUMN, new QTableWidgetItem(After));
			ui->SplitTable->setItem(newrow, SPLIT_ADJUST_BEFORE_COLUMN, new QTableWidgetItem(AdjustedBefore));
			ui->SplitTable->setItem(newrow, SPLIT_ADJUST_AFTER_COLUMN, new QTableWidgetItem(AdjustedAfter));
			ui->SplitTable->setItem(newrow, SPLIT_FILE_COLUMN, new QTableWidgetItem(File));
			ui->SplitTable->blockSignals(false);
			continue;
		}
	}

	if (Version != "") {
		int FileVersion = 0;
		int ProgramVersion = 0;
		static QRegularExpression ExtractVersion("^VR2Normal (\\d+).(\\d+)(\\w*)$");
		QRegularExpressionMatch Match = ExtractVersion.match(Version);
		if (Match.hasMatch()) {
			FileVersion = Match.captured(1).toInt() * 1000;
			FileVersion += Match.captured(2).toInt();
		}

		Match = ExtractVersion.match(WindowTitleVersion);
		if (Match.hasMatch()) {
			ProgramVersion = Match.captured(1).toInt() * 1000;
			ProgramVersion += Match.captured(2).toInt();
		}

		if (ProgramVersion < FileVersion) {
			int ret = QMessageBox::warning(this, "File version problem",
										   "This file was created by a higher version of the program,\n"
										   "if you continue some of the new features will be lost when you save the file.",
										   QMessageBox::Ok | QMessageBox::Cancel,
										   QMessageBox::Ok);
			switch (ret) {
				case QMessageBox::Ok:
					break;
				case QMessageBox::Cancel:
					TableUndo->clear();
					CleanTable(ui->Table);
					CleanTable(ui->FiltersList);
					CleanTable(ui->SplitTable);
					SetDefaultInputOptions();
					ui->MovementFile->setText("");
					break;
				default:
					break;
			}
		}
	}

	if (CurrentSelectedRow >= 0) {
		ui->Table->blockSignals(true);
		ui->Table->selectRow(CurrentSelectedRow);
		ui->Table->scrollToItem(ReadItem(CurrentSelectedRow, TIME_COLUMN));
		ui->Table->blockSignals(false);
	}

	BlockRefresh = false;
	ReloadRawImage(ui->RawTime->time());
	PendingChanges = false;
	setWindowTitle(WindowTitleVersion);
}

void MainWindow::SaveMovementFile(QString TmpFile) {
	bool Alternative = false;
	if (TmpFile != "") {
		Alternative = true;
	} else {
		if (ui->MovementFile->text() == "") {
			if (ui->InputFile->text() != "") {
				ui->MovementFile->setText(ui->InputFile->text() + ".vr2n");
			} else {
				return;
			}
		}
	}

	QString Output;

	Output.append("#VR2Normal File (https://gitlab.com/vongooB9/vr2normal)\n");
	Output.append("#Version: " + WindowTitleVersion + "\n");

	if (Alternative) {
		Output.append("#File: " + QFileInfo(ui->InputFile->text()).fileName() + "\n");
		if (ui->SmallInputFile->text() != "")
			Output.append("#SmallFile: " + QFileInfo(ui->SmallInputFile->text()).fileName() + "\n");
	} else {
		Output.append("#File: " + ui->InputFile->text() + "\n");
		if (ui->SmallInputFile->text() != "")
			Output.append("#SmallFile: " + ui->SmallInputFile->text() + "\n");
	}

	Output.append("#Format: " + ui->formatComboBox->currentText() + "\n");
	Output.append("#Mode: " + ui->modeComboBox->currentText() + "\n");
	Output.append("#OutputMode: " + ui->OutputMode->currentText() + "\n");
	Output.append("#VFOV: " + ui->VFovSpinBox->text() + "\n");
	Output.append("#HFOV: " + ui->HFovSpinBox->text() + "\n");
	Output.append("#FPS: " + QString::number(ui->FPSSpinBox->value(), 'f', 2) + "\n");
	if (!Alternative && ui->CMDFile->text() != "")
		Output.append("#CMDFile: " + ui->CMDFile->text() + "\n");
	Output.append("#InputResolutionW: " + QString::number(ui->InputResolutionW->value()) + "\n");
	Output.append("#InputResolutionH: " + QString::number(ui->InputResolutionH->value()) + "\n");
	Output.append("#InputPixelFormat: " + ui->InputPixelFormat->text() + "\n");
	Output.append("#VideoLength: " + ui->VideoLength->text() + "\n");

	Output.append("#InputPadW: " + QString::number(ui->InputPadW->value()) + "\n");
	Output.append("#InputPadH: " + QString::number(ui->InputPadH->value()) + "\n");
	Output.append("#InputPadX: " + QString::number(ui->InputPadX->value()) + "\n");
	Output.append("#InputPadY: " + QString::number(ui->InputPadY->value()) + "\n");

	if (!Alternative && ui->CMDFile->text() != "")
		Output.append("#OutputFile: " + ui->OutFile->text() + "\n");
	Output.append("#OutputResolutionW: " + QString::number(ui->OutW->value()) + "\n");
	Output.append("#OutputResolutionH: " + QString::number(ui->OutH->value()) + "\n");
	Output.append("#OutputPixelFormat: " + ui->OutPXFormat->currentText() + "\n");

	Output.append("#MovementAlg: " + ui->MovementAlg->currentText() + "\n");
	if (ui->MovementAlg->currentText() == "Intermediate")
		Output.append("#IntermediatePercent: " + QString::number(ui->IntermediatePercent->value()) + "\n");
	if (ui->CustomV360->text() != "")
		Output.append("#CustomV360: " + ui->CustomV360->text() + "\n");

	if (ui->CustomInputFilters->text() != "")
		Output.append("#CustomInputFilters: " + ui->CustomInputFilters->text() + "\n");
	if (ui->CustomFilters->text() != "")
		Output.append("#CustomFilters: " + ui->CustomFilters->text() + "\n");

	if (ui->FilterExposure->isChecked())
		Output.append("#FilterExposure: " + QString::number(ui->FilterExposure->isChecked()) + "\n");
	if (ui->FilterColorTemperature->isChecked())
		Output.append("#FilterColorTemperature: " + QString::number(ui->FilterColorTemperature->isChecked()) + "\n");
	if (ui->FilterEQ->isChecked())
		Output.append("#FilterEQ: " + QString::number(ui->FilterEQ->isChecked()) + "\n");
	if (ui->FilterColorBalance->isChecked())
		Output.append("#FilterColorBalance: " + QString::number(ui->FilterColorBalance->isChecked()) + "\n");

	if (ui->MetadataTitle->text() != "")
		Output.append("#MetadataTitle: " + ui->MetadataTitle->text() + "\n");
	if (ui->MetadataStudio->text() != "")
		Output.append("#MetadataStudio: " + ui->MetadataStudio->text() + "\n");
	if (ui->MetadataActors->text() != "")
		Output.append("#MetadataActors: " + ui->MetadataActors->text() + "\n");
	if (ui->MetadataReleaseDate->text() != "" && ui->MetadataReleaseDate->date().year() != 2000)
		Output.append("#MetadataReleaseDate: " + ui->MetadataReleaseDate->text() + "\n");

	if (!Alternative) {
		Output.append("#CurrentTime: " + ui->RawTime->text() + "\n");
		Output.append("#CurrentPitch: " + QString::number(ui->Pitch->value()) + "\n");
		Output.append("#CurrentYaw: " + QString::number(ui->Yaw->value()) + "\n");
		Output.append("#CurrentRoll: " + QString::number(ui->Roll->value()) + "\n");
		Output.append("#CurrentFOV: " + QString::number(ui->Fov->value()) + "\n");
		Output.append("#CurrentEye: " + QString::number(ui->RightEye->isChecked()) + "\n");
		Output.append("#CurrentVOffset: " + QString::number(ui->VOffset->value()) + "\n");
		Output.append("#CurrentHOffset: " + QString::number(ui->HOffset->value()) + "\n");
		Output.append("#CurrentSelected: " + QString::number(GetSelectedRow()) + "\n");
		Output.append("#PausedOndemandCache: " + QString::number(ui->PauseOndemand->isChecked()) + "\n");
		Output.append("#GenerateChapters: " + QString::number(ui->GenerateChapters->isChecked()) + "\n");
		Output.append("#FilterCompare: " + QString::number(ui->FilterCompare->isChecked()) + "\n");
		QString CurrentFilters = GenerateFiltersString();
		if (CurrentFilters != "") Output.append("#CurrentFilters: " + CurrentFilters + "\n");

		Output.append("#SplitSeconds: " + QString::number(ui->SplitSeconds->value()) + "\n");
		Output.append("#SplitConvert: " + QString::number(ui->SplitConvert->isChecked()) + "\n");
		Output.append("#SplitTMPDir: " + QDir::toNativeSeparators(ui->SplitTMPDir->text()) + "\n");
		Output.append("#SplitFormat: " + SplitFormat + "\n");
		Output.append("#SplitOrder: " + ui->SplitOrder->currentText() + "\n");
	}

	Output.append("\n\n");
	QStringList Text = Table2TSV(ui->Table);
	Output.append(Text.join("\n"));

	Output.append("\n\n");
	QStringList FitersText = Table2TSV(ui->FiltersList, "FilterList: ");
	Output.append(FitersText.join("\n"));

	if (!Alternative) {
		Output.append("\n\n");
		QStringList SplitTable = Table2TSV(ui->SplitTable, "SplitList: ", SPLIT_STATE_COLUMN);
		Output.append(SplitTable.join("\n"));
	}

	if (Alternative) {
		WriteFile(TmpFile, Output);
	} else {
		WriteFile(ui->MovementFile->text(), Output);
		PendingChanges = false;
		setWindowTitle(WindowTitleVersion);
	}
}

void MainWindow::SaveDefaultMovementFile() {
	SaveMovementFile("");
}

void MainWindow::on_NewMovement_clicked() {
	QString File = QFileDialog::getSaveFileName(this, "New VR2Normal file", "", "VR2Normal File (*.vr2n)");

	if (File == "") return;
	BlockRefresh = true;
	TableUndo->clear();
	CleanTable(ui->Table);
	CleanTable(ui->FiltersList);
	CleanTable(ui->SplitTable);

	SetDefaultInputOptions();

	ui->MovementFile->setText(File);
	BlockRefresh = false;
	ui->RightTabs->setCurrentIndex(RIGHT_TAB_INPUT);
}

void MainWindow::on_NewCMDFile_clicked() {
	QFileInfo input(ui->MovementFile->text());
	QString File = QFileDialog::getSaveFileName(this, "New CMD file", input.absoluteFilePath() + ".cmd",
				   "CMD file (*.cmd) ;; All Files (*)");
	if (File == "") return;
	ui->CMDFile->setText(File);
}

void MainWindow::SetDefaultInputOptions() {
	RawData.clear();
	FilterResultData.clear();
	ui->RawFrame->clear();
	ui->CameraFrame->clear();

	ui->InputFile->setText("");
	ui->SmallInputFile->setText("");
	ui->formatComboBox->setCurrentText("hequirect");
	ui->modeComboBox->setCurrentText("sbs");
	ui->OutputMode->setCurrentText("2d");
	ui->FPSSpinBox->setValue(0);
	ui->VFovSpinBox->setValue(180);
	ui->HFovSpinBox->setValue(180);
	ui->VideoLength->setTime(QTime::fromMSecsSinceStartOfDay(0));
	ui->InputResolutionW->setValue(0);
	ui->InputResolutionH->setValue(0);
	ui->InputPixelFormat->setText("");
	ui->InputPadW->setValue(0);
	ui->InputPadH->setValue(0);
	ui->InputPadX->setValue(0);
	ui->InputPadY->setValue(0);
	ui->CMDFile->setText("");
	ui->RawTime->setTime(QTime::fromMSecsSinceStartOfDay(0));
	ui->Pitch->setValue(0);
	ui->Yaw->setValue(0);
	ui->Roll->setValue(0);
	ui->Fov->setValue(100);
	ui->Trans->setValue(1);
	ui->Text->setText("");
	ui->RightEye->setChecked(false);
	ui->MovementAlg->setCurrentText("Smooth");
	ui->IntermediatePercent->setValue(50);
	ui->CustomV360->setText("");
	ui->CustomInputFilters->setText("");
	ui->CustomFilters->setText("");
	ui->FilterExposure->setChecked(false);
	ui->FilterColorTemperature->setChecked(false);
	ui->FilterEQ->setChecked(false);
	ui->FilterColorBalance->setChecked(false);
	LoadFiltersFields(GenerateDefaultFilters());
	ui->FilterCompare->setChecked(false);
	ui->MetadataTitle->setText("");
	ui->MetadataStudio->setText("");
	ui->MetadataActors->setText("");
	ui->MetadataReleaseDate->setDate(QDate::fromString("2000-01-01", ui->MetadataReleaseDate->displayFormat()));
	ui->VOffset->setValue(0);
	ui->HOffset->setValue(0);
	ui->GenerateChapters->setChecked(true);

	ui->OutW->setValue(0);
	ui->OutH->setValue(0);
	ui->OutPXFormat->setCurrentText("Auto");
	ui->OutFile->setText("");
	ui->CPreviewFile->setText("");
	ui->SplitTMPDir->setText("");
	ui->SplitSeconds->setValue(60);
	ui->SplitConvert->setChecked(true);
	SplitFormat = "";
	SplitFormatOptions.clear();
}

QString MainWindow::ReadItemValue(QTableWidget *Table, int Row, int Column, const QString DefaultValue) {
	QTableWidgetItem *Item;
	Item = Table->item(Row, Column);
	if (Item == nullptr) {
		Table->setItem(Row, Column, new QTableWidgetItem(DefaultValue));
		return DefaultValue;
	} else {
		return Item->text();
	}
}

QString MainWindow::ReadItemValue(int Row, int Column, const QString DefaultValue) {
	return ReadItemValue(ui->Table, Row, Column, DefaultValue);
}

QTableWidgetItem *MainWindow::ReadItem(int Row, int Column) {
	return ui->Table->item(Row, Column);
}

bool MainWindow::CheckValidTableTime(int Row, QTime &Time) {
	QTableWidgetItem *Item = ReadItem(Row, TIME_COLUMN);
	if (Item == nullptr) {
		Item = new QTableWidgetItem("00:00:00.000");
		Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		ui->Table->setItem(Row, TIME_COLUMN, Item);
		return false;
	}
	QString Value = Item->text();
	Time = QTime::fromString(Value, TIME_FORMAT);
	if (!Time.isValid()) {
		Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		return false;
	}

	if (Row > 0) {
		QString PrevValue = ReadItemValue(Row - 1, TIME_COLUMN);
		QTime PrevTime = QTime::fromString(PrevValue, TIME_FORMAT);
		if (Time <= PrevTime) {
			Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
			return false;
		}
	}

	Item->setForeground(QBrush());
	return true;
}

void MainWindow::LoadShortcuts() {
	QString ShortcutString;
	Settings->beginGroup("Shortcuts");


#define LOAD_SHORTCUT(name, edit_name, key) ShortcutString = Settings->value(#name, key).toString();\
	ui->edit_name->setKeySequence(QKeySequence(ShortcutString));\
	name = new QShortcut(ui->edit_name->keySequence(), this)

#define LOAD_SHORTCUT_UI(name, edit_name, key) ui->edit_name->setKeySequence(QKeySequence(Settings->value(#name, key).toString()));\
	ui->name->setShortcut(ui->edit_name->keySequence());

	LOAD_SHORTCUT(PitchUP, PitchUpKS, "W");
	LOAD_SHORTCUT(PitchDown, PitchDownKS, "S");
	LOAD_SHORTCUT(YawUP, YawUpKS, "D");
	LOAD_SHORTCUT(YawDown, YawDownKS, "A");
	LOAD_SHORTCUT(RollUP, RollUpKS, "X");
	LOAD_SHORTCUT(RollDown, RollDownKS, "C");
	LOAD_SHORTCUT(FovUP, FovUpKS, "Q");
	LOAD_SHORTCUT(FovDown, FovDownKS, "E");
	LOAD_SHORTCUT(TranUP, TranUpKS, "R");
	LOAD_SHORTCUT(TranDown, TranDownKS, "F");
	LOAD_SHORTCUT(VUP, VUpKS, "Shift+S");
	LOAD_SHORTCUT(VDown, VDownKS, "Shift+W");
	LOAD_SHORTCUT(HUP, HUpKS, "Shift+D");
	LOAD_SHORTCUT(HDown, HDownKS, "Shift+A");
	LOAD_SHORTCUT(FocusText, FocusTextKS, "T");
	LOAD_SHORTCUT(RowUp, RowUpKS, "Shift+Up");
	LOAD_SHORTCUT(RowDown, RowDownKS, "Shift+Down");
	LOAD_SHORTCUT(Undo, UndoKS, "Ctrl+Z");
	LOAD_SHORTCUT(Redo, RedoKS, "Ctrl+Shift+Z");
	LOAD_SHORTCUT(Unselect, UnselectKS, "Ctrl+<");

	LOAD_SHORTCUT_UI(Save, SaveKS, "Ctrl+S");
	ui->FilterSave->setShortcut(ui->SaveKS->keySequence());
	LOAD_SHORTCUT_UI(AddLine, AddLineKS, "Ins");
	LOAD_SHORTCUT_UI(AddTansZero, AddTansZeroKS, "Shift+Ins");
	LOAD_SHORTCUT_UI(AddPrev, AddPrevKS, "Ctrl+Ins");
	LOAD_SHORTCUT_UI(RemoveLine, RemoveLineKS, "Del");
	LOAD_SHORTCUT_UI(UpdateTime, UpdateTimeKS, "Home");
	LOAD_SHORTCUT_UI(UnselectRow, UnselectRowKS, "Esc");
	LOAD_SHORTCUT_UI(NextMinute, NextMinuteKS, "Ctrl+Up");
	LOAD_SHORTCUT_UI(PrevMinute, PrevMinuteKS, "Ctrl+Down");
	LOAD_SHORTCUT_UI(PrevSec, PrevSecondKS, "Left");
	LOAD_SHORTCUT_UI(NextSecond, NextSecondKS, "Right");
	LOAD_SHORTCUT_UI(Prev10Sec, Prev10SecKS, "Down");
	LOAD_SHORTCUT_UI(Next10Sec, Next10SecKS, "Up");
	LOAD_SHORTCUT_UI(PrevFrame, PrevFrameKS, "Ctrl+Left");
	LOAD_SHORTCUT_UI(NextFrame, NextFrameKS, "Ctrl+Right");
	LOAD_SHORTCUT_UI(RightEye, RightEyeKS, "Z");
	LOAD_SHORTCUT_UI(PreviewCurrentLine, PreviewCurrentLineKS, "P");
	LOAD_SHORTCUT_UI(UpdateTransition, UpdateTransitionKS, "End");

	Settings->endGroup();
}

void MainWindow::SaveShortcuts() {
	Settings->beginGroup("Shortcuts");
	QKeySequence Key;

#define SAVE_SHORTCUT(name, edit_name, defkey) Key = ui->edit_name->keySequence();\
	if (!Key.isEmpty() && Key.toString() != defkey) Settings->setValue(#name, Key.toString());\
	else Settings->remove(#name);

	SAVE_SHORTCUT(PitchUP, PitchUpKS, "W");
	SAVE_SHORTCUT(PitchDown, PitchDownKS, "S");
	SAVE_SHORTCUT(YawUP, YawUpKS, "D");
	SAVE_SHORTCUT(YawDown, YawDownKS, "A");
	SAVE_SHORTCUT(RollUP, RollUpKS, "X");
	SAVE_SHORTCUT(RollDown, RollDownKS, "C");
	SAVE_SHORTCUT(FovUP, FovUpKS, "Q");
	SAVE_SHORTCUT(FovDown, FovDownKS, "E");
	SAVE_SHORTCUT(TranUP, TranUpKS, "R");
	SAVE_SHORTCUT(TranDown, TranDownKS, "F");
	SAVE_SHORTCUT(VUP, VUpKS, "Shift+S");
	SAVE_SHORTCUT(VDown, VDownKS, "Shift+W");
	SAVE_SHORTCUT(HUP, HUpKS, "Shift+D");
	SAVE_SHORTCUT(HDown, HDownKS, "Shift+A");
	SAVE_SHORTCUT(FocusText, FocusTextKS, "T");
	SAVE_SHORTCUT(RowUp, RowUpKS, "Shift+Up");
	SAVE_SHORTCUT(RowDown, RowDownKS, "Shift+Down");
	SAVE_SHORTCUT(Undo, UndoKS, "Ctrl+Z");
	SAVE_SHORTCUT(Redo, RedoKS, "Ctrl+Shift+Z");
	SAVE_SHORTCUT(Unselect, UnselectKS, "Ctrl+<");

	SAVE_SHORTCUT(Save, SaveKS, "Ctrl+S");
	SAVE_SHORTCUT(AddLine, AddLineKS, "Ins");
	SAVE_SHORTCUT(AddTansZero, AddTansZeroKS, "Shift+Ins");
	SAVE_SHORTCUT(AddPrev, AddPrevKS, "Ctrl+Ins");
	SAVE_SHORTCUT(RemoveLine, RemoveLineKS, "Del");
	SAVE_SHORTCUT(UpdateTime, UpdateTimeKS, "Home");
	SAVE_SHORTCUT(UnselectRow, UnselectRowKS, "Esc");
	SAVE_SHORTCUT(NextMinute, NextMinuteKS, "Ctrl+Up");
	SAVE_SHORTCUT(PrevMinute, PrevMinuteKS, "Ctrl+Down");
	SAVE_SHORTCUT(PrevSecond, PrevSecondKS, "Left");
	SAVE_SHORTCUT(Prev10Sec, Prev10SecKS, "Down");
	SAVE_SHORTCUT(Next10Sec, Next10SecKS, "Up");
	SAVE_SHORTCUT(PrevFrame, PrevFrameKS, "Ctrl+Left");
	SAVE_SHORTCUT(NextSecond, NextSecondKS, "Right");
	SAVE_SHORTCUT(NextFrame, NextFrameKS, "Ctrl+Right");
	SAVE_SHORTCUT(RightEye, RightEyeKS, "Z");
	SAVE_SHORTCUT(PreviewCurrentLine, PreviewCurrentLineKS, "P");
	SAVE_SHORTCUT(UpdateTransition, UpdateTransitionKS, "End");

	Settings->endGroup();

	Settings->sync();
}

void MainWindow::ApplyShortcuts() {

#define APPLY_SHORTCUT(name, edit_name) name->setKey(ui->edit_name->keySequence())
#define APPLY_SHORTCUT_UI(name, edit_name) ui->name->setShortcut(ui->edit_name->keySequence())

	APPLY_SHORTCUT(PitchUP, PitchUpKS);
	APPLY_SHORTCUT(PitchDown, PitchDownKS);
	APPLY_SHORTCUT(YawUP, YawUpKS);
	APPLY_SHORTCUT(YawDown, YawDownKS);
	APPLY_SHORTCUT(RollUP, RollUpKS);
	APPLY_SHORTCUT(RollDown, RollDownKS);
	APPLY_SHORTCUT(FovUP, FovUpKS);
	APPLY_SHORTCUT(FovDown, FovDownKS);
	APPLY_SHORTCUT(TranUP, TranUpKS);
	APPLY_SHORTCUT(TranDown, TranDownKS);
	APPLY_SHORTCUT(VUP, VUpKS);
	APPLY_SHORTCUT(VDown, VDownKS);
	APPLY_SHORTCUT(HUP, HUpKS);
	APPLY_SHORTCUT(HDown, HDownKS);
	APPLY_SHORTCUT(FocusText, FocusTextKS);
	APPLY_SHORTCUT(RowUp, RowUpKS);
	APPLY_SHORTCUT(RowDown, RowDownKS);
	APPLY_SHORTCUT(Undo, UndoKS);
	APPLY_SHORTCUT(Redo, RedoKS);
	APPLY_SHORTCUT(Unselect, UnselectKS);

	APPLY_SHORTCUT_UI(Save, SaveKS);
	APPLY_SHORTCUT_UI(AddLine, AddLineKS);
	APPLY_SHORTCUT_UI(AddTansZero, AddTansZeroKS);
	APPLY_SHORTCUT_UI(AddPrev, AddPrevKS);
	APPLY_SHORTCUT_UI(RemoveLine, RemoveLineKS);
	APPLY_SHORTCUT_UI(UpdateTime, UpdateTimeKS);
	APPLY_SHORTCUT_UI(UnselectRow, UnselectRowKS);
	APPLY_SHORTCUT_UI(NextMinute, NextMinuteKS);
	APPLY_SHORTCUT_UI(PrevMinute, PrevMinuteKS);
	APPLY_SHORTCUT_UI(PrevSec, PrevSecondKS);
	APPLY_SHORTCUT_UI(NextSecond, NextSecondKS);
	APPLY_SHORTCUT_UI(Prev10Sec, Prev10SecKS);
	APPLY_SHORTCUT_UI(Next10Sec, Next10SecKS);
	APPLY_SHORTCUT_UI(PrevFrame, PrevFrameKS);
	APPLY_SHORTCUT_UI(NextFrame, NextFrameKS);
	APPLY_SHORTCUT_UI(RightEye, RightEyeKS);
	APPLY_SHORTCUT_UI(PreviewCurrentLine, PreviewCurrentLineKS);
	APPLY_SHORTCUT_UI(UpdateTransition, UpdateTransitionKS);
}

tableline_t MainWindow::ReadRow(int Row) {
	ui->Table->blockSignals(true);
	tableline_t Out;
	Out.Valid = true;

	QString itemvalue;
	bool ok_conversion;

	QTableWidgetItem *Item = nullptr;

	QTime Time;

	if (CheckValidTableTime(Row, Time)) {
		Out.Time = Time.msecsSinceStartOfDay();
	} else {
		Out.Time = -1;
		Out.Valid = false;
	}

	itemvalue = ReadItemValue(Row, TRANSITION_COLUMN, "0");
	Item = ReadItem(Row, TRANSITION_COLUMN);
	//TODO: Check when it is null
	Out.Transition = itemvalue.toDouble(&ok_conversion) * 1000;
	if (!ok_conversion) {
		if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		Out.Transition = 0;
		Out.Valid = false;
	} else {
		if (Item != nullptr) Item->setForeground(QBrush());
	}

	if (Row < ui->Table->rowCount() - 1) {
		QString NextTimeValue = ReadItemValue(Row + 1, TIME_COLUMN);
		QTime NextTime = QTime::fromString(NextTimeValue, TIME_FORMAT);
		QTime TimeTansition = Time.addMSecs(Out.Transition);
		if (Time.msecsSinceStartOfDay() < NextTime.msecsSinceStartOfDay() &&
			TimeTansition.msecsSinceStartOfDay() > NextTime.msecsSinceStartOfDay()) {
			if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
			Out.Valid = false;
		} else {
			if (Item != nullptr) Item->setForeground(QBrush());
		}
	}

	itemvalue = ReadItemValue(Row, PITCH_COLUMN, "0");
	Item = ReadItem(Row, PITCH_COLUMN);
	Out.Pitch = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		Out.Pitch = 0;
		Out.Valid = false;
	} else {
		if (Item != nullptr) Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, YAW_COLUMN, "0");
	Item = ReadItem(Row, YAW_COLUMN);
	Out.Yaw = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		Out.Yaw = 0;
		Out.Valid = false;
	} else {
		if (Item != nullptr) Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, ROLL_COLUMN, "0");
	Item = ReadItem(Row, ROLL_COLUMN);
	Out.Roll = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		Out.Roll = 0;
		Out.Valid = false;
	} else {
		if (Item != nullptr) Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, FOV_COLUMN, "100");
	Item = ReadItem(Row, FOV_COLUMN);
	Out.FOV = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		Out.FOV = 0;
		Out.Valid = false;
	} else {
		double hfov = CalcHFOV(ui->GUIPreviewW->value(), ui->GUIPreviewH->value(), Out.FOV);
		double vfov = CalcVFOV(ui->GUIPreviewW->value(), ui->GUIPreviewH->value(), Out.FOV);
		if (hfov > 100 || vfov > 100) {
			if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->HighWarningColor->text())));
		} else if (hfov > 92 || vfov > 92) {
			if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->LowWarningColor->text())));
		} else {
			if (Item != nullptr) Item->setForeground(QBrush());
		}
	}

	itemvalue = ReadItemValue(Row, EYE_COLUMN, "0");
	if (Row > 0) {
		QString AntEyeValue = ReadItemValue(Row - 1, EYE_COLUMN, "0");
		Item = ReadItem(Row, EYE_COLUMN);
		if (AntEyeValue != itemvalue && Out.Transition > 0) {
			if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		} else {
			if (Item != nullptr) Item->setForeground(QBrush());
		}
	}
	if (itemvalue == "0")
		Out.RightEye = false;
	else
		Out.RightEye = true;

	itemvalue = ReadItemValue(Row, V_OFFSET_COLUMN, "0");
	Item = ReadItem(Row, V_OFFSET_COLUMN);
	Out.V = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		Out.V = 0;
		Out.Valid = false;
	} else {
		if (Item != nullptr) Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, H_OFFSET_COLUMN, "0");
	Item = ReadItem(Row, H_OFFSET_COLUMN);
	Out.H = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		if (Item != nullptr) Item->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		Out.H = 0;
		Out.Valid = false;
	} else {
		if (Item != nullptr) Item->setForeground(QBrush());
	}

	Out.Text = ReadItemValue(Row, TEXT_COLUMN, "");

	QTableWidgetItem *VertItem = ui->Table->takeVerticalHeaderItem(Row);
	if (VertItem == nullptr) {
		VertItem = new QTableWidgetItem(QString::number(Row + 1));
	}
	if (!Out.Valid) {
		#if QT_VERSION <= QT_VERSION_CHECK(6, 0, 0) && defined (Q_OS_WIN)
		VertItem->setForeground(QBrush(QColor(ui->ErrorColor->text())));
		#else
		VertItem->setBackground(QBrush(QColor(ui->ErrorColor->text())));
		#endif
		VertItem->setText("E");
	} else {
		#if QT_VERSION <= QT_VERSION_CHECK(6, 0, 0) && defined (Q_OS_WIN)
		VertItem->setForeground(QBrush());
		#else
		VertItem->setBackground(QBrush());
		#endif
		VertItem->setText(QString::number(Row + 1));
	}

	ui->Table->setVerticalHeaderItem(Row, VertItem);

	ui->Table->blockSignals(false);

	return Out;
}

tableline_t MainWindow::LocateRow(const QTime &Time, bool CurrentCamera) {
	int Line = LocateRow(ui->Table, TIME_COLUMN, Time, CurrentCamera);
	if (Line >= 0)
		return ReadRow(Line);
	else
		return DefaultTableLine;
}

int MainWindow::LocateRow(QTableWidget *Table, int Colum, const QTime &Time, bool CurrentCamera) {
	int Line = -1;
	for (int i = 0; i < Table->rowCount(); ++i) {
		QString RowTimeValue = ReadItemValue(Table, i, Colum);
		QTime RowTime = QTime::fromString(RowTimeValue, TIME_FORMAT);
		if (CurrentCamera) {
			if (Time.msecsSinceStartOfDay() <= RowTime.msecsSinceStartOfDay()) break;
		} else {
			if (Time.msecsSinceStartOfDay() < RowTime.msecsSinceStartOfDay()) break;
		}
		Line = i;
	}
	return Line;
}

void MainWindow::on_SaveConfig_clicked() {
	Settings->setValue("ffmpegbin", ui->FfmpegBin->text());
	Settings->setValue("ffprobebin", ui->FfprobeBin->text());
	Settings->setValue("VideoPlayer", ui->VideoPlayer->text());
	Settings->setValue("ImageViewer", ui->ImageViewer->text());
	Settings->setValue("AutoRun", ui->AutoRun->isChecked());
	Settings->setValue("AutoPlay", ui->AutoPlay->isChecked());
	Settings->setValue("AutoSaveConfig", ui->AutoSaveConfig->isChecked());
	Settings->setValue("AutoSaveMovement", ui->AutoSaveMovement->isChecked());
	Settings->setValue("SaveWindowSettings", ui->SaveWindowSettings->isChecked());
	Settings->setValue("GUIPreviewW", QString::number(ui->GUIPreviewW->value()));
	Settings->setValue("GUIPreviewH", QString::number(ui->GUIPreviewH->value()));
	Settings->setValue("GUIPreviewInterpolation", ui->GUIPreviewInterpolation->currentText());
	Settings->setValue("GUIPreviewScale", ui->GUIPreviewScale->currentText());
	Settings->setValue("DefaultTranValue", QString::number(ui->DefaultTranValue->value(), 'f',
					   ui->DefaultTranValue->decimals()));
	Settings->setValue("RAMCache", QString::number(ui->RAMCache->value()));
	Settings->setValue("DiskCache", QString::number(ui->DiskCache->value()));
	Settings->setValue("DiskCacheDir", ui->DiskCacheDir->text());
	Settings->setValue("CacheFormat", ui->CacheFormat->currentText());
	Settings->setValue("CacheProcess", QString::number(ui->CacheProcess->value()));
	Settings->setValue("PreloadCacheSeoconds", QString::number(ui->PreloadCacheSeoconds->value()));
	Settings->setValue("CachePriority", ui->CachePriority->currentText());
	Settings->setValue("Priority", ui->Priority->currentText());
	Settings->setValue("ErrorColor", ui->ErrorColor->text());
	Settings->setValue("LowWarningColor", ui->LowWarningColor->text());
	Settings->setValue("HighWarningColor", ui->HighWarningColor->text());
	Settings->setValue("IncludeFile", ui->IncludeFile->isChecked());
	Settings->setValue("ShowTrayIcon", ui->ShowTrayIcon->isChecked());
	Settings->setValue("TaskbarProgress", ui->TaskbarProgress->isChecked());
	Settings->setValue("ShowFramingLines", ui->ShowFramingLines->isChecked());
	Settings->setValue("ShowEstimatedResolution", ui->ShowEstimatedResolution->isChecked());
	Settings->setValue("LimitMovement", ui->LimitMovement->isChecked());
	Settings->setValue("ShowSendcmdTab", ui->ShowSendcmdTab->isChecked());
	Settings->setValue("EnableDebug", ui->EnableDebug->isChecked());
	Settings->setValue("HWDec", ui->HWDec->currentText());
	Settings->setValue("MinFOV", QString::number(ui->MinFov->value()));
	Settings->setValue("MaxFOV", QString::number(ui->MaxFov->value()));
	Settings->setValue("PitchYawSteps", QString::number(ui->PYSteps->value(), 'f', 2));
	Settings->setValue("RollSteps", QString::number(ui->RollSteps->value(), 'f', 2));
	Settings->setValue("FOVSteps", QString::number(ui->FOVSteps->value(), 'f', 2));
	Settings->setValue("TransSteps", QString::number(ui->TransSteps->value(), 'f', 2));

	Settings->setValue("PreviewVideoFormat", ui->PreviewVideoFormat->currentText());
	Settings->setValue("PreviewPreset", ui->PreviewPreset->currentText());
	Settings->setValue("PreviewCRF", QString::number(ui->PreviewCRF->value()));
	Settings->setValue("PreviewAudioFormat", ui->PreviewAudioFormat->currentText());
	Settings->setValue("PreviewAudioBitrate", QString::number(ui->PreviewAudioBitrate->value()));
	Settings->setValue("PreviewCustomFormat", ui->PreviewCustomFormat->text());
	Settings->setValue("PreviewW", QString::number(ui->PreviewW->value()));
	Settings->setValue("PreviewH", QString::number(ui->PreviewH->value()));
	Settings->setValue("PreviewFPS", QString::number(ui->PreviewFPS->value(), 'f', 2));
	Settings->setValue("PreviewInterpolation", ui->PreviewInterpolation->currentText());
	Settings->setValue("PreviewPXFormat", ui->PreviewPXFormat->currentText());
	Settings->setValue("PreviewDir", ui->PreviewDir->text());
	Settings->setValue("PreviewTimeBefore", QString::number(ui->PreviewTimeBefore->value()));
	Settings->setValue("PreviewTimeAfter", QString::number(ui->PreviewTimeAfter->value()));
	Settings->setValue("PreviewHWDec", ui->PreviewHWDec->currentText());

	Settings->setValue("CPreviewVideoFormat", ui->CPreviewVideoFormat->currentText());
	Settings->setValue("CPreviewPreset", ui->CPreviewPreset->currentText());
	Settings->setValue("CPreviewCRF", QString::number(ui->CPreviewCRF->value()));
	Settings->setValue("CPreviewAudioFormat", ui->CPreviewAudioFormat->currentText());
	Settings->setValue("CPreviewAudioBitrate", QString::number(ui->CPreviewAudioBitrate->value()));
	Settings->setValue("CPreviewCustomFormat", ui->CPreviewCustomFormat->text());
	Settings->setValue("CPreviewW", QString::number(ui->CPreviewW->value()));
	Settings->setValue("CPreviewH", QString::number(ui->CPreviewH->value()));
	Settings->setValue("CPreviewFPS", QString::number(ui->CPreviewFPS->value(), 'f', 2));
	Settings->setValue("CPreviewInterpolation", ui->CPreviewInterpolation->currentText());
	Settings->setValue("CPreviewPXFormat", ui->CPreviewPXFormat->currentText());
	Settings->setValue("CPreviewHWDec", ui->CPreviewHWDec->currentText());

	Settings->setValue("OutVideoFormat", ui->OutVideoFormat->currentText());
	Settings->setValue("OutPreset", ui->OutPreset->currentText());
	Settings->setValue("OutCRF", QString::number(ui->OutCRF->value()));
	Settings->setValue("OutAudioFormat", ui->OutAudioFormat->currentText());
	Settings->setValue("OutAudioBitrate", QString::number(ui->OutAudioBitrate->value()));
	Settings->setValue("OutCustomFormat", ui->OutCustomFormat->text());
	Settings->setValue("OutInterpolation", ui->OutInterpolation->currentText());
	Settings->setValue("OutDefaultDir", ui->OutDefaultDir->text());
	Settings->setValue("OutSuffix", ui->OutSuffix->text());
	Settings->setValue("OutHWDec", ui->OutHWDec->currentText());
	Settings->setValue("ExecuteOnFinish", ui->ExecuteOnFinish->text());

	if (ui->MovementFile->text() != "")
		Settings->setValue("MovementFile", ui->MovementFile->text());

	Settings->setValue("ScreenshotsDir", ui->ScreenshotsDir->text());
	Settings->setValue("ScreenshotsV360W", QString::number(ui->ScreenshotsV360W->value()));
	Settings->setValue("ScreenshotsV360H", QString::number(ui->ScreenshotsV360H->value()));
	Settings->setValue("ScreenshotsW", QString::number(ui->ScreenshotsW->value()));
	Settings->setValue("ScreenshotsH", QString::number(ui->ScreenshotsH->value()));
	Settings->setValue("ScreenshotsFormat", ui->ScreenshotsFormat->currentText());
	Settings->setValue("ScreenshotsQuality", QString::number(ui->ScreenshotsQuality->value()));
	Settings->setValue("ScreenshotAutoOpen", ui->ScreenshotAutoOpen->isChecked());

	switch (ui->ScreenshotDisableFilters->checkState()) {
		case Qt::Unchecked:
			Settings->setValue("ScreenshotDisableFilters", 0);
			break;
		case Qt::PartiallyChecked:
			Settings->setValue("ScreenshotDisableFilters", 1);
			break;
		case Qt::Checked:
			Settings->setValue("ScreenshotDisableFilters", 2);
			break;
	}

	Settings->setValue("IPHWDec", ui->IPHWDec->currentText());
	Settings->setValue("IPDetectBlack", ui->IPDetectBlack->isChecked());
	Settings->setValue("IPBlackAmount", QString::number(ui->IPBlackAmount->value()));
	Settings->setValue("IPBlackThreshold", QString::number(ui->IPBlackThreshold->value()));

	Settings->setValue("IPVideoFormat", ui->IPVideoFormat->currentText());
	Settings->setValue("IPPreset", ui->IPPreset->currentText());
	Settings->setValue("IPCRF", QString::number(ui->IPCRF->value()));
	Settings->setValue("IPAudioFormat", ui->IPAudioFormat->currentText());
	Settings->setValue("IPAudioBitrate", QString::number(ui->IPAudioBitrate->value()));
	Settings->setValue("IPW", QString::number(ui->IPW->value()));
	Settings->setValue("IPH", QString::number(ui->IPH->value()));
	Settings->setValue("IPHWScale", ui->IPHWScale->isChecked());

	if (ui->SaveWindowSettings->isChecked()) {
		Settings->beginGroup("MainWindow");
		Settings->setValue("Maximized", isMaximized());
		Settings->setValue("fullscreen", isFullScreen());
		if (!isMaximized() && !isFullScreen()) {
			Settings->setValue("size", size());
			Settings->setValue("pos", pos());
		}
		Settings->setValue("LeftTabs", ui->LeftTabs->currentIndex());
		Settings->setValue("RightTabs", ui->RightTabs->currentIndex());
		Settings->setValue("TimeRowSize", ui->Table->columnWidth(TIME_COLUMN));
		Settings->setValue("TransitionRowSize", ui->Table->columnWidth(TRANSITION_COLUMN));
		Settings->setValue("PitchRowSize", ui->Table->columnWidth(PITCH_COLUMN));
		Settings->setValue("YawRowSize", ui->Table->columnWidth(YAW_COLUMN));
		Settings->setValue("RollRowSize", ui->Table->columnWidth(ROLL_COLUMN));
		Settings->setValue("FovRowSize", ui->Table->columnWidth(FOV_COLUMN));
		Settings->setValue("EyeRowSize", ui->Table->columnWidth(EYE_COLUMN));
		Settings->setValue("VOffsetSize", ui->Table->columnWidth(V_OFFSET_COLUMN));
		Settings->setValue("HOffsetSize", ui->Table->columnWidth(H_OFFSET_COLUMN));
		Settings->setValue("EyeRowSize", ui->Table->columnWidth(EYE_COLUMN));
		Settings->setValue("TextRowSize", ui->Table->columnWidth(TEXT_COLUMN));
		Settings->setValue("PreviewSpliter", ui->PreviewSpliter->saveState());
		Settings->setValue("TabsSpliter", ui->TabsSpliter->saveState());
		Settings->setValue("FiltersSplitter", ui->FiltersSplitter->saveState());
		Settings->setValue("FiltersTimeRowSize", ui->FiltersList->columnWidth(FILTERS_TIME_COLUMN));
		Settings->setValue("FiltersRowSize", ui->FiltersList->columnWidth(FILTERS_FILTER_COLUMN));
		Settings->setValue("SplitStateSize", ui->SplitTable->columnWidth(SPLIT_STATE_COLUMN));
		Settings->setValue("SplitTimeSize", ui->SplitTable->columnWidth(SPLIT_TIME_COLUMN));
		Settings->setValue("SplitBeforeSize", ui->SplitTable->columnWidth(SPLIT_BEFORE_COLUMN));
		Settings->setValue("SplitAfterSize", ui->SplitTable->columnWidth(SPLIT_AFTER_COLUMN));
		Settings->setValue("SplitAdjustedBeforeSize", ui->SplitTable->columnWidth(SPLIT_ADJUST_BEFORE_COLUMN));
		Settings->setValue("SplitAdjustedAfterSize", ui->SplitTable->columnWidth(SPLIT_ADJUST_AFTER_COLUMN));
		Settings->setValue("SplitFileSize", ui->SplitTable->columnWidth(SPLIT_FILE_COLUMN));
		Settings->endGroup();
	}

	SaveShortcuts();
}

void MainWindow::ReloadRawImage(const QTime &time) {
	if (BlockRefresh) return;

	if (!FileOk) {
		if (!CheckInputFiles()) return;
	}

	if (FfmpegRaw.state() != QProcess::NotRunning) {
		FfmpegRawCanceled = true;
		FfmpegRaw.close();
	}

	if (FfmpegFilter.state() != QProcess::NotRunning) {
		FfmpegFilterCanceled = true;
		FfmpegFilter.close();
	}

	RawData.clear();

	CurrentRawTime = time;

	RAMCacheRead = false;
	DiskCacheRead = false;

	if (Debug) {
		RawStartTime = 0;
		RawReadTime = 0;
		RawPrevirewTime = 0;
		RawTotalTime = 0;
		RawElapsedTimer.start();
		ui->DebugLog->appendPlainText("Moving to time " + time.toString(TIME_FORMAT));
	}

	if (RAMCache) {
		if (RawCache.contains(time)) {
			RawData = *RawCache[time];
			RAMCacheRead = true;
			GenerateRawImageTerminate(0, QProcess::NormalExit);
			return;
		}
	}

	if (DiskCache) {
		if (OndemandCache) {
			GenerateDiskCache(1,
							  ui->RawTime->time().addSecs(1),
							  ui->RawTime->time().addSecs(ui->PreloadCacheSeoconds->value()),
							  true);
			OndemandCacheTimer.setSingleShot(true);
			OndemandCacheTimer.start(500);
		}
		QString filename = ComposeCacheFilename(SmallInputFilenameHash, time.msecsSinceStartOfDay());
		if (QFile::exists(filename)) {
			QFile file(filename);
			if (file.open(QFile::ReadOnly)) {
				RawData = file.readAll();
				DiskCacheRead = true;
				GenerateRawImageTerminate(0, QProcess::NormalExit);
			}
			return;
		}
	}

	QStringList params = RawFfmpegParams(SmallInputFile, "", time.msecsSinceStartOfDay());

	FfmpegRawCanceled = false;
	FfmpegRaw.start(ui->FfmpegBin->text(), params);
	FfmpegRaw.waitForStarted(-1);
	if (Debug) {
		ui->DebugLog->appendPlainText(params.join(" "));
		RawStartTime = RawElapsedTimer.restart();
	}
}

void MainWindow::ReloadFilterImageForced() {
	if (RAMCache) CameraCache.clear();
	RAMCamereTimeMovement = false;
	ReloadFilterImage();
}

void MainWindow::ReloadFilterImage() {
	if (BlockRefresh) return;

	if (FfmpegRaw.state() != QProcess::NotRunning) return;

	if (FfmpegFilter.state() != QProcess::NotRunning) {
		NeedFilterUpdate = true;
		return;
	}

	if (RawData.isEmpty()) return;

	FilterResultData.clear();

	RAMCameraCacheRead = false;

	if (RAMCache) {
		if (CameraCache.contains(CurrentRawTime)) {
			if (Debug) FilterElapsedTimer.start();
			FilterResultData = *CameraCache[CurrentRawTime];
			RAMCameraCacheRead = true;
			GenerateFilterImageTerminate(0, QProcess::NormalExit);
			return;
		}
	}

	QString InputFilters = v360Filters(
									   ui->formatComboBox->currentText(),
									   ui->CustomV360->text(),
									   ui->modeComboBox->currentText(),
									   ui->OutputMode->currentText(),
									   ui->VFovSpinBox->value(),
									   ui->HFovSpinBox->value(),
									   ui->Fov->value(),
									   ui->Pitch->value(),
									   ui->Yaw->value(),
									   ui->Roll->value(),
									   ui->VOffset->value(),
									   ui->HOffset->value(),
									   ui->RightEye->isChecked(),
									   ui->GUIPreviewW->value(),
									   ui->GUIPreviewH->value(),
									   ui->GUIPreviewInterpolation->currentText(),
									   false,
									   ui->InputPadW->value(),
									   ui->InputPadH->value(),
									   ui->InputPadX->value(),
									   ui->InputPadY->value()
						   );

	if (ui->CustomInputFilters->text() != "")
		InputFilters = ui->CustomInputFilters->text() + "," + InputFilters;

	QString OutputFilters = "";

	if (ui->FilterExposure->isChecked() || ui->FilterColorTemperature->isChecked() || ui->FilterEQ->isChecked() ||
		ui->FilterColorBalance->isChecked()) {
		OutputFilters += GenerateFiltersString();
	}

	if (ui->CustomFilters->text() != "") {
		if (OutputFilters != "") OutputFilters += ",";
		OutputFilters += ui->CustomFilters->text();
	}

	QString FilterType = "-vf";
	QString Filters = "";

	if (ui->FilterCompare->isChecked() && OutputFilters != "") {
		Filters = "[0] " + InputFilters + " [v360]; "
				  "[v360] split [ori][filter]; "
				  "[filter] crop=w=iw/2:h=ih:x=0," + OutputFilters + " [filter_out]; "
				  "[ori][filter_out] overlay=format=auto";
		FilterType = "-filter_complex";
	} else {
		Filters = InputFilters;
		if (OutputFilters != "") Filters += "," + OutputFilters;
	}

	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y" <<
		   "-i" << "-" << FilterType << Filters <<
		   "-vframes" << "1" << "-c:v" << "png" << "-f" << "image2pipe" << "-";
	FfmpegFilterCanceled = false;
	if (Debug) FilterElapsedTimer.start();
	if (Debug) ui->DebugLog->appendPlainText(ui->FfmpegBin->text() + " " + params.join(" "));
	FfmpegFilter.start(ui->FfmpegBin->text(), params);
}

void MainWindow::GenerateFilterImageTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (FfmpegFilterCanceled) {
		FfmpegFilterCanceled = false;
		return;
	}
	if (exitCode != 0) {
		ShowProcessError(&FfmpegFilter);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;


	if (RAMCache && RAMCamereTimeMovement && !RAMCameraCacheRead && !CameraCache.contains(CurrentRawTime))
		CameraCache.insert(CurrentRawTime, new QByteArray(FilterResultData), FilterResultData.size());

	if (Debug && !RAMCameraCacheRead) FilterReadTime = FilterElapsedTimer.restart();

	FilterPixmap.loadFromData(FilterResultData);
	RefreshCameraImage();

	if (Debug) {
		FilterPrevirewTime = FilterElapsedTimer.elapsed();
		if (RAMCameraCacheRead) {
			ui->DebugLog->appendPlainText("Filter - Ram Cache read - Total:\t" + QString::number(FilterPrevirewTime));
		} else {
			FilterTotalTime = FilterStartTime + FilterSendTime + FilterReadTime + FilterPrevirewTime;
			ui->DebugLog->appendPlainText("Filter - Start:\t" + QString::number(FilterStartTime) +
										  "\tSend:\t" + QString::number(FilterSendTime) +
										  "\tRead:\t" + QString::number(FilterReadTime) +
										  "\tPreview:\t" + QString::number(FilterPrevirewTime) +
										  "\tTotal:\t" + QString::number(FilterTotalTime));
		}
	}

	if (NeedFilterUpdate) {
		NeedFilterUpdate = false;
		ReloadFilterImage();
	}
}

void MainWindow::ReadRawData() {
	RawData.append(FfmpegRaw.readAllStandardOutput());
}

void MainWindow::ReadFilterData() {
	FilterResultData.append(FfmpegFilter.readAllStandardOutput());
}

void MainWindow::StartFilterProcess() {
	if (Debug) FilterStartTime = FilterElapsedTimer.restart();
	FfmpegFilter.write(RawData);
	FfmpegFilter.closeWriteChannel();
	if (Debug) FilterSendTime = FilterElapsedTimer.restart();
}

void MainWindow::resizeEvent(QResizeEvent */*event*/) {
	RefreshPreviews();
}

void MainWindow::RefreshRawImage() {
	if (RawPixmap.isNull()) return;
	int height = ui->RawFrame->height();
	int width =  ui->RawFrame->width();
	if (!ui->RawFrame->isVisible()) {
		height = 400;
		width = 800;
	}
	double car = (double)width / (double)height;
	double ar = (double)RawPixmap.width() / (double)RawPixmap.height();
	if (car < ar) {
		ScaledFilterPixmap = FilterPixmap.scaledToWidth(width);
		ScaledRawPixmap = RawPixmap.scaledToWidth(width);
	} else {
		ScaledRawPixmap = RawPixmap.scaledToHeight(height);
	}
	ui->RawFrame->setPixmap(ScaledRawPixmap);
}

void MainWindow::RefreshCameraImage() {
	if (FilterPixmap.isNull()) return;
	int height = ui->CameraFrame->height();
	int width =  ui->CameraFrame->width();
	if (!ui->CameraFrame->isVisible()) {
		height = ui->GUIPreviewH->value();
		width =  ui->GUIPreviewW->value();
	}
	double car = (double)width / (double)height;
	double ar = (double)FilterPixmap.width() / (double)FilterPixmap.height();
	if (car < ar) {
		ScaledFilterPixmap = FilterPixmap.scaledToWidth(width);
	} else {
		ScaledFilterPixmap = FilterPixmap.scaledToHeight(height);
	}
	if (ui->ShowFramingLines->isChecked()) PrintFramingLines();
	ui->CameraFrame->setPixmap(ScaledFilterPixmap);
}

void MainWindow::UpdateTableTime() {
	UpdateItem(TIME_COLUMN, ui->RawTime->text());
}

void MainWindow::UpdateItem(int column, QString value, QString color) {
	if (!ui->Table->isVisible()) { return; }
	int selectedrow = GetSelectedRow();
	if (selectedrow >= 0) {
		QTableWidgetItem *CurrentItem = ui->Table->item(selectedrow, column);
		if (CurrentItem != nullptr) {
			if (value == CurrentItem->text()) return;
		}
		QTableWidgetItem *content = new QTableWidgetItem(value);
		if (color != "") content->setForeground(QBrush(QColor(color)));
		QUndoCommand *ChangeItem = new QTableWidgetChange(ui->Table, content, CurrentItem);
		TableUndo->push(ChangeItem);
		Changes();
	}
}

int MainWindow::GetSelectedRow(QTableWidget *Table) {
	QItemSelectionModel *model = Table->selectionModel();
	if (model->hasSelection()) {
		QModelIndexList list = model->selectedRows(0);
		return list.at(0).row();
	}
	return -1;
}

int MainWindow::GetSelectedRow() {
	return GetSelectedRow(ui->Table);
}

QString MainWindow::Row2TSV(QTableWidget *Table, int row, int exclude) {
	QStringList RowText;
	for (int i = 0; i < Table->columnCount(); ++i) {
		if (exclude >= 0 && i == exclude) continue;
		QTableWidgetItem *item = Table->item(row, i);
		if (item == nullptr) {
			RowText.append("");
		} else {
			RowText.append(item->text());
		}
	}
	return RowText.join("\t");
}

QStringList MainWindow::Table2TSV(QTableWidget *Table, QString Prefix, int exclude) {
	QStringList Output;
	for (int i = 0; i < Table->rowCount(); ++i) {
		Output.append(Prefix + Row2TSV(Table, i, exclude));
	}
	return Output;
}

void MainWindow::GenerateRawImageTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (FfmpegRawCanceled) {
		FfmpegRawCanceled = false;
		return;
	}
	if (exitCode != 0) {
		ShowProcessError(&FfmpegRaw);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;

	if (Debug && (!RAMCacheRead || !DiskCacheRead)) RawReadTime = RawElapsedTimer.restart();

	if (RAMCache && !RAMCacheRead && !RawCache.contains(CurrentRawTime)) {
		RawCache.insert(CurrentRawTime, new QByteArray(RawData), RawData.size());
		ui->CurrentRAMUsage->setText(QString::number(RawCache.totalCost() / 1024 / 1024, 'f', 2));
	}

	if (DiskCache && !DiskCacheRead) {
		QString filename = ComposeCacheFilename(SmallInputFilenameHash, ui->RawTime->time().msecsSinceStartOfDay());
		if (!QFile::exists(filename)) {
			QFile file(filename);
			if (file.open(QFile::WriteOnly)) {
				file.write(RawData);
				file.close();
			}
			emit UpdateDiskCacheAndRemove();
		}
	}

	RawPixmap.loadFromData(RawData);
	RefreshRawImage();

	if (Debug) {
		RawPrevirewTime = RawElapsedTimer.restart();
		if (RAMCacheRead) {
			ui->DebugLog->appendPlainText("Raw - Ram Cache read - Total:\t" + QString::number(RawPrevirewTime));
		} else if (DiskCacheRead) {
			ui->DebugLog->appendPlainText("Raw - Disk Cache read - Total:\t" + QString::number(RawPrevirewTime));
		} else {
			RawTotalTime = RawStartTime + RawReadTime + RawPrevirewTime;
			ui->DebugLog->appendPlainText("Raw - Start:\t" + QString::number(RawStartTime) +
										  "\tRead:\t" + QString::number(RawReadTime) +
										  "\tPreview:\t" + QString::number(RawPrevirewTime) +
										  "\tTotal:\t" + QString::number(RawTotalTime));
		}
	}

	RAMCamereTimeMovement = true;
	ReloadFilterImage();
}

void MainWindow::FfprobeTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (exitCode != 0) {
		ShowProcessError(&Ffprobe);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;

	QJsonParseError error;
	VideoInfo = QJsonDocument::fromJson(Ffprobe.readAll(), &error);
	QJsonObject JsonRoot = VideoInfo.object();
	QJsonObject VideoFormat = JsonRoot.value("format").toObject();

	QJsonArray VideoStreams = JsonRoot.value("streams").toArray();
	for (int i = 0; i < VideoStreams.count(); i++) {
		QJsonObject CurrentStream = VideoStreams.at(i).toObject();
		if (CurrentStream.value("codec_type").toString() == "video") {
			QString FrameRate = CurrentStream.value("r_frame_rate").toString();
			QStringList SplitFrameRate = FrameRate.split('/');
			double div1 = SplitFrameRate.first().toInt();
			double div2 = SplitFrameRate.last().toInt();
			ui->FPSSpinBox->setValue(div1 / div2);
			ui->InputResolutionW->setValue(CurrentStream.value("width").toInt());
			ui->InputResolutionH->setValue(CurrentStream.value("height").toInt());
			ui->InputPixelFormat->setText(CurrentStream.value("pix_fmt").toString());
			break;
		}
	}

	ui->VideoLength->setTime(QTime::fromMSecsSinceStartOfDay(VideoFormat.value("duration").toString().toDouble() * 1000));
}

void MainWindow::ReadProgress() {
	UpdateProgress(&Execution, &MainProgress, &ExecutionBuffer, &ProcessErrorString, FfmpegEnableBlackFrames);

	ui->Progress->setValue(MainProgress.CurrentTime.msecsSinceStartOfDay() / 1000);
	if (ui->TaskbarProgress->isChecked()) Task->Value(MainProgress.CurrentTime.msecsSinceStartOfDay() / 1000);

	qint64 RealTimeSeconds = QDateTime::currentSecsSinceEpoch() - MainProgress.ExecutionStartTime.toSecsSinceEpoch();

	#if QT_VERSION >= QT_VERSION_CHECK(6, 5, 0)
	QDateTime RT = QDateTime::fromSecsSinceEpoch(RealTimeSeconds,  QTimeZone(QTimeZone::UTC));
	#else
	QDateTime RT = QDateTime::fromSecsSinceEpoch(RealTimeSeconds,  Qt::UTC);
	#endif

	QString Text = "Real Time: " + RT.toString("H:mm:ss");
	Text += ", Video Time: " + MainProgress.CurrentTime.toString("H:mm:ss.zzz");
	Text += ", Speed: " + QString::number(MainProgress.Speed, 'f', 3) + "x";
	if (MainProgress.FPS > 0)
		Text += ", Fps: " + QString::number(MainProgress.FPS, 'f', 2);
	if (MainProgress.Size > 0) Text += ", Size: " +
										   QLocale().formattedDataSize(MainProgress.Size, 2, QLocale::DataSizeIecFormat);
	if (MainProgress.Bitrate > 1) Text += ", Bitrate: " + QString::number(MainProgress.Bitrate, 'f', 1) + "kbits/s";
	Text += ", ETR: " + MainProgress.ETR;
	if (MainProgress.Size > 0) Text += ", Estimated Size: " + QLocale().formattedDataSize(MainProgress.EstimatedSize, 2,
										   QLocale::DataSizeIecFormat);
	ui->RunState->setText(Text);

	if (MainProgress.BlackFrames != "") {
		ui->IPText->appendPlainText(MainProgress.BlackFrames);
		MainProgress.BlackFrames.clear();
	}
}

void MainWindow::ProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (exitStatus == QProcess::CrashExit || exitCode != 0) {
		if (ExecutionCanceled) {
			ExecutionCanceled = false;
			if (ProcessIsPreview)
				ui->RunState->setText("Preview process canceled");
			else
				ui->RunState->setText("Process canceled");
		} else {
			ShowProcessError(&Execution, ProcessErrorString);
			if (ProcessIsPreview)
				ui->RunState->setText("Preview process failed");
			else
				ui->RunState->setText("Process failed");
		}
	} else {
		qint64 RealTimeSeconds = QDateTime::currentSecsSinceEpoch() - MainProgress.ExecutionStartTime.toSecsSinceEpoch();
		#if QT_VERSION >= QT_VERSION_CHECK(6, 5, 0)
		QDateTime RT = QDateTime::fromSecsSinceEpoch(RealTimeSeconds,  QTimeZone(QTimeZone::UTC));
		#else
		QDateTime RT = QDateTime::fromSecsSinceEpoch(RealTimeSeconds,  Qt::UTC);
		#endif
		if (ProcessIsPreview) {
			ui->RunState->setText("Preview process terminated, total time: " + RT.toString("H:mm:ss"));
			if (ui->AutoPlay->isChecked() && ui->VideoPlayer->text() != "" && PreviewFile != "")
				QProcess::startDetached(ui->VideoPlayer->text(), QStringList(PreviewFile));
			PreviewFile = "";
			ProcessIsPreview = false;
		} else {
			ui->RunState->setText("Process terminated, total time: " + RT.toString("H:mm:ss"));
			if (ui->ExecuteOnFinish->text() != "") {
				QString command = ui->ExecuteOnFinish->text();
				if (command.contains("%OUTPUT%")) command.replace("%OUTPUT%", ui->OutFile->text());
				QStringList Params = QProcess::splitCommand(command);
				command = Params.takeFirst();
				QProcess::startDetached(command, Params);
			}
		}
		if (GenerateSmallFile && ui->IPOutputFile->text() != "") {
			ui->RightTabs->setCurrentIndex(RIGHT_TAB_INPUT);
			ui->SmallInputFile->setText(ui->IPOutputFile->text());
		}
	}
	ui->ProgressArea->setVisible(false);
	if (ui->TaskbarProgress->isChecked()) Task->EndProgress();
	FfmpegEnableBlackFrames = false;
	GenerateSmallFile = false;
}

void MainWindow::UndoSlot() {
	if (!ui->Table->isVisible()) return;
	TableUndo->undo();
	on_Table_itemSelectionChanged();
	Changes();
}

void MainWindow::RedoSlot() {
	if (!ui->Table->isVisible()) return;
	TableUndo->redo();
	on_Table_itemSelectionChanged();
	Changes();
}

void MainWindow::UnselectSlot() {
	QWidget *Focused = QApplication::focusWidget();
	if (Focused != nullptr) Focused->clearFocus();
}

void MainWindow::EditTextFocus() {
	ui->Text->setFocus();
}

void MainWindow::ExecuteProcess() {
	if (Execution.state() != QProcess::NotRunning) return;
	ProcessErrorString = "";
	ui->RunState->setText("");
	ui->RunState->setVisible(true);
	ui->Progress->setMaximum(MainProgress.TotalTime.msecsSinceStartOfDay() / 1000);
	ui->Progress->setValue(0);
	ui->ProgressArea->setVisible(true);
	RefreshPreviews();
	if (ui->TaskbarProgress->isChecked()) Task->BeginProgress(MainProgress.TotalTime.msecsSinceStartOfDay() / 1000);
	ExecutionCanceled = false;
	MainProgress.ExecutionStartTime = QDateTime::currentDateTime();
	Execution.start(ProcessProgram, ProcessParams);
	if (!Execution.waitForStarted(-1)) {
		QProcess::ProcessError Error = Execution.error();
		switch (Error) {
			case QProcess::FailedToStart:
				ui->RunState->setText("Process failed to start");
				break;
			case QProcess::Crashed:
				ui->RunState->setText("Process failed, crashed");
				break;
			case QProcess::Timedout:
				ui->RunState->setText("Process failed, timed out");
				break;
			case QProcess::WriteError:
				ui->RunState->setText("Process failed, write error");
				break;
			case QProcess::ReadError:
				ui->RunState->setText("Process failed, read error");
				break;
			case QProcess::UnknownError:
				ui->RunState->setText("Process failed, unknown error");
				break;
		}
		ui->ProgressArea->setVisible(false);
		if (ui->TaskbarProgress->isChecked()) Task->EndProgress();
		FfmpegEnableBlackFrames = false;
		GenerateSmallFile = false;
	}

	if (ui->Priority->currentText() == "Low")
		ProcessLowPriority(Execution.processId());
	else if (ui->Priority->currentText() == "Idle")
		ProcessIdlePriority(Execution.processId());
}

void MainWindow::on_NextFrame_clicked() {
	QTime newtime = ui->RawTime->time().addMSecs((1000 / ui->FPSSpinBox->value()) + 1);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_PrevFrame_clicked() {
	QTime newtime = ui->RawTime->time().addMSecs((-1000 / ui->FPSSpinBox->value()) - 1);
	if (newtime.msecsSinceStartOfDay() > ui->RawTime->time().msecsSinceStartOfDay()) return;
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_NextSecond_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(1);
	newtime.setHMS(newtime.hour(), newtime.minute(), newtime.second());
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_PrevSec_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(-1);
	if (newtime.msecsSinceStartOfDay() > ui->RawTime->time().msecsSinceStartOfDay()) return;
	newtime.setHMS(newtime.hour(), newtime.minute(), newtime.second());
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_Next10Sec_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(10);
	int seconds = (newtime.second() / 10) * 10;
	newtime.setHMS(newtime.hour(), newtime.minute(), seconds);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_Prev10Sec_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(-10);
	if (newtime.msecsSinceStartOfDay() > ui->RawTime->time().msecsSinceStartOfDay()) return;
	int seconds = (newtime.second() / 10) * 10;
	newtime.setHMS(newtime.hour(), newtime.minute(), seconds);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_NextMinute_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(60);
	newtime.setHMS(newtime.hour(), newtime.minute(), 0);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_PrevMinute_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(-60);
	if (newtime.msecsSinceStartOfDay() > ui->RawTime->time().msecsSinceStartOfDay()) return;
	newtime.setHMS(newtime.hour(), newtime.minute(), 0);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_SelectFile_clicked() {
	ForceDetectVideo = true;
	QString File = QFileDialog::getOpenFileName(this, "Open Video", "",
				   "Video Files (*.mkv *.mp4 *.avi *.mov *.wmv) ;; All Files (*)");
	if (File != "") ui->InputFile->setText(QDir::toNativeSeparators(File));
}

void MainWindow::on_SelectSmallFile_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Open Small Video", "",
				   "Video Files (*.mkv *.mp4 *.avi *.mov *.wmv) ;; All Files (*)");
	if (File != "") ui->SmallInputFile->setText(QDir::toNativeSeparators(File));
}

bool MainWindow::CheckInputFiles() {
	QFileInfo InputInfo(ui->InputFile->text());
	bool InputExist = InputInfo.exists();

	QString OldSmallInputFilenameHash = SmallInputFilenameHash;

	InputFile = "";
	InputFilename = "";
	InputFileDir = "";
	InputFilenameHash = "";
	InputFileExt = "";
	SmallInputFile = "";
	SmallInputFilename = "";
	SmallInputFileDir = "";
	SmallInputFilenameHash = "";

	FileOk = InputExist;

	if (!FileOk) {
		CheckCache();
		return false;
	}

	if (InputExist) {
		InputFile = ui->InputFile->text();
		InputFilename = InputInfo.completeBaseName();
		InputFileDir = InputInfo.path();
		InputFileExt = InputInfo.suffix();
	}

	QFileInfo SmallInfo(ui->SmallInputFile->text());
	bool SmallInputExist = SmallInfo.exists();

	if (SmallInputExist) {
		SmallInputFile = ui->SmallInputFile->text();
		SmallInputFilename = SmallInfo.completeBaseName();
		SmallInputFileDir = SmallInfo.path();
	} else {
		SmallInputFile = InputFile;
		SmallInputFilename = InputFilename;
		SmallInputFileDir = InputFileDir;
	}

	QByteArray file = SmallInputFile.toUtf8();
	QByteArray hash = QCryptographicHash::hash(file, QCryptographicHash::Md5)
					  .toBase64(QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals);
	SmallInputFilenameHash = SmallInputFilename + QString::fromUtf8(hash);

	file = InputFilename.toUtf8();
	hash = QCryptographicHash::hash(file, QCryptographicHash::Md5)
		   .toBase64(QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals);
	InputFilenameHash = InputFilename + QString::fromUtf8(hash);

	if (OldSmallInputFilenameHash != SmallInputFilenameHash) {
		DiskCacheQueue.clear();
		OndemandDiskCacheQueue.clear();
		RawCache.clear();
	}

	if (ui->VideoLength->time().msecsSinceStartOfDay() == 0 || ForceDetectVideo) {
		QString FilenameUpper = InputFilename.toUpper();
		if (FilenameUpper.contains("MKX200")) {
			ui->formatComboBox->setCurrentText("fisheye");
			ui->modeComboBox->setCurrentText("sbs");
			ui->HFovSpinBox->setValue(200);
			ui->VFovSpinBox->setValue(200);
		} else if (FilenameUpper.contains("MKX22") || FilenameUpper.contains("VRCA220")) {
			ui->formatComboBox->setCurrentText("fisheye");
			ui->modeComboBox->setCurrentText("sbs");
			ui->HFovSpinBox->setValue(220);
			ui->VFovSpinBox->setValue(220);
		} else if (FilenameUpper.contains("FISHEYE190") || FilenameUpper.contains("RF52")) {
			ui->formatComboBox->setCurrentText("fisheye");
			ui->modeComboBox->setCurrentText("sbs");
			ui->HFovSpinBox->setValue(190);
			ui->VFovSpinBox->setValue(190);
		} else if (FilenameUpper.contains("F180") || FilenameUpper.contains("180F")) {
			ui->formatComboBox->setCurrentText("fisheye");
			ui->modeComboBox->setCurrentText("sbs");
			ui->HFovSpinBox->setValue(180);
			ui->VFovSpinBox->setValue(180);
		} else if (FilenameUpper.contains("360_SBS") || FilenameUpper.contains("SBS_360")) {
			ui->formatComboBox->setCurrentText("equirect");
			ui->modeComboBox->setCurrentText("sbs");
			ui->HFovSpinBox->setValue(360);
			ui->VFovSpinBox->setValue(180);
		} else if (FilenameUpper.contains("TB_360") || FilenameUpper.contains("360_TB") || FilenameUpper.contains("360_OI") ||
				   FilenameUpper.contains("OI_360")) {
			ui->formatComboBox->setCurrentText("equirect");
			ui->modeComboBox->setCurrentText("tb");
			ui->HFovSpinBox->setValue(360);
			ui->VFovSpinBox->setValue(180);
		} else if (FilenameUpper.contains("MONO_360") || FilenameUpper.contains("360_MONO") ||
				   FilenameUpper.contains("360MONO") || FilenameUpper.contains("MONO360")) {
			ui->formatComboBox->setCurrentText("equirect");
			ui->modeComboBox->setCurrentText("2d");
			ui->HFovSpinBox->setValue(360);
			ui->VFovSpinBox->setValue(180);
		}
		/*
		There has been some requests for the rules of filenames to set VR formats.
		That is, to set video VR format by changing filename.

		The following rules only apply to Oculus Go / Gear VR / Daydream / MiVR after SKYBOX V0.2.2.

		So here they are:

		3D format
		3dv, TB, Top + Bottom, OU, Over + Under, HOU, Half + OU, Half + Over + Under
		3dh, LR, Left + Right, SBS, Side + By + Side, HSBS, Half + SBS, Half + Side + By + Side

		Angles
		360, 360°, 180x180, 180°

		Fisheye aka Youtube 180
		F180, VR180

		EAC aka Youtube 360
		EAC360

		The '+' sign could be replaced by one space or any sign or nothing.
		You do not have to worry about the order of the two information or capitalization.
		The default setting is 2D normal movie (2d screen).

		Some examples:
		moviename.mp4 (2d screen)
		moviename_360.mp4 (2D 360)
		moviename_360_TB.mp4 (3D-topbottom 360)
		moviename_180x180_Side-By-Side.mp4 (3D-sidebyside 360)
		moviename_EAC360.mp4 (2d EAC)
		moviename_F180_OU.mp4 (3D-topbottom Fisheye)

		The following rules only apply to Oculus Rift / Steam VR.

		Here they are:

		3D format
		3dv, TB, Top + Bottom, OU, Over + Under, HOU, Half + Over + Under
		3dh, LR, Left + Right, SBS, Side + By + Side, HSBS, Half + Side + By + Side

		Angles
		360°, 180x180, 180°

		The '+' sign could be replaced by one space or any sign or nothing.
		You do not have to worry about the order of the two information or capitalization.
		The default setting is 2D normal movie (2d screen).

		P.S. Just know you don't really have to do this for your files in order for them to get recognized correctly. In versions after 0.1.8, we upgraded our recognition engine, which should have an overall accuracy of over 95%.
		*/


	}

	if (ui->FfprobeBin->text() != "" && !BlockRefresh) {
		if (ui->VideoLength->time().msecsSinceStartOfDay() == 0 || ForceDetectVideo) {
			ForceDetectVideo = false;
			QStringList params = {"-i", InputFile, "-print_format", "json", "-show_format", "-show_streams"};
			Ffprobe.start(ui->FfprobeBin->text(), params);
			Ffprobe.waitForStarted(-1);
		}
	}
	CheckCache();
	ui->RightTabs->setTabVisible(RIGHT_TAB_COMPARESMALL, (SmallInputExist && FileOk));
	if (SmallInputExist) {
		ui->IPSmallFile->setChecked(false);
		ui->IPSmallFile->setEnabled(false);
		ui->InputPadH->setEnabled(false);
		ui->InputPadW->setEnabled(false);
		ui->InputPadX->setEnabled(false);
		ui->InputPadY->setEnabled(false);
	} else {
		ui->IPSmallFile->setEnabled(true);
		ui->InputPadH->setEnabled(true);
		ui->InputPadW->setEnabled(true);
		ui->InputPadX->setEnabled(true);
		ui->InputPadY->setEnabled(true);
	}
	return true;
}

void MainWindow::CheckCache() {
	RAMCache = true;
	DiskCache = true;
	OndemandCache = true;

	if (!FileOk) {
		RAMCache = false;
		DiskCache = false;
		OndemandCache = false;
		return;
	}

	if (ui->PauseOndemand->isChecked()) {
		OndemandCache = false;
	}

	if (ui->PreloadCacheSeoconds->value() == 0) {
		OndemandCache = false;
	}

	if (ui->DiskCache->value() == 0) {
		DiskCache = false;
		OndemandCache = false;
	}

	if (ui->DiskCacheDir->text() != "") {
		QDir Dir;
		if (!Dir.exists(ui->DiskCacheDir->text())) {
			if (!Dir.mkpath(ui->DiskCacheDir->text())) {
				DiskCache = false;
				OndemandCache = false;
			}
		}
	} else {
		DiskCache = false;
		OndemandCache = false;
	}

	if (ui->RAMCache->value() == 0) {
		RAMCache = false;
	}
}

void MainWindow::on_InputFile_textChanged(const QString &/*arg1*/) {
	CheckInputFiles();
}

void MainWindow::on_SmallInputFile_textChanged(const QString &/*arg1*/) {
	CheckInputFiles();
}

void MainWindow::on_Trans_valueChanged(double arg1) {
	if (RAMCache) CameraCache.clear();
	UpdateItem(TRANSITION_COLUMN, QString::number(arg1));
}

void MainWindow::on_Pitch_valueChanged(double arg1) {
	if (RAMCache) CameraCache.clear();
	UpdateItem(PITCH_COLUMN, QString::number(arg1));
}

void MainWindow::on_Yaw_valueChanged(double arg1) {
	if (RAMCache) CameraCache.clear();
	UpdateItem(YAW_COLUMN, QString::number(arg1));
}

void MainWindow::on_Roll_valueChanged(double arg1) {
	if (RAMCache) CameraCache.clear();
	UpdateItem(ROLL_COLUMN, QString::number(arg1));
}

void MainWindow::on_Fov_valueChanged(double arg1) {
	double hfov = 0;
	double vfov = 0;
	QString Color = "";

	if (RAMCache) CameraCache.clear();

	if (ui->InputResolutionW->value() != 0 && ui->InputResolutionH->value() != 0) {
		hfov = CalculateEstimatedHFOV();
		vfov = CalculateEstimatedVFOV();
		if (hfov > 100 || vfov > 100) {
			Color = ui->HighWarningColor->text();
			ui->Fov->setStyleSheet("QDoubleSpinBox { color: " + Color + "; selection-color: " + Color + "; }");
		} else if (hfov > 92 || vfov > 92) {
			Color = ui->LowWarningColor->text();
			ui->Fov->setStyleSheet("QDoubleSpinBox { color: " + Color + "; selection-color: " + Color + "; }");
		} else {
			ui->Fov->setStyleSheet("");
		}
	}

	UpdateItem(FOV_COLUMN, QString::number(arg1), Color);

	if (!ui->ShowEstimatedResolution->isChecked()) return;

	if (ui->InputResolutionW->value() == 0 || ui->InputResolutionH->value() == 0) return;

	int hpixels = CalculateEstimatedWidth(hfov);
	int vpixels = CalculateEstimatedHeight(vfov);

	ui->EstHFOV->setText("HFOV: " + QString::number(hfov, 'f', 1));
	if (hfov > 100) ui->EstHFOV->setStyleSheet("QLabel { color: " + ui->HighWarningColor->text() + "; }");
	else if (hfov > 92) ui->EstHFOV->setStyleSheet("QLabel { color: " + ui->LowWarningColor->text() + "; }");
	else ui->EstHFOV->setStyleSheet("");
	ui->EstVFOV->setText("VFOV: " + QString::number(vfov, 'f', 1));
	if (vfov > 100) ui->EstVFOV->setStyleSheet("QLabel { color: " + ui->HighWarningColor->text() + "; }");
	else if (vfov > 92) ui->EstVFOV->setStyleSheet("QLabel { color: " + ui->LowWarningColor->text() + "; }");
	else ui->EstVFOV->setStyleSheet("");

	ui->EstimatedW->setText("Width: " + QString::number(hpixels));
	ui->EstimatedH->setText("Height: " + QString::number(vpixels));
}

double MainWindow::CalculateEstimatedHFOV() {
	return CalcHFOV(ui->GUIPreviewW->value(), ui->GUIPreviewH->value(), ui->Fov->value());
}

double MainWindow::CalculateEstimatedVFOV() {
	return CalcVFOV(ui->GUIPreviewW->value(), ui->GUIPreviewH->value(), ui->Fov->value());
}

double MainWindow::CalcHFOV(int w, int h, double fov) {
	return sqrt(pow(fov, 2.0) / (pow((double)h / (double)w, 2.0) + 1.0));
}

double MainWindow::CalcVFOV(int w, int h, double fov) {
	return sqrt(pow(fov, 2.0) / (pow((double)w / (double)h, 2.0) + 1.0));
}

int MainWindow::CalculateEstimatedWidth() {
	double hfov = CalculateEstimatedHFOV();
	return CalculateEstimatedWidth(hfov);
}

int MainWindow::CalculateEstimatedHeight() {
	double vfov = CalculateEstimatedVFOV();
	return CalculateEstimatedHeight(vfov);
}

int MainWindow::CalculateEstimatedWidth(double hfov) {
	if (ui->InputResolutionW->value() == 0 || ui->InputResolutionH->value() == 0) return 0;

	double ihfov;

	if (ui->HFovSpinBox->value() != 0 && ui->VFovSpinBox->value() != 0) {
		ihfov = ui->HFovSpinBox->value();
	} else {
		return 0;
	}

	if (ui->modeComboBox->currentText() == "sbs")
		return ((double)ui->InputResolutionW->value() / 2.0) * (hfov / ihfov);
	else
		return (double)ui->InputResolutionW->value() * (hfov / ihfov);
}

int MainWindow::CalculateEstimatedHeight(double vfov) {
	if (ui->InputResolutionW->value() == 0 || ui->InputResolutionH->value() == 0) return 0;

	double ivfov;

	if (ui->HFovSpinBox->value() != 0 && ui->VFovSpinBox->value() != 0) {
		ivfov = ui->VFovSpinBox->value();
	} else {
		return 0;
	}

	if (ui->modeComboBox->currentText() == "tb")
		return ((double)ui->InputResolutionH->value() / 2.0) * (vfov / ivfov);
	else
		return (double)ui->InputResolutionH->value() * (vfov / ivfov);
}


void MainWindow::on_Text_editingFinished() {
	UpdateItem(TEXT_COLUMN, ui->Text->text());
	ui->Table->setFocus();
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void MainWindow::on_RightEye_checkStateChanged(Qt::CheckState state) {
#else
void MainWindow::on_RightEye_stateChanged(int state) {
#endif
	if (RAMCache) CameraCache.clear();
	if (state == Qt::Unchecked)
		UpdateItem(EYE_COLUMN, "0");
	else
		UpdateItem(EYE_COLUMN, "1");
}

void MainWindow::on_VOffset_valueChanged(double arg1) {
	UpdateItem(V_OFFSET_COLUMN, QString::number(arg1));
}

void MainWindow::on_HOffset_valueChanged(double arg1) {
	UpdateItem(H_OFFSET_COLUMN, QString::number(arg1));
}

void MainWindow::AddLine(QTime time, double trans, double pitch, double yaw, double roll, double fov,
						 bool righteye, double v, double h, QString text) {
	if (ui->Table->rowCount() == 0) {
		QUndoCommand *addstart = new QTableWidgetAdd(ui->Table, TIME_FORMAT, 0,
				QTime::fromMSecsSinceStartOfDay(0), 0, 0, 0, 0, 100, 0, 0, 0, "START");
		TableUndo->push(addstart);

		if (ui->VideoLength->time().msecsSinceStartOfDay() > 0) {
			QUndoCommand *addend = new QTableWidgetAdd(ui->Table, TIME_FORMAT, 1,
					ui->VideoLength->time(), 0, 0, 0, 0, 100, 0, 0, 0, "END");
			TableUndo->push(addend);
		}

		if (time.msecsSinceStartOfDay() != 0) {
			ui->Table->blockSignals(true);
			ui->Table->selectRow(0);
			ui->Table->blockSignals(false);
		} else {
			ui->Table->selectRow(0);
			return;
		}
	}

	int selectedrow = GetSelectedRow();
	int newrow = selectedrow + 1;

	QUndoCommand *add = new QTableWidgetAdd(ui->Table,
											TIME_FORMAT,
											newrow,
											time,
											trans,
											pitch,
											yaw,
											roll,
											fov,
											righteye,
											v,
											h,
											text);
	TableUndo->push(add);
	Changes();
	ReadRow(newrow);
	ui->Table->selectRow(newrow);
}

void MainWindow::on_AddLine_clicked() {
	AddLine(ui->RawTime->time(),
			ui->DefaultTranValue->value(),
			ui->Pitch->value(),
			ui->Yaw->value(),
			ui->Roll->value(),
			ui->Fov->value(),
			ui->RightEye->isChecked(),
			ui->VOffset->value(),
			ui->HOffset->value(),
			"");
}

void MainWindow::on_AddTansZero_clicked() {
	AddLine(ui->RawTime->time(),
			0,
			ui->Pitch->value(),
			ui->Yaw->value(),
			ui->Roll->value(),
			ui->Fov->value(),
			ui->RightEye->isChecked(),
			ui->VOffset->value(),
			ui->HOffset->value(),
			"");
}

void MainWindow::on_AddPrev_clicked() {
	int antrowid = GetSelectedRow() - 1;
	tableline_t AntLine = ReadRow(antrowid);

	AddLine(ui->RawTime->time(),
			ui->DefaultTranValue->value(),
			AntLine.Pitch,
			AntLine.Yaw,
			AntLine.Roll,
			AntLine.FOV,
			AntLine.RightEye,
			AntLine.V,
			AntLine.H,
			"");
}

void MainWindow::on_UnselectRow_clicked() {
	ui->Table->blockSignals(true);
	ui->Table->clearSelection();
	ui->Table->blockSignals(false);
	ui->FiltersList->blockSignals(true);
	ui->FiltersList->clearSelection();
	ui->FiltersList->blockSignals(false);
}


void MainWindow::on_RemoveLine_clicked() {
	int selectedrow = GetSelectedRow();
	if (selectedrow != -1 ) {
		QUndoCommand *deletecommand = new QTableWidgetDelete(ui->Table, selectedrow);
		TableUndo->push(deletecommand);
		Changes();
	}
}

void MainWindow::on_UpdateTime_clicked() {
	int selectedrow = GetSelectedRow();
	if (selectedrow == -1 ) return;

	QString value = ui->RawTime->text();
	QTableWidgetItem *CurrentItem = ui->Table->item(selectedrow, TIME_COLUMN);
	if (CurrentItem != nullptr) {
		if (value == CurrentItem->text()) return;
	}
	QTableWidgetItem *content = new QTableWidgetItem(value);
	QUndoCommand *ChangeItem = new QTableWidgetChange(ui->Table, content, CurrentItem);
	TableUndo->push(ChangeItem);
	Changes();
	ReadRow(selectedrow);
}

void MainWindow::on_UpdateTransition_clicked() {
	int selectedrow = GetSelectedRow();
	if (selectedrow == -1 ) return;
	QTime StartTime;

	QTableWidgetItem *ItemStartTime = ui->Table->item(selectedrow, TIME_COLUMN);
	if (ItemStartTime != nullptr) {
		StartTime = QTime::fromString(ItemStartTime->text(), TIME_FORMAT);
	} else {
		return;
	}

	int TransitionMS = ui->RawTime->time().msecsSinceStartOfDay() - StartTime.msecsSinceStartOfDay();
	if (TransitionMS < 0) return;

	double TransitionS = (double)TransitionMS / 1000;
	QString value = QString::number(TransitionS, 'f', 1);
	QTableWidgetItem *CurrentItem = ui->Table->item(selectedrow, TRANSITION_COLUMN);
	QTableWidgetItem *content = new QTableWidgetItem(value);
	QUndoCommand *ChangeItem = new QTableWidgetChange(ui->Table, content, CurrentItem);
	TableUndo->push(ChangeItem);
	Changes();
	ReadRow(selectedrow);
}

void MainWindow::TableUpSelect() {
	QTableWidget *Table;

	if (ui->Table->isVisible())
		Table = ui->Table;
	else if (ui->FiltersList->isVisible())
		Table = ui->FiltersList;
	else
		return;

	int selected = GetSelectedRow(Table);
	if (selected < 1) return;
	Table->selectRow(selected - 1);
}

void MainWindow::TableDownSelect() {
	QTableWidget *Table;

	if (ui->Table->isVisible())
		Table = ui->Table;
	else if (ui->FiltersList->isVisible())
		Table = ui->FiltersList;
	else
		return;

	int selected = GetSelectedRow(Table);
	if (selected == Table->rowCount() - 1) return;
	Table->selectRow(selected + 1);
}

void MainWindow::ShowProcessError(QProcess *process, QString CustomErrorLog) {
	QString FailCommand = process->program();
	for (int i = 0; i < process->arguments().count(); ++i) {
		if (process->arguments().at(i).contains(" ")) {
			FailCommand.append(" '" + process->arguments().at(i) + "'");
		} else {
			FailCommand.append(" " + process->arguments().at(i));
		}
	}
	if (!ui->LeftTabs->isTabVisible(LEFT_TAB_ERROR_LOG)) {
		ui->LeftTabs->setTabVisible(LEFT_TAB_ERROR_LOG, true);
		ui->LeftTabs->setCurrentIndex(LEFT_TAB_ERROR_LOG);
	}
	ui->ErrorLog->appendHtml("<p><b>Error in command: " + FailCommand + "</b></p>");

	QStringList ErrorLines;
	if (CustomErrorLog != "")
		ErrorLines = CustomErrorLog.split("\n");
	else
		ErrorLines = QString(process->readAllStandardError()).split("\n");

	for (int i = 0; i < ErrorLines.count(); ++i)
		ui->ErrorLog->appendHtml("<p style=color:" + ui->ErrorColor->text() + ";>" + ErrorLines.at(i) + "</p>");

	ui->ErrorLog->appendHtml("<hr>");
}

void MainWindow::CleanTable(QTableWidget *Table) {
	Table->blockSignals(true);
	Table->clearSelection();
	for (int i = Table->rowCount(); i > 0 ; i--) {
		Table->removeRow(i - 1);
	}
	Table->blockSignals(false);
}

void MainWindow::on_Table_itemSelectionChanged() {
	if (!ui->RawFrame->isVisible()) return;
	bool needrawupdate = false;
	bool needfilterupdate = false;
	BlockRefresh = true;

	int selected = GetSelectedRow();

	double trans = ReadItemValue(selected, TRANSITION_COLUMN).toDouble();
	double pitch = ReadItemValue(selected, PITCH_COLUMN).toDouble();
	double yaw = ReadItemValue(selected, YAW_COLUMN).toDouble();
	double roll = ReadItemValue(selected, ROLL_COLUMN).toDouble();
	double fov = ReadItemValue(selected, FOV_COLUMN).toDouble();
	double v =  ReadItemValue(selected, V_OFFSET_COLUMN).toDouble();
	double h =  ReadItemValue(selected, H_OFFSET_COLUMN).toDouble();

	QString text = ReadItemValue(selected, TEXT_COLUMN);

	if (trans != ui->Trans->value()) {
		ui->Trans->setValue(trans);
	}
	if (pitch != ui->Pitch->value()) {
		needfilterupdate = true;
		ui->Pitch->setValue(pitch);
	}
	if (yaw != ui->Yaw->value()) {
		needfilterupdate = true;
		ui->Yaw->setValue(yaw);
	}
	if (roll != ui->Roll->value()) {
		needfilterupdate = true;
		ui->Roll->setValue(roll);
	}
	if (fov != ui->Fov->value()) {
		needfilterupdate = true;
		if (fov > ui->MaxFov->value())
			ui->Fov->setMaximum(fov);
		else
			ui->Fov->setMaximum(ui->MaxFov->value());
		if (fov < ui->MinFov->value())
			ui->Fov->setMinimum(fov);
		else
			ui->Fov->setMinimum(ui->MinFov->value());
		ui->Fov->setValue(fov);
	}
	if (fov != ui->VOffset->value()) {
		needfilterupdate = true;
		ui->VOffset->setValue(v);
	}
	if (fov != ui->HOffset->value()) {
		needfilterupdate = true;
		ui->HOffset->setValue(h);
	}
	if (text != ui->Text->text()) {
		ui->Text->setText(text);
	}

	bool righteye = (ReadItemValue(selected, EYE_COLUMN) != "0");
	if (righteye != ui->RightEye->isChecked()) {
		needfilterupdate = true;
		ui->RightEye->setChecked(righteye);
	}

	BlockRefresh = false;

	if (ui->Table->isVisible()) {

		BlockRefresh = true;
		QTime time = QTime::fromString(ReadItemValue(selected, TIME_COLUMN), TIME_FORMAT);
		if (time != ui->RawTime->time()) {
			needrawupdate = true;
			ui->RawTime->setTime(time);
		}
		BlockRefresh = false;

		int TableRow = LocateRow(ui->FiltersList, FILTERS_TIME_COLUMN, time);
		if (TableRow == -1) {
			ui->FiltersList->blockSignals(true);
			ui->FiltersList->clearSelection();
			ui->FiltersList->blockSignals(false);
			LoadFiltersFields(GenerateDefaultFilters());
		} else {
			ui->FiltersList->selectRow(TableRow);
		}

		if (needrawupdate) {
			ReloadRawImage(ui->RawTime->time());
			return;
		}

		if (needfilterupdate) {
			ReloadFilterImage();
			return;
		}
	}
}

void MainWindow::on_OpenMovementFile_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Open Movement File", "", "VR2Normal File (*.vr2n) ;; All Files (*)");
	if (File == "") return;
	RawData.clear();
	FilterResultData.clear();
	ui->RawFrame->clear();
	ui->CameraFrame->clear();
	ui->MovementFile->setText(QDir::toNativeSeparators(File));
	if (!QFile::exists(ui->MovementFile->text())) return;

	LoadMovenmentFile();
}

void MainWindow::on_SaveCMD_clicked() {
	CurrentSendcmdFile = QDir::toNativeSeparators(ui->CMDFile->text());
	if (CurrentSendcmdFile == "") {
		if (TmpSendcmdFile == "")
			TmpSendcmdFile = QDir::toNativeSeparators(Tmp.filePath(InputFilename.replace("'", "") + ".cmd"));
		CurrentSendcmdFile = TmpSendcmdFile;
	}
	if (ui->CMDText->toPlainText() == "") on_GenerateCMD_clicked();
	WriteFile(CurrentSendcmdFile, ui->CMDText->toPlainText());
}

QString MainWindow::CMDOption(QString Filter, QString Option, QString Value) {
	return "[enter] " + Filter + " " + Option + " '" + Value + "'";
}

QString MainWindow::CMDOption(QString Filter, QString Option, int Value) {
	return CMDOption(Filter, Option, QString::number(Value));
}
QString MainWindow::CMDOption(QString Filter, QString Option, double Value) {
	return CMDOption(Filter, Option, QString::number(Value));
}

QString MainWindow::CMDTransition(QString Filter, QString Option, QString OldValue, QString NewValue) {
	QString Movement = "TI";
	if (ui->MovementAlg->currentText() == "Smooth")
		Movement = "(cos(lerp(PI,0,TI))+1)/2";
	if (ui->MovementAlg->currentText() == "Intermediate")
		Movement = "((" + QString::number(ui->IntermediatePercent->value()) + "/100)*(cos(lerp(PI,0,TI))+1)/2)+(1-" +
				   QString::number(ui->IntermediatePercent->value()) + "/100)*TI";

	return "[expr] " + Filter + " " + Option + " 'lerp(" + OldValue + "," + NewValue + "," + Movement + ")'";
}

QString MainWindow::CMDTransition(QString Filter, QString Option, int OldValue, int NewValue) {
	return CMDTransition(Filter, Option, QString::number(OldValue), QString::number(NewValue));
}

QString MainWindow::CMDTransition(QString Filter, QString Option, double OldValue, double NewValue) {
	return CMDTransition(Filter, Option, QString::number(OldValue), QString::number(NewValue));
}

QString MainWindow::CMDInterval(int Start, int End) {
	return QString::number(Start) + "ms-" + QString::number(End) + "ms";
}

QString MainWindow::CMDline(QString Interval, QStringList Commands, bool Line) {
	QString Result = "\t" + Interval + " " + Commands.join(", ") + ";\n";
	if (Line) Result += "\n";
	return Result;
}

void MainWindow::on_GenerateCMD_clicked() {
	bool AllValid = true;

	bool Filters = false;
	if (ui->FilterExposure->isChecked() || ui->FilterColorTemperature->isChecked() ||
		ui->FilterEQ->isChecked() || ui->FilterColorBalance->isChecked()) Filters = true;

	QList<tableline_t> Lines;
	exposure_t LastValidExposure;
	colortemperature_t LastValidColorTemperature;
	eq_t LastValidEQ;
	colorbalance_t LastValidColorBalance;
	for (int i = 0; i < ui->Table->rowCount(); ++i) {
		tableline_t Line = ReadRow(i);
		if (Line.Valid) {
			int FiltersCurrentRow = -1;
			if (Filters)
				FiltersCurrentRow = LocateRow(ui->FiltersList, FILTERS_TIME_COLUMN, QTime::fromMSecsSinceStartOfDay(Line.Time));

			if (FiltersCurrentRow >= 0) {
				QString FiltersString = ReadItemValue(ui->FiltersList, FiltersCurrentRow, FILTERS_FILTER_COLUMN);
				if (ui->FilterExposure->isChecked()) {
					Line.Exposure.fromString(FiltersString);
					if (Line.Exposure.Valid) {
						LastValidExposure = Line.Exposure;
					} else {
						Line.Exposure = LastValidExposure;
					}
				}
				if (ui->FilterColorTemperature->isChecked()) {
					Line.ColorTemperature.fromString(FiltersString);
					if (Line.ColorTemperature.Valid) {
						LastValidColorTemperature = Line.ColorTemperature;
					} else {
						Line.ColorTemperature = LastValidColorTemperature;
					}
				}
				if (ui->FilterEQ->isChecked()) {
					Line.EQ.fromString(FiltersString);
					if (Line.EQ.Valid) {
						LastValidEQ = Line.EQ;
					} else {
						Line.EQ = LastValidEQ;
					}
				}
				if (ui->FilterColorBalance->isChecked()) {
					Line.ColorBlance.fromString(FiltersString);
					if (Line.ColorBlance.Valid) {
						LastValidColorBalance = Line.ColorBlance;
					} else {
						Line.ColorBlance = LastValidColorBalance;
					}
				}
			}

			Lines.append(Line);
		} else {
			AllValid = false;
		}
	}

	if (AllValid) {
		ui->ErrorLines->setVisible(false);
	} else {
		ui->ErrorLines->setVisible(true);
	}

	RefreshPreviews();

	QString LeftEye;
	QString RightEye;

	if (ui->modeComboBox->currentText() == "sbs") {
		LeftEye = CMDOption("crop", "x", "0");
		RightEye = CMDOption("crop", "x", "iw/2");
	}

	if (ui->modeComboBox->currentText() == "tb") {
		LeftEye = CMDOption("crop", "y", "0");
		RightEye = CMDOption("crop", "y", "ih/2");
	}

	tableline_t *CurrentLine;
	tableline_t *PrevLine;
	tableline_t *NextLine;
	QString Output;

	for (int i = 0; i < Lines.count(); ++i) {
		QStringList Commands;
		QString TimeIntervar;

		CurrentLine = &Lines[i];

		if (i != 0) {
			PrevLine = &Lines[i - 1];
		} else {
			PrevLine = &DefaultTableLine;
		}

		if ((i + 1) < Lines.count())
			NextLine = &Lines[i + 1];
		else
			break;

		if (CurrentLine->Text != "" && CurrentLine->Text != PrevLine->Text)
			Output.append("#" + CurrentLine->Text + "\n");

		if (CurrentLine->Transition > 0) {
			int TransitionStart = CurrentLine->Time + CurrentLine->Transition;
			TimeIntervar = CMDInterval(CurrentLine->Time, TransitionStart);

			if (CurrentLine->Pitch != PrevLine->Pitch)
				Commands.append(CMDTransition("v360", "pitch", PrevLine->Pitch, CurrentLine->Pitch));

			if (CurrentLine->Yaw != PrevLine->Yaw)
				Commands.append(CMDTransition("v360", "yaw", PrevLine->Yaw, CurrentLine->Yaw));

			if (CurrentLine->Roll != PrevLine->Roll)
				Commands.append(CMDTransition("v360", "roll", PrevLine->Roll, CurrentLine->Roll));

			if (CurrentLine->FOV != PrevLine->FOV)
				Commands.append(CMDTransition("v360", "d_fov", PrevLine->FOV, CurrentLine->FOV));

			if (CurrentLine->V != PrevLine->V)
				Commands.append(CMDTransition("v360", "v_offset", PrevLine->V, CurrentLine->V));

			if (CurrentLine->H != PrevLine->H)
				Commands.append(CMDTransition("v360", "h_offset", PrevLine->H, CurrentLine->H));

			if (CurrentLine->H != PrevLine->H)
				Commands.append(CMDTransition("v360", "h_offset", PrevLine->H, CurrentLine->H));

			if (ui->FilterExposure->isChecked()) {
				if (CurrentLine->Exposure.exposure != PrevLine->Exposure.exposure)
					Commands.append(CMDTransition("exposure", "exposure", PrevLine->Exposure.exposure, CurrentLine->Exposure.exposure));

				if (CurrentLine->Exposure.black != PrevLine->Exposure.black)
					Commands.append(CMDTransition("exposure", "black", PrevLine->Exposure.black, CurrentLine->Exposure.black));
			}

			if (ui->FilterColorTemperature->isChecked()) {
				if (CurrentLine->ColorTemperature.temperature != PrevLine->ColorTemperature.temperature)
					Commands.append(CMDTransition("colortemperature", "temperature", PrevLine->ColorTemperature.temperature,
												  CurrentLine->ColorTemperature.temperature));

				if (CurrentLine->ColorTemperature.mix != PrevLine->ColorTemperature.mix)
					Commands.append(CMDTransition("colortemperature", "mix", PrevLine->ColorTemperature.mix,
												  CurrentLine->ColorTemperature.mix));

				if (CurrentLine->ColorTemperature.pl != PrevLine->ColorTemperature.pl)
					Commands.append(CMDTransition("colortemperature", "pl", PrevLine->ColorTemperature.pl,
												  CurrentLine->ColorTemperature.pl));
			}
			if (ui->FilterEQ->isChecked()) {
				if (CurrentLine->EQ.contrast != PrevLine->EQ.contrast)
					Commands.append(CMDTransition("eq", "contrast", PrevLine->EQ.contrast, CurrentLine->EQ.contrast));

				if (CurrentLine->EQ.brightness != PrevLine->EQ.brightness)
					Commands.append(CMDTransition("eq", "brightness", PrevLine->EQ.brightness, CurrentLine->EQ.brightness));

				if (CurrentLine->EQ.saturation != PrevLine->EQ.saturation)
					Commands.append(CMDTransition("eq", "saturation", PrevLine->EQ.saturation, CurrentLine->EQ.saturation));

				if (CurrentLine->EQ.gamma != PrevLine->EQ.gamma)
					Commands.append(CMDTransition("eq", "gamma", PrevLine->EQ.gamma, CurrentLine->EQ.gamma));

				if (CurrentLine->EQ.gamma_r != PrevLine->EQ.gamma_r)
					Commands.append(CMDTransition("eq", "gamma_r", PrevLine->EQ.gamma_r, CurrentLine->EQ.gamma_r));

				if (CurrentLine->EQ.gamma_g != PrevLine->EQ.gamma_g)
					Commands.append(CMDTransition("eq", "gamma_g", PrevLine->EQ.gamma_g, CurrentLine->EQ.gamma_g));

				if (CurrentLine->EQ.gamma_b != PrevLine->EQ.gamma_b)
					Commands.append(CMDTransition("eq", "gamma_b", PrevLine->EQ.gamma_b, CurrentLine->EQ.gamma_b));

				if (CurrentLine->EQ.gamma_weight != PrevLine->EQ.gamma_weight)
					Commands.append(CMDTransition("eq", "gamma_weight", PrevLine->EQ.gamma_weight, CurrentLine->EQ.gamma_weight));
			}

			if (ui->FilterColorBalance->isChecked()) {
				if (CurrentLine->ColorBlance.rs != PrevLine->ColorBlance.rs)
					Commands.append(CMDTransition("colorbalance", "rs", PrevLine->ColorBlance.rs, CurrentLine->ColorBlance.rs));

				if (CurrentLine->ColorBlance.gs != PrevLine->ColorBlance.gs)
					Commands.append(CMDTransition("colorbalance", "gs", PrevLine->ColorBlance.gs, CurrentLine->ColorBlance.gs));

				if (CurrentLine->ColorBlance.bs != PrevLine->ColorBlance.bs)
					Commands.append(CMDTransition("colorbalance", "bs", PrevLine->ColorBlance.bs, CurrentLine->ColorBlance.bs));

				if (CurrentLine->ColorBlance.rm != PrevLine->ColorBlance.rm)
					Commands.append(CMDTransition("colorbalance", "rm", PrevLine->ColorBlance.rm, CurrentLine->ColorBlance.rm));

				if (CurrentLine->ColorBlance.gm != PrevLine->ColorBlance.gm)
					Commands.append(CMDTransition("colorbalance", "gm", PrevLine->ColorBlance.gm, CurrentLine->ColorBlance.gm));

				if (CurrentLine->ColorBlance.bm != PrevLine->ColorBlance.bm)
					Commands.append(CMDTransition("colorbalance", "bm", PrevLine->ColorBlance.bm, CurrentLine->ColorBlance.bm));

				if (CurrentLine->ColorBlance.rh != PrevLine->ColorBlance.rh)
					Commands.append(CMDTransition("colorbalance", "rh", PrevLine->ColorBlance.rh, CurrentLine->ColorBlance.rh));

				if (CurrentLine->ColorBlance.gh != PrevLine->ColorBlance.gh)
					Commands.append(CMDTransition("colorbalance", "gh", PrevLine->ColorBlance.gh, CurrentLine->ColorBlance.gh));

				if (CurrentLine->ColorBlance.bh != PrevLine->ColorBlance.bh)
					Commands.append(CMDTransition("colorbalance", "bh", PrevLine->ColorBlance.bh, CurrentLine->ColorBlance.bh));
			}

			if (Commands.count() > 0)
				Output.append(CMDline(TimeIntervar, Commands, false));

			TimeIntervar = CMDInterval(TransitionStart, NextLine->Time);
			Commands.clear();

		} else {
			TimeIntervar = CMDInterval(CurrentLine->Time, NextLine->Time);
		}

		if (CurrentLine->Pitch != PrevLine->Pitch)
			Commands.append(CMDOption("v360", "pitch", CurrentLine->Pitch));

		if (CurrentLine->Yaw != PrevLine->Yaw)
			Commands.append(CMDOption("v360", "yaw", CurrentLine->Yaw));

		if (CurrentLine->Roll != PrevLine->Roll)
			Commands.append(CMDOption("v360", "roll", CurrentLine->Roll));

		if (CurrentLine->FOV != PrevLine->FOV)
			Commands.append(CMDOption("v360", "d_fov", CurrentLine->FOV));

		if (CurrentLine->V != PrevLine->V)
			Commands.append(CMDOption("v360", "v_offset", CurrentLine->V));

		if (CurrentLine->H != PrevLine->H)
			Commands.append(CMDOption("v360", "h_offset", CurrentLine->H));

		if (CurrentLine->RightEye != PrevLine->RightEye) {
			if (CurrentLine->RightEye)
				Commands.append(RightEye);
			else
				Commands.append(LeftEye);
		}

		if (ui->FilterExposure->isChecked()) {
			if (CurrentLine->Exposure.exposure != PrevLine->Exposure.exposure)
				Commands.append(CMDOption("exposure", "exposure", CurrentLine->Exposure.exposure));

			if (CurrentLine->Exposure.black != PrevLine->Exposure.black)
				Commands.append(CMDOption("exposure", "black", CurrentLine->Exposure.black));
		}

		if (ui->FilterColorTemperature->isChecked()) {
			if (CurrentLine->ColorTemperature.temperature != PrevLine->ColorTemperature.temperature)
				Commands.append(CMDOption("colortemperature", "temperature", CurrentLine->ColorTemperature.temperature));

			if (CurrentLine->ColorTemperature.mix != PrevLine->ColorTemperature.mix)
				Commands.append(CMDOption("colortemperature", "mix", CurrentLine->ColorTemperature.mix));

			if (CurrentLine->ColorTemperature.pl != PrevLine->ColorTemperature.pl)
				Commands.append(CMDOption("colortemperature", "pl", CurrentLine->ColorTemperature.pl));
		}

		if (ui->FilterEQ->isChecked()) {
			if (CurrentLine->EQ.contrast != PrevLine->EQ.contrast)
				Commands.append(CMDOption("eq", "contrast", CurrentLine->EQ.contrast));

			if (CurrentLine->EQ.brightness != PrevLine->EQ.brightness)
				Commands.append(CMDOption("eq", "brightness", CurrentLine->EQ.brightness));

			if (CurrentLine->EQ.saturation != PrevLine->EQ.saturation)
				Commands.append(CMDOption("eq", "saturation", CurrentLine->EQ.saturation));

			if (CurrentLine->EQ.gamma != PrevLine->EQ.gamma)
				Commands.append(CMDOption("eq", "gamma", CurrentLine->EQ.gamma));

			if (CurrentLine->EQ.gamma_r != PrevLine->EQ.gamma_r)
				Commands.append(CMDOption("eq", "gamma_r", CurrentLine->EQ.gamma_r));

			if (CurrentLine->EQ.gamma_g != PrevLine->EQ.gamma_g)
				Commands.append(CMDOption("eq", "gamma_g", CurrentLine->EQ.gamma_g));

			if (CurrentLine->EQ.gamma_b != PrevLine->EQ.gamma_b)
				Commands.append(CMDOption("eq", "gamma_b", CurrentLine->EQ.gamma_b));

			if (CurrentLine->EQ.gamma_weight != PrevLine->EQ.gamma_weight)
				Commands.append(CMDOption("eq", "gamma_weight", CurrentLine->EQ.gamma_weight));
		}

		if (ui->FilterColorBalance->isChecked()) {
			if (CurrentLine->ColorBlance.rs != PrevLine->ColorBlance.rs)
				Commands.append(CMDOption("colorbalance", "rs", CurrentLine->ColorBlance.rs));

			if (CurrentLine->ColorBlance.gs != PrevLine->ColorBlance.gs)
				Commands.append(CMDOption("colorbalance", "gs", CurrentLine->ColorBlance.gs));

			if (CurrentLine->ColorBlance.bs != PrevLine->ColorBlance.bs)
				Commands.append(CMDOption("colorbalance", "bs", CurrentLine->ColorBlance.bs));

			if (CurrentLine->ColorBlance.rm != PrevLine->ColorBlance.rm)
				Commands.append(CMDOption("colorbalance", "rm", CurrentLine->ColorBlance.rm));

			if (CurrentLine->ColorBlance.gm != PrevLine->ColorBlance.gm)
				Commands.append(CMDOption("colorbalance", "gm", CurrentLine->ColorBlance.gm));

			if (CurrentLine->ColorBlance.bm != PrevLine->ColorBlance.bm)
				Commands.append(CMDOption("colorbalance", "bm", CurrentLine->ColorBlance.bm));

			if (CurrentLine->ColorBlance.rh != PrevLine->ColorBlance.rh)
				Commands.append(CMDOption("colorbalance", "rh", CurrentLine->ColorBlance.rh));

			if (CurrentLine->ColorBlance.gh != PrevLine->ColorBlance.gh)
				Commands.append(CMDOption("colorbalance", "gh", CurrentLine->ColorBlance.gh));

			if (CurrentLine->ColorBlance.bh != PrevLine->ColorBlance.bh)
				Commands.append(CMDOption("colorbalance", "bh", CurrentLine->ColorBlance.bh));

			if (CurrentLine->ColorBlance.pl != PrevLine->ColorBlance.pl)
				Commands.append(CMDOption("colorbalance", "pl", CurrentLine->ColorBlance.pl));
		}

		if (Commands.count() > 0)
			Output.append(CMDline(TimeIntervar, Commands, true));
	}

	ui->CMDText->setPlainText(Output);

}

void MainWindow::DisableRunButtons() {
	ui->ExecutePreviewCommand->setEnabled(false);
	ui->CPreviewRun->setEnabled(false);
	ui->ExecuteOutCommand->setEnabled(false);
}

void MainWindow::GenerateDiskCache(int seconds) {
	GenerateDiskCache(seconds, QTime::fromMSecsSinceStartOfDay(0), ui->VideoLength->time(), false);
}

void MainWindow::GenerateDiskCache(int seconds, QTime start, QTime end, bool ondemand) {
	if (!DiskCache) return;
	if (end.msecsSinceStartOfDay() == 0) return;

	QTime Current;
	Current.setHMS(start.hour(), start.minute(), start.second());
	while (Current.msecsSinceStartOfDay() <= end.msecsSinceStartOfDay()) {
		if (Current.msecsSinceStartOfDay() >= ui->VideoLength->time().msecsSinceStartOfDay()) return;
		if (ondemand) {
			if (!OndemandDiskCacheQueue.contains(Current))
				OndemandDiskCacheQueue.insert(Current, 0);
		} else {
			if (!DiskCacheQueue.contains(Current))
				DiskCacheQueue.enqueue(Current);
		}
		Current = Current.addSecs(seconds);
	}
}

void MainWindow::TakeScreenshot(QTime Time, int V360W, int V360H, int FinalW, int FinalH) {
	if (ui->ScreenshotsDir->text() == "") return;

	QString V360Resolution = QString::number(V360W) + "x" + QString::number(V360H);
	ScreenshotFilename = ui->ScreenshotsDir->text() + QDir::separator() + InputFilename + "-" +
						 QString::number(Time.msecsSinceStartOfDay()) + "-" + V360Resolution;
	if (ui->ScreenshotDisableFilters->checkState() == Qt::Unchecked) ScreenshotFilename += "-Filters";
	if (ui->ScreenshotDisableFilters->checkState() == Qt::PartiallyChecked) ScreenshotFilename += "-HalfFilters";
	if (FinalW != 0 || FinalH != 0) ScreenshotFilename += "-Rescaled";
	ScreenshotFilename += "." + ui->ScreenshotsFormat->currentText();

	QString v360 = v360Filters(
							   ui->formatComboBox->currentText(),
							   ui->CustomV360->text(),
							   ui->modeComboBox->currentText(),
							   ui->OutputMode->currentText(),
							   ui->VFovSpinBox->value(),
							   ui->HFovSpinBox->value(),
							   ui->Fov->value(),
							   ui->Pitch->value(),
							   ui->Yaw->value(),
							   ui->Roll->value(),
							   ui->VOffset->value(),
							   ui->HOffset->value(),
							   ui->RightEye->isChecked(),
							   V360W,
							   V360H,
							   "lanczos",
							   false,
							   ui->InputPadW->value(),
							   ui->InputPadH->value(),
							   ui->InputPadX->value(),
							   ui->InputPadY->value()
				   );


	QString NormalFilter = "";
	QString ImageFilters = "";
	QString FilterType = "-vf";
	QString Filters = "";

	if (ui->CustomInputFilters->text() != "") NormalFilter = ui->CustomInputFilters->text();
	if (NormalFilter != "") NormalFilter += ",";
	NormalFilter += v360;

	if (ui->ScreenshotDisableFilters->checkState() != Qt::Checked) {
		if (ui->FilterExposure->isChecked() ||
			ui->FilterColorTemperature->isChecked() ||
			ui->FilterEQ->isChecked() ||
			ui->FilterColorBalance->isChecked())
			ImageFilters = GenerateFiltersString();

		if (ui->CustomFilters->text() != "") {
			if (ImageFilters != "") ImageFilters += ",";
			ImageFilters += ui->CustomFilters->text();
		}
	}

	QString ScaleFilter = "";
	if (FinalW != 0 || FinalH != 0) {
		ScaleFilter = "scale=";
		if (FinalW == 0)
			ScaleFilter += "-2";
		else
			ScaleFilter += QString::number(FinalW);
		ScaleFilter += ":";
		if (FinalH == 0)
			ScaleFilter += "-2";
		else
			ScaleFilter += QString::number(FinalH);
		ScaleFilter += ":flags=lanczos+full_chroma_int+bitexact+accurate_rnd";
	}

	if (ui->ScreenshotDisableFilters->checkState() == Qt::PartiallyChecked) {
		if (ImageFilters != "") {
			FilterType = "-filter_complex";
			Filters = "[0] " + NormalFilter + " [v360]; "
					  "[v360] split [ori][filter]; "
					  "[filter] crop=w=iw/2:h=ih:x=0," + ImageFilters + " [filter_out]; "
					  "[ori][filter_out] overlay=format=rgb";
		} else {
			Filters = NormalFilter;
		}
	} else if (ui->ScreenshotDisableFilters->checkState() == Qt::Unchecked) {
		Filters = NormalFilter;
		if (ImageFilters != "") Filters += "," + ImageFilters;
	} else {
		Filters = NormalFilter;
	}

	if (ScaleFilter != "") Filters += "," + ScaleFilter;

	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y" <<
		   "-ss" << QString::number(Time.msecsSinceStartOfDay()) + "ms" <<
		   "-i" << InputFile <<
		   FilterType << Filters <<
		   "-vframes" << "1";

	if (ui->ScreenshotsFormat->currentText() == "png") {
		params << "-c:v" << "png" << "-compression_level" << QString::number(ui->ScreenshotsQuality->value());
		params << "-pred" << "mixed";
		if (ui->InputPixelFormat->text().contains("10le")) {
			params << "-pix_fmt" << "rgb48be";
		} else {
			params << "-pix_fmt" << "rgb24";
		}
	}

	if (ui->ScreenshotsFormat->currentText() == "jpg") {
		params << "-q:v" << QString::number(ui->ScreenshotsQuality->value());
	}
	if (ui->ScreenshotsFormat->currentText() == "webp") {
		params << "-c:v" << "libwebp" << "-q:v" << QString::number(ui->ScreenshotsQuality->value()) << "-compression_level" <<
			   "6";
	}

	params << ScreenshotFilename;

	EnableScreenshotsButtons(false);
	ui->ScreenshotInfo->setText("Processing...");

	if (Debug) ui->DebugLog->appendPlainText(ui->FfmpegBin->text() + " " + params.join(" "));
	ScreenshotProcess.start(ui->FfmpegBin->text(), params);
	ScreenshotProcess.waitForStarted(-1);
}

void MainWindow::EnableScreenshotsButtons(bool Enable) {
	ui->TakeScreenshot->setEnabled(Enable);
	ui->TakeScreenshot1280->setEnabled(Enable);
	ui->TakeScreenshot1920->setEnabled(Enable);
	ui->TakeScreenshotRescaled->setEnabled(Enable);
	ui->TakeScreenshotEstimated->setEnabled(Enable);
	ui->TakeScreenshotEstimRescaled->setEnabled(Enable);
}

void MainWindow::PrintFramingLines() {
	int width = ScaledFilterPixmap.width();
	int height = ScaledFilterPixmap.height();
	if (ui->OutputMode->currentText() == "sbs") {
		width = width / 2;
		PrintEyeFramingLines(width, height, width, 0);
	} else if (ui->OutputMode->currentText() == "tb") {
		height = height / 2;
		PrintEyeFramingLines(width, height, 0, height);
	}
	PrintEyeFramingLines(width, height, 0, 0);
}

void MainWindow::DrawLines(QPainter &paint, int width, int height, int x, int y) {
	int halfw = width / 2;
	int halfh = height / 2;

	int thirdw = width / 3;
	int thirdh = height / 3;

	int sixthw = width / 6;
	int sixthh = height / 6;

	int twelfthw = width / 12;
	int twelfthh = height / 12;

	paint.drawLine(x,
				   halfh + y,
				   sixthw + x,
				   halfh + y);

	paint.drawLine(width + x,
				   halfh + y,
				   width - sixthw + x,
				   halfh + y);

	paint.drawLine(halfw + x,
				   y,
				   halfw + x,
				   sixthh + y);

	paint.drawLine(halfw + x,
				   height + y,
				   halfw + x,
				   height - sixthh + y);

	paint.drawLine(thirdw + x,
				   thirdh + y,
				   thirdw + twelfthw + x,
				   thirdh + y);

	paint.drawLine(thirdw + x,
				   thirdh + y,
				   thirdw + x,
				   thirdh + twelfthh + y);

	paint.drawLine(width - thirdw + x,
				   thirdh + y,
				   width - thirdw - twelfthw + x,
				   thirdh + y);

	paint.drawLine(width - thirdw + x,
				   thirdh + y,
				   width - thirdw + x,
				   thirdh + twelfthh + y);

	paint.drawLine(thirdw + x,
				   height - thirdh + y,
				   thirdw + twelfthw + x,
				   height - thirdh + y);

	paint.drawLine(thirdw + x,
				   height - thirdh + y,
				   thirdw + x,
				   height - thirdh - twelfthh + y);

	paint.drawLine(width - thirdw + x,
				   height - thirdh + y,
				   width - thirdw - twelfthw + x,
				   height - thirdh + y);

	paint.drawLine(width - thirdw + x,
				   height - thirdh + y,
				   width - thirdw + x,
				   height - thirdh - twelfthh + y);
}

void MainWindow::PrintEyeFramingLines(int width, int height, int x, int y) {
	QPainter paint(&ScaledFilterPixmap);
	QPen pen(Qt::white);
	pen.setWidth(2);
	paint.setPen(pen);

	DrawLines(paint, width, height, x, y);

	pen.setWidth(1);
	pen.setColor(Qt::black);
	paint.setPen(pen);

	DrawLines(paint, width, height, x, y);
}

void MainWindow::DiskCacheStart() {
	QList<QProcess *> Process = CacheProcess.keys();
	for (int i = 0; i < Process.count(); ++i) {
		QProcess *P = Process.value(i);
		cache_time_t CT = CacheProcess[P];
		CT.ElapsedTimer->start();
		CacheProcess[P] = CT;
		StartCacheProcess(P);
	}
}

void MainWindow::AutosaveTimeout() {
	if (PendingChanges) SaveMovementFile();
}

void MainWindow::ScreenshotProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (exitCode != 0) {
		ShowProcessError(&ScreenshotProcess);
		ui->ScreenshotInfo->setText("Failed taking screenshot");
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;
	if (ScreenshotProcess.state() != QProcess::NotRunning) return;

	ui->ScreenshotInfo->setText("Done");
	EnableScreenshotsButtons(true);

	if (ui->ScreenshotAutoOpen->isChecked() && ui->ImageViewer->text() != "")
		QProcess::startDetached(ui->ImageViewer->text(), QStringList(ScreenshotFilename));
	ScreenshotFilename = "";
}

void MainWindow::PitchUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Pitch->stepUp();
}

void MainWindow::PitchDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Pitch->stepDown();
}

void MainWindow::YawUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Yaw->stepUp();
}

void MainWindow::YawDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Yaw->stepDown();
}

void MainWindow::RollUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Roll->stepUp();
}

void MainWindow::RollDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Roll->stepDown();
}

void MainWindow::FovUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Fov->stepUp();
}

void MainWindow::FovDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Fov->stepDown();
}

void MainWindow::VUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->VOffset->stepUp();
}

void MainWindow::VDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->VOffset->stepDown();
}

void MainWindow::HUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->HOffset->stepUp();
}

void MainWindow::HDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->HOffset->stepDown();
}

void MainWindow::TrayClick(QSystemTrayIcon::ActivationReason reason) {
	if (reason != QSystemTrayIcon::Trigger) return;
	if (this->isHidden()) {
		this->show();
	} else {
		this->hide();
	}
}

void MainWindow::DiskCacheProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (!DiskCache) return;
	QProcess *Process = qobject_cast<QProcess *>(sender());
	if (exitCode != 0) {
		if (Process != nullptr) ShowProcessError(Process);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;

	if (Debug) {
		if (CacheProcess.contains(Process)) {
			cache_time_t CT = CacheProcess[Process];

			if (CT.WaitTime) {
				CT.ProcessTime = CT.ElapsedTimer->restart();
				CT.TotalTime = CT.QueueTime + CT.StartTime + CT.ProcessTime;
				ui->DebugLog->appendPlainText("Cache - Queue:\t" + QString::number(CT.QueueTime) +
											  "\tStart:\t" + QString::number(CT.StartTime) +
											  "\tProcecss:\t" + QString::number(CT.ProcessTime) +
											  "\tTotal:\t" + QString::number(CT.TotalTime));
			}
			CT.TotalTime = 0;
			CT.QueueTime = 0;
			CT.StartTime = 0;
			CT.ProcessTime = 0;
			CT.WaitTime = false;

			CT.ElapsedTimer->start();
			CacheProcess[Process] = CT;
		}
	}

	if (Process != nullptr) StartCacheProcess(Process);
}

void MainWindow::StartCacheProcess(QProcess *Process) {
	if (!DiskCache) return;
	if (Process->state() != QProcess::NotRunning) return;
	QTime time;
	QString file;

	do {
		if (OndemandCache && !OndemandDiskCacheQueue.isEmpty()) {
			time = OndemandDiskCacheQueue.firstKey();
			OndemandDiskCacheQueue.remove(time);
			if (ui->RawTime->time() > time) continue;
		} else {
			if (DiskCacheQueue.isEmpty()) return;
			time = DiskCacheQueue.dequeue();
		}
		if (time >= ui->VideoLength->time()) continue;

		file = ComposeCacheFilename(SmallInputFilenameHash, time.msecsSinceStartOfDay());

	} while (file == "" || QFile::exists(file));

	if (Debug) {
		if (CacheProcess.contains(Process)) {
			cache_time_t CT = CacheProcess[Process];
			CT.QueueTime = CT.ElapsedTimer->restart();
			CT.WaitTime = true;
			CacheProcess[Process] = CT;
		}
	}

	QStringList params = RawFfmpegParams(SmallInputFile, file, time.msecsSinceStartOfDay());

	if (Debug) {
		ui->DebugLog->appendPlainText("Cache Generating time: " + time.toString(TIME_FORMAT));
		//		ui->DebugLog->appendPlainText(params.join(" "));
	}

	Process->start(ui->FfmpegBin->text(), params);
	Process->waitForStarted(-1);
	if (ui->CachePriority->currentText() == "Low")
		ProcessLowPriority(Process->processId());
	else if (ui->CachePriority->currentText() == "Idle")
		ProcessIdlePriority(Process->processId());
	if (Debug) {
		if (CacheProcess.contains(Process)) {
			cache_time_t CT = CacheProcess[Process];
			CT.StartTime = CT.ElapsedTimer->restart();
			CacheProcess[Process] = CT;
		}
	}
}

QStringList MainWindow::RawFfmpegParams(QString in, QString out, uint64_t Time) {
	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y";

	QString HWDec = ui->HWDec->currentText();
	if (HWDec == "CUDA") {
		params << "-hwaccel" << "cuda";
	} else if (HWDec == "VAAPI (linux)") {
		params << "-hwaccel" << "vaapi";
		params << "-vaapi_device" << GetVAAPIDevice();
	} else if (HWDec == "DXVA2 (windows)") {
		params << "-hwaccel" << "dxva2";
	} else if (HWDec == "D3D11 (windows)") {
		params << "-hwaccel" << "d3d11va";
	}

	params << "-ss" << QString::number(Time) + "ms" <<
		   "-i" << in << "-map" << "0:v" << "-vframes" << "1";

	if (GUIPreviewScale == "Auto x 720") {
		params << "-vf" << "scale=-2:720:sws_flags=fast_bilinear";
	} else if (GUIPreviewScale == "Auto x 1080") {
		params << "-vf" << "scale=-2:1080:sws_flags=fast_bilinear";
	} else if (GUIPreviewScale == "Auto x 2160") {
		params << "-vf" << "scale=-2:2160:sws_flags=fast_bilinear";
	} else if (GUIPreviewScale == "1280 x Auto") {
		params << "-vf" << "scale=1280:-2:sws_flags=fast_bilinear";
	} else if (GUIPreviewScale == "1920 x Auto") {
		params << "-vf" << "scale=1920:-2:sws_flags=fast_bilinear";
	} else if (GUIPreviewScale == "3840 x Auto") {
		params << "-vf" << "scale=3840:-2:sws_flags=fast_bilinear";
	}

	if (ui->CacheFormat->currentText() == "JPG") {
		if (out == "") params << "-c:v" << "mjpeg";
		params << "-q:v" << "3";
	} else {
		if (out == "") params << "-c:v" << ui->CacheFormat->currentText().toLower();
	}

	if (out != "") {
		params << out;
	} else {
		params << "-f" << "data" << "-";
	}
	return params;
}

QString MainWindow::ComposeCacheFilename(QString FilenameHash, uint64_t Time) {
	return QDir::toNativeSeparators(
					   ui->DiskCacheDir->text() + QDir::separator() + FilenameHash + "-" +
					   QString::number(Time) + "." + ui->CacheFormat->currentText().toLower()
		   );
}

void MainWindow::on_PreviewTransition_clicked() {
	PreviewTransition_click();
}

bool MainWindow::PreviewTransition_click() {
	tableline_t Line = DefaultTableLine;
	int selected = GetSelectedRow();
	if (selected == -1) return false;
	if (selected == ui->Table->rowCount() - 1) return false;
	Line = ReadRow(selected);
	if (!Line.Valid) return false;

	if (!FileOk || SmallInputFile == "") {
		ui->PreviewCommand->clear();
		ui->PreviewCommand->appendHtml("<p style=color:" + ui->ErrorColor->text() +
									   ";><b>Error accessing the input file</b></p>");
		return false;
	}

	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	tableline_t AntLine = LocateRow(QTime::fromMSecsSinceStartOfDay(Line.Time - ui->PreviewTimeBefore->value()), true);
	QString Filters = SearchFiltersString(QTime::fromMSecsSinceStartOfDay(AntLine.Time));

	QString Ext = GetExtension(ui->PreviewVideoFormat->currentText());

	PreviewFile = ui->PreviewDir->text();
	if (PreviewFile == "") PreviewFile = InputFileDir;
	PreviewFile += QDir::separator() + InputFilename + "_Transition_";
	if (Line.Text == "")
		PreviewFile += QString::number(Line.Time) + Ext;
	else
		PreviewFile += Line.Text + Ext;

	PreviewFile = QDir::toNativeSeparators(PreviewFile);

	QString Template = ui->PreviewCustomFormat->text();
	if (Template == "") Template = GetTemplate(ui->PreviewVideoFormat->currentText());

	QString FfmpegCommand = ComposeFfmpegCommand(ui->FfmpegBin->text(),
							SmallInputFile,
							PreviewFile,
							Template,
							ui->PreviewVideoFormat->currentText(),
							ui->PreviewCRF->value(),
							ui->PreviewPreset->currentText(),
							ui->PreviewAudioFormat->currentText(),
							ui->PreviewAudioBitrate->value(),
							QString::number(Line.Time - ui->PreviewTimeBefore->value()) + "ms",
							QString::number((int)(ui->PreviewTimeBefore->value() + Line.Transition + ui->PreviewTimeAfter->value())) + "ms",
							ui->PreviewFPS->value(),
							ui->FPSSpinBox->value(),
							ui->formatComboBox->currentText(),
							ui->CustomV360->text(),
							ui->modeComboBox->currentText(),
							ui->OutputMode->currentText(),
							ui->VFovSpinBox->value(),
							ui->HFovSpinBox->value(),
							AntLine.FOV,
							AntLine.Pitch,
							AntLine.Yaw,
							AntLine.Roll,
							AntLine.V,
							AntLine.H,
							AntLine.RightEye,
							ui->PreviewW->value(),
							ui->PreviewH->value(),
							ui->PreviewInterpolation->currentText(),
							true,
							ui->InputPadW->value(),
							ui->InputPadH->value(),
							ui->InputPadX->value(),
							ui->InputPadY->value(),
							CurrentSendcmdFile,
							Filters,
							ui->PreviewPXFormat->currentText(),
							"",
							"Preview transition " + QString::number(Line.Time),
							"",
							"",
							"",
							ui->PreviewHWDec->currentText(),
							"",
							ui->CustomFilters->text(),
							ui->CustomInputFilters->text(),
							true);

	if (FfmpegCommand == "") return false;

	DisableRunButtons();

	ProcessParams = QProcess::splitCommand(FfmpegCommand);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	MainProgress.TotalTime = QTime::fromMSecsSinceStartOfDay(
										 Line.Transition +
										 ui->PreviewTimeBefore->value() +
										 ui->PreviewTimeAfter->value());

	ProcessIsPreview = true;
	GenerateSmallFile = false;

	ui->ExecutePreviewCommand->setEnabled(true);

	ui->PreviewCommand->clear();
	ui->PreviewCommand->setPlainText(FfmpegCommand);
	if (ui->AutoRun->isChecked()) ExecuteProcess();
	return true;
}

void MainWindow::on_CPreviewGenerateFfmpeg_clicked() {
	if (ui->Table->rowCount() == 0) {
		ui->CPreviewCommand->clear();
		ui->CPreviewCommand->appendHtml("<p style=color:" + ui->ErrorColor->text() +
										";><b>Error generating ffmpeg command, the movement list is empty.</b></p>");
		return;
	}

	if (!FileOk || SmallInputFile == "") {
		ui->CPreviewCommand->clear();
		ui->CPreviewCommand->appendHtml("<p style=color:" + ui->ErrorColor->text() +
										";><b>Error accessing the input file</b></p>");
		return;
	}

	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	QString Ext = GetExtension(ui->CPreviewVideoFormat->currentText());

	QString OutFilename = ui->CPreviewFile->text();

	if (OutFilename == "") {
		OutFilename = SmallInputFile;
		OutFilename += "_Complete_preview" + Ext;
		ui->CPreviewFile->setText(QDir::toNativeSeparators(OutFilename));
	}

	PreviewFile = "";

	QString Template = ui->CPreviewCustomFormat->text();
	if (Template == "") Template = GetTemplate(ui->CPreviewVideoFormat->currentText());

	tableline_t FirstLine = ReadRow(0);
	if (FirstLine.Time != 0) FirstLine = DefaultTableLine;

	QString FfmpegCommand = ComposeFfmpegCommand(
										ui->FfmpegBin->text(),
										SmallInputFile,
										OutFilename,
										Template,
										ui->CPreviewVideoFormat->currentText(),
										ui->CPreviewCRF->value(),
										ui->CPreviewPreset->currentText(),
										ui->CPreviewAudioFormat->currentText(),
										ui->CPreviewAudioBitrate->value(),
										"",
										"",
										ui->CPreviewFPS->value(),
										ui->FPSSpinBox->value(),
										ui->formatComboBox->currentText(),
										ui->CustomV360->text(),
										ui->modeComboBox->currentText(),
										ui->OutputMode->currentText(),
										ui->VFovSpinBox->value(),
										ui->HFovSpinBox->value(),
										FirstLine.FOV,
										FirstLine.Pitch,
										FirstLine.Yaw,
										FirstLine.Roll,
										FirstLine.V,
										FirstLine.H,
										FirstLine.RightEye,
										ui->CPreviewW->value(),
										ui->CPreviewH->value(),
										ui->CPreviewInterpolation->currentText(),
										true,
										ui->InputPadW->value(),
										ui->InputPadH->value(),
										ui->InputPadX->value(),
										ui->InputPadY->value(),
										CurrentSendcmdFile,
										GenerateDefaultFilters(),
										ui->CPreviewPXFormat->currentText(),
										"",
										"",
										"",
										"",
										"",
										ui->CPreviewHWDec->currentText(),
										ExportMetadataFile(),
										ui->CustomFilters->text(),
										ui->CustomInputFilters->text()
							);

	if (FfmpegCommand == "") return;

	DisableRunButtons();

	ProcessParams = QProcess::splitCommand(FfmpegCommand);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	MainProgress.TotalTime = ui->VideoLength->time();

	ui->CPreviewRun->setEnabled(true);

	ui->CPreviewCommand->clear();
	ui->CPreviewCommand->setPlainText(FfmpegCommand);
	ProcessIsPreview = true;
	GenerateSmallFile = false;
	if (ui->AutoRun->isChecked()) ExecuteProcess();
}

void MainWindow::on_PreviewFragment_clicked() {
	if (ui->Table->rowCount() == 0) {
		ui->PreviewCommand->clear();
		ui->PreviewCommand->appendHtml("<p style=color:" + ui->ErrorColor->text() +
									   ";><b>Error generating ffmpeg command, the movement list is empty.</b></p>");
		return;
	}

	if (!FileOk || SmallInputFile == "") {
		ui->PreviewCommand->clear();
		ui->PreviewCommand->appendHtml("<p style=color:" + ui->ErrorColor->text() +
									   ";><b>Error accessing the input file</b></p>");
		return;
	}

	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	QString Ext = GetExtension(ui->PreviewVideoFormat->currentText());

	PreviewFile = ui->PreviewDir->text();
	if (PreviewFile == "") PreviewFile = InputFileDir;
	PreviewFile += QDir::separator() + SmallInputFilename + "_PrevieFragment" +
				   QString::number(ui->PreviewStart->time().msecsSinceStartOfDay()) + "-" +
				   QString::number(ui->PreviewEnd->time().msecsSinceStartOfDay()) + Ext;
	PreviewFile = QDir::toNativeSeparators(PreviewFile);

	QString Template = ui->PreviewCustomFormat->text();
	if (Template == "") Template = GetTemplate(ui->PreviewVideoFormat->currentText());

	tableline_t StartLine = LocateRow(ui->PreviewStart->time(), true);

	if (!StartLine.Valid) {
		StartLine.FOV = 100;
		StartLine.Pitch = 0;
		StartLine.Yaw = 0;
		StartLine.Roll = 0;
		StartLine.RightEye = false;
	}

	QString Filters = SearchFiltersString(ui->PreviewStart->time(), true);

	QString FfmpegCommand = ComposeFfmpegCommand(
										ui->FfmpegBin->text(),
										SmallInputFile,
										PreviewFile,
										Template,
										ui->PreviewVideoFormat->currentText(),
										ui->PreviewCRF->value(),
										ui->PreviewPreset->currentText(),
										ui->PreviewAudioFormat->currentText(),
										ui->PreviewAudioBitrate->value(),
										QString::number(ui->PreviewStart->time().msecsSinceStartOfDay()) + "ms",
										QString::number(ui->PreviewEnd->time().msecsSinceStartOfDay() - ui->PreviewStart->time().msecsSinceStartOfDay()) + "ms",
										ui->PreviewFPS->value(),
										ui->FPSSpinBox->value(),
										ui->formatComboBox->currentText(),
										ui->CustomV360->text(),
										ui->modeComboBox->currentText(),
										ui->OutputMode->currentText(),
										ui->VFovSpinBox->value(),
										ui->HFovSpinBox->value(),
										StartLine.FOV,
										StartLine.Pitch,
										StartLine.Yaw,
										StartLine.Roll,
										StartLine.V,
										StartLine.H,
										StartLine.RightEye,
										ui->PreviewW->value(),
										ui->PreviewH->value(),
										ui->PreviewInterpolation->currentText(),
										true,
										ui->InputPadW->value(),
										ui->InputPadH->value(),
										ui->InputPadX->value(),
										ui->InputPadY->value(),
										CurrentSendcmdFile,
										Filters,
										ui->PreviewPXFormat->currentText(),
										"",
										"",
										"",
										"",
										"",
										ui->PreviewHWDec->currentText(),
										"",
										ui->CustomFilters->text(),
										ui->CustomInputFilters->text(),
										true
							);

	if (FfmpegCommand == "") return;

	DisableRunButtons();

	ProcessParams = QProcess::splitCommand(FfmpegCommand);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	if (ui->PreviewEnd->time().msecsSinceStartOfDay() == 0) {
		MainProgress.TotalTime = QTime::fromMSecsSinceStartOfDay(ui->VideoLength->time().msecsSinceStartOfDay() -
								 ui->PreviewStart->time().msecsSinceStartOfDay());
	} else {
		MainProgress.TotalTime = QTime::fromMSecsSinceStartOfDay(ui->PreviewEnd->time().msecsSinceStartOfDay() -
								 ui->PreviewStart->time().msecsSinceStartOfDay());
	}

	ui->ExecutePreviewCommand->setEnabled(true);

	ui->PreviewCommand->clear();
	ui->PreviewCommand->setPlainText(FfmpegCommand);
	ProcessIsPreview = true;
	GenerateSmallFile = false;
	if (ui->AutoRun->isChecked()) ExecuteProcess();
}

QString MainWindow::GenerateDefaultOutFile() {
	QString Ext = GetExtension(ui->OutVideoFormat->currentText());
	if (ui->OutDefaultDir->text() != "")
		return ui->OutDefaultDir->text() + QDir::separator() + InputFilename + ui->OutSuffix->text() + Ext;

	return InputFileDir + QDir::separator() + InputFilename + ui->OutSuffix->text() + Ext;
}

void MainWindow::on_SelectOutFile_clicked() {

	QString fileName = QFileDialog::getSaveFileName(this,
					   "New output file",
					   GenerateDefaultOutFile(),
					   "MKV File (*.mkv) ;; WebM File (*.webm) ;; MP4 File (*.mp4)");
	if (fileName != "") ui->OutFile->setText(QDir::toNativeSeparators(fileName));
}

void MainWindow::on_OutGenerateCommand_clicked() {
	if (ui->Table->rowCount() == 0) {
		ui->OutCommand->clear();
		ui->OutCommand->appendHtml("<p style=color:" + ui->ErrorColor->text() +
								   ";><b>Error generating ffmpeg command, the movement list is empty.</b></p>");
		return;
	}

	if (!FileOk || InputFile == "") {
		ui->OutCommand->clear();
		ui->OutCommand->appendHtml("<p style=color:" + ui->ErrorColor->text() + ";><b>Error accessing the input file</b></p>");
		return;
	}

	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	if (ui->OutW->value() == 0 || ui->OutH->value() == 0) CalculateOutputResolution();
	if (ui->OutPXFormat->currentText() == "Auto") CalculateOutputPXFormat();

	if (ui->OutW->value() == 0 || ui->OutH->value() == 0) {
		ui->OutCommand->clear();
		ui->OutCommand->appendHtml("<p style=color:" + ui->ErrorColor->text() +
								   ";><b>Error generating ffmpeg command, the output resolution is not valid.</b></p>");
		return;
	}

	if (ui->OutFile->text() == "") ui->OutFile->setText(GenerateDefaultOutFile());

	QString TmpFile = "";
	if (ui->IncludeFile->isChecked()) {
		TmpFile = Tmp.filePath("VR2Normal_Export_" + InputFilename);
		SaveMovementFile(TmpFile);
	}

	QString Template = ui->OutCustomFormat->text();
	if (Template == "") Template = GetTemplate(ui->OutVideoFormat->currentText());

	tableline_t FirstLine = ReadRow(0);
	if (FirstLine.Time != 0) FirstLine = DefaultTableLine;

	QString FfmpegCommand = ComposeFfmpegCommand(
										ui->FfmpegBin->text(),
										InputFile,
										ui->OutFile->text(),
										Template,
										ui->OutVideoFormat->currentText(),
										ui->OutCRF->value(),
										ui->OutPreset->currentText(),
										ui->OutAudioFormat->currentText(),
										ui->OutAudioBitrate->value(),
										"",
										"",
										0,
										ui->FPSSpinBox->value(),
										ui->formatComboBox->currentText(),
										ui->CustomV360->text(),
										ui->modeComboBox->currentText(),
										ui->OutputMode->currentText(),
										ui->VFovSpinBox->value(),
										ui->HFovSpinBox->value(),
										FirstLine.FOV,
										FirstLine.Pitch,
										FirstLine.Yaw,
										FirstLine.Roll,
										FirstLine.V,
										FirstLine.H,
										FirstLine.RightEye,
										ui->OutW->value(),
										ui->OutH->value(),
										ui->OutInterpolation->currentText(),
										true,
										ui->InputPadW->value(),
										ui->InputPadH->value(),
										ui->InputPadX->value(),
										ui->InputPadY->value(),
										CurrentSendcmdFile,
										GenerateDefaultFilters(),
										ui->OutPXFormat->currentText(),
										TmpFile,
										ui->MetadataTitle->text(),
										ui->MetadataStudio->text(),
										ui->MetadataActors->text(),
										ui->MetadataReleaseDate->text(),
										ui->OutHWDec->currentText(),
										ExportMetadataFile(),
										ui->CustomFilters->text(),
										ui->CustomInputFilters->text()
							);

	if (FfmpegCommand == "") return;

	DisableRunButtons();

	ProcessParams = QProcess::splitCommand(FfmpegCommand);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	MainProgress.TotalTime = ui->VideoLength->time();
	PreviewFile = "";
	ProcessIsPreview = false;

	ui->ExecuteOutCommand->setEnabled(true);

	ui->OutCommand->clear();
	ui->OutCommand->setPlainText(FfmpegCommand);
}

void MainWindow::on_SelectPreviewFile_clicked() {
	QString fileName = QFileDialog::getSaveFileName(this, "New preview file",
					   SmallInputFile + "_Complete_preview.mkv", "MKV File (*.mkv) ;; WebM File (*.webm) ;; MP4 File (*.mp4)");
	if (fileName != "") ui->CPreviewFile->setText(QDir::toNativeSeparators(fileName));
}

void MainWindow::on_formatComboBox_currentTextChanged(const QString &/*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImageForced();
}

void MainWindow::on_modeComboBox_currentTextChanged(const QString &/*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImageForced();
}

void MainWindow::on_OutputMode_currentTextChanged(const QString &/*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImageForced();
}

void MainWindow::on_VFovSpinBox_valueChanged(int /*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImageForced();
}

void MainWindow::on_HFovSpinBox_valueChanged(int /*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImageForced();
}

void MainWindow::on_GUIPreviewInterpolation_currentTextChanged(const QString &/*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImageForced();
}

void MainWindow::on_Apply_clicked() {
	if (ui->CameraFrame->isVisible()) ReloadFilterImageForced();
}

void MainWindow::on_FfmpegBinSelect_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Select ffmpeg binary", "", "All Files (*)");
	if (File != "") ui->FfmpegBin->setText(QDir::toNativeSeparators(File));
}

void MainWindow::on_ClearErrorLog_clicked() {
	ui->ErrorLog->clear();
	ui->LeftTabs->setCurrentIndex(LEFT_TAB_CAMERA);
	ui->LeftTabs->setTabVisible(LEFT_TAB_ERROR_LOG, false);
}

void MainWindow::on_VideoLength_userTimeChanged(const QTime &time) {
	if (time.msecsSinceStartOfDay() == 0) {
		ui->RawTime->clearMaximumTime();
	} else {
		QTime MaxTime = time.addMSecs((-4000 / ui->FPSSpinBox->value()) - 1);
		ui->RawTime->setMaximumTime(MaxTime);
		ui->PreviewStart->setMaximumTime(MaxTime);
		ui->PreviewEnd->setMaximumTime(MaxTime);
	}
}

void MainWindow::on_SelectVideoPlayer_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Select video player binary", "", "All Files (*)");
	if (File != "") ui->VideoPlayer->setText(File);
}

void MainWindow::on_SelectFfprobe_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Select ffprobe binary", "", "All Files (*)");
	if (File != "") ui->FfprobeBin->setText(File);
}

void MainWindow::on_SelectImageViewer_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Select image viewer binary", "", "All Files (*)");
	if (File != "") ui->ImageViewer->setText(File);
}

void MainWindow::on_Sort_clicked() {
	TableUndo->clear();
	ui->Table->sortByColumn(TIME_COLUMN, Qt::AscendingOrder);
	for (int i = 0; i < ui->Table->rowCount(); ++i) {
		tableline_t Line = ReadRow(i);
	}
	ui->FiltersList->sortByColumn(FILTERS_TIME_COLUMN, Qt::AscendingOrder);
	Changes();
}

void MainWindow::on_Cancel_clicked() {
	ExecutionCanceled = true;
	Execution.close();
}

void MainWindow::on_actionSave_screen_triggered() {

}

void MainWindow::on_PreviewCurrentLine_clicked() {
	if (PreviewTransition_click()) ExecuteProcess();
}

void MainWindow::on_RAMCache_valueChanged(int arg1) {
	RawCache.setMaxCost(arg1 / 2 * 1024 * 1024);
	CameraCache.setMaxCost(arg1 / 2 * 1024 * 1024);
	CheckCache();
}

void MainWindow::on_GenerateDiskCache_clicked() {
	if (!DiskCache) return;
	GenerateDiskCache(60);
	DiskCacheStart();
}


void MainWindow::on_Gen10SCache_clicked() {
	if (!DiskCache) return;
	GenerateDiskCache(10);
	DiskCacheStart();
}

void MainWindow::on_ClearRAMCache_clicked() {
	RawCache.clear();
	CameraCache.clear();
	ui->CurrentRAMUsage->setText(QString::number(0, 'f', 2));
}


void MainWindow::on_DiskCacheDirSelect_clicked() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  "Open Cache Directory",
				  QDir::tempPath(),
				  QFileDialog::ShowDirsOnly);

	if (!QDir(Dir).isEmpty()) {
		QMessageBox::StandardButton message = QMessageBox::question(this,
											  "Cache Directory not empty",
											  "The cache directory should be excusive for VR2Normal, "
											  "all files inside could be deleted. "
											  "Do you want to use this folder?");
		if (message == QMessageBox::No) return;
	}
	if (Dir != "") ui->DiskCacheDir->setText(QDir::toNativeSeparators(Dir));
	CheckCache();
}


void MainWindow::on_TakeScreenshot1920_clicked() {
	TakeScreenshot(ui->RawTime->time(), 1920, 1080, 0, 0);
}

void MainWindow::on_TakeScreenshot1280_clicked() {
	TakeScreenshot(ui->RawTime->time(), 1280, 720, 0, 0);
}

void MainWindow::on_TakeScreenshot_clicked() {
	TakeScreenshot(
				ui->RawTime->time(),
				ui->ScreenshotsV360W->value(),
				ui->ScreenshotsV360H->value(),
				ui->ScreenshotsW->value(),
				ui->ScreenshotsH->value());
}

void MainWindow::on_TakeScreenshotRescaled_clicked() {
	TakeScreenshot(ui->RawTime->time(), 1280, 720, 1920, 1080);
}

void MainWindow::on_TakeScreenshotEstimated_clicked() {
	int hpixels = CalculateEstimatedWidth();
	int vpixels = CalculateEstimatedHeight();
	if (hpixels == 0 || vpixels == 0) return;
	TakeScreenshot(ui->RawTime->time(), hpixels, vpixels, 0, 0);
}

void MainWindow::on_TakeScreenshotEstimRescaled_clicked() {
	int hpixels = CalculateEstimatedWidth();
	int vpixels = CalculateEstimatedHeight();
	if (hpixels == 0 || vpixels == 0) return;
	TakeScreenshot(ui->RawTime->time(), hpixels, vpixels, 1920, 1080);
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void MainWindow::on_ShowFramingLines_checkStateChanged(Qt::CheckState /*state*/) {
#else
void MainWindow::on_ShowFramingLines_stateChanged(int /*state*/) {
#endif
	RefreshCameraImage();
}

void MainWindow::on_PreviewDirSelect_clicked() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  "Open Previews Directory",
				  ui->PreviewDir->text(),
				  QFileDialog::ShowDirsOnly);

	if (Dir != "") ui->PreviewDir->setText(QDir::toNativeSeparators(Dir));
}

void MainWindow::on_SelectScreenshotsDir_clicked() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  "Open Screenshots Directory",
				  ui->ScreenshotsDir->text(),
				  QFileDialog::ShowDirsOnly);

	if (Dir != "") ui->ScreenshotsDir->setText(QDir::toNativeSeparators(Dir));
}

void MainWindow::on_SelectOutDefaultDir_clicked() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  "Open Default Output Directory",
				  ui->OutDefaultDir->text(),
				  QFileDialog::ShowDirsOnly);

	if (Dir != "") ui->OutDefaultDir->setText(QDir::toNativeSeparators(Dir));
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void MainWindow::on_EnableDebug_checkStateChanged(Qt::CheckState state) {
#else
void MainWindow::on_EnableDebug_stateChanged(int state) {
#endif
	if (state == Qt::Checked) {
		Debug = true;
		ui->RightTabs->setTabVisible(RIGHT_TAB_DEBUG, true);
	} else {
		Debug = false;
		ui->RightTabs->setTabVisible(RIGHT_TAB_DEBUG, false);
	}
}

void MainWindow::on_PauseOndemand_toggled(bool /*checked*/) {
	CheckCache();
}

void MainWindow::on_DiskCacheDir_textChanged(const QString &/*arg1*/) {
	CheckCache();
}

void MainWindow::on_PreloadCacheSeoconds_textChanged(const QString &/*arg1*/) {
	CheckCache();
}

void MainWindow::on_DiskCache_textChanged(const QString &/*arg1*/) {
	CheckCache();
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void MainWindow::on_ShowEstimatedResolution_checkStateChanged(Qt::CheckState state) {
#else
void MainWindow::on_ShowEstimatedResolution_stateChanged(int state) {
#endif
	if (state == Qt::Checked)
		ui->EstimatedBlock->setVisible(true);
	else
		ui->EstimatedBlock->setVisible(false);
}

void MainWindow::on_FfmpegBin_textChanged(const QString &arg1) {
	if (Loading) return;
	if (!QFileInfo::exists(arg1)) return;

	QFileInfo File(arg1);

	if (ui->FfprobeBin->text() == "") {
		#ifdef Q_OS_WIN
		QString ffprobe = File.path() + QDir::separator() + "ffprobe.exe";
		#else
		QString ffprobe = File.path() + QDir::separator() + "ffprobe";
		#endif
		if (QFileInfo::exists(ffprobe)) {
			ui->FfprobeBin->setText(QDir::toNativeSeparators(ffprobe));
		}
	}

	#ifdef Q_OS_WIN
	QString ffplay = File.path() + QDir::separator() + "ffplay.exe";
	#else
	QString ffplay = File.path() + QDir::separator() + "ffplay";
	#endif
	if (QFileInfo::exists(ffplay)) {
		if (ui->VideoPlayer->text() == "") ui->VideoPlayer->setText(QDir::toNativeSeparators(ffplay));
		if (ui->ImageViewer->text() == "") ui->ImageViewer->setText(QDir::toNativeSeparators(ffplay));
	}
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void MainWindow::on_ShowSendcmdTab_checkStateChanged(Qt::CheckState state) {
#else
void MainWindow::on_ShowSendcmdTab_stateChanged(int state) {
#endif
	ui->LeftTabs->setTabVisible(LEFT_TAB_SENDCMD, state == Qt::Checked);
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void MainWindow::on_ShowTrayIcon_checkStateChanged(Qt::CheckState state) {
#else
void MainWindow::on_ShowTrayIcon_stateChanged(int state) {
#endif
	if (state == Qt::Unchecked) {
		Tray->hide();
	} else {
		Tray->show();
	}
}
#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void MainWindow::on_TaskbarProgress_checkStateChanged(Qt::CheckState state) {
#else
void MainWindow::on_TaskbarProgress_stateChanged(int state) {
#endif
	if (state == Qt::Unchecked) {
		Task->EndProgress();
	} else {
		if (Execution.state() != QProcess::Running) return;
		Task->BeginProgress(MainProgress.TotalTime.msecsSinceStartOfDay() / 1000);
	}
}

void MainWindow::on_UpdateCompareSmall_clicked() {
	QGridLayout *layout = ui->scrollAreaWidgetContents_4->findChild<QGridLayout *>(QString(), Qt::FindDirectChildrenOnly);
	if (layout != nullptr) {
		QList<QWidget *> List = ui->scrollAreaWidgetContents_4->findChildren<QWidget *>("");
		for (int i = 0; i < List.size(); ++i) {
			layout->removeWidget(List.at(i));
			List.at(i)->deleteLater();
		}
	} else {
		layout = new QGridLayout(ui->scrollAreaWidgetContents_4);
	}

	for (int i = CompareScreens.size() - 1; i >= 0 ; i--) {
		list_compare_screens_t element = CompareScreens.at(i);
		element.Label->deleteLater();
		delete element.Pixmap;
		CompareScreens.removeAt(i);
	}

	CurrentCompareScreen = -1;

	QLabel *NormalLabel = new QLabel("Normal", ui->scrollAreaWidgetContents_4);
	NormalLabel->setAlignment(Qt::AlignHCenter);
	NormalLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	QLabel *SmallLabel = new QLabel("Small", ui->scrollAreaWidgetContents_4);
	SmallLabel->setAlignment(Qt::AlignHCenter);
	SmallLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	layout->addWidget(NormalLabel, 0, 0);
	layout->addWidget(SmallLabel, 0, 1);

	QPixmap Noise(":/Noise.jpg");

	uint64_t SBetweenFrames = (ui->VideoLength->time().msecsSinceStartOfDay() / 1000) / ui->CompPreviews->value();

	for (int i = 0; i < ui->CompPreviews->value(); ++i) {
		QLabel *NewNormalLabel = new QLabel(ui->scrollAreaWidgetContents_4);
		NewNormalLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		QLabel *NewSmallLabel = new QLabel(ui->scrollAreaWidgetContents_4);
		NewSmallLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

		NewNormalLabel->setPixmap(Noise.scaled(ui->CompSizeW->value(), ui->CompSizeH->value(), Qt::IgnoreAspectRatio));
		NewSmallLabel->setPixmap(Noise.scaled(ui->CompSizeW->value(), ui->CompSizeH->value(), Qt::IgnoreAspectRatio));

		layout->addWidget(NewNormalLabel, (i + 1), 0);
		layout->addWidget(NewSmallLabel, (i + 1), 1);

		uint64_t Time = 30 + (i * SBetweenFrames);

		CompareScreens.append({NewNormalLabel, new QPixmap(), Time, InputFile, InputFilenameHash, false, false});
		CompareScreens.append({NewSmallLabel, new QPixmap(), Time, SmallInputFile, SmallInputFilenameHash, false, false});
	}

	if (DiskCache) {
		for (int i = 0; i < CompareScreens.size(); ++i) {
			list_compare_screens_t screen = CompareScreens.at(i);
			QString filename = ComposeCacheFilename(screen.CacheFilename, screen.Time * 1000);
			if (QFile::exists(filename)) {
				QFile file(filename);
				if (file.open(QFile::ReadOnly)) {
					QByteArray Raw = file.readAll();
					screen.Pixmap->loadFromData(Raw);
					screen.Label->setPixmap(ProcessCompareFrame(screen.Pixmap));
					CompareScreens[i].Finished = true;
				}
			}
		}
	}

	CompareProcessTerminate(0, QProcess::NormalExit);
}

void MainWindow::CompareProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus) {

	if (exitCode != 0) {
		ShowProcessError(&CompareSmallProcess);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;

	if (CurrentCompareScreen >= 0) {
		list_compare_screens_t screen = CompareScreens.at(CurrentCompareScreen);
		if (DiskCache) {
			QString filename = ComposeCacheFilename(screen.CacheFilename, screen.Time * 1000);
			if (!QFile::exists(filename)) {
				QFile file(filename);
				if (file.open(QFile::WriteOnly)) {
					file.write(CompareRawData);
					file.close();
				}
			}
		}
		screen.Pixmap->loadFromData(CompareRawData);
		screen.Label->setPixmap(ProcessCompareFrame(screen.Pixmap));

		CompareRawData.clear();

		CompareScreens[CurrentCompareScreen].InProgress = false;
		CompareScreens[CurrentCompareScreen].Finished = true;
		CurrentCompareScreen = -1;

	}

	for (int i = 0; i < CompareScreens.size(); ++i) {
		list_compare_screens_t screen = CompareScreens.at(i);
		if (screen.Finished || screen.InProgress) continue;
		CurrentCompareScreen = i;
		if (DiskCache) {
			QString filename = ComposeCacheFilename(screen.CacheFilename, screen.Time * 1000);
			if (QFile::exists(filename)) {
				QFile file(filename);
				if (file.open(QFile::ReadOnly)) {
					QByteArray Raw = file.readAll();
					screen.Pixmap->loadFromData(Raw);
					screen.Label->setPixmap(ProcessCompareFrame(screen.Pixmap));
					CompareScreens[i].Finished = true;
					continue;
				}
			}
		}
		break;
	}

	if (CurrentCompareScreen >= 0) {
		list_compare_screens_t screen = CompareScreens.at(CurrentCompareScreen);
		QStringList params = RawFfmpegParams(screen.Filename, "", screen.Time * 1000);
		CompareSmallProcess.start(ui->FfmpegBin->text(), params);
		CompareSmallProcess.waitForStarted(-1);
	}
}

void MainWindow::ReadCompareData() {
	CompareRawData.append(CompareSmallProcess.readAllStandardOutput());
}

void MainWindow::RefreshPreviews() {
	RefreshRawImage();
	RefreshCameraImage();
}

QPixmap MainWindow::ProcessCompareFrame(QPixmap *input) {
	QPixmap Cut;
	if (ui->modeComboBox->currentText() == "sbs")
		Cut = input->copy(0, 0, input->width() / 2, input->height());
	else if (ui->modeComboBox->currentText() == "tb")
		Cut = input->copy(0, 0, input->width(), input->height() / 2);
	else
		Cut = *input;
	return Cut.scaled(ui->CompSizeW->value(), ui->CompSizeH->value(), Qt::KeepAspectRatio);
}

QString MainWindow::ExportMetadataFile(QString OutputFile) {
	if (!ui->GenerateChapters->isChecked()) return "";

	QList<tableline_t> Lines;
	for (int i = 0; i < ui->Table->rowCount(); ++i) {
		tableline_t Line = ReadRow(i);
		if (Line.Valid) {
			Lines.append(Line);
		}
	}

	tableline_t *CurrentLine;
	tableline_t *PrevLine;
	QString Output = ";FFMETADATA1";

	int StartTime = -1;
	int EndTime = -1;
	QString Text = "";
	int ChaptersCount = 0;

	for (int i = 0; i < Lines.count(); ++i) {
		CurrentLine = &Lines[i];

		if (i != 0) {
			PrevLine = &Lines[i - 1];
		} else {
			PrevLine = &DefaultTableLine;
		}

		if (Text == "" && CurrentLine->Text != "") {
			Text = CurrentLine->Text;
			StartTime = CurrentLine->Time + CurrentLine->Transition;
			continue;
		}

		if ((CurrentLine->Text != "" && CurrentLine->Text != PrevLine->Text) || i == Lines.count() - 1) {
			EndTime = CurrentLine->Time - 1;
			Output.append("\n\n[CHAPTER]\n"
						  "TIMEBASE=1/1000\n"
						  "START=" + QString::number(StartTime) + "\n"
						  "END=" + QString::number(EndTime) + "\n"
						  "title=" + Text
						 );
			StartTime = CurrentLine->Time + CurrentLine->Transition;
			Text = CurrentLine->Text;
			ChaptersCount++;
		}
	}

	if (ChaptersCount < 2) return "";
	if (OutputFile == "")
		OutputFile = QDir::toNativeSeparators(Tmp.filePath("Chapters.txt"));
	WriteFile(OutputFile, Output);
	return OutputFile;
}

void MainWindow::on_TabsSpliter_splitterMoved(int /*pos*/, int /*index*/) {
	RefreshPreviews();
}

void MainWindow::on_PreviewSpliter_splitterMoved(int /*pos*/, int /*index*/) {
	RefreshPreviews();
}

void MainWindow::on_IPStart_clicked() {
	if (Execution.state() != QProcess::NotRunning) return;
	ui->IPText->clear();
	FfmpegEnableBlackFrames = ui->IPDetectBlack->isChecked();
	GenerateSmallFile = ui->IPSmallFile->isChecked();

	QString FFmpeg = ui->FfmpegBin->text();
	if (FFmpeg.contains(" ")) FFmpeg = "\"" + FFmpeg + "\"";

	QString Command = FFmpeg + " -hide_banner -loglevel info -stats -y";
	QString InputParam = "-i \"" + SmallInputFile + "\"";
	QString Map = "-map 0:v:0";
	if (ui->IPSmallFile->isChecked() && ui->IPAudioFormat->currentText() != "None") Map += " -map 0:a";

	QString HWDecode = "";
	if (ui->IPHWDec->currentText() == "CUDA") {
		HWDecode = "-hwaccel cuda";
	} else if (ui->IPHWDec->currentText() == "VAAPI (linux)") {
		HWDecode = "-hwaccel vaapi";
		HWDecode += " -vaapi_device " + GetVAAPIDevice();
	} else if (ui->IPHWDec->currentText() == "DXVA2 (windows)") {
		HWDecode = "-hwaccel dxva2";
	} else if (ui->IPHWDec->currentText() == "D3D11 (windows)") {
		HWDecode = "-hwaccel d3d11va";
	}

	QString FormatVideo = "";
	QString InitHWEncode = "";
	QString FormatAudio = "";
	QString Output = "";
	QString BlackFramesFilter = "";
	QStringList Filters;
	QString Scale = "scale=" + QString::number(ui->IPW->value()) + ":" +
					QString::number(ui->IPH->value()) + ":flags=fast_bilinear";

	if (ui->IPDetectBlack->isChecked()) {
		BlackFramesFilter = "blackframe=amount=" + QString::number(ui->IPBlackAmount->value()) +
							":threshold=" + QString::number(ui->IPBlackThreshold->value());
	}

	if (ui->IPSmallFile->isChecked()) {
		if (ui->IPVideoFormat->currentText() == "x265") {
			FormatVideo = "-c:v libx265 -preset " + ui->IPPreset->currentText() +
						  " -x265-params log-level=warning -crf " + QString::number(ui->IPCRF->value());
			Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
		} else if (ui->IPVideoFormat->currentText() == "x264") {
			FormatVideo = "-c:v libx264 -preset " + ui->IPPreset->currentText() +
						  " -crf " + QString::number(ui->IPCRF->value());
			Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
		} else if (ui->IPVideoFormat->currentText() == "SVT-AV1") {
			FormatVideo = "-c:v libsvtav1 -crf " + QString::number(ui->IPCRF->value());
			QString Preset = AV1Presets(ui->IPPreset->currentText());
			if (Preset != "") FormatVideo += " -preset " + Preset;
			Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
		} else if (ui->IPVideoFormat->currentText() == "Xvid") {
			FormatVideo = "-c:v libxvid -qscale:v " + QString::number(ui->IPCRF->value());
			Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
		} else if (ui->IPVideoFormat->currentText() == "VAAPI H264 (linux)") {
			InitHWEncode = "-vaapi_device " + GetVAAPIDevice();
			FormatVideo = "-c:v h264_vaapi -qp " + QString::number(ui->IPCRF->value());
			if (!ui->IPHWScale->isChecked()) Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
			Filters.append("format=nv12");
			Filters.append("hwupload");
			if (ui->IPHWScale->isChecked())
				Filters.append("scale_vaapi=w=" + QString::number(ui->IPW->value()) + ":h=" + QString::number(ui->IPH->value()));
		} else if (ui->IPVideoFormat->currentText() == "VAAPI H265 (linux)") {
			InitHWEncode = "-vaapi_device " + GetVAAPIDevice();
			FormatVideo = "-c:v hevc_vaapi -qp " + QString::number(ui->IPCRF->value());
			if (!ui->IPHWScale->isChecked()) Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
			Filters.append("format=nv12");
			Filters.append("hwupload");
			if (ui->IPHWScale->isChecked())
				Filters.append("scale_vaapi=w=" + QString::number(ui->IPW->value()) + ":h=" + QString::number(ui->IPH->value()));
		} else if (ui->IPVideoFormat->currentText() == "NVENC H264") {
			InitHWEncode = "-hwaccel_output_format cuda";
			FormatVideo = "-c:v h264_nvenc -preset " + ui->IPPreset->currentText() +
						  " -cq " + QString::number(ui->IPCRF->value()) + " -profile:v high";
			if (!ui->IPHWScale->isChecked()) Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
			Filters.append("hwupload_cuda");
			if (ui->IPHWScale->isChecked())
				Filters.append("scale_cuda=" + QString::number(ui->IPW->value()) + ":" +
							   QString::number(ui->IPH->value()) + ":interp_algo=lanczos");
		} else if (ui->IPVideoFormat->currentText() == "NVENC H265") {
			InitHWEncode = "-hwaccel_output_format cuda";
			FormatVideo = "-c:v hevc_nvenc -preset " + ui->IPPreset->currentText() +
						  " -cq " + QString::number(ui->IPCRF->value());
			if (!ui->IPHWScale->isChecked()) Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
			Filters.append("hwupload_cuda");
			if (ui->IPHWScale->isChecked())
				Filters.append("scale_cuda=" + QString::number(ui->IPW->value()) + ":" +
							   QString::number(ui->IPH->value()) + ":interp_algo=lanczos");
		} else if (ui->IPVideoFormat->currentText() == "Intel QSV H264") {
			FormatVideo = "-c:v h264_qsv -global_quality " + QString::number(ui->IPCRF->value()) + " -preset " +
						  ui->IPPreset->currentText();
			if (!ui->IPHWScale->isChecked()) Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
			Filters.append("hwupload=extra_hw_frames=64");
			Filters.append("format=qsv");
			if (ui->IPHWScale->isChecked())
				Filters.append("scale_qsv=w=" + QString::number(ui->IPW->value()) + ":h=" + QString::number(ui->IPH->value()));
		} else if (ui->IPVideoFormat->currentText() == "Intel QSV H265") {
			FormatVideo = "-c:v hevc_qsv -global_quality " + QString::number(ui->IPCRF->value()) + " -preset " +
						  ui->IPPreset->currentText();
			if (!ui->IPHWScale->isChecked()) Filters.append(Scale);
			if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
			Filters.append("hwupload=extra_hw_frames=64");
			Filters.append("format=qsv");
			if (ui->IPHWScale->isChecked())
				Filters.append("scale_qsv=w=" + QString::number(ui->IPW->value()) + ":h=" + QString::number(ui->IPH->value()));
		}

		if (ui->IPAudioFormat->currentText() == "Copy") {
			FormatAudio = "-c:a copy";
		} else if (ui->IPAudioFormat->currentText() == "MP3") {
			FormatAudio = "-c:a libmp3lame -b:a " + QString::number(ui->IPAudioBitrate->value());
		} else if (ui->IPAudioFormat->currentText() == "AAC") {
			FormatAudio = "-c:a aac -b:a " + QString::number(ui->IPAudioBitrate->value());
		} else if (ui->IPAudioFormat->currentText() == "Vorbis") {
			FormatAudio = "-c:a libvorbis -b:a " + QString::number(ui->IPAudioBitrate->value());
		} else if (ui->IPAudioFormat->currentText() == "Opus") {
			FormatAudio = "-c:a libopus -b:a " + QString::number(ui->IPAudioBitrate->value());
		}

		if (ui->IPOutputFile->text() == "") {
			ui->IPOutputFile->setText(QDir::toNativeSeparators(InputFileDir + QDir::separator() +
									  InputFilename + ".SmallFile." + InputFileExt));
		}
		if (ui->IPOutputFile->text().toLower().endsWith(".mp4")) {
			Output = "-movflags faststart ";
		}

		Output += "\"" + ui->IPOutputFile->text() + "\"";

	} else {
		if (BlackFramesFilter != "")Filters.append(BlackFramesFilter);
		Output = "-f null -";
	}

	Command += " " + HWDecode;
	if (InitHWEncode != "") Command += " " + InitHWEncode;
	Command += " " + InputParam + " " + Map + " -vf " + Filters.join(",") + " " + FormatVideo;
	if (FormatAudio != "") Command += " " + FormatAudio;
	Command += " " + Output;

	if (Debug) ui->DebugLog->appendPlainText(Command);

	ProcessParams = QProcess::splitCommand(Command);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();
	MainProgress.TotalTime = ui->VideoLength->time();
	PreviewFile = "";
	ProcessIsPreview = false;
	ExecuteProcess();
}

void MainWindow::on_IPSelectOutFile_clicked() {
	QString DefaultType = "MP4 File (*.mp4) ;; MKV File (*.mkv)";
	if (InputFileExt == "mkv") DefaultType = "MKV File (*.mkv) ;; MP4 File (*.mp4)";

	QString fileName = QFileDialog::getSaveFileName(this, "New small file",
					   QDir::toNativeSeparators(InputFileDir + QDir::separator() + InputFilename + ".SmallFile." + InputFileExt),
					   DefaultType);
	if (fileName != "") ui->IPOutputFile->setText(QDir::toNativeSeparators(fileName));
}

void MainWindow::AddBlackTransition(QTime Time) {
	bool TimeExists = false;
	for (int i = 0; i < ui->Table->rowCount(); ++i) {
		QTableWidgetItem *item = ReadItem(i, TIME_COLUMN);
		if (item == nullptr) continue;
		QTime SearchTime = QTime::fromString(item->text(), TIME_FORMAT);
		if (SearchTime.msecsSinceStartOfDay() != Time.msecsSinceStartOfDay()) continue;
		TimeExists = true;
		break;
	}
	if (!TimeExists) {
		AddLine(Time,
				0,
				0,
				0,
				0,
				100,
				false,
				0,
				0,
				"");
		if (DiskCache) {
			if (!DiskCacheQueue.contains(Time))
				DiskCacheQueue.prepend(Time);
			OndemandCacheTimer.setSingleShot(true);
			OndemandCacheTimer.start(500);
		}
	}
}

void MainWindow::on_IPAddTrans_clicked() {
	ui->RightTabs->setCurrentIndex(RIGHT_TAB_MOVEMENTS);
	QString TextInput = ui->IPText->toPlainText();
	QRegularExpressionMatchIterator GMatch = BlackFrames.globalMatch(TextInput);

	int CurrentMaxBlack = 0;
	QList<QTime> CurrentTimeList;
	int AntFrame = 0;

	int End = ui->VideoLength->time().msecsSinceStartOfDay() - (ui->IPExcludeLast->value() * 1000);

	while (GMatch.hasNext()) {
		QRegularExpressionMatch Match = GMatch.next();

		int Frame = Match.captured(1).toInt();
		int PBlack = Match.captured(2).toInt();
		//		int PTS = Match.captured(3).toInt();
		double RawTime = Match.captured(4).toDouble();
		//		QString Type = Match.captured(5);
		//		int LastKey = Match.captured(6).toInt();
		if (RawTime < ui->IPExcludeFirst->value()) continue;
		QTime Time = QTime::fromMSecsSinceStartOfDay(RawTime * 1000);
		if (Time.msecsSinceStartOfDay() > End) continue;

		if (AntFrame != Frame - 1) {
			if (!CurrentTimeList.isEmpty()) {
				int MiddlePos = (CurrentTimeList.count() / 2);
				QTime MiddleTime = CurrentTimeList.at(MiddlePos);
				AddBlackTransition(MiddleTime);
				CurrentTimeList.clear();
			}
			CurrentMaxBlack = 0;
		}

		if (PBlack > CurrentMaxBlack) {
			CurrentTimeList.clear();
			CurrentMaxBlack = PBlack;
		}

		if (PBlack == CurrentMaxBlack) {
			CurrentTimeList.append(Time);
		}

		AntFrame = Frame;
	}
	if (!CurrentTimeList.isEmpty()) {
		int MiddlePos = (CurrentTimeList.count() / 2);
		QTime MiddleTime = CurrentTimeList.at(MiddlePos);
		AddBlackTransition(MiddleTime);
	}
	on_Sort_clicked();
}
#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void MainWindow::on_AutoSaveMovement_checkStateChanged(Qt::CheckState state) {
#else
void MainWindow::on_AutoSaveMovement_stateChanged(int state) {
#endif
	if (state == Qt::Checked) {
		Autosave.setInterval(60000);
		Autosave.start();
	} else {
		Autosave.stop();
	}
}

void MainWindow::LoadFiltersFields(QString Filters) {
	exposure_t EX(Filters);
	colortemperature_t CT(Filters);
	eq_t EQ(Filters);
	colorbalance_t CB(Filters);
	LoadExposure(EX);
	LoadColorTemperature(CT);
	LoadEQ(EQ);
	LoadColorBalance(CB);
}

void MainWindow::LoadExposure(exposure_t &EX) {
	ui->Exposure->setValue(EX.exposure);
	ui->ExposureBlack->setValue(EX.black);
}

void MainWindow::LoadColorTemperature(colortemperature_t &CT) {
	ui->Temperature->setValue(CT.temperature);
	ui->Mix->setValue(CT.mix);
	ui->Pl->setValue(CT.pl);
}

void MainWindow::LoadEQ(eq_t &EQ) {
	ui->Contrast->setValue(EQ.contrast);
	ui->Brightness->setValue(EQ.brightness);
	ui->Saturation->setValue(EQ.saturation);
	ui->Gamma->setValue(EQ.gamma);
	ui->GammaRed->setValue(EQ.gamma_r);
	ui->GammaGreen->setValue(EQ.gamma_g);
	ui->GammaBlue->setValue(EQ.gamma_b);
	ui->GammaWeight->setValue(EQ.gamma_weight);
}

void MainWindow::LoadColorBalance(colorbalance_t &CB) {
	ui->rs->setValue(CB.rs);
	ui->gs->setValue(CB.gs);
	ui->bs->setValue(CB.bs);
	ui->rm->setValue(CB.rm);
	ui->gm->setValue(CB.gm);
	ui->bm->setValue(CB.bm);
	ui->rh->setValue(CB.rh);
	ui->gh->setValue(CB.gh);
	ui->bh->setValue(CB.bh);
	ui->pl->setChecked(CB.pl);
}

void MainWindow::on_FiltersButton_clicked() {
	int selectedrow = GetSelectedRow();
	if (selectedrow == -1 ) return;
	ui->RightTabs->setCurrentIndex(RIGHT_TAB_FILTERS);

	QString Time = ReadItemValue(selectedrow, TIME_COLUMN);

	int newrow = ui->FiltersList->rowCount();
	ui->FiltersList->blockSignals(true);
	ui->FiltersList->insertRow(newrow);
	ui->FiltersList->setItem(newrow, FILTERS_TIME_COLUMN, new QTableWidgetItem(Time));
	ui->FiltersList->setItem(newrow, FILTERS_FILTER_COLUMN, new QTableWidgetItem(GenerateFiltersString()));
	ui->FiltersList->selectRow(newrow);
	ui->FiltersList->blockSignals(false);
	Changes();
}

void MainWindow::on_FiltersList_itemSelectionChanged() {
	if (!ui->RawFrame->isVisible()) return;
	BlockRefresh = true;

	bool needrawupdate = false;
	int selected = GetSelectedRow(ui->FiltersList);

	LoadFiltersFields(ReadItemValue(ui->FiltersList, selected, FILTERS_FILTER_COLUMN));

	BlockRefresh = false;

	if (ui->FiltersList->isVisible()) {
		BlockRefresh = true;
		QTime time = QTime::fromString(ReadItemValue(ui->FiltersList, selected, FILTERS_TIME_COLUMN), TIME_FORMAT);
		if (time != ui->RawTime->time()) {
			needrawupdate = true;
			ui->RawTime->setTime(time);
		}
		BlockRefresh = false;

		int TableRow = LocateRow(ui->Table, TIME_COLUMN, time);
		if (TableRow >= 0) ui->Table->selectRow(TableRow);

		if (needrawupdate) {
			ReloadRawImage(ui->RawTime->time());
			return;
		}

		ReloadFilterImageForced();
	}
}

QString MainWindow::GenerateDefaultFilters() {
	QStringList Filters;

	if (ui->FilterExposure->isChecked()) {
		exposure_t EX;
		Filters.append(EX.toString());
	}

	if (ui->FilterColorTemperature->isChecked()) {
		colortemperature_t CT;
		Filters.append(CT.toString());
	}

	if (ui->FilterEQ->isChecked()) {
		eq_t EQ;
		Filters.append(EQ.toString());
	}

	if (ui->FilterColorBalance->isChecked()) {
		colorbalance_t CB;
		Filters.append(CB.toString());
	}

	return Filters.join(",");
}

QString MainWindow::SearchFiltersString(QTime Time, bool CurrentCamera) {
	int FiltersRow = LocateRow(ui->FiltersList, FILTERS_TIME_COLUMN, Time, CurrentCamera);
	QString Filters;
	if (FiltersRow >= 0) {
		Filters = ReadItemValue(ui->FiltersList, FiltersRow, FILTERS_FILTER_COLUMN);
		QStringList FilterList;
		if (ui->FilterExposure->isChecked()) {
			exposure_t Exposure(Filters);
			FilterList.append(Exposure.toString());
		}
		if (ui->FilterColorTemperature->isChecked()) {
			colortemperature_t CT(Filters);
			FilterList.append(CT.toString());
		}
		if (ui->FilterEQ->isChecked()) {
			eq_t EQ(Filters);
			FilterList.append(EQ.toString());
		}
		if (ui->FilterColorBalance->isChecked()) {
			colorbalance_t CB(Filters);
			FilterList.append(CB.toString());
		}
		Filters = FilterList.join(",");
	} else {
		Filters = GenerateDefaultFilters();
	}
	return Filters;
}

QString MainWindow::GenerateFiltersString() {
	QStringList Filters;

	if (ui->FilterExposure->isChecked()) {
		exposure_t Exposure;
		Exposure.exposure = ui->Exposure->value();
		Exposure.black = ui->ExposureBlack->value();
		Filters.append(Exposure.toString());
	}

	if (ui->FilterColorTemperature->isChecked()) {
		colortemperature_t ColorTemp;
		ColorTemp.temperature = ui->Temperature->value();
		ColorTemp.mix = ui->Mix->value();
		ColorTemp.pl = ui->Pl->value();
		Filters.append(ColorTemp.toString());
	}

	if (ui->FilterEQ->isChecked()) {
		eq_t EQ;
		EQ.contrast = ui->Contrast->value();
		EQ.brightness = ui->Brightness->value();
		EQ.saturation = ui->Saturation->value();
		EQ.gamma = ui->Gamma->value();
		EQ.gamma_r = ui->GammaRed->value();
		EQ.gamma_g = ui->GammaGreen->value();
		EQ.gamma_b = ui->GammaBlue->value();
		EQ.gamma_weight = ui->GammaWeight->value();
		Filters.append(EQ.toString());
	}

	if (ui->FilterColorBalance->isChecked()) {
		colorbalance_t ColorBalance;
		ColorBalance.rs = ui->rs->value();
		ColorBalance.gs = ui->gs->value();
		ColorBalance.bs = ui->bs->value();
		ColorBalance.rm = ui->rm->value();
		ColorBalance.gm = ui->gm->value();
		ColorBalance.bm = ui->bm->value();
		ColorBalance.rh = ui->rh->value();
		ColorBalance.gh = ui->gh->value();
		ColorBalance.bh = ui->bh->value();
		ColorBalance.pl = ui->pl->isChecked();
		Filters.append(ColorBalance.toString());
	}

	QString Result = "";
	if (!Filters.empty()) {
		Result = Filters.join(",");
	}
	return Result;
}

void MainWindow::UpdateFiltersCell() {
	int SelectedRow = GetSelectedRow(ui->FiltersList);
	if (SelectedRow == -1) return;
	if (BlockRefresh) return;
	ui->FiltersList->setItem(SelectedRow, FILTERS_FILTER_COLUMN, new QTableWidgetItem(GenerateFiltersString()));
	Changes();
}

void MainWindow::on_FilterRemove_clicked() {
	int selectedrow = GetSelectedRow(ui->FiltersList);
	if (selectedrow != -1 ) {
		ui->FiltersList->blockSignals(true);
		ui->FiltersList->removeRow(selectedrow);
		ui->FiltersList->blockSignals(false);
		Changes();
	}
}

void MainWindow::VisibleAllChildrens(QWidget *Parent, bool Visible) {
	QList<QWidget *> Childrens = Parent->findChildren<QWidget *>();
	for (int i = 0; i < Childrens.count(); ++i) {
		Childrens.at(i)->setVisible(Visible);
	}
}

void MainWindow::CheckFilterWarnings() {
	if (ui->InputPixelFormat->text().contains("10le") &&
		ui->FilterEQ->isChecked())
		ui->FiltersWarning->setVisible(true);
	else
		ui->FiltersWarning->setVisible(false);
}

void MainWindow::on_FilterExposure_toggled(bool arg1) {
	VisibleAllChildrens(ui->FilterExposure, arg1);
	CheckFilterWarnings();
	Changes();
}

void MainWindow::on_FilterColorTemperature_toggled(bool arg1) {
	VisibleAllChildrens(ui->FilterColorTemperature, arg1);
	CheckFilterWarnings();
	Changes();
}

void MainWindow::on_FilterEQ_toggled(bool arg1) {
	VisibleAllChildrens(ui->FilterEQ, arg1);
	CheckFilterWarnings();
	Changes();
}

void MainWindow::on_FilterColorBalance_toggled(bool arg1) {
	VisibleAllChildrens(ui->FilterColorBalance, arg1);
	CheckFilterWarnings();
	Changes();
}

void MainWindow::on_MaxFov_valueChanged(int arg1) {
	ui->Fov->setMaximum(arg1);
}

void MainWindow::on_MinFov_valueChanged(int arg1) {
	ui->Fov->setMinimum(arg1);
}

void MainWindow::ChangeInputPad() {
	if (ui->InputPadW->value() > 0 ||
		ui->InputPadH->value() > 0 ||
		ui->InputPadX->value() > 0 ||
		ui->InputPadY->value() > 0 ) {
		ui->SmallInputFile->setText("");
		ui->SmallInputFile->setEnabled(false);
		GUIPreviewScale = "None";
		ui->GUIPreviewScale->setEnabled(false);
	} else {
		ui->SmallInputFile->setEnabled(true);
		ui->GUIPreviewScale->setEnabled(true);
		GUIPreviewScale = ui->GUIPreviewScale->currentText();
	}
}

void MainWindow::on_GUIPreviewScale_currentTextChanged(const QString &arg1) {
	if (ui->InputPadW->value() > 0 ||
		ui->InputPadH->value() > 0 ||
		ui->InputPadX->value() > 0 ||
		ui->InputPadY->value() > 0 ) {
		GUIPreviewScale = "None";
	} else {
		GUIPreviewScale = arg1;
	}
}

void MainWindow::CalculateOutputResolution() {
	if (ui->InputResolutionW->value() == 0) return;

	int InputW = ui->InputResolutionW->value();
	if (ui->modeComboBox->currentText() == "sbs") InputW = InputW / 2;

	int InputHorizontalFov = ui->HFovSpinBox->value();
	int HorizontalFov = 90;

	if (ui->Table->rowCount() > 1) {
		double med = 0;
		int TotalSeconds = 0;
		int FovCount = 0;
		for (int i = 0; i < ui->Table->rowCount(); ++i) {
			QString RowTimeValue = ReadItemValue(i, TIME_COLUMN);
			QTime RowTime = QTime::fromString(RowTimeValue, TIME_FORMAT);
			int seconds = RowTime.msecsSinceStartOfDay() / 1000;
			if (seconds == 0) continue;

			double CurrentFov = ReadItemValue(i, FOV_COLUMN, "0").toDouble();
			if (CurrentFov <= 0) continue;

			TotalSeconds += seconds;
			FovCount += CurrentFov * seconds;
		}
		med = (double)FovCount / (double)TotalSeconds;

		HorizontalFov = std::round(CalcHFOV(16, 9, med));
	}

	int ResW, ResH;

	ResW = (InputW * HorizontalFov) / InputHorizontalFov;

	if (ResW < 700) {
		ResW = 640; ResH = 360;
	} else if (ResW >= 700 && ResW < 900) {
		ResW = 854; ResH = 480;
	} else if (ResW >= 900 && ResW < 1000) {
		ResW = 960; ResH = 540;
	} else if (ResW >= 1000 && ResW < 1340) {
		ResW = 1280; ResH = 720;
	} else if (ResW >= 1340 && ResW < 2000) {
		ResW = 1920; ResH = 1080;
	} else if (ResW >= 2000 && ResW < 2660) {
		ResW = 2560; ResH = 1440;
	} else if (ResW >= 2660 && ResW < 3320) {
		ResW = 3200; ResH = 1800;
	} else if (ResW >= 3320) {
		ResW = 3840; ResH = 2160;
	} else {
		ResW = 1920; ResH = 1080;
	}

	ui->OutW->setValue(ResW);
	ui->OutH->setValue(ResH);
}

void MainWindow::CalculateOutputPXFormat() {
	QString InputPixelFormat = ui->InputPixelFormat->text();
	QString OutputPixelFormat = "";

	if (ui->FilterEQ->isChecked()) {
		if (InputPixelFormat == "yuv420p10le")
			OutputPixelFormat = "yuv420p";
		else if (InputPixelFormat == "yuv422p10le")
			OutputPixelFormat = "yuv422p";
		else if (InputPixelFormat == "yuv444p10le")
			OutputPixelFormat = "yuv444p";
		else
			OutputPixelFormat = InputPixelFormat;
	} else if (ui->FilterExposure->isChecked() ||
			   ui->FilterColorTemperature->isChecked() ||
			   ui->FilterColorBalance->isChecked()) {
		OutputPixelFormat = InputPixelFormat;
	}

	if (OutputPixelFormat != "")
		ui->OutPXFormat->setCurrentText(OutputPixelFormat);
}

void MainWindow::Changes() {
	if (!PendingChanges) {
		PendingChanges = true;
		setWindowTitle(WindowTitleVersion + " * (unsaved)");
	}
}

void MainWindow::on_ExposureDefaults_clicked() {
	exposure_t EX;
	LoadExposure(EX);
}

void MainWindow::on_ColorTemperatureDefaults_clicked() {
	colortemperature_t CT;
	LoadColorTemperature(CT);
}

void MainWindow::on_EQDefaults_clicked() {
	eq_t EQ;
	LoadEQ(EQ);
}

void MainWindow::on_ColorBalanceDefaults_clicked() {
	colorbalance_t CB;
	LoadColorBalance(CB);
}

void MainWindow::on_PYSteps_valueChanged(double arg1) {
	ui->Pitch->setSingleStep(arg1);
	ui->Yaw->setSingleStep(arg1);
}

void MainWindow::on_RollSteps_valueChanged(double arg1) {
	ui->Roll->setSingleStep(arg1);
}

void MainWindow::on_FOVSteps_valueChanged(double arg1) {
	ui->Fov->setSingleStep(arg1);
}

void MainWindow::on_TransSteps_valueChanged(double arg1) {
	ui->Trans->setSingleStep(arg1);
}

void MainWindow::on_SplitGenerateTable_clicked() {
	if (ProcessChunks.state() != QProcess::NotRunning) return;
	if (GenerateSplits->isRunning()) return;

	if (!FileOk) {
		if (!CheckInputFiles()) return;
	}

	on_SplitConvert_toggled(ui->SplitConvert->isChecked());

	if (ui->SplitConvert->isChecked()) {
		if (ui->OutW->value() == 0 || ui->OutH->value() == 0) CalculateOutputResolution();
		if (ui->OutPXFormat->currentText() == "Auto") CalculateOutputPXFormat();
	}

	CleanTable(ui->SplitTable);
	SplitCount = 0;

	if (ui->SplitTMPDir->text() == "") {
		QDir Parent(InputFileDir);
		Parent.mkdir("VR2NormalSplitDir");
		ui->SplitTMPDir->setText(QDir::toNativeSeparators(InputFileDir + QDir::separator() + "VR2NormalSplitDir"));
	}

	SplitSaveFormat();

	ui->CMDFile->setText(QDir::toNativeSeparators(ui->SplitTMPDir->text() + QDir::separator() + "CMDFile.cmd"));
	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	QString Cache = "";
	if (DiskCache) Cache = ui->DiskCacheDir->text();

	int Count = GenerateSplits->SetOptions(ui->FfmpegBin->text(),
										   ui->FfprobeBin->text(),
										   Cache,
										   SplitInputFile,
										   SplitInputHash,
										   ui->VideoLength->time().msecsSinceStartOfDay(),
										   ui->FPSSpinBox->value(),
										   ui->SplitSeconds->value(),
										   Tmp.path(),
										   Debug,
										   ui->ErrorColor->text());

	ui->SplitCurrentProgress->setValue(0);
	ui->SplitCurrentProgress->setMaximum(Count);
	ui->SplitCurrentState->setText("Generating split points (0/" + QString::number(Count) + ")");
	ui->SplitCurrentWidget->setVisible(true);

	GenerateSplits->start();
}

int MainWindow::InsertSplitTableLine(QString Time, QString Before, QString After, QString AdjustedBefore,
									 QString AdjustedAfter, QString File, QString State) {
	int newrow = ui->SplitTable->rowCount();
	ui->SplitTable->blockSignals(true);
	ui->SplitTable->insertRow(newrow);
	ui->SplitTable->setItem(newrow, SPLIT_STATE_COLUMN, new QTableWidgetItem(State));
	ui->SplitTable->setItem(newrow, SPLIT_TIME_COLUMN, new QTableWidgetItem(Time));
	ui->SplitTable->setItem(newrow, SPLIT_BEFORE_COLUMN, new QTableWidgetItem(Before));
	ui->SplitTable->setItem(newrow, SPLIT_AFTER_COLUMN, new QTableWidgetItem(After));
	ui->SplitTable->setItem(newrow, SPLIT_ADJUST_BEFORE_COLUMN, new QTableWidgetItem(AdjustedBefore));
	ui->SplitTable->setItem(newrow, SPLIT_ADJUST_AFTER_COLUMN, new QTableWidgetItem(AdjustedAfter));
	ui->SplitTable->setItem(newrow, SPLIT_FILE_COLUMN, new QTableWidgetItem(File));
	ui->SplitTable->blockSignals(false);
	Changes();
	return newrow;
}

void MainWindow::UpdateSplitTableFile(int row) {
	QString Start = ReadItemValue(ui->SplitTable, row, SPLIT_ADJUST_AFTER_COLUMN, "");
	QString StartTime = ReadItemValue(ui->SplitTable, row, SPLIT_TIME_COLUMN, "");
	QString End = "";
	QString EndTime = "";
	if (row + 1 < ui->SplitTable->rowCount()) {
		End = ReadItemValue(ui->SplitTable, row + 1, SPLIT_ADJUST_BEFORE_COLUMN, "");
		EndTime = ReadItemValue(ui->SplitTable, row + 1, SPLIT_TIME_COLUMN, "");
		if (End == "") return;
	}

	QString HashSource = SplitInputHash +
						 "_" + Start +
						 "_" + End +
						 "_" + QString::number(!ui->SplitConvert->isChecked()) +
						 "_" + SplitFormat +
						 "_" + ui->formatComboBox->currentText() +
						 "_" + ui->modeComboBox->currentText() +
						 "_" + ui->OutputMode->currentText() +
						 "_" + ui->VFovSpinBox->text() +
						 "_" + ui->HFovSpinBox->text() +
						 "_" + ui->MovementAlg->currentText() +
						 "_" + ui->IntermediatePercent->text() +
						 "_" + ui->CustomV360->text() +
						 "_" + ui->CustomInputFilters->text() +
						 "_" + ui->CustomFilters->text() +
						 "_" + ui->InputPadH->text() +
						 "_" + ui->InputPadW->text() +
						 "_" + ui->InputPadX->text() +
						 "_" + ui->InputPadY->text();

	int camerarow = LocateRow(ui->Table, TIME_COLUMN, QTime::fromString(StartTime, TIME_FORMAT), true);
	if (camerarow == -1) camerarow = 0;
	int cameraend = ui->Table->rowCount();
	if (EndTime != "")
		cameraend = LocateRow(ui->Table, TIME_COLUMN, QTime::fromString(EndTime, TIME_FORMAT), false);
	if (cameraend == ui->Table->rowCount()) cameraend--;

	// if (Debug) ui->DebugLog->appendPlainText(StartTime + " = " + ReadItemValue(camerarow, TIME_COLUMN, "")
	// 			+ " -> " + ReadItemValue(cameraend, TIME_COLUMN, ""));
	for (int i = camerarow; i <= cameraend; ++i) {
		HashSource += "_" + Row2TSV(ui->Table, i, TEXT_COLUMN);
		// if (Debug) ui->DebugLog->appendPlainText("-->" + Row2TSV(ui->Table, i, TEXT_COLUMN));
	}

	if (ui->FilterExposure->isChecked()) HashSource += "_Exposure";
	if (ui->FilterColorTemperature->isChecked()) HashSource += "_ColorTemperature";
	if (ui->FilterEQ->isChecked()) HashSource += "_EQ";
	if (ui->FilterColorBalance->isChecked()) HashSource += "_ColorBalance";

	int filtersend = ui->FiltersList->rowCount() - 1;
	if (filtersend >= 0) {
		int filtersrow = LocateRow(ui->FiltersList, FILTERS_TIME_COLUMN, QTime::fromString(StartTime, TIME_FORMAT), true);
		if (EndTime != "")
			filtersend = LocateRow(ui->FiltersList, FILTERS_TIME_COLUMN, QTime::fromString(EndTime, TIME_FORMAT), false);

		if (filtersrow == -1) {
			HashSource += "_" + GenerateDefaultFilters();
			filtersrow++;
		}

		for (int i = filtersrow; i <= filtersend + 1; ++i) {
			HashSource += "_" + Row2TSV(ui->FiltersList, i);
		}
	}

	QByteArray hash = QCryptographicHash::hash(HashSource.toUtf8(), QCryptographicHash::Md5)
					  .toBase64(QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals);
	QString File = QString::number(row + 1).rightJustified(3, '0') + "_" + QString::fromUtf8(hash) + ".mkv";
	ui->SplitTable->blockSignals(true);
	ui->SplitTable->setItem(row, SPLIT_FILE_COLUMN, new QTableWidgetItem(File));
	ui->SplitTable->blockSignals(false);
}

void MainWindow::CheckSplitState() {
	if (!FileOk) return;
	if (ui->SplitTable->rowCount() == 0) return;
	SplitErrors = false;
	SplitsFinishedCount = 0;
	for (int i = 0; i < ui->SplitTable->rowCount(); ++i) {
		QString CurrentFile = ReadItemValue(ui->SplitTable, i, SPLIT_FILE_COLUMN, "");
		if (CurrentFile == "") {
			SetSplitState(i, "Error", QBrush(QColor(ui->ErrorColor->text())));
			SplitErrors = true;
			continue;
		}
		CurrentFile = ui->SplitTMPDir->text() + QDir::separator() + CurrentFile;
		QString PartialFile = CurrentFile + ".tmp";
		QFileInfo File(CurrentFile);
		if (File.exists() && File.isFile() && File.size() > 0) {
			SetSplitState(i, "Finished", QBrush(Qt::green));
			SplitsFinishedCount++;
			continue;
		}
		QFileInfo PFile(PartialFile);
		if (PFile.exists() && PFile.isFile()) {
			if (CurrentSplitRow == i) continue;
			SetSplitState(i, "Procesing", QBrush(Qt::blue));
			continue;
		}
		ui->SplitTable->setItem(i, SPLIT_STATE_COLUMN, new QTableWidgetItem(""));
	}
}

void MainWindow::SplitSaveFormat() {
	SplitFormatOptions.clear();
	if (ui->SplitConvert->isChecked()) {
		SplitFormatOptions.insert("VideoFormat", ui->OutVideoFormat->currentText());
		SplitFormatOptions.insert("CRF", QString::number(ui->OutCRF->value()));
		SplitFormatOptions.insert("Preset", ui->OutPreset->currentText());
		SplitFormatOptions.insert("W", QString::number(ui->OutW->value()));
		SplitFormatOptions.insert("H", QString::number(ui->OutH->value()));
		SplitFormatOptions.insert("Interpolation", ui->OutInterpolation->currentText());
		SplitFormatOptions.insert("PXFormat", ui->OutPXFormat->currentText());
	} else {
		SplitFormatOptions.insert("VideoFormat", ui->CPreviewVideoFormat->currentText());
		SplitFormatOptions.insert("CRF", QString::number(ui->CPreviewCRF->value()));
		SplitFormatOptions.insert("Preset", ui->CPreviewPreset->currentText());
		SplitFormatOptions.insert("W", QString::number(ui->CPreviewW->value()));
		SplitFormatOptions.insert("H", QString::number(ui->CPreviewH->value()));
		SplitFormatOptions.insert("Interpolation", ui->CPreviewInterpolation->currentText());
		SplitFormatOptions.insert("PXFormat", ui->CPreviewPXFormat->currentText());
	}

	SplitFormat = "";
	while (!SplitFormatOptions.isEmpty()) {
		if (SplitFormat != "") SplitFormat += "|";
		QString Key = SplitFormatOptions.firstKey();
		QString Value = SplitFormatOptions.take(Key);
		SplitFormat += Key + "=" + Value;
	}
}

void MainWindow::SplitLoadFormat() {
	SplitFormatOptions.clear();
	QStringList OptionsList = SplitFormat.split("|");
	for (int i = 0; i < OptionsList.count(); ++i) {
		QString Option = OptionsList.at(i).section("=", 0, 0);
		QString Value = OptionsList.at(i).section("=", 1);
		SplitFormatOptions.insert(Option, Value);
	}
}

void MainWindow::SetSplitState(int Row, QString Text, const QBrush &Color, int Align) {
	QTableWidgetItem *Item = new QTableWidgetItem(Text);
	if (Align != 0) Item->setTextAlignment((Qt::Alignment)Align);
	Item->setForeground(Color);
	ui->SplitTable->setItem(Row, SPLIT_STATE_COLUMN, Item);
}

QString MainWindow::GenerateSplitFfmpegCommand(int row, bool tmp) {
	if (SplitInputFile == "") on_SplitConvert_toggled(ui->SplitConvert->isChecked());
	if (CurrentSendcmdFile == "") {
		on_GenerateCMD_clicked();
		on_SaveCMD_clicked();
	}
	SplitLoadFormat();
	QString File = ui->SplitTMPDir->text() + QDir::separator() + ReadItemValue(ui->SplitTable, row, SPLIT_FILE_COLUMN, "");
	if (tmp) File += ".tmp";
	QString Start = ReadItemValue(ui->SplitTable, row, SPLIT_ADJUST_AFTER_COLUMN, "");
	QString End = "";
	if (row + 1 < ui->SplitTable->rowCount()) {
		End = ReadItemValue(ui->SplitTable, row + 1, SPLIT_ADJUST_BEFORE_COLUMN, "");
	}
	QString Time = ReadItemValue(ui->SplitTable, row, SPLIT_TIME_COLUMN, "");

	int FPS = qRound(ui->FPSSpinBox->value());
	double S = Start.toDouble();
	double E = End.toDouble();
	double L = (E - S) * FPS;
	int KeyInt = FPS * 10;
	int R = qRound(L) % KeyInt;
	if (Debug) ui->DebugLog->appendPlainText("R=" + QString::number(R));
	if (R > 0 && R < KeyInt / 2)
		KeyInt = KeyInt - ((KeyInt - R) / 7) + 1;

	QTime time = QTime::fromString(Time, TIME_FORMAT);
	tableline_t FirstLine = LocateRow(time, true);
	QString Filters = SearchFiltersString(time, true);
	QString TimeFilter = ui->CustomFilters->text();
	if (!ui->SplitConvert->isChecked()) {
		if (TimeFilter != "") TimeFilter = TimeFilter + ",";
		TimeFilter = TimeFilter +
					 "drawtext=text='%{pts\\:flt}':fontsize=18:fontcolor=0xFFFFFFFF:box=1:boxborderw=4:"
					 "boxcolor=0x000000FF:borderw=0:bordercolor=000000:shadowcolor=000000:"
					 "shadowx=0:shadowy=0:x=w-tw-6:y=6";
	}

	QString Command;
	Command = ComposeFfmpegCommand(
						  ui->FfmpegBin->text(),
						  SplitInputFile,
						  File,
						  GetTemplate(SplitFormatOptions["VideoFormat"]),
						  SplitFormatOptions["VideoFormat"],
						  SplitFormatOptions["CRF"].toInt(),
						  SplitFormatOptions["Preset"],
						  "None",
						  0,
						  Start,
						  End,
						  0,
						  ui->FPSSpinBox->value(),
						  ui->formatComboBox->currentText(),
						  ui->CustomV360->text(),
						  ui->modeComboBox->currentText(),
						  ui->OutputMode->currentText(),
						  ui->VFovSpinBox->value(),
						  ui->HFovSpinBox->value(),
						  FirstLine.FOV,
						  FirstLine.Pitch,
						  FirstLine.Yaw,
						  FirstLine.Roll,
						  FirstLine.V,
						  FirstLine.H,
						  FirstLine.RightEye,
						  SplitFormatOptions["W"].toInt(),
						  SplitFormatOptions["H"].toInt(),
						  SplitFormatOptions["Interpolation"],
						  true,
						  ui->InputPadW->value(),
						  ui->InputPadH->value(),
						  ui->InputPadX->value(),
						  ui->InputPadY->value(),
						  CurrentSendcmdFile,
						  Filters,
						  SplitFormatOptions["PXFormat"],
						  "",
						  "",
						  "",
						  "",
						  "",
						  "None",
						  "",
						  TimeFilter,
						  ui->CustomInputFilters->text(),
						  false,
						  "mkv.tmp",
						  KeyInt
			  );
	return Command;
}

void MainWindow::on_SelectSplitTMPDir_clicked() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  "Select Output Split Directory",
				  ui->SplitTMPDir->text(),
				  QFileDialog::ShowDirsOnly);
	if (Dir != "") ui->SplitTMPDir->setText(QDir::toNativeSeparators(Dir));
}

void MainWindow::on_SplitSaveScript_clicked() {
	if (!FileOk) return;
	if (ui->SplitTable->rowCount() == 0) return;
	QStringList Output;
	for (int i = 0; i < ui->SplitTable->rowCount(); ++i) {
		Output.append(GenerateSplitFfmpegCommand(i, false));
		Output.append("");
	}
	Output.append(SplitComposeFinalCommand());
	WriteFile(ui->SplitTMPDir->text() + QDir::separator() + "FfmpegCommand.txt", Output.join("\n"));
}

void MainWindow::on_SplitConvert_toggled(bool checked) {
	if (checked) {
		SplitInputFile = InputFile;
		SplitInputHash = InputFilenameHash;
	} else {
		SplitInputFile = SmallInputFile;
		SplitInputHash = SmallInputFilenameHash;
	}
}

void MainWindow::on_SplitRecheckState_clicked() {
	CheckSplitState();
}

void MainWindow::on_SplitRun_toggled(bool checked) {
	if (!FileOk) return;
	if (ui->SplitTable->rowCount() == 0) return;
	if (GenerateSplits->isRunning()) return;
	CheckSplitState();
	if (SplitErrors) return;

	if (checked) {
		if (ProcessChunks.state() != QProcess::NotRunning) return;

		ui->SplitCurrentWidget->setVisible(true);
		ui->SplitCurrentProgress->setMaximum(ui->SplitTable->rowCount());
		ui->SplitCurrentProgress->setValue(SplitsFinishedCount);
		SplitTimer.setInterval(10000);
		SplitTimer.start();

		StartSplitProcess();
	}
}

void MainWindow::on_SplitCancel_clicked() {
	if (ProcessChunks.state() != QProcess::NotRunning) {
		ExecutionCanceled = true;
		ui->SplitRun->setChecked(false);
		ProcessChunks.close();
		ui->SplitCurrentWidget->setVisible(false);
	} else if (GenerateSplits->isRunning()) {
		GenerateSplits->Cancel();
	}
}

void MainWindow::SplitTimerTimeout() {
	if (!FileOk) return;
	CheckSplitState();
	ui->SplitCurrentProgress->setMaximum(ui->SplitTable->rowCount());
	ui->SplitCurrentProgress->setValue(SplitsFinishedCount);
	if (ui->TaskbarProgress->isChecked()) {
		Task->BeginProgress(ui->SplitTable->rowCount(), false);
		Task->Value(SplitsFinishedCount);
		if (SplitsFinishedCount == ui->SplitTable->rowCount()) {
			Task->EndProgress(true);
		}
	}
}

bool MainWindow::CheckSplitFile(int row, bool checkstate) {
	if (checkstate && ReadItemValue(ui->SplitTable, row, SPLIT_STATE_COLUMN, "Finished") != "")
		return false;

	SplitCurrentFile = ui->SplitTMPDir->text() + QDir::separator() +
					   ReadItemValue(ui->SplitTable, row, SPLIT_FILE_COLUMN, "");
	SplitCurrentTmpFile = SplitCurrentFile + ".tmp";
	if (QFile::exists(SplitCurrentFile)) return false;
	if (QFile::exists(SplitCurrentTmpFile)) return false;
	return true;
}

void MainWindow::StartSplitProcess(int selectedrow) {
	if (!FileOk) return;
	if (ui->SplitTable->rowCount() == 0) return;
	if (selectedrow == -1) {
		if (ui->SplitOrder->currentText() == "Process in order") {
			for (int i = 0; i < ui->SplitTable->rowCount(); ++i) {
				if (!CheckSplitFile(i)) continue;
				selectedrow = i;
				break;
			}
		} else if (ui->SplitOrder->currentText() == "Process in inverse order") {
			for (int i = ui->SplitTable->rowCount() - 1; i >= 0; --i) {
				if (!CheckSplitFile(i)) continue;
				selectedrow = i;
				break;
			}
		} else if (ui->SplitOrder->currentText() == "Process in random order") {
			int count = 0;
			for (int i = 0; i < ui->SplitTable->rowCount(); ++i) {
				if (!CheckSplitFile(i)) continue;
				count++;
			}
			int random = QRandomGenerator::global()->bounded((int)0, count);
			count = 0;
			for (int i = 0; i < ui->SplitTable->rowCount(); ++i) {
				if (!CheckSplitFile(i)) continue;
				count++;
				if (count < random) continue;
				selectedrow = i;
				break;
			}
		}

		CurrentSplitRow = selectedrow;
		CheckSplitState();
		if (selectedrow == -1 || !ui->SplitRun->isChecked()) {
			CurrentSplitRow = -1;
			SplitTimer.stop();
			ui->SplitRun->setChecked(false);
			ui->SplitCurrentWidget->setVisible(false);
			if (ui->TaskbarProgress->isChecked()) Task->EndProgress(true);
			return;
		}
	} else {
		if (!CheckSplitFile(selectedrow, false)) return;
		CurrentSplitRow = selectedrow;
		CheckSplitState();
	}

	WriteFile(SplitCurrentTmpFile, "");

	QString command = GenerateSplitFfmpegCommand(selectedrow, true);

	ui->SplitTable->selectRow(selectedrow);
	ui->SplitTable->scrollToItem(ReadItem(selectedrow, SPLIT_STATE_COLUMN));

	ProcessParams = QProcess::splitCommand(command);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	if (Debug) ui->DebugLog->appendPlainText(command);

	ExecutionCanceled = false;
	FfmpegEnableBlackFrames = false;
	GenerateSmallFile = false;
	ChunkProgress.ExecutionStartTime = QDateTime::currentDateTime();
	ChunkProgress.TotalTime = QTime::fromMSecsSinceStartOfDay(CalcChunkDuration(selectedrow) * 1000);
	ChunkProgress.CurrentTime = QTime::fromMSecsSinceStartOfDay(0);
	ChunkProgress.Speed = 0;
	ChunkProgress.FPS = 0;
	ChunkProgress.Bitrate = 0;
	ProcessChunks.start(ProcessProgram, ProcessParams);
	if (!ProcessChunks.waitForStarted(-1)) {
		QProcess::ProcessError Error = ProcessChunks.error();
		switch (Error) {
			case QProcess::FailedToStart:
			case QProcess::Crashed:
			case QProcess::Timedout:
			case QProcess::WriteError:
			case QProcess::ReadError:
			case QProcess::UnknownError:
				SetSplitState(selectedrow, "Error", QBrush(QColor(ui->ErrorColor->text())));
				QFile::remove(SplitCurrentTmpFile);
		}
	}
	if (ui->Priority->currentText() == "Low")
		ProcessLowPriority(ProcessChunks.processId());
	else if (ui->Priority->currentText() == "Idle")
		ProcessIdlePriority(ProcessChunks.processId());
}

void MainWindow::ProcessChunksData() {
	UpdateProgress(&ProcessChunks, &ChunkProgress, &ExecutionBuffer, &ProcessErrorString);
	int Porcent = (ChunkProgress.CurrentTime.msecsSinceStartOfDay()) * 100 / ChunkProgress.TotalTime.msecsSinceStartOfDay();
	SetSplitState(CurrentSplitRow, QString::number(Porcent) + "%", QBrush(Qt::blue), Qt::AlignRight | Qt::AlignVCenter);

	QString Text = "Procesing List (" +
				   QString::number(ui->SplitCurrentProgress->value()) + "/" +
				   QString::number(ui->SplitCurrentProgress->maximum()) + ") " +
				   "Speed: " + QString::number(ChunkProgress.Speed) + "x, " +
				   "Fps: " + QString::number(ChunkProgress.FPS);
	if (ChunkProgress.Bitrate > 1) Text += ", Bitrate: " + QString::number(ChunkProgress.Bitrate, 'f', 1) + "kbits/s";
	ui->SplitCurrentState->setText(Text);
}

void MainWindow::ProcessChunksTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (ExecutionCanceled || exitCode != 0 || exitStatus == QProcess::CrashExit) {
		if (QFile::exists(SplitCurrentTmpFile)) QFile::remove(SplitCurrentTmpFile);
	}
	if (ExecutionCanceled) {
		ExecutionCanceled = false;
		CurrentSplitRow = -1;
		SplitTimer.stop();
		ui->SplitCurrentWidget->setVisible(false);
		if (ui->TaskbarProgress->isChecked()) Task->EndProgress(true);
		CheckSplitState();
		return;
	}
	if (exitCode != 0) {
		ShowProcessError(&ProcessChunks, ProcessErrorString);
		SetSplitState(CurrentSplitRow, "Error", QBrush(QColor(ui->ErrorColor->text())));
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;

	QFile::rename(SplitCurrentTmpFile, SplitCurrentFile);

	StartSplitProcess();
}

double MainWindow::CalcChunkDuration(int row) {
	double Start = ReadItemValue(ui->SplitTable, row, SPLIT_ADJUST_AFTER_COLUMN, "0").toDouble();
	double End;
	if (row + 1 < ui->SplitTable->rowCount()) {
		End = ReadItemValue(ui->SplitTable, row + 1, SPLIT_ADJUST_BEFORE_COLUMN, "0").toDouble();
	} else  {
		End = ui->VideoLength->time().msecsSinceStartOfDay() / 1000;
	}
	return End - Start;
}

QString MainWindow::SplitComposeFinalCommand() {
	QStringList Files;
	Files.append("ffconcat version 1.0");
	for (int i = 0; i < ui->SplitTable->rowCount(); ++i) {
		QString File = ReadItemValue(ui->SplitTable, i, SPLIT_FILE_COLUMN, "");
		double Duration = CalcChunkDuration(i);
		QString FileScaped = QDir::toNativeSeparators(ui->SplitTMPDir->text() + QDir::separator() + File);
		if (FileScaped.contains("'")) FileScaped.replace("'", "'\\''");
		Files.append("file '" + FileScaped + "'");
		Files.append("duration " + QString::number(Duration, 'f', 6) + "");
	}

	QString Concat = QDir::toNativeSeparators(ui->SplitTMPDir->text() + QDir::separator() + "Concat.txt");
	WriteFile(Concat, Files.join("\n"));

	QString Chapters = ExportMetadataFile(QDir::toNativeSeparators(ui->SplitTMPDir->text() + QDir::separator() +
										  "Chapters.txt"));
	QString vr2nFile = "";
	QString OutFilename;
	QString AudioCodec;
	QString AudioBitrate;
	if (ui->SplitConvert->isChecked()) {
		OutFilename = ui->OutFile->text();
		if (OutFilename == "") {
			OutFilename = GenerateDefaultOutFile();
			ui->OutFile->setText(OutFilename);
		}

		if (ui->IncludeFile->isChecked()) {
			vr2nFile = QDir::toNativeSeparators(ui->SplitTMPDir->text() + QDir::separator() + "VR2Normal file.vr2n");
			SaveMovementFile(vr2nFile);
		}
		AudioCodec = ui->OutAudioFormat->currentText();
		AudioBitrate = ui->OutAudioBitrate->text();
	} else {
		OutFilename = ui->CPreviewFile->text();
		if (OutFilename == "") {
			QString Ext = GetExtension(ui->CPreviewVideoFormat->currentText());
			OutFilename = SmallInputFile;
			OutFilename += "_Complete_preview" + Ext;
			OutFilename = QDir::toNativeSeparators(OutFilename);
			ui->CPreviewFile->setText(OutFilename);
		}
		AudioCodec = ui->CPreviewAudioFormat->currentText();
		AudioBitrate = ui->CPreviewAudioBitrate->text();
	}

	QString Command = ui->FfmpegBin->text() + " -hide_banner -loglevel error -stats -y -i \"" + SplitInputFile + "\"";
	Command += " -f concat -safe 0 -i \"" + Concat + "\"";
	if (Chapters != "") Command += " -i \"" + Chapters + "\"";
	Command += " -map 1:v:0";
	Command += " -map 0:a:0";
	Command += " -c:v copy";

	if (AudioCodec == "Copy") {
		Command += " -c:a copy";
	} else if (AudioCodec == "MP3") {
		Command += " -c:a libmp3lame -b:a " + AudioBitrate + "K";
	} else if (AudioCodec == "AAC") {
		Command += " -c:a aac -b:a " + AudioBitrate + "K";
	} else if (AudioCodec == "Vorbis") {
		Command += " -c:a libvorbis -b:a " + AudioBitrate + "K";
	} else if (AudioCodec == "Opus") {
		Command += " -c:a libopus -b:a " + AudioBitrate + "K";
	}

	Command += " -map_metadata:g -1 -map_metadata:s:v -1";
	Command += " -map_metadata:s:a 0:s:a";

	if (ui->MetadataTitle->text() != "")
		Command += " -metadata \"TITLE=" + ui->MetadataTitle->text() + "\"";

	if (ui->MetadataStudio->text() != "")
		Command += " -metadata \"PRODUCTION_STUDIO=" + ui->MetadataStudio->text() + "\"";

	if (ui->MetadataActors->text() != "")
		Command += " -metadata \"ACTOR=" + ui->MetadataActors->text() + "\"";

	if (ui->MetadataReleaseDate->text() != "" && ui->MetadataReleaseDate->text() != "2000-01-01")
		Command += " -metadata \"DATE_RELEASED=" + ui->MetadataReleaseDate->text() + "\"";

	if (OutFilename.endsWith("mkv", Qt::CaseInsensitive) || OutFilename.endsWith("webm", Qt::CaseInsensitive)) {
		QString Comment = "Made with VR2Normal " + QString(VR2NORMAL_VERSION);
		Command += " -metadata \"COMMENT=" + Comment + "\"";
		Command += " -metadata:s:v \"COMMENT=" + Comment + "\"";
		Command += " -metadata \"URL=https://gitlab.com/vongooB9/vr2normal\"";
		Command += " -metadata:s:v \"URL=https://gitlab.com/vongooB9/vr2normal\"";
		Command +=	" -metadata:s \"VENDOR_ID=\" -metadata:s \"HANDLER_NAME=\"";
	} else {
		Command += " -metadata \"COMMENT=Made with VR2Normal (https://gitlab.com/vongooB9/vr2normal)\"";
	}

	if (vr2nFile != "" && OutFilename.endsWith("mkv", Qt::CaseInsensitive)) {
		Command += " -attach \"" + vr2nFile + "\"" +
				   " -metadata:s:t:0 mimetype=text/plain -metadata:s:t:0 \"filename=VR2Normal file.vr2n\"";
	}

	if (Chapters != "") Command += " -map_chapters 2";

	Command += " \"" + OutFilename + "\"";
	return Command;
}

void MainWindow::on_SplitJoin_clicked() {
	if (!FileOk) {
		if (!CheckInputFiles()) return;
	}

	on_SplitConvert_toggled(ui->SplitConvert->isChecked());

	CheckSplitState();

	for (int i = 0; i < ui->SplitTable->rowCount(); ++i) {
		if (ReadItemValue(ui->SplitTable, i, SPLIT_STATE_COLUMN, "") != "Finished") return;
		if (ReadItemValue(ui->SplitTable, i, SPLIT_FILE_COLUMN, "") == "") return;
	}

	QString Command = SplitComposeFinalCommand();
	if (Command == "") return;
	ProcessParams = QProcess::splitCommand(Command);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	if (Debug) ui->DebugLog->appendPlainText(Command);

	MainProgress.TotalTime = ui->VideoLength->time();
	PreviewFile = "";
	ProcessIsPreview = false;

	ExecuteProcess();

}

void MainWindow::on_SplitRemoveTmp_clicked() {
	if (!FileOk) return;
	if (ProcessChunks.state() != QProcess::NotRunning) return;
	if (GenerateSplits->isRunning()) return;
	QDir Dir(ui->SplitTMPDir->text());
	QFileInfoList List = Dir.entryInfoList(QStringList("*.tmp"));
	bool force = false;
	for (int i = 0; i < List.count(); ++i) {
		if (List.at(i).lastModified().secsTo(QDateTime::currentDateTime()) < 10) {
			QMessageBox::StandardButton Result = QMessageBox::question(this, "Delete Confirmation",
												 "There are temporal files that seem to be in use at the moment (maybe on another computer).\nDo you want to delete then anyway?",
												 QMessageBox::Yes | QMessageBox::No);
			if (Result == QMessageBox::Yes) {
				force = true;
			}
			break;
		}
	}
	for (int i = 0; i < List.count(); ++i) {
		if (!force && List.at(i).lastModified().secsTo(QDateTime::currentDateTime()) < 10) continue;
		Dir.remove(List.at(i).fileName());
	}
	CheckSplitState();
}

void MainWindow::NewSplit(QTime Time, QString Before, QString After, QString BeforeAdj, QString AfterAdj) {
	int newrow = InsertSplitTableLine(Time.toString(TIME_FORMAT), Before, After, BeforeAdj, AfterAdj, "");
	if (newrow > 0) UpdateSplitTableFile(newrow - 1);

	ui->SplitTable->selectRow(newrow);
	ui->SplitTable->scrollToItem(ReadItem(newrow, SPLIT_STATE_COLUMN), QAbstractItemView::PositionAtBottom);

	ui->SplitCurrentProgress->setValue(ui->SplitCurrentProgress->value() + 1);
	ui->SplitCurrentState->setText("Generating split points (" +
								   QString::number(ui->SplitCurrentProgress->value()) + "/" +
								   QString::number(ui->SplitCurrentProgress->maximum()) + ")");
}

void MainWindow::UpdateSplitText(QString Text) {
	ui->SplitCurrentState->setText("Generating split points (" +
								   QString::number(ui->SplitCurrentProgress->value()) + "/" +
								   QString::number(ui->SplitCurrentProgress->maximum()) + ") " + Text);
}

void MainWindow::NewSplitDebug(QString DebugMsg) {
	if (Debug) ui->DebugLog->appendPlainText(DebugMsg);
}

void MainWindow::NewSplitError(QStringList ErrorMsg) {
	if (!ui->LeftTabs->isTabVisible(LEFT_TAB_ERROR_LOG)) {
		ui->LeftTabs->setTabVisible(LEFT_TAB_ERROR_LOG, true);
		ui->LeftTabs->setCurrentIndex(LEFT_TAB_ERROR_LOG);
	}
	for (int i = 0; i < ErrorMsg.count(); ++i)
		ui->ErrorLog->appendHtml(ErrorMsg.at(i));
	ui->ErrorLog->appendHtml("<hr>");
}

void MainWindow::NewSplitFinish() {
	if (!GenerateSplits->WasCanceled()) {
		UpdateSplitTableFile(ui->SplitTable->rowCount() - 1);
	}
	ui->SplitTable->clearSelection();
	ui->SplitCurrentWidget->setVisible(false);
	ui->SplitCurrentState->setText("");
}

void MainWindow::on_SplitRunSelected_clicked() {
	if (!FileOk) return;
	if (ui->SplitTable->rowCount() == 0) return;
	if (GenerateSplits->isRunning()) return;
	CheckSplitState();
	if (SplitErrors) return;

	if (ProcessChunks.state() != QProcess::NotRunning) return;

	int selected = GetSelectedRow(ui->SplitTable);
	if (selected == -1) return;
	if (ReadItemValue(ui->SplitTable, selected, SPLIT_STATE_COLUMN, "Finished") != "") return;

	ui->SplitCurrentWidget->setVisible(true);
	ui->SplitCurrentProgress->setMaximum(ui->SplitTable->rowCount());
	ui->SplitCurrentProgress->setValue(SplitsFinishedCount);
	SplitTimer.setInterval(10000);
	SplitTimer.start();

	StartSplitProcess(selected);
}

void MainWindow::on_ErrorColor_textChanged(const QString &/*arg1*/) {
	ui->PreviewErrorColor->setStyleSheet("background-color: " + ui->ErrorColor->text());
	ui->ErrorLinesLabel->setStyleSheet("color: " + ui->ErrorColor->text());
}

void MainWindow::on_SelectErrorColor_clicked() {
	QString Color = SelectColor("Select Error Color", ui->ErrorColor->text());
	ui->ErrorColor->setText("#" + Color);
}

void MainWindow::on_LowWarningColor_textChanged(const QString &/*arg1*/) {
	ui->PreviewLowWarningColor->setStyleSheet("background-color: " + ui->LowWarningColor->text());
}

void MainWindow::on_SelectLowWarningColor_clicked() {
	QString Color = SelectColor("Select Low Warning Color", ui->LowWarningColor->text());
	ui->LowWarningColor->setText("#" + Color);
}

void MainWindow::on_HighWarningColor_textChanged(const QString &/*arg1*/) {
	ui->PreviewHighWarningColor->setStyleSheet("background-color: " + ui->HighWarningColor->text());
}

void MainWindow::on_SelectHighWarningColor_clicked() {
	QString Color = SelectColor("Select High Warning color", ui->HighWarningColor->text());
	ui->HighWarningColor->setText("#" + Color);
}

//TODO: Delete tis alter check that shorcuts load correctly o qt6 linux
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0) && defined (Q_OS_LINUX)
void MainWindow::on_LeftTabs_currentChanged(int index) {
	if (index == LEFT_TAB_CAMERA) ApplyShortcuts();
}

void MainWindow::on_RightTabs_currentChanged(int index) {
	if (index == RIGHT_TAB_MOVEMENTS) ApplyShortcuts();
}
#endif

