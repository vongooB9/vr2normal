#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QShortcut>
#include <QSettings>
#include <QJsonDocument>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QUndoStack>
#include <QCache>
#include <QMap>
#include <QQueue>
#include <QTimer>
#include <QElapsedTimer>
#include <QTemporaryDir>
#include <QSystemTrayIcon>
#include "taskbarprogress.h"
#include <QLabel>
#include "filters.h"
#include <QThread>
#include <QProgressBar>
#include <QBrush>
#include "common.h"
#include "generatesplits.h"
#include <QPainter>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


struct tableline_t {
	int Time;
	double Transition;
	double Pitch;
	double Yaw;
	double Roll;
	double FOV;
	bool RightEye;
	double V;
	double H;
	QString Text;
	bool Valid;
	exposure_t Exposure;
	colortemperature_t ColorTemperature;
	eq_t EQ;
	colorbalance_t ColorBlance;
	tableline_t():
		Time(0),
		Transition(0),
		Pitch(0),
		Yaw(0),
		Roll(0),
		FOV(0),
		RightEye(false),
		V(0),
		H(0),
		Text(""),
		Valid(false) {};
};

struct cache_time_t {
	bool WaitTime;
	qint64 QueueTime;
	qint64 StartTime;
	qint64 ProcessTime;
	qint64 TotalTime;
	QElapsedTimer *ElapsedTimer;
};

struct list_compare_screens_t {
	QLabel *Label;
	QPixmap *Pixmap;
	uint64_t Time;
	QString Filename;
	QString CacheFilename;
	bool Finished;
	bool InProgress;
};

class MainWindow : public QMainWindow {
		Q_OBJECT

	public:
		MainWindow(QWidget *parent = nullptr, bool Autoload = true);
		~MainWindow();
		void OpenFile(QString File);
	private:
		Ui::MainWindow *ui;
		QPixmap RawPixmap;
		QPixmap ScaledRawPixmap;
		QPixmap FilterPixmap;
		QPixmap ScaledFilterPixmap;
		QProcess FfmpegRaw;
		QProcess FfmpegFilter;
		QProcess Ffprobe;
		QByteArray RawData;
		QByteArray FilterResultData;
		QByteArray PreviewErrorData;
		QString ProcessErrorString;
		QCache<QTime, QByteArray> RawCache;
		QCache<QTime, QByteArray> CameraCache;
		QTime CurrentRawTime;
		bool FileOk;
		QString InputFile;
		QString InputFilename;
		QString InputFileDir;
		QString InputFilenameHash;
		QString InputFileExt;
		QString SmallInputFile;
		QString SmallInputFilename;
		QString SmallInputFilenameHash;
		QString SmallInputFileDir;
		QString ScreenshotFilename;
		bool NeedFilterUpdate;
		bool ForceDetectVideo;

		QJsonDocument VideoInfo;

		QUndoStack *TableUndo;

		QShortcut *PitchUP;
		QShortcut *PitchDown;
		QShortcut *YawUP;
		QShortcut *YawDown;
		QShortcut *RollUP;
		QShortcut *RollDown;
		QShortcut *FovUP;
		QShortcut *FovDown;
		QShortcut *FocusText;
		QShortcut *TranUP;
		QShortcut *TranDown;
		QShortcut *VUP;
		QShortcut *VDown;
		QShortcut *HUP;
		QShortcut *HDown;

		QShortcut *RowUp;
		QShortcut *RowDown;

		QShortcut *Undo;
		QShortcut *Redo;

		QShortcut *Unselect;

		QProcess Execution;
		bool FfmpegEnableBlackFrames;
		bool GenerateSmallFile;
		QString ExecutionBuffer;
		ffmpeg_progress_t MainProgress;

		bool FfmpegRawCanceled;
		bool FfmpegFilterCanceled;
		bool ExecutionCanceled;

		QSettings *Settings;

		QMap<QProcess *, cache_time_t> CacheProcess;
		QMap<QTime, int> OndemandDiskCacheQueue;
		QQueue<QTime> DiskCacheQueue;

		QTimer OndemandCacheTimer;
		QTimer Autosave;

		QProcess ScreenshotProcess;
		QString PreviewFile;
		//		QElapsedTimer TimeCounter;

		bool Debug;
		QElapsedTimer RawElapsedTimer;
		QElapsedTimer FilterElapsedTimer;
		qint64 RawStartTime;
		qint64 RawReadTime;
		qint64 RawPrevirewTime;
		qint64 RawTotalTime;
		bool RAMCacheRead;
		bool RAMCameraCacheRead;
		bool RAMCamereTimeMovement;
		bool DiskCacheRead;

		qint64 FilterStartTime;
		qint64 FilterSendTime;
		qint64 FilterReadTime;
		qint64 FilterPrevirewTime;
		qint64 FilterTotalTime;

		bool OndemandCache;
		bool DiskCache;
		bool RAMCache;

		bool Loading;

		QTemporaryDir Tmp;
		QString TmpSendcmdFile;
		QString CurrentSendcmdFile;

		QSystemTrayIcon *Tray;
		TaskbarProgress *Task;

		QProcess CompareSmallProcess;
		QList<list_compare_screens_t> CompareScreens;
		QByteArray CompareRawData;
		int CurrentCompareScreen;

		QString GUIPreviewScale;

		bool PendingChanges;
		QString WindowTitleVersion;
		QThread DiskCacheThread;

		QString SplitInputFile;
		QString SplitInputHash;
		double FrameDuration;
		QString CurrentSearch;
		QString CurrentCacheName;
		int SplitCount;
		bool SplitErrors;
		QTimer SplitTimer;
		QProcess ProcessChunks;
		QString SplitCurrentFile;
		QString SplitCurrentTmpFile;
		int SplitsFinishedCount;
		QMap<QString, QString> SplitFormatOptions;
		QString SplitFormat;
		int CurrentSplitRow;
		ffmpeg_progress_t ChunkProgress;
		GenerateSplitsThread *GenerateSplits;

	protected:
		void closeEvent(QCloseEvent *event) override;
		void resizeEvent(QResizeEvent *event) override;
		void RefreshRawImage();
		void RefreshCameraImage();
		void UpdateTableTime();
		void UpdateItem(int column, QString value, QString color = "");
		int GetSelectedRow(QTableWidget *Table);
		int GetSelectedRow();
		QString Row2TSV(QTableWidget *Table, int row, int exclude = -1);
		QStringList Table2TSV(QTableWidget *Table, QString Prefix = "", int exclude = -1);
		void LoadMovenmentFile();
		bool BlockRefresh;
		QString ReadItemValue(QTableWidget *Table, int Row, int Column, const QString DefaultValue = "");
		QString ReadItemValue(int Row, int Column, const QString DefaultValue = "");
		QTableWidgetItem *ReadItem(int Row, int Column);
		tableline_t ReadRow(int Row);
		tableline_t LocateRow(const QTime &Time, bool CurrentCamera = false);
		int LocateRow(QTableWidget *Table, int Colum, const QTime &Time, bool CurrentCamera = false);
		QString ProcessProgram;
		QStringList ProcessParams;
		bool ProcessIsPreview;
		void TableUpSelect();
		void TableDownSelect();
		void ShowProcessError(QProcess *process, QString CustomErrorLog = "");
		void CleanTable(QTableWidget *Table);

		bool CheckValidTableTime(int Row, QTime &Time);

		void LoadShortcuts();
		void SaveShortcuts();
		void ApplyShortcuts();

		void DisableRunButtons();

		void GenerateDiskCache(int seconds);
		void GenerateDiskCache(int seconds, QTime start, QTime end, bool ondemand);

		void TakeScreenshot(QTime Time, int V360W, int V360H, int FinalW, int FinalH);
		void EnableScreenshotsButtons(bool Enable);
		void PrintFramingLines();
		void PrintEyeFramingLines(int width, int height, int x, int y);
		void DrawLines(QPainter &paint, int width, int height, int x, int y);
		bool CheckInputFiles();
		void CheckCache();


		int CalculateEstimatedWidth();
		int CalculateEstimatedHeight();
		int CalculateEstimatedWidth(double hfov);
		int CalculateEstimatedHeight(double vfov);
		double CalculateEstimatedHFOV();
		double CalculateEstimatedVFOV();
		double CalcHFOV(int w, int h, double fov);
		double CalcVFOV(int w, int h, double fov);

		void SetDefaultInputOptions();

		void AddLine(QTime time, double trans, double pitch, double yaw, double roll, double fov, bool righteye, double v,
					 double h, QString text);
		QString GenerateDefaultOutFile();
		void StartCacheProcess(QProcess *Process);
		QStringList RawFfmpegParams(QString in, QString out, uint64_t Time);
		QString ComposeCacheFilename(QString FilenameHash, uint64_t Time);
		QPixmap ProcessCompareFrame(QPixmap *input);

		QString ExportMetadataFile(QString OutputFile = "");
		bool PreviewTransition_click();

		void AddBlackTransition(QTime Time);

		QString GenerateFiltersString();
		QString GenerateDefaultFilters();

		QString SearchFiltersString(QTime Time, bool CurrentCamera = false);

		void LoadFiltersFields(QString Filters);
		void LoadExposure(exposure_t &EX);
		void LoadColorTemperature(colortemperature_t &CT);
		void LoadEQ(eq_t &EQ);
		void LoadColorBalance(colorbalance_t &CB);
		QString CMDOption(QString Filter, QString Option, QString Value);
		QString CMDOption(QString Filter, QString Option, int Value);
		QString CMDOption(QString Filter, QString Option, double Value);
		QString CMDTransition(QString Filter, QString Option, QString OldValue, QString NewValue);
		QString CMDTransition(QString Filter, QString Option, int OldValue, int NewValue);
		QString CMDTransition(QString Filter, QString Option, double OldValue, double NewValue);
		QString CMDInterval(int Start, int End);
		QString CMDline(QString Interval, QStringList Commands, bool Line);
		void VisibleAllChildrens(QWidget *Parent, bool Visible);
		void CheckFilterWarnings();
		void CalculateOutputResolution();
		void CalculateOutputPXFormat();
		void Changes();

		int InsertSplitTableLine(QString Time, QString Before, QString After, QString AdjustedBefore, QString AdjustedAfter,
								 QString File, QString State = "");
		void UpdateSplitTableFile(int row);
		void CheckSplitState();
		QString GenerateSplitFfmpegCommand(int row, bool tmp);
		void StartSplitProcess(int selectedrow = -1);
		QString SplitComposeFinalCommand();
		void SplitSaveFormat();
		void SplitLoadFormat();
		void SetSplitState(int Row, QString Text, const QBrush &Color, int Align = 0);
		double CalcChunkDuration(int row);
		bool CheckSplitFile(int row, bool checkstate = true);

	signals:
		void UpdateDiskCacheAndRemove();
		void FrameTimesCacheTerminate (int exitCode, QProcess::ExitStatus exitStatus);

	public slots:
		void SaveMovementFile(QString TmpFile = "");
		void SaveDefaultMovementFile();
		void ReloadRawImage(const QTime &time);
		void ReloadFilterImage();
		void ReloadFilterImageForced();
		void ReadRawData();
		void ReadFilterData();
		void StartFilterProcess();
		void GenerateRawImageTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void GenerateFilterImageTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void FfprobeTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void ReadProgress();
		void ProcessTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void UndoSlot();
		void RedoSlot();
		void UnselectSlot();
		void EditTextFocus();
		void ExecuteProcess();
		void DiskCacheProcessTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void DiskCacheStart();
		void AutosaveTimeout();
		void ScreenshotProcessTerminate (int exitCode, QProcess::ExitStatus exitStatus);

		void PitchUPEvent();
		void PitchDownEvent();
		void YawUPEvent();
		void YawDownEvent();
		void RollUPEvent();
		void RollDownEvent();
		void FovUPEvent();
		void FovDownEvent();
		void VUPEvent();
		void VDownEvent();
		void HUPEvent();
		void HDownEvent();

		void TrayClick(QSystemTrayIcon::ActivationReason reason);

		void CompareProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus);
		void ReadCompareData();

		void RefreshPreviews();

		void UpdateFiltersCell();
		void ChangeInputPad();
		void SplitTimerTimeout();
		void ProcessChunksData();
		void ProcessChunksTerminate(int exitCode, QProcess::ExitStatus exitStatus);

		void NewSplit(QTime QTime, QString Before, QString After, QString BeforeAdj, QString AfterAdj);
		void UpdateSplitText(QString Text);
		void NewSplitDebug(QString DebugMsg);
		void NewSplitError(QStringList ErrorMsg);
		void NewSplitFinish();
	private slots:
		void on_NextSecond_clicked();
		void on_PrevSec_clicked();
		void on_Next10Sec_clicked();
		void on_Prev10Sec_clicked();
		void on_NextMinute_clicked();
		void on_PrevMinute_clicked();
		void on_SelectFile_clicked();
		void on_InputFile_textChanged(const QString &arg1);
		void on_Pitch_valueChanged(double arg1);
		void on_Yaw_valueChanged(double arg1);
		void on_Roll_valueChanged(double arg1);
		void on_Fov_valueChanged(double arg1);
		#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
		void on_RightEye_checkStateChanged(Qt::CheckState state);
		#else
		void on_RightEye_stateChanged(int arg1);
		#endif
		void on_AddLine_clicked();
		void on_UnselectRow_clicked();
		void on_RemoveLine_clicked();
		void on_UpdateTime_clicked();
		void on_Table_itemSelectionChanged();
		void on_NextFrame_clicked();
		void on_PrevFrame_clicked();
		void on_OpenMovementFile_clicked();
		void on_SaveCMD_clicked();
		void on_GenerateCMD_clicked();
		void on_PreviewTransition_clicked();
		void on_PreviewFragment_clicked();
		void on_NewMovement_clicked();
		void on_SelectOutFile_clicked();
		void on_OutGenerateCommand_clicked();
		void on_formatComboBox_currentTextChanged(const QString &arg1);
		void on_modeComboBox_currentTextChanged(const QString &arg1);
		void on_OutputMode_currentTextChanged(const QString &arg1);
		void on_VFovSpinBox_valueChanged(int arg1);
		void on_HFovSpinBox_valueChanged(int arg1);
		void on_GUIPreviewInterpolation_currentTextChanged(const QString &arg1);
		void on_FfmpegBinSelect_clicked();
		void on_ClearErrorLog_clicked();
		void on_VideoLength_userTimeChanged(const QTime &time);
		void on_SelectVideoPlayer_clicked();
		void on_SelectFfprobe_clicked();
		void on_Sort_clicked();
		void on_actionSave_screen_triggered();
		void on_SaveConfig_clicked();
		void on_Trans_valueChanged(double arg1);
		void on_Text_editingFinished();
		void on_Cancel_clicked();
		void on_CPreviewGenerateFfmpeg_clicked();
		void on_PreviewCurrentLine_clicked();
		void on_SelectPreviewFile_clicked();
		void on_RAMCache_valueChanged(int arg1);
		void on_VOffset_valueChanged(double arg1);
		void on_HOffset_valueChanged(double arg1);
		void on_GenerateDiskCache_clicked();
		void on_Gen10SCache_clicked();
		void on_ClearRAMCache_clicked();
		void on_DiskCacheDirSelect_clicked();
		void on_TakeScreenshot1920_clicked();
		void on_TakeScreenshot1280_clicked();
		void on_TakeScreenshot_clicked();
		void on_TakeScreenshotRescaled_clicked();
		#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
		void on_ShowFramingLines_checkStateChanged(Qt::CheckState state);
		#else
		void on_ShowFramingLines_stateChanged(int state);
		#endif
		void on_PreviewDirSelect_clicked();
		void on_SelectScreenshotsDir_clicked();
		#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
		void on_EnableDebug_checkStateChanged(Qt::CheckState state);
		#else
		void on_EnableDebug_stateChanged(int state);
		#endif
		void on_SelectSmallFile_clicked();
		void on_SmallInputFile_textChanged(const QString &arg1);
		void on_PauseOndemand_toggled(bool checked);
		void on_DiskCacheDir_textChanged(const QString &arg1);
		void on_PreloadCacheSeoconds_textChanged(const QString &arg1);
		void on_DiskCache_textChanged(const QString &arg1);
		#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
		void on_ShowEstimatedResolution_checkStateChanged(Qt::CheckState state);
		#else
		void on_ShowEstimatedResolution_stateChanged(int state);
		#endif
		void on_FfmpegBin_textChanged(const QString &arg1);
		void on_TakeScreenshotEstimated_clicked();
		void on_TakeScreenshotEstimRescaled_clicked();
		void on_Apply_clicked();
		void on_SelectImageViewer_clicked();
		void on_UpdateTransition_clicked();
		void on_AddTansZero_clicked();
		void on_AddPrev_clicked();
		#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
		void on_ShowSendcmdTab_checkStateChanged(Qt::CheckState state);
		#else
		void on_ShowSendcmdTab_stateChanged(int state);
		#endif
		void on_NewCMDFile_clicked();
		void on_SelectOutDefaultDir_clicked();
		#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
		void on_ShowTrayIcon_checkStateChanged(Qt::CheckState state);
		#else
		void on_ShowTrayIcon_stateChanged(int state);
		#endif
		#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
		void on_TaskbarProgress_checkStateChanged(Qt::CheckState state);
		#else
		void on_TaskbarProgress_stateChanged(int state);
		#endif
		void on_UpdateCompareSmall_clicked();
		void on_TabsSpliter_splitterMoved(int pos, int index);
		void on_PreviewSpliter_splitterMoved(int pos, int index);
		void on_IPStart_clicked();
		void on_IPSelectOutFile_clicked();
		void on_IPAddTrans_clicked();
		#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
		void on_AutoSaveMovement_checkStateChanged(Qt::CheckState state);
		#else
		void on_AutoSaveMovement_stateChanged(int state);
		#endif
		void on_FiltersButton_clicked();
		void on_FiltersList_itemSelectionChanged();
		void on_FilterRemove_clicked();
		void on_FilterExposure_toggled(bool arg1);
		void on_FilterEQ_toggled(bool arg1);
		void on_FilterColorTemperature_toggled(bool arg1);
		void on_MaxFov_valueChanged(int arg1);
		void on_MinFov_valueChanged(int arg1);
		void on_GUIPreviewScale_currentTextChanged(const QString &arg1);
		void on_FilterColorBalance_toggled(bool arg1);
		void on_ExposureDefaults_clicked();
		void on_ColorTemperatureDefaults_clicked();
		void on_EQDefaults_clicked();
		void on_ColorBalanceDefaults_clicked();
		void on_PYSteps_valueChanged(double arg1);
		void on_RollSteps_valueChanged(double arg1);
		void on_FOVSteps_valueChanged(double arg1);
		void on_TransSteps_valueChanged(double arg1);
		void on_SplitGenerateTable_clicked();
		void on_SelectSplitTMPDir_clicked();
		void on_SplitSaveScript_clicked();
		void on_SplitConvert_toggled(bool checked);
		void on_SplitRecheckState_clicked();
		void on_SplitCancel_clicked();
		void on_SplitRun_toggled(bool checked);
		void on_SplitJoin_clicked();
		void on_SplitRemoveTmp_clicked();
		void on_SplitRunSelected_clicked();
		void on_ErrorColor_textChanged(const QString &arg1);
		void on_SelectErrorColor_clicked();


		//TODO: Delete tis alter check that shorcuts load correctly o qt6 linux
		#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0) && defined (Q_OS_LINUX)
		void on_LeftTabs_currentChanged(int index);
		void on_RightTabs_currentChanged(int index);
		#endif
		void on_LowWarningColor_textChanged(const QString &arg1);
		void on_SelectLowWarningColor_clicked();
		void on_HighWarningColor_textChanged(const QString &arg1);
		void on_SelectHighWarningColor_clicked();
};
#endif // MAINWINDOW_H
