#include "processpriority.h"
#include <QApplication>

#ifdef Q_OS_WIN
#include <processthreadsapi.h>
#include <handleapi.h>

void ProcessLowPriority(long long Pid) {
	HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, false, Pid);
	SetPriorityClass(handle, 0x00004000);
	CloseHandle(handle);
}

void ProcessIdlePriority(long long Pid) {
	HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, false, Pid);
	SetPriorityClass(handle, 0x00000040);
	CloseHandle(handle);
}

#else
#include <sys/resource.h>

void ProcessLowPriority(long long Pid) {
	setpriority(PRIO_PROCESS, Pid, 10);
}

void ProcessIdlePriority(long long Pid) {
	setpriority(PRIO_PROCESS, Pid, 19);
}

#endif






