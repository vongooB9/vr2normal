#ifndef PROCESSPRIORITY_H
#define PROCESSPRIORITY_H

void ProcessLowPriority(long long Pid);
void ProcessIdlePriority(long long Pid);

#endif // PROCESSPRIORITY_H
