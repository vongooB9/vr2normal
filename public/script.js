function themetougle() {
	document.body.classList.toggle("theme-change");
}

window.addEventListener('load', function() {
	document.getElementsByTagName("nav")[0].innerHTML += "<a class='theme' onclick='themetougle();'></a>";

	document.querySelectorAll('a[href^="#"]').forEach(anchor => {
		anchor.addEventListener('click', function(e) {
			e.preventDefault();

			document.querySelector(this.getAttribute('href')).scrollIntoView({
				behavior: 'smooth'
			});
		});
	});
});
