#include "qtablewidgetundo.h"

QTableWidgetDelete::QTableWidgetDelete(QTableWidget *table, int row) {
	Table = table;
	Row = row;

	for (int i = 0; i < Table->columnCount(); ++i) {
		Items.append(Table->takeItem(Row, i));
	}
}

void QTableWidgetDelete::undo() {
	Table->blockSignals(true);
	Table->insertRow(Row);
	for (int i = 0; i < Table->columnCount(); ++i) {
		Table->setItem(Row, i, Items.at(i));
	}
	Table->blockSignals(false);
}

void QTableWidgetDelete::redo() {
	Table->blockSignals(true);
	Table->removeRow(Row);
	Table->blockSignals(false);
}

QTableWidgetChange::QTableWidgetChange(QTableWidget *table, QTableWidgetItem *newitem,
									   QTableWidgetItem *olditem) {
	Table = table;
	Row = olditem->row();
	Col = olditem->column();

	NewItem = newitem;
	OldItem = olditem;
}

void QTableWidgetChange::undo() {
	Table->takeItem(Row, Col);
	Table->setItem(Row, Col, OldItem);
}

void QTableWidgetChange::redo() {
	Table->takeItem(Row, Col);
	Table->setItem(Row, Col, NewItem);
}

int QTableWidgetChange::id() const {
	return (Row * 100) + Col;
}

bool QTableWidgetChange::mergeWith(const QUndoCommand *command) {
	const QTableWidgetChange *other = static_cast<const QTableWidgetChange *>(command);
	if (command->id() != this->id()) return false;
	NewItem = other->NewItem;
	return true;
}

QTableWidgetAdd::QTableWidgetAdd(QTableWidget *table, QString format, int row, QTime time, double trans, double pitch,
								 double yaw, double roll, double fov, bool righteye, double v, double h, QString text) {
	Table = table;
	Format = format;
	Row = row;
	Time = time;
	Trans = trans;
	Pitch = pitch;
	Yaw = yaw;
	Roll = roll;
	Fov = fov;
	Righteye = righteye;
	V = v;
	H = h;
	Text = text;
}

void QTableWidgetAdd::undo() {
	Table->blockSignals(true);
	Table->removeRow(Row);
	Table->blockSignals(false);
}

void QTableWidgetAdd::redo() {
	Table->blockSignals(true);
	Table->insertRow(Row);
	Table->setItem(Row, TIME_COLUMN, new QTableWidgetItem(Time.toString(Format)));
	Table->setItem(Row, TRANSITION_COLUMN, new QTableWidgetItem(QString::number(Trans)));
	Table->setItem(Row, PITCH_COLUMN, new QTableWidgetItem(QString::number(Pitch)));
	Table->setItem(Row, YAW_COLUMN, new QTableWidgetItem(QString::number(Yaw)));
	Table->setItem(Row, ROLL_COLUMN, new QTableWidgetItem(QString::number(Roll)));
	Table->setItem(Row, FOV_COLUMN, new QTableWidgetItem(QString::number(Fov)));
	if (Righteye)
		Table->setItem(Row, EYE_COLUMN, new QTableWidgetItem("1"));
	else
		Table->setItem(Row, EYE_COLUMN, new QTableWidgetItem("0"));
	Table->setItem(Row, H_OFFSET_COLUMN, new QTableWidgetItem(QString::number(H)));
	Table->setItem(Row, V_OFFSET_COLUMN, new QTableWidgetItem(QString::number(V)));
	Table->setItem(Row, TEXT_COLUMN, new QTableWidgetItem(Text));
	Table->blockSignals(false);
}
