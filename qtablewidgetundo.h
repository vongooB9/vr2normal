#ifndef QTABLEWIDGETUNDO_H
#define QTABLEWIDGETUNDO_H
#include <QTableWidget>
#include <QTimeEdit>
#include <QUndoCommand>

#define TIME_COLUMN 0
#define TRANSITION_COLUMN 1
#define PITCH_COLUMN 2
#define YAW_COLUMN 3
#define ROLL_COLUMN 4
#define FOV_COLUMN 5
#define EYE_COLUMN 6
#define V_OFFSET_COLUMN 7
#define H_OFFSET_COLUMN 8
#define TEXT_COLUMN 9
#define TOTAL_COLUMNS 10

#define FILTERS_TIME_COLUMN 0
#define FILTERS_FILTER_COLUMN 1

class QTableWidgetDelete : public QUndoCommand {
	public:
		QTableWidgetDelete(QTableWidget *table, int row);
		void undo() override;
		void redo() override;
	private:
		QTableWidget *Table;
		int Row;
		QList<QTableWidgetItem *> Items;
};

class QTableWidgetAdd : public QUndoCommand {
	public:
		QTableWidgetAdd(QTableWidget *table, QString format, int row, QTime time, double trans, double pitch,
						double yaw, double roll, double fov, bool righteye, double v, double h, QString text);
		void undo() override;
		void redo() override;
	private:
		QTableWidget *Table;
		QString Format;
		int Row;
		QTime Time;
		double Trans;
		double Pitch;
		double Yaw;
		double Roll;
		double Fov;
		bool Righteye;
		double V;
		double H;
		QString Text;
};

class QTableWidgetChange : public QUndoCommand {
	public:
		QTableWidgetChange(QTableWidget *table, QTableWidgetItem *newitem, QTableWidgetItem *olditem);
		void undo() override;
		void redo() override;
		int id() const override;
		bool mergeWith(const QUndoCommand *command) override;
		QTableWidgetItem *NewItem;
		QTableWidgetItem *OldItem;
	private:
		QTableWidget *Table;
		int Row;
		int Col;

};

#endif // QTABLEWIDGETUNDO_H
