#include "taskbarprogress.h"


TaskbarProgress::TaskbarProgress(QObject *parent)
	: QObject{parent} {
}

#ifdef Q_OS_WIN
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)

#define TASKBARPROGRESS_DISABLED

#else
#include <QWinTaskbarButton>
#include <QWinTaskbarProgress>
#include <QMainWindow>
#include <QWidget>

QWinTaskbarButton *Button = nullptr;
QWinTaskbarProgress *WinProgress = nullptr;

void TaskbarProgress::BeginProgress(int Max, bool /*CallUpdate*/) {
	if (Button == nullptr) {
		QMainWindow *ParentWidget = qobject_cast<QMainWindow *>(this->parent());
		Button = new QWinTaskbarButton(this->parent());
		Button->setWindow(ParentWidget->windowHandle());
		//        Button->setOverlayIcon(QIcon(":/VR2Normal.ico"));
		WinProgress = Button->progress();
	}
	WinProgress->setMaximum(Max);
	WinProgress->setValue(0);
	WinProgress->setVisible(true);
}

void TaskbarProgress::EndProgress(bool /*CallUpdate*/) {
	if (WinProgress == nullptr) return;
	WinProgress->setVisible(false);
}

void TaskbarProgress::Value(int NewValue, bool /*CallUpdate*/) {
	if (WinProgress == nullptr) return;
	WinProgress->setValue(NewValue);
}

void TaskbarProgress::Update() {}
#endif
#endif

#ifdef Q_OS_LINUX

#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusMessage>
#include <QVariant>
#include <QVariantMap>

void TaskbarProgress::BeginProgress(int Max, bool CallUpdate) {
	MaxValue = Max;
	CurrentValue = 0;
	Progress = true;
	Count = true;
	if (CallUpdate) Update();
}

void TaskbarProgress::EndProgress(bool CallUpdate) {
	Progress = false;
	Count = false;
	CurrentValue = 0;
	if (CallUpdate) Update();
}

void TaskbarProgress::Value(int NewValue, bool CallUpdate) {
	CurrentValue = NewValue;
	if (CallUpdate) Update();
}

void TaskbarProgress::Update() {
	QVariantMap properties;
	properties.insert(QStringLiteral("progress-visible"), Progress);
	properties.insert(QStringLiteral("count-visible"), Count);
	double tmpvalue = (double)CurrentValue / (double)MaxValue;
	properties.insert(QStringLiteral("progress"), tmpvalue);
	properties.insert(QStringLiteral("count"), (int)(tmpvalue * 100));

	auto message = QDBusMessage::createSignal(QStringLiteral("/com/example/VR2Normal"),
				   QStringLiteral("com.canonical.Unity.LauncherEntry"),
				   QStringLiteral("Update"));

	message << QStringLiteral("application://VR2Normal.desktop") << properties;
	QDBusConnection::sessionBus().send(message);
}

#endif

#ifdef Q_OS_MAC
	#define TASKBARPROGRESS_DISABLED
#endif

#ifdef TASKBARPROGRESS_DISABLED
void TaskbarProgress::BeginProgress(int /*Max*/, bool /*CallUpdate*/) {}
void TaskbarProgress::EndProgress(bool /*CallUpdate*/) {}
void TaskbarProgress::Value(int /*NewValue*/, bool /*CallUpdate*/) {}
void TaskbarProgress::Update() {}
#endif
