#ifndef TASKBARPROGRESS_H
#define TASKBARPROGRESS_H

#include <QObject>

class TaskbarProgress : public QObject {
		Q_OBJECT
	public:
		explicit TaskbarProgress(QObject *parent = nullptr);

		void BeginProgress(int Max, bool CallUpdate = true);
		void EndProgress(bool CallUpdate = true);
		void Value(int NewValue, bool CallUpdate = true);
		void Update();

	private:
		bool Progress;
		bool Count;
		int MaxValue;
		int CurrentValue;

	signals:

};

#endif // TASKBARPROGRESS_H
